// 기준 변수문제
		short snum = 32767;
		int inum = 100;
		long lnum = 10000L;
		float fnum = 0.123f;
		double dnum = 3.14;
		char ch = 'A';
		String str = "ABC";
		
		System.out.println("snum : " + snum);
		System.out.println("inum : " + inum);
		System.out.println("lnum : " + lnum);
		System.out.println("fnum : " + fnum); 
		System.out.println("dnum : " + dnum);
		System.out.println("char : " + ch);
		System.out.println("String : " + str);
		System.out.println("dnum : " + (int)dnum);
		
		
		// 영일
		int x = 1;
		int y = 2;
		int z = 3;
		int temp;
		
		temp = x;
		x = y;
		y = z;
		z = temp;
		
		
		System.out.println(x + " , " + y + " , " + z);
		
		// 문정 문제
		//1.
		System.out.print("1 ~ 127 사이의 정수를 입력하시오 : ");
		int testInt = sc.nextInt();
		
		System.out.println((char)testInt);
		
		
		// 2. 1을 입력한경우 정수형 데이터가 실수형데이터에 자동형변환되어 들어가고
		//    1.0 을 입력한경우는 리터럴에 f 를 붙여주지않아 에러 일으킨다.
		float fnum2 = 1;
		float fnum2_2 = 1.0f;
		
		System.out.println(fnum2 + " , " + fnum2_2);
		

// 원준문제
1. toString()
2. this()
3. super()
4. java.lang
5. return();

2 

3. -111

4. 		for (int gu = 2; gu < 10; gu++ ) {
			for (int dan = 1; dan < 10; dan++ ) {
				System.out.println(gu + " x " + dan + " = " + (gu*dan));
			}
			System.out.println();
		}

// 현주문제
1. 		int ame = 2500;
		double weight = 100.8;
		
		System.out.println("아아메 가격 : " + ame + " weight : " + weight);
		
======================
연산자
기준
1 
	1 : 4
	2 : 3
	3 : -4
	4 : -1
	5 : 5
	6 : 1
	7 : -6
	8 : -2

2 영일
() , >> ,  !=, && , =

4. 문정
		System.out.print("정수입력 : ");
		int num = sc.nextInt();
		
		if ( num >= 'A' && num <= 'z') {
			System.out.println("정수가 알파멧 입니다");
		} else {
			System.out.println("정수가 노알파벳 입니다");
		}
5. 현주
		System.out.print("정수입력 : ");
		int num2 = sc.nextInt();
		
		System.out.println(num2>0?"양수다":"음수다");

============제어문
기준 		int kor = 75;
		int math = 84;
		int eng = 70;
		
		double result = (kor+math+eng)/3;
		
		if ( result >= 77 ) {
			System.out.println("점수 : " + result);
			System.out.println("합격");
		} else {
			System.out.println("점수 : " + result);
			System.out.println("불합격");
		}
-----------------------------
		System.out.print("정수하나 입력 : ");
		int point = sc.nextInt();
		
		if ( point >= 80 && point <= 100 ) {
			System.out.println("안전");
		} else if ( point >= 60 ) {
			System.out.println("보통");
		}
--------------------------------
		System.out.print("정수 1 입력 : ");
		int num1 = sc.nextInt();
		sc.nextLine();
		System.out.print("정수 2 입력 : ");
		int num2 = sc.nextInt();
		sc.nextLine();
		System.out.print("사칙연산 입력 (+ , - , / , * ) : ");
		char ch = sc.next().charAt(0);
		
		switch(ch) {
		case '+' : System.out.println("결과 : " + (num1+num2));break;
		case '-' : System.out.println("결과 : " + (num1-num2));break;
		case '*' : System.out.println("결과 : " + (num1*num2));break;
		case '/' : System.out.println("결과 : " + (num1/num2));break;
		default : System.out.println("그딴거없음");
		}

영일----
System.out.print("국어 : ");
		int kor = sc.nextInt();
		sc.nextLine();
		System.out.print("영어 : ");
		int eng = sc.nextInt();
		sc.nextLine();
		System.out.print("수학 : ");
		int math = sc.nextInt();
		sc.nextLine();
		System.out.print("컴퓨터 : ");
		int com = sc.nextInt();
		sc.nextLine();
		System.out.print("과학 : ");
		int sic = sc.nextInt();
		sc.nextLine();
		
		double result = (kor+eng+math+com+sic)/5;
		
		String str = "";
		
		if (kor < result) {
			System.out.println("국어미달");
		} else if (eng < result) {
			System.out.println("영어미달");
		} else if (math < result) {
			System.out.println("수학미달");
		} else if (com < result) {
			System.out.println("컴퓨터미달");
		} else if (sic < result) {
			System.out.println("과학미달");
		} else {
			System.out.println("PASS");
		}
----------------
for (int i = 1; i < 6; i++) {
			for (int j = 1; j < 6; j++ ) {
				if ( i <= j ) {
					System.out.print("*");
				}
			}
			System.out.println();
----------------
		for ( int i = 1; i < 16; i++ ) {
			if (i % 2 == 1) {
				System.out.println(i);
			}
		}
-- 문정 
// 1. Scanner를 사용해 정수를 하나 입력받아, 1 ~ 3이면 "n번 메뉴를 선택하셨습니다."라고 출력하고 
// 그 외의 정수이면 "다시 입력하세요"를 출력하고, 입력을 다시 받도록 작성하세요.
// 단, 조건문은 switch문을 이용하세요.

// 결과 구문
// 번호를 입력하세요 : 4
// 다시 입력하세요.
// 번호를 입력하세요 : 3
// 3번 메뉴를 선택하셨습니다

		do {
			System.out.print("번호 입력 (1 ~ 3 번만)  : ");
			int num = sc.nextInt();

			switch(num) {
			case 1 : System.out.println("1번 메뉴를 선택하였습니다"); break;
			case 2 : System.out.println("2번 메뉴를 선택하였습니다"); break;
			case 3 : System.out.println("3번 메뉴를 선택하였습니다"); break;
			case 0 : return;
			default : System.out.println("메뉴 다시선택해라");
			}
		}while(true);

-----
		System.out.print(" 2 ~ 100 사이의 정수를 입력하시오 : ");
		int num = sc.nextInt();
		
		if ( num > 1 && num < 101 ) {
			for ( int i = 1; i <= num; i++ ) {
				if ( i % 2 == 1 ) {
				System.out.print(i + " ");
				}
			}
		} else {
			System.out.println("범위를 벗어났다");
		}
-------
		for ( int gu = 2; gu < 10; gu++ ) {
			if ( gu % 2 == 0 ) {
				for ( int dan = 1; dan <= gu; dan++ ) {
					System.out.println(gu + " x " + dan + " = " + (gu*dan));
				}
				System.out.println();
			}
		}
-------
		System.out.print("정수  1 입력 : ");
		int num1 = sc.nextInt();
		sc.nextLine();
		System.out.print("정수  2 입력 : ");
		int num2 = sc.nextInt();
		sc.nextLine();
		System.out.print("1 : 더하기  2 : 빼기  3 : 곱하기  4 : 나누기 :");
		int num3 = sc.nextInt();
		sc.nextLine();
		
		switch(num3) {
		case 1 : System.out.println("result : " + (num1+num2));break;
		case 2 : System.out.println("result : " + (num1-num2));break;
		case 3 : System.out.println("result : " + (num1*num2));break;
		case 4 : System.out.println("result : " + (num1/num2));break;
		default : System.out.println("종료");return;
		}
---------
// 5. 사용자로부터 5개의값을 입력받아 합을 구하는 프로그램 . 정수는 반드시 0보다 큰수여야한다.

// 결과 구문
		int sum = 0;
		
		for ( int i = 1; i < 6; i++ ) {
			System.out.print("0 보다 큰 정수 " + i + " 번째 입력 : ");
			int num = sc.nextInt();
			if ( num > 0 ) {
				sum += num;
			} else {
				--i;
			}
		}
		System.out.println("sum : " + sum);
-----
		int sosoo = new Random().nextInt(100)+1;
		

		int sum = 0;
		for (int i = 2; i <= sosoo; i++) {
			int count = 0;
			for (int j = 2; j < i; j++ ) {
				if ( i % j == 0 ) {
					count++;
				} 
			} 
			if (count == 0) {
				System.out.println(i);
			}
		}