-- DAY2 수업내용
-- IN 연산자 : 비교하려는 값 목록에 일치하는 값이 있는지 확인
SELECT EMP_NAME, DEPT_CODE, SALARY
FROM EMPLOYEE
WHERE DEPT_CODE IN ('D6', 'D8');

SELECT EMP_NAME, DEPT_CODE, SALARY
FROM EMPLOYEE
WHERE DEPT_CODE NOT IN ('D6', 'D8');

-- 연산자 우선순위
/*
1. 산술연산자
2. 연결연산자
3. 비교연산자
4. IS NULL/IS NOT NULL, LIKE/NOT LIKE, IN/NOT IN
5. BETWEEN AND/NOT BETWEEN AND
6. NOT
7. AND
8. OR
*/

-- J7직급이거나 J2 직급인 직원 중 급여가 200만원 이상인 직원의 
-- 이름, 급여, 직급코드를 조회
SELECT EMP_NAME, SALARY, JOB_CODE
FROM EMPLOYEE
WHERE (JOB_CODE = 'J7'
OR JOB_CODE = 'J2')
AND SALARY > 2000000;

-- 함수(FUNCTION) : 컬럼 값을 읽어서 계산한 결과를 리턴함
-- 단일행 함수(SINGLE ROW FUNCTION)
-- : 컬럼에 기록된 N개의 값을 읽어서 N개의 결과를 리턴
-- 그룹 함수(GROUP FUNCTION)
-- : 컬럼에 기록된 N개의 값을 읽어서 한 개의 결과를 리턴

-- SELECT 절에 그룹함수와 단일행 함수를 함께 사용하지 못한다.

-- 함수를 사용할 수 있는 위치 : SELECT절, WHERE절, GROUP BY절, HAVING절, ORDER BY절

-- 그룹함수 : SUM, AVG, MAX, MIN, COUNT
-- SUM(숫자 혹은 숫자가 기록된 컬럼명) : 합계를 구하여 리턴
SELECT SUM(SALARY)
FROM EMPLOYEE;

-- AVG(숫자가 기록된 컬럼명) : 평균을 구하여 리턴
SELECT AVG(SALARY)
FROM EMPLOYEE;

-- MIN(컬럼명) : 컬럼들 중에서 가장 작은 값을 리턴
-- 취급하는 자료형은 ANY TYPE임
SELECT MIN(EMAIL), MIN(HIRE_DATE), MIN(SALARY)
FROM EMPLOYEE;

-- MAX(컬럼명) : 컬럼들 중에서 가장 큰 값을 리턴
-- 취급하는 자료형은 ANY TYPE임
SELECT MAX(EMAIL), MAX(HIRE_DATE), MAX(SALARY)
FROM EMPLOYEE
WHERE EMP_ID <> 200;

SELECT AVG(BONUS)기본평균, 
       AVG(DISTINCT BONUS) 중복제거평균, 
       AVG(NVL(BONUS, 0)) NULL포함평균
FROM EMPLOYEE;

-- COUNT(* | 컬럼명) : 행의 갯수를 헤아려서 리턴
-- COUNT((DISTINCT)컬럼명) : 중복을 제거한 함수 리턴
-- COUNT(*) : 모든 행의 갯수 리턴
-- COUNT(컬럼명) : NULL을 제외한 실제 값이 기록된 행 갯수 리턴
SELECT COUNT(*), COUNT(DEPT_CODE), COUNT(DISTINCT DEPT_CODE)
FROM EMPLOYEE;

-- 단일행 함수
-- 문자 관련 함수
-- : LENGTH, LENGTHB, SUBSTR, UPPER, LOWER, INSTR...
SELECT LENGTH('오라클'), LENGTHB('오라클')
FROM DUAL; -- DUAL : DUMMY TABLE(가상의 테이블)

SELECT LENGTH(EMAIL), LENGTHB(EMAIL)
FROM EMPLOYEE;

--INSTR('문자열'|컬럼명, '문자', 찾을 위치에 시작값, [빈도])
SELECT EMAIL, INSTR(EMAIL, '@', -1) 위치  -- -1(오->왼)/ 1(왼->오)
FROM EMPLOYEE;

SELECT INSTR('AABAACAABBAA', 'B') FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', 1) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', -1) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', 1, 2) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', -1, 2) FROM DUAL;

-- EMPLOYEE 테이블에서 사원명, 이메일, @이후 제외한 아이디 조회
SELECT EMP_NAME, EMAIL, SUBSTR(EMAIL, 1, INSTR(EMAIL, '@') -1)
FROM EMPLOYEE;

-- LPAD / RPAD : 주어진 문자열에 임의의 문자열을 덧붙여 길이 N의 문자열을 반환
SELECT LPAD(EMAIL, 20, '#')
FROM EMPLOYEE;

SELECT RPAD(EMAIL, 20, '#')
FROM EMPLOYEE;

SELECT LPAD(EMAIL, 10)
FROM EMPLOYEE;

SELECT RPAD(EMAIL, 10)
FROM EMPLOYEE;


-- LTRIM / RTRIM : 주어진 컬럼이나 문자열 왼쪽/오른쪽에서 지정한 문자 혹은 문자열을 제거한 나머지 반환
SELECT LTRIM('   KH') FROM DUAL;
SELECT LTRIM('   KH', ' ') FROM DUAL;
SELECT LTRIM('000123456', '0') FROM DUAL;
SELECT LTRIM('123123KH', '123') FROM DUAL;
SELECT LTRIM('123123KH123', '123') FROM DUAL;
SELECT LTRIM('ACABACCKH', 'ABC') FROM DUAL;
SELECT LTRIM('5782KH', '123456789') FROM DUAL;

SELECT RTRIM('KH   ') FROM DUAL;
SELECT RTRIM('KH   ', ' ') FROM DUAL;
SELECT RTRIM('123456000', '0') FROM DUAL;
SELECT RTRIM('KH123123', '123') FROM DUAL;
SELECT RTRIM('KHACABACC', 'ABC') FROM DUAL;
SELECT RTRIM('KH5782', '0123456789') FROM DUAL;

-- TRIM : 주어진 컬럼이나 문자열의 앞/뒤에 지정한 문자를 제거 (기본으로 공백제거)
SELECT TRIM('   KH   ') FROM DUAL;
SELECT TRIM('Z' FROM 'ZZZKHZZZ') FROM DUAL;
SELECT TRIM(LEADING 'Z' FROM 'ZZZ123456ZZZ') FROM DUAL;
SELECT TRIM(TRAILING '3' FROM '333KH33333') FROM DUAL;
SELECT TRIM(BOTH '3' FROM '333KH33333') FROM DUAL;

-- SUBSTR : 컬럼이나 문자열에서 지정한 위치로부터 지정한 갯수의 문자열을 잘라서 리턴하는 함수
SELECT SUBSTR('SHOWMETHEMONEY', 5, 2) FROM DUAL;
SELECT SUBSTR('SHOWMETHEMONEY', 7) FROM DUAL;
SELECT SUBSTR('SHOWMETHEMONEY', -8, 3) FROM DUAL;
SELECT SUBSTR('쇼우 미 더 머니', 2, 5) FROM DUAL;

SELECT EMP_NAME, SUBSTR(EMP_NO, 8, 1)
FROM EMPLOYEE;

-- LOWER / UPPER / INITCAP : 대소문자로 변경해주는 함수
-- LOWER(문자열 | 컬럼) : 소문자로 변경해주는 함수
SELECT LOWER('Welcom To My World')
FROM DUAL;

-- UPPER(문자열 | 컬럼) : 대문자로 변경해주는 함수
SELECT UPPER('Welcom To My World')
FROM DUAL;

-- INITCAP : 앞글자만 대문자로 변경해주는 함수
SELECT INITCAP('welcom to my world')
FROM DUAL;

-- CONCAT : 문자열 혹은 컬럼 두 개를 입력받아서 하나로 합친 후 리턴
SELECT CONCAT('가나다라', 'ABC') 
FROM DUAL;
SELECT '가나다라' || 'ABC'
FROM DUAL;

-- REPLACE : 문자열 혹은 컬럼을 입력받아 변경하고자 하는 문자열을 변경하려고 하는 문자열로 바꾼 후 리턴
SELECT REPLACE('서울시 강남구 역삼동', '역삼동', '삼성동')
FROM DUAL;

-- EMPLOYEE 테이블에서 직원들의 주민번호를 조회하여 
-- 사원명, 생년, 생월, 생일을 각각 분리해서 조회
-- 단, 컬럼의 별칭은 사원명, 생년, 생월, 생일로 한다.
SELECT EMP_NAME 사원명, SUBSTR(EMP_NO, 1, 2) 생년, SUBSTR(EMP_NO, 3, 2) 생월, SUBSTR(EMP_NO, 5, 2) 생일
FROM EMPLOYEE;

-- WHERE 절에서 함수 사용
-- 전직원들의 급여 평균보다 많이 받는 사원들의 모든 컬럼 조회
SELECT *
FROM EMPLOYEE
WHERE SALARY > AVG(SALARY); --WHERE절에는 그룹함수 사용 불가

SELECT * 
FROM EMPLOYEE
WHERE SALARY > (SELECT AVG(SALARY) FROM EMPLOYEE);

-- 여직원들의 모든 컬럼 정보를 조회
SELECT *
FROM EMPLOYEE
WHERE SUBSTR(EMP_NO, 8, 1) = 2;

-- 날짜 데이터에서도 SUBSTR을 사용할 수 있다.
-- 직원들의 입사일에도 입사년도, 입사월, 입사날짜를 분리 조회
SELECT HIRE_DATE, 
       SUBSTR(HIRE_DATE, 1, 2) 입사년도,
       SUBSTR(HIRE_DATE, 4, 2) 입사월,
       SUBSTR(HIRE_DATE, 7, 2) 입사날짜
FROM EMPLOYEE;

-- SUBSTRB : 바이트 단위로 추출하는 함수
SELECT SUBSTR('ORACLE', 3, 2), SUBSTRB('ORACLE', 3, 2)
FROM DUAL;

SELECT SUBSTR('오라클', 2, 2), SUBSTRB('오라클', 4, 6)
FROM DUAL;

-- EMPLOYEE 테이블에서 사원명, 주민번호 조회
-- 단, 주민번호는 생년월일만 보이게 하고, '-' 다음 값은 '*'로 바꿔서 출력
-- 함수 중첩 사용 가능
SELECT EMP_NAME 사원명, REPLACE(EMP_NO, SUBSTR(EMP_NO, 8, 7), '*******') 주민번호
FROM EMPLOYEE;

SELECT EMP_NAME 사원명, RPAD(SUBSTR(EMP_NO, 1, 7), 14, '*') 주민번호
FROM EMPLOYEE;

-- 숫자 처리 함수 : ABS, MOD, ROUND, FLOOR, TRUNC, CEIL
-- ABS(숫자 | 숫자로 된 컬럼명) : 절대값을 리턴하는 함수
SELECT ABS(-10), ABS(10) FROM DUAL;

-- MOD(숫자 | 숫자로 된 컬럼명, 숫자 | 숫자로 된 컬럼명) 
-- : 두 수를 나누어서 나머지를 구하는 함수
-- 처음 인자는 나누어지는 수, 두번째 인자는 나눌 수
SELECT MOD(10, 5), MOD(10, 3) FROM DUAL;

-- ROUND(숫자 | 숫자로 된 컬럼명, [위치])
-- : 반올림해서 리턴하는 함수
SELECT ROUND(123.456) FROM DUAL;
SELECT ROUND(123.456, 0) FROM DUAL; 
SELECT ROUND(123.456, 1) FROM DUAL; --소수점 첫째 자리에서 반올림
SELECT ROUND(123.456, -2) FROM DUAL;

-- FLOOR(숫자 | 숫자로 된 컬럼명) : 내림처리 하는 함수 (소수점 자리 제거)
SELECT FLOOR(123.456) FROM DUAL;
SELECT FLOOR(123.678) FROM DUAL;

-- TRUNC(숫자 | 숫자로 된 컬럼명, [위치]) 
-- : 내림처리함수(절삭)
SELECT TRUNC(123.456) FROM DUAL;
SELECT TRUNC(123.678) FROM DUAL;
SELECT TRUNC(123.456, 1) FROM DUAL; 
SELECT TRUNC(123.456, -1) FROM DUAL;

-- CEIL(숫자 | 숫자로 된 컬렴명, [위치]) : 올림처리 함수
SELECT CEIL(123.456) FROM DUAL;
SELECT CEIL(123.567) FROM DUAL;

SELECT ROUND(123.456),
       FLOOR(123.456),
       TRUNC(123.456),
       CEIL(123.456)
FROM DUAL;

