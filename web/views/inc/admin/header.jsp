<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>애드립 관리자</title>	
	<link rel="shortcut icon" href="<%=request.getContextPath() %>/favicon.ico?v=2" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
  <div class="wrap">
    <header id="header">
      <div class="inner_ct clearfix">
        <div class="logo_wrap">
          <h1><a href="/edr/views/admin/index.jsp"><img src="<%=request.getContextPath()%>/imgs/logo_white.png" alt="로고"></a></h1>
        </div>
        <div class="logout_wrap">
          <div class="logout">
            <a href="<%=request.getContextPath() %>/adLogout.ad" ><img src="<%=request.getContextPath()%>/imgs/logout_icon.png" alt="로그아웃"></a>
          </div>
        </div>
      </div>
      <div class="nav_wrap">
        <nav class="nav">
          <ul>
            <li class="nav_list">
              <a>이용자</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/selectMember.ad">이용자 관리</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>가이드</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/adGuideHistory.gd">가이드 신청내역</a></li>
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/selectGuide.ad">가이드 관리</a></li>

                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/adGuideCalculate.gd">정산 신청 관리</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>맞춤 프로그램</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/adCustomProgram.cp">맞춤 프로그램 관리</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>일반 프로그램</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/adWaitProgramList.gp">승인대기 프로그램</a></li>
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/selectGP.ad">모든 프로그램</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>주문 내역</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/adOrderList.od">모든 주문 내역</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>리뷰</a>
              <ul class="nav_cnt">
               <li class="nav_cnt_list"><a href="<%=request.getContextPath()%>/selectReview.ad">리뷰 관리</a></li>
              </ul>
            </li>
  <!--           <li class="nav_list">
              <a>블랙 리스트</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="/edr/views/admin/sub/blacklist/reportTourist.jsp;">투어객 신고관리</a></li>
                <li class="nav_cnt_list"><a href="javascript: void(0);">가이드 신고관리</a></li>
                <li class="nav_cnt_list"><a href="/edr/views/admin/sub/blacklist/blackList.jsp">블랙리스트 관리</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>1:1</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="/edr/views/admin/sub/qna/touristQuestion.jsp;">투어객 문의내역</a></li>
                <li class="nav_cnt_list"><a href="/edr/views/admin/sub/qna/guideQuestion.jsp;">가이드 문의내역</a></li>
              </ul>
            </li>
            <li class="nav_list">
              <a>FAQ</a>
              <ul class="nav_cnt">
                <li class="nav_cnt_list"><a href="/edr/views/admin/sub/faq/faq.jsp;">자주 묻는 질문 관리</a></li>
              </ul>
            </li> -->
          </ul>
        </nav>
      </div>
      
    </header>

