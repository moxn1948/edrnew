<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 메세지 서브 메뉴 -->
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">메세지</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="/edr/views/user/sub/message/messageSend.jsp">메세지 보내기</a></li>
      <li class="sub_menu_list"><a href="/edr/views/user/sub/message/messageBox.jsp">받은 메세지</a></li>
      <li class="sub_menu_list"><a href="/edr/views/user/sub/message/messageBox.jsp">보낸 메세지</a></li>
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->