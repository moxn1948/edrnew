<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 가이드 보기 서브 메뉴 -->
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">가이드 보기</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="javascript:void(0);">서울</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">경기</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">경상</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">강원</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">전라</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">충청</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">제주</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">부산</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">대구</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">대전</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">광주</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">인천</a></li>
      <li class="sub_menu_list"><a href="javascript:void(0);">울산</a></li>
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->