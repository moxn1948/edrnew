<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">가이드 페이지</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="javascript:void(0);">계정관리</a></li>
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">열리는메뉴</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="javascript: void(0);">임시1</a></li>
          <li class="sub_side_list"><a href="javascript:void(0);">임시2</a></li>
          <li class="sub_side_list"><a href="javascript:void(0);">임시3</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
      <!-- 열리는 메뉴 시작 : 로딩 되었을 때 열려있어야 하고, 볼드 처리 되어야 하는 곳에 open, on 클래스 삽입 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">열리는메뉴</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="javascript: void(0);">임시1</a></li>
          <li class="sub_side_list"><a href="javascript:void(0);">임시2</a></li>
          <li class="sub_side_list"><a href="javascript:void(0);">임시3</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->