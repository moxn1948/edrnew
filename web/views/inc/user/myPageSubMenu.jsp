<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.member.model.vo.*"%>
<%
	int mno = (Integer) ((Member) (request.getSession().getAttribute("loginUser"))).getMno();
%>
<!-- 마이페이지 보기 -->
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">마이 페이지</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="/edr/views/user/sub/my_page/myPage.jsp">프로필 관리</a></li>
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">일반 프로그램 관리</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="<%= request.getContextPath() %>/selectList.ugp?mno=<%= mno %>">준비 중인 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath() %>/selectList.cgp?mno=<%= mno %>">완료된 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath() %>/selectList.cangp?mno=<%= mno %>">취소된 프로그램</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">맞춤 프로그램 관리</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="<%= request.getContextPath() %>/selectMyCpList.cp?mno=<%= mno %>">진행 중인 맞춤 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath() %>/selectCompMyCp.cp?mno=<%= mno %>">결제된 맞춤 프로그램</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->