<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.member.model.vo.*"%>
<%
	Member loginUser = (Member)request.getSession().getAttribute("loginUser");
%>
    <!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모두가 꿈꾸던 여행, 애드립</title>
	<link rel="shortcut icon" href="<%=request.getContextPath() %>/favicon.ico?v=2" type="image/x-icon">
	<!-- font-family: 'Noto Sans KR', sans-serif; -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:400,700&display=swap&subset=korean"
		rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>
		$(function(){
			<% if(loginUser != null) {%>
				$.ajax({
					url : "<%=request.getContextPath() %>/memberDivision.common?mno=<%=loginUser.getMno()%>&mtype=<%=loginUser.getMtype()%>",
					type : "get",
					success : function(data){
						// data = 1 > 가이드 guideMemBox
						// data = 2 > 가이드 신청한 투어객 reqMemBox
						// data = 3 > 투어객 memberMemBox
						
						if(data == 1){
							$("#reqMemBox").remove();
							$("#memberMemBox").remove();
						}else if(data == 2){
							$("#guideMemBox").remove();
							$("#memberMemBox").remove();
						}else{
							$("#guideMemBox").remove();
							$("#reqMemBox").remove();
						}
						
					},
					error : function(data){
						console.log("회원 판별 error");
					}
				});
			<% } %>
			
		});
	</script>
</head>
<body>
	<div class="wrap">
		<header id="header">
			<div class="inner_ct">
				<div class="gnb_wrap clearfix">
					<div class="logo_wrap">
						<a href="<%=request.getContextPath()%>/index"><img src="<%=request.getContextPath()%>/imgs/logo_black.png" alt="EDR 모두가 꿈꾸던 여행, 애드립 로고" class="logo_img"></a>
					</div>
					<!-- 검색폼 -->
					<form action="<%= request.getContextPath() %>/mainSearch.srch" method="get" class="srch_form">
						<div class="srch_wrap">
							<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt="" class="srch_img">
							<input type="text" name="keyWord" id="searchIpt" class="srch_ipt"
								placeholder="프로그램명으로 검색할 수 있습니다." autocomplete="off">
						</div>
					</form>
					<div class="menu_wrap">
						<!-- 비회원의 경우 나오는 요소 -->
						<% if( loginUser == null ){ %>
						<div class="n_mem_wrap clearfix">
							<ul class="n_mem">
								<li class="n_mem_list"><a href="/edr/views/user/sub/member/login.jsp">로그인</a></li>
								<li class="n_mem_list"><a href="/edr/views/user/sub/member/join.jsp">회원가입</a></li>
							</ul>
						</div>
						<% } else { %>
					<!-- 회원인 경우 나오는 요소 -->
						<div class="mem_wrap clearfix">
							<ul class="mem">
							<%-- 	<li class="mem_list"><a href="/edr/views/user/sub/general_program/wishList.jsp"><img src="<%=request.getContextPath()%>/imgs/wish_icon.png" alt="찜한 목록 보기"
											class="mem_img"></a></li>
								<li class="mem_list"><a href="/edr/views/user/sub/message/messageBox.jsp"><img src="<%=request.getContextPath()%>/imgs/msg_icon.png" alt="메세지 함으로 가기"
											class="mem_img"></a></li>
								<li class="mem_list box_wrap">
									<a href="#"><img src="<%=request.getContextPath()%>/imgs/notice_icon.png" alt="알림 보기"
											class="mem_img box_img"></a>
									<div class="box_cnt notice_box">
										<ul class="box_list_wrap">
											<li class="box_list notice_box_list"><a href="#">
													<p class="title">[맞춤 프로그램]</p>
													<p class="cnt ellipsis">가이드가 배정되었습니다.</p>
												</a></li>
											<li class="box_list notice_box_list"><a href="#">
													<p class="title">[메세지] 송기준 가이드</p>
													<p class="cnt ellipsis">이런거 문의하셨죠???이런거 문의하셨죠???이런거 문의하셨죠???이런거
														문의하셨죠???이런거 문의하셨죠???</p>
												</a></li>
											<li class="box_list notice_box_list"><a href="#">
													<p class="title">[메세지] 송기준 가이드</p>
													<p class="cnt ellipsis">이런거 문의하셨죠???이런거 문의하셨죠???이런거 문의하셨죠???이런거
														문의하셨죠???이런거 문의하셨죠???</p>
												</a></li>
											<li class="box_list notice_box_list"><a href="#">
													<p class="title">[메세지] 송기준 가이드</p>
													<p class="cnt ellipsis">이런거 문의하셨죠???이런거 문의하셨죠???이런거 문의하셨죠???이런거
														문의하셨죠???이런거 문의하셨죠???</p>
												</a></li>
											<li class="box_list notice_box_list"><a href="#">
													<p class="title">[메세지] 송기준 가이드</p>
													<p class="cnt ellipsis">이런거 문의하셨죠???이런거 문의하셨죠???이런거 문의하셨죠???이런거
														문의하셨죠???이런거 문의하셨죠???</p>
												</a></li>
										</ul>
										<button type="submit" class="notice_del">지우기</button>
									</div>
								</li> --%>
								<li class="mem_list box_wrap">
									<a href="javascript: void(0);"><img src="<%=request.getContextPath()%>/imgs/mem_icon.png" alt="내 정보 보기" class="mem_img box_img"></a>
									<!-- 내 정보 보기 박스 -->
									<ul class="box_cnt mem_box">
										<li class="box_list mem_box_list"><a href="/edr/views/user/sub/my_page/myPage.jsp">마이페이지</a></li>
										<!-- <li class="box_list mem_box_list"><a href="/edr/views/user/sub/guide_page/guideReport.jsp">가이드 신고하기</a></li> -->
										<li class="box_list mem_box_list logout_top" id="memberMemBox"><a href="/edr/views/user/sub/member/guideSignUp.jsp">가이드 신청하기</a></li>
										<li class="box_list mem_box_list logout_top" id="reqMemBox"><a href="<%= request.getContextPath()%>/selectOne.gd?mno=<%= loginUser.getMno() %>">가이드 신청 현황</a></li>
										<li class="box_list mem_box_list logout_top" id="guideMemBox"><a href="<%=request.getContextPath()%>/gdProfile.gd?mno=<%= loginUser.getMno() %>">가이드 페이지</a></li>
										<!-- <li class="box_list mem_box_list"><a href="/edr/views/user/sub/faq/QnA_list.jsp">1:1 문의하기</a></li> -->
										<li class="box_list mem_box_list logout"><a href="<%= request.getContextPath() %>/logout.me">로그아웃</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<% } %>
						<!-- 메뉴 리스트 -->
						<nav class="nav_wrap">
							<ul class="nav clearfix">
								<li class="nav_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=1">프로그램 보기</a></li>
								<!-- <li class="nav_list"><a href="/edr/views/user/sub/guide_list/guideList.jsp">가이드 보기</a></li> -->
								<li class="nav_list"><a href="/edr/views/user/sub/custom_program/customProgram_info.jsp">맞춤 프로그램 신청</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>