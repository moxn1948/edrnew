<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- faq 서브 메뉴 -->
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">FAQ</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="/edr/views/user/sub/faq/FAQ.jsp">자주 묻는 질문</a></li>
      <li class="sub_menu_list"><a href="/edr/views/user/sub/faq/QnA_List.jsp">1:1 문의 내역</a></li>
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->