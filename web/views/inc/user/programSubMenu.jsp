<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 프로그램 보기 서브 메뉴 -->
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">프로그램 보기</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=1">서울</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=2">경기</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=3">경상</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=4">강원</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=5">전라</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=6">충청</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=7">제주</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=8">부산</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=9">대구</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=10">대전</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=11">광주</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=12">인천</a></li>
      <li class="sub_menu_list"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=13">울산</a></li>
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->