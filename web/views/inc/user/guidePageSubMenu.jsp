<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.member.model.vo.Member"%>
<%
	int mno = (Integer) ((Member) (request.getSession().getAttribute("loginUser"))).getMno();
%>
<!-- 가이드 페이지 서브 메뉴  -->  
<!-- 왼쪽 메뉴 시작 -->
<div class="sub_menu_wrap">
    <h3 class="sub_menu_tit">가이드 페이지</h3>
    <ul class="sub_menu_cnt">
      <li class="sub_menu_list"><a href="<%= request.getContextPath() %>/selectGd.gd?mno=<%= mno %>">계정관리</a></li>
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">일반 프로그램</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="/edr/views/user/sub/guide_page/programAdd.jsp">프로그램 신청</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/allowList.gp?mno=<%=mno%>">승인대기 중인 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectMyProgramList.gp?mno=<%= mno %>">내 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/processList.gp?mno=<%=mno%>">진행 중인 프로그램</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectCompGp.gd?mno=<%=mno%>">완료된 프로그램</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">맞춤 프로그램</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectCp.cp?mno=<%= mno%>">맞춤 프로그램 목록</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectMCP.cp?mno=<%= mno%>">배정된 맞춤 프로그램</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
      <!-- 열리는 메뉴 시작 -->
      <li class="sub_menu_list add"><a href="javascript:void(0);">정산관리</a>
        <ul class="sub_side_wrap">
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectList.calc?mno=<%= mno %>">정산 가능 내역</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectList.wcalc?mno=<%= mno %>">정산 대기 내역</a></li>
          <li class="sub_side_list"><a href="<%= request.getContextPath()%>/selectList.ccalc?mno=<%= mno %>">정산 완료 내역</a></li>
          <li class="sub_side_list"><a href="/edr/views/user/sub/guide_page/registerAccountPopup.jsp">정산 계좌 등록</a></li>
        </ul>
      </li>
      <!-- 열리는 메뉴 끝 -->
      <!-- <li class="sub_menu_list"><a href="guide_QnA_Send.jsp">1:1 문의하기</a></li> -->
    </ul>
  </div>
  <!-- 왼쪽 메뉴 끝 -->