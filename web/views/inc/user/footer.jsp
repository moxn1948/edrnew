<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<footer id="footer"> 
			<div class="inner_ct">
				<div class="policy_wrap">
					<ul class="policy clearfix">
						<li class="policy_list"><a href="#">이용정보</a></li>
						<li class="policy_list"><a href="#">개인정보 처리방침</a></li>
						<li class="policy_list"><a href="#">취소 및 환불 정책</a></li>
					</ul>
				</div>
				<div class="customer_wrap">
					<ul class="center_cnt">
						<li class="center_list center_tit">고객센터</li>
						<li class="center_list center_tel">1566-1113</li>
						<li class="center_list center_noti">일반문의 09:00 ~ 22:00</li>
					</ul>
					<div class="faq_link"><a href="/edr/views/user/sub/faq/FAQ.jsp">자주하는 질문</a></div>
				</div>
				<div class="office_wrap">
					<ul class="office_cnt clearfix">
						<li class="office_list">상호명 (주)애드립</li>
						<li class="office_list">대표 뱁새</li>
						<li class="office_list">개인정보보호책임자 전세환</li>
						<li class="office_list">사업자등록번호 209-81-54539</li>
						<li class="office_list">통신판매업신고번호 2019-서울서초-0345</li>
					</ul>
					<ul class="office_cnt clearfix">
						<li class="office_list">
							<address>주소 서울특별시 서초구 강남대로 327, 남도빌딩 29-2</address>
						</li>
						<li class="office_list">이메일 home@edr.com</li>
						<li class="office_list">마케팅/제휴 문의 marketing@edr.com</li>
					</ul>
				</div>
			</div>
		</footer>
	</div>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/user/script.js"></script>
</body>
</html>