<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.Gp, com.edr.guide.model.vo.GuideDetail, com.edr.common.model.vo.Attachment"%>
<%
	String msg = (String)request.getAttribute("msg");
	
	HashMap<String, Object> list = (HashMap<String, Object>) request.getAttribute("list");
	ArrayList<HashMap<String, Object>> gpList = (ArrayList<HashMap<String, Object>>) list.get("gpList");
	ArrayList<HashMap<String, Object>> selectAllReviewCnt = (ArrayList<HashMap<String, Object>>) list.get("selectAllReviewCnt");
%>
<%@ include file="../inc/user/mainHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/mjStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container main_page">
  <div class="inner_ct clearfix">
    <div class="new_program_wrap">
      <h3 class="tit">오늘의 신규 프로그램</h3>
      <!-- 큰 카드 시작 -->
      <div class="new_wide_wrap new_program_wide">
        <div class="new_wide">
          <a href="<%=request.getContextPath() %>/selectProgramDetail.gp?no=<%= ((Gp) gpList.get(0).get("GpObj")).getGpNo() %>" class="clearfix">
            <span class="new_wide_img"><img src="/edr/uploadFiles/<%= ((Attachment) gpList.get(0).get("AttaObj")).getChangeName() %>" alt="프로그램 이미지"></span>
            <div class="new_wide_cnt_wrap">
              <div class="top_cnt clearfix">
                <div class="grade_cnt clearfix">
                   <% if((double) (selectAllReviewCnt.get(0).get("reviewAvg")) > 0){ %>
                  <span class="grade_tit">평점</span>
                   <% } %>
                   
                  <span class="star_wrap">
                  <%
               			for(int j = 0; j < Integer.parseInt((selectAllReviewCnt.get(0).get("reviewAvg") + "").split("\\.")[0]); j++){
                 	  %>
               				<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                 	  <% 
               			}

                		   
                	   if((selectAllReviewCnt.get(0).get("reviewAvg") + "").split("\\.")[1] == "5"){
                 	   %>
                   
                      <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                 	
                      <% }%>
                </div>
                <div class="review_cnt">후기 <%= selectAllReviewCnt.get(0).get("reviewCount") %>개</div>
              </div>
              <div class="location_cnt">
               <% 
                 String localName = "";
                 switch(((Gp) gpList.get(0).get("GpObj")).getLocalNo()) {	
                case 1: localName = "서울"; break;
              	case 2: localName = "경기"; break;
            	case 3: localName = "경상"; break;
            	case 4: localName = "강원"; break;
            	case 5: localName = "전라"; break;
            	case 6: localName = "충청"; break;
            	case 7: localName = "제주"; break;
            	case 8: localName = "부산"; break;
            	case 9: localName = "대구"; break;
            	case 10: localName = "대전"; break;
            	case 11: localName = "광주"; break;
            	case 12: localName = "인천"; break;
            	case 13: localName = "울산"; break;
                 } %>
                 <%= localName %>
              </div>
              <div class="guide_cnt"><%= ((GuideDetail) gpList.get(0).get("GuideObj")).getGname() %> 가이드의</div>
              <div class="program_tit_cnt ellipsis"><%= ((Gp) gpList.get(0).get("GpObj")).getGpName() %></div>
              <div class="price_cnt"><%= ((Gp) gpList.get(0).get("GpObj")).getGpCost() %>원</div>
              <div class="desc_cnt"><%= ((Gp) gpList.get(0).get("GpObj")).getGpDescr() %></div>
              <div class="bot_cnt clearfix">
                <div class="person_cnt">참가 가능 인원 <%= ((Gp) gpList.get(0).get("GpObj")).getGpMin() %>명 ~ <%= ((Gp) gpList.get(0).get("GpObj")).getGpMax() %>명</div>
                <div class="time_cnt"><%= ((Gp) gpList.get(0).get("GpObj")).getGpTday() %>일</div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <!-- 큰 카드 끝 -->
	    <!-- 배너 시작 -->
	    <div class="main_banner_wrap">
	      <a href="<%= request.getContextPath() %>/views/user/sub/custom_program/customProgram_info.jsp" class="main_banner clearfix">
	        <div class="main_banner_cnt">
	          <p>프로그램을 내 입맛에 맞게</p>
	          <p>바꿔보자!</p>
	        </div>
	        <div class="main_banner_cnt">
	          <p>일정 신청 방법 보러가기 →</p>
	        </div>
	      </a>
	    </div>
	    <!-- 배너 끝 -->
      <!-- 작은 카드 시작 -->
      <ul class="all_card_wrap new_program_card_wrap clearfix">
   	<% for(int i = 1; i < gpList.size(); i++){ %>
          <li class="card_wrap new_program_card">
            <div class="card">
	          <input type="hidden" value="">
              <a href="<%=request.getContextPath() %>/selectProgramDetail.gp?no=<%= ((Gp) gpList.get(i).get("GpObj")).getGpNo() %>">
                <span class="card_img"><img src="/edr/uploadFiles/<%= ((Attachment) gpList.get(i).get("AttaObj")).getChangeName() %>" alt=""></span>
                <div class="card_cnt_wrap">
                  <div class="top_cnt clearfix">
                      <div class="guide_cnt"><%= ((GuideDetail) gpList.get(i).get("GuideObj")).getGname() %> 가이드의</div>
                      <div class="location_cnt locationCnt">
                      <% 
                      String localNameS = "";
                      switch(((Gp) gpList.get(i).get("GpObj")).getLocalNo()) {	
	                    case 1: localNameS = "서울"; break;
	                  	case 2: localNameS = "경기"; break;
	                	case 3: localNameS = "경상"; break;
	                	case 4: localNameS = "강원"; break;
	                	case 5: localNameS = "전라"; break;
	                	case 6: localNameS = "충청"; break;
	                	case 7: localNameS = "제주"; break;
	                	case 8: localNameS = "부산"; break;
	                	case 9: localNameS = "대구"; break;
	                	case 10: localNameS = "대전"; break;
	                	case 11: localNameS = "광주"; break;
	                	case 12: localNameS = "인천"; break;
	                	case 13: localNameS = "울산"; break;
                      } %>
                      <%= localName %>
                      </div>
                    </div>
                    <div class="program_tit_cnt ellipsis"><%= ((Gp) gpList.get(i).get("GpObj")).getGpName() %></div>
                    <div class="price_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpCost() %>원</div>
                    <div class="time_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpTday() %>일</div>
                    <div class="bot_cnt clearfix">
                      <div class="grade_cnt">
                        <% if((double) (selectAllReviewCnt.get(i).get("reviewAvg")) > 0){ %>
		                  <span class="grade_tit">평점</span>
	                    <% } %>
	                    
		                  <span class="star_wrap">
		                  <%
                 			for(int j = 0; j < Integer.parseInt((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[0]); j++){
	                  	  %>
                 				<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
	                  	  <% 
                 			}

	                 		   
	                 	   if((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[1] == "5"){
	                  	   %>
	                    
	                       <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
	                  	
	                       <% }%>
		                  </span>
                      </div>
                      <div class="review_cnt">후기 <%= selectAllReviewCnt.get(i).get("reviewCount") %>개</div>
                    </div>
                </div>
              </a>
            </div>
          </li>
          <% } %>
      </ul>
      <!-- 작은 카드 끝 -->
      <!-- 버튼 시작  -->
      <div class="btn_wrap more_btn_wrap index_more">
        <a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?localNo=1" class="btn_com btn_blue">프로그램 더보기</a>
      </div>
      <!-- 버튼 끝  -->
    </div>
    </div>
  </div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
	if("<%= msg %>" != "null"){
		$(function(){
			alert(<%= msg %>);
		});
	}
</script>
<%@ include file="../inc/user/footer.jsp"%>

</body>

</html>