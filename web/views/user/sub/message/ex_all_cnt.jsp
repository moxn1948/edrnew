<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_1_wrap">
			<!-- 제목 -->
			<h1 class="main_title">맞춤 프로그램 신청</h1>
			<!-- 내용 시작 -->
			<div class="main_cnt">
				<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<!-- 기본적인 input 시작 -->
					<div class="box_input"><input type="text" name="" id="" placeholder="100자 이내로 입력해주세요.">
					<!-- 기본적인 input 끝 -->
					</div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<div class="box_input"><input type="text" name="" id=""></div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<!-- 라디오 시작 -->
					<div class="box_radio">
						<input type="radio" name="" id="test1" value=""><label for="test1">모양만 만든것</label>
						<input type="radio" name="" id="test2" value=""><label for="test2">name 연결 알아서 하기</label>
					</div>
					<!-- 라디오 끝 -->
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<!-- 체크박스 시작 -->
					<div class="box_chk">
						<input type="checkbox" name="" id="test3" value=""><label for="test3">모양만 만든것</label>
						<input type="checkbox" name="" id="test4" value=""><label for="test4">name 연결 알아서 하기</label>
					</div>
					<!-- 체크박스 끝 -->
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<div class="box_select">
						<select name="" id="">
							<option value="">01</option>
							<option value="">02</option>
							<option value="">03</option>
							<option value="">04</option>
						</select>		
					</div>
				</div>
				<!-- 버튼 시작  -->
				<div class="btn_wrap">
					<button type="submit" class="btn_com btn_white">신청하기</button>
					<button type="submit" class="btn_com btn_blue">신청하기</button>
				</div>
				<!-- 버튼 끝  -->
			</div>
			<!-- 내용 끝 -->
		</div>
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp" %>
