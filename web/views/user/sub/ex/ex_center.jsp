<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

 <!-- 컨텐츠가 들어가는 부분 start -->
 <main class="container">
   <div class="inner_ct clearfix">
     <div class="layout_2_wrap">
       <!-- 제목 -->
       <h1 class="main_title">회원가입</h1>
       <!-- 내용 -->
       <div class="main_cnt">
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id="" placeholder="100자 이내로 입력해주세요."></div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id=""></div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id=""></div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id=""></div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
			<!-- 라디오 시작 -->
			<div class="box_radio">
				<input type="radio" name="" id="test1" value=""><label for="test1">모양만 만든것</label>
				<input type="radio" name="" id="test2" value=""><label for="test2">name 연결 알아서 하기</label>
			</div>
			<!-- 라디오 끝 -->
         </div>
         <!-- 버튼 시작 -->
         <div class="btn_wrap">
           <button type="button" class="btn_com btn_white">취소하기</button>
           <button type="submit" class="btn_com btn_blue">신청하기</button>
         </div>
         <!-- 버튼 끝 -->
       </div>
     </div>
   </div>
 </main>
 <!-- 컨텐츠가 들어가는 부분 end -->
 
<%@ include file="../../../inc/user/footer.jsp" %>
