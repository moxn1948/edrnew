<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/subMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">계정 관리111</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="10%">
							<col width="30%">
							<col width="20%">
							<col width="20%">
							<col width="10%">
						</colgroup>
						<tr class="tbl_tit">
							<th>메뉴1</th>
							<th>메뉴2</th>
							<th>메뉴2</th>
							<th>메뉴3</th>
							<th>메뉴4</th>
							<th>메뉴5</th>
						</tr>
						<tr class="tbl_cnt">
							<td>cnt1</td>
							<td>cnt1</td>
							<td><a href="#">cnt2</a></td>
							<td>cnt3</td>
							<td>cnt4</td>
							<td>cnt5</td>
						</tr>
						<tr class="tbl_cnt">
							<td>cnt1</td>
							<td>cnt1</td>
							<td><a href="#">cnt2</a></td>
							<td>cnt3</td>
							<td>cnt4</td>
							<td>
								<span class="squ_tbl">진행중</span>
							</td>
						</tr>
					</table>
				</div>
				<!-- table 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp" %>
