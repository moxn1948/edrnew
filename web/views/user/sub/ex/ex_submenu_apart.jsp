<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/programSubMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">계정 관리</h4>
			<div class="sub_ctn">
        <!-- 영역분리 시작 -->
        <div class="layout_3_wrap">
            <!-- 내용 -->
            <div class="main_cnt">
              <div class="main_cnt_list clearfix">
                <h4 class="title">제목</h4>
                <div class="box_input"><input type="text" name="" id="" placeholder="100자 이내로 입력해주세요."></div>
              </div>
              <div class="main_cnt_list clearfix">
                <h4 class="title">제목</h4>
                <div class="box_input"><input type="text" name="" id=""></div>
              </div>
              <!-- 버튼 시작 -->
              <div class="btn_wrap">
                <button type="button" class="btn_com btn_white">취소하기</button>
                <button type="submit" class="btn_com btn_blue">신청하기</button>
              </div>
              <!-- 버튼 끝 -->
            </div>
          </div>
				<!-- 영역분리 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp" %>
