<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="sub_cnt_wrap day_cnt_wrap">
	<p class="sub_tit">Day1</p>
	<div class="sub_cnt">
		<div class="day_main_cnt">
			<div class="day_cnt_desc">
				<div class="day_cnt_tit">일정1</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">장소</h4>
					<div class="box_input"><input type="text" name="" id="" class="placeDay" placeholder=""></div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">소요시간</h4>
					<div class="box_input timeDayWrap"><input type="text" name="" id=""  class="timeDay" placeholder="" onkeyup="timeDayNum(this);"></div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">설명</h4>
					<div class="box_input"><textarea name="" id="" class="ipt_txt_box ipt_txt_notice descDay" placeholder=""></textarea></div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">사진</h4>
					<button type="button" class="btn_com btn_blue file_btn">사진등록</button>
					<input type="file" name="" id="" class="ipt_file pictureDay" onchange="loadImg(this);">
					<i class="fileArea"></i>
					<input type="hidden">
				</div>
			</div>
		</div>
		<div class="btn_wrap">
			<a href="javascript: void(0);" class="day_add_btn">+</a>
			<span>일정 추가</span>
		</div>
	</div>
</div>