<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String pageName = request.getParameter("page");
	System.out.println(pageName);
	
	String msg = "";
	
	switch(pageName){
	case "insertNewGp" : msg = "프로그램이 정상적으로 신청되었습니다."; break;
	case "insertGuideSignUp" : msg = "가이드 신청이 접수 되었습니다"; break;
	case "updateHistory" : msg = "인터뷰 날짜가 변경되었습니다"; break;
	case "insertCp" : msg = "맞춤프로그램이 신청 되었습니다"; break;
	case "cpList" : msg = "맞춤프로그램 배정신청을 하셨습니다"; break;
	
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		alert('<%= msg %>');
		location.href = "<%= request.getContextPath()%>/index";
	</script>
</body>
</html>