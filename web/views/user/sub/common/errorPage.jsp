<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String pageName = request.getParameter("page");
	System.out.println(pageName);
	
	String msg = "";
	
	switch(pageName){
	case "insertNewGp" : msg = "프로그램이 신청에 실패했습니다."; break;
	case "insertGuideSignUp" : msg = "가이드 신청에 실패했습니다"; break;
	case "updateHistory" : msg = "날짜 수정에 실패 했습니다"; break;
	case "SelectAllGPListWithPaging" : msg = "프로그램 보기 조회에 실패했습니다."; break;
	case "selectMyProgramList" : msg = "내 프로그램 보기 조회에 실패했습니다."; break;
	case "insertMyProgram" : msg = "프로그램 개설에 실패했습니다."; break;
	case "selectMyProgramDetail" : msg = "내 프로그램 상세보기 조회에 실패했습니다."; break;
	case "updateMyProgramNotice" : msg = "내 프로그램 공지 수정에 실패했습니다."; break;
	case "selectProgramDetail" : msg = "프로그램 상세보기 조회에 실패했습니다."; break;
	case "selectCompGp" : msg = "완료된 프로그램 리스트 조회에 실패했습니다."; break;
	case "mainSearch" : msg = "검색에 실패했습니다."; break;
	case "index" : msg = "메인페이지 접속에 실패했습니다."; break;
	case "selectMyCpList" : msg = "내 맞춤 프로그램 목록 조회에 실패했습니다."; break;
	case "insertReview" : msg ="리뷰등록에 실패했습니다."; break;
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		alert('<%= msg %>');
		location.href = "<%= request.getContextPath() %>/index";
	</script>
</body>
</html>