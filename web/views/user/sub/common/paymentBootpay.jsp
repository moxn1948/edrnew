<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.sql.Date,java.text.SimpleDateFormat"%>
<%
	int programPrice = (int)request.getAttribute("programPrice");								// 프로그램 가격
	int programPno = (int)request.getAttribute("programPno");									// PRODUCT 번호
	int userMno = (int)request.getAttribute("userMno");											// 결제진행하는 USER NO
	String programName = (String)request.getAttribute("programName");							// 프로그램 명
	String orderName = (String)request.getAttribute("orderName");								// 주문자 명
	String orderPhoneNumber = (String)request.getAttribute("orderPhoneNumber");					// 주문자 핸드폰 번호
	String orderEmail = (String)request.getAttribute("orderEmail");								// 주문자 이메일
	
	String receiverName = (String)request.getAttribute("receiverName");							// 수령인 이름
	String userGender = (String)request.getAttribute("userGender");								// 수령인 성별
	String receiverAge = (String)request.getAttribute("receiverAge");							// 수령인 연령대
	String receiverPhoneNumber = (String)request.getAttribute("receiverPhoneNumber");			// 수령인 연락처
	String receiverSubPhoneNumber = (String)request.getAttribute("receiverSubPhoneNumber");		// 수령인 비상연락망
	String receiverEmail = (String)request.getAttribute("receiverEmail");						// 수령인 이메일
	String requestContent = (String)request.getAttribute("requestContent");						// 수령인 요청사항
	
	int recPerson = (Integer)request.getAttribute("recPerson");
	String payDate = (String)request.getAttribute("payDate");
	
	long currentTime = System.currentTimeMillis();
	int randomNumber = (int) (Math.random() * 100000);
	SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmmss");
	String orderCode = ft.format(new Date(currentTime)) + randomNumber;
	
/* 	String orderInfo = "?programPrice="+programPrice+"&programPno="+programPno+"&userMno="+userMno+"&programName="+programName+"&orderName="+orderName
			+"&orderPhoneNumber="+orderPhoneNumber+"&orderEmail="+orderEmail+"&receiverName="+receiverName+"&userGender="+userGender+"&receiverAge="+receiverAge
			+"&receiverPhoneNumber="+receiverPhoneNumber+"&receiverSubPhoneNumber="+receiverSubPhoneNumber+"&receiverEmail="+receiverEmail+"&requestContent="+requestContent
			+"&orderCode="+orderCode; */
%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- 결제관련 js  -->
<script src="https://cdn.bootpay.co.kr/js/bootpay-3.0.2.min.js" type="application/javascript"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<form action="<%= request.getContextPath() %>/insertOrder.od" method="post" id="orderInfo">
	<input type="hidden" name="programPrice" value="<%= programPrice %>">
	<input type="hidden" name="programPno" value="<%= programPno %>">
	<input type="hidden" name="userMno" value="<%= userMno %>">
	<input type="hidden" name="programName" value="<%= programName %>">
	<input type="hidden" name="orderName" value="<%= orderName %>">
	<input type="hidden" name="orderPhoneNumber" value="<%= orderPhoneNumber %>">
	<input type="hidden" name="orderEmail" value="<%= orderEmail %>">
	<input type="hidden" name="receiverName" value="<%= receiverName %>">
	<input type="hidden" name="userGender" value="<%= userGender %>">
	<input type="hidden" name="receiverAge" value="<%= receiverAge %>">
	<input type="hidden" name="receiverPhoneNumber" value="<%= receiverPhoneNumber %>">
	<input type="hidden" name="receiverSubPhoneNumber" value="<%= receiverSubPhoneNumber %>">
	<input type="hidden" name="receiverEmail" value="<%= receiverEmail %>">
	<input type="hidden" name="requestContent" value="<%= requestContent %>">
	<input type="hidden" name="orderCode" value="<%= orderCode %>">
	<input type="hidden" name="payDate" value="<%= payDate %>">
	<input type="hidden" name="recPerson" value="<%= recPerson %>">
	<input type="hidden" id="payType" name="payType" value="">
</form>
<script>
BootPay.request({
	price: '<%= programPrice %>',
	application_id: "5def16834f74b4002992469e",
	name: '<%= programName %>',
	pg: 'inicis',
	method: '',
	show_agree_window: 0,
	items: [
		{
			item_name: '나는 아이템',
			qty: 1,
			unique: '123',
			price: 1000,
			cat1: 'TOP',
			cat2: '티셔츠',
			cat3: '라운드 티',
		}
	],
	user_info: {
		username: '전세환',
		email: 'shjeontest@gmail.com',
		addr: '강남지원1관C강의장',
		phone: '010-1111-1111'
	},
	order_id: '<%= orderCode %>',
	params: {callback1: '그대로 콜백받을 변수 1', callback2: '그대로 콜백받을 변수 2', customvar1234: '변수명도 마음대로'},
	account_expire_at: '2018-05-25',
	extra: {
	    start_at: '2019-05-10',
		end_at: '2022-05-10',
        vbank_result: 1,
        quota: '0,2,3'
	}
}).error(function (data) {
	
}).cancel(function (data) {
	location.href="/edr/views/user/sub/common/payment.jsp";
}).ready(function (data) {
	
}).confirm(function (data) {
	var enable = true;
	if (enable) {
		BootPay.transactionConfirm(data);
	} else {
		BootPay.removePaymentWindow();
	}
}).close(function (data) {
	
}).done(function (data) {
	// #test의 value 로  orderInfo 를 대잉
	// #test를 submit 
	var payType = data.payment_group_name;
	$("#payType").val(payType);
	console.log(data);
	$("#orderInfo").submit();
	
});
</script>
</body>
</html>