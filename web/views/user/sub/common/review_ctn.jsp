<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="review_ctn">
<!-- 사용자 리뷰 시작 -->
<div class="user_review_wrap">
  <div class="review_top_cnt clearfix">
    <span class="user_name_cnt" id="userNameArea"></span>
    <span class="star_wrap" id="reviewStarArea">
    </span>
  </div>
  <!-- <p class="date_cnt">2019-11-11</p> -->
  <p class="review_main_cnt" id="userCntArea"></p>
</div>
<!-- 사용자 리뷰 끝 -->

</div><!-- review ctn end -->