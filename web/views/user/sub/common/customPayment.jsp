<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.customProgram.model.vo.*, com.edr.product.model.vo.*, com.edr.common.model.vo.*,java.text.SimpleDateFormat"%>
<%
	CpRequest cpRequest = (CpRequest)request.getAttribute("cpRequest");
	CpGuideEst cpGuideEst = (CpGuideEst)request.getAttribute("cpGuideEst");
	ArrayList<CpGuideDetail> cpGuideDetailList = (ArrayList)request.getAttribute("cpGuideDetailList");
	
%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
<style>
	.tableArea3 td{
		padding-left:10px;
	}
</style>


	<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_2_wrap">
			<!-- 제목 -->
			<form action="<%=request.getContextPath()%>/customPaymentProduct.common" method="post">
			<h1 class="main_title">맞춤 프로그램 결제</h1>
			<!-- 내용 -->
				<div class="main_cnt clearfix">
					<!-- 왼쪽 영역 -->
					<div class="left">
						<label class="area"><b>맞춤 프로그램</b></label><br>
						<label><%= cpRequest.getCpTitle() %></label>
						<hr>
						<label class="schedule"><b>상세 일정</b></label><br><br>
						<% for(int i = 0; i < cpGuideDetailList.size(); i++){ %>	
						<div class="scheduleDetail">
							<label>Day <%= cpGuideDetailList.get(i).getCpDay() %></label>
							<br><br>
							<p>일정 : <%= cpGuideDetailList.get(i).getCpCnt() %></p>
						</div>
						<br>
						<% } %>
						<hr>
						 시작일 : <label class="startDay"><%= cpRequest.getCpSdate() %></label><br>
						 종료일 : <label class="lastDay"><%= cpRequest.getCpEdate() %></label>
						<hr>
						<table align="center" class="tableArea3">
							<tr>
								<td><b>기본금</b></td>
								<td><%= cpGuideEst.getTotalCost() %> 원</td>
							</tr>
							<tr>
								<td><b>수수료</b></td>
								<% int vat = (int)(cpGuideEst.getTotalCost()*0.1); %>
								<td><%= vat %> 원</td>
							</tr>
							<tr>
								<td><b>총비용</b></td>
								<% int resultPay = vat + cpGuideEst.getTotalCost(); %>
								<td><%= resultPay %> 원</td>
							</tr>
						</table>
					</div>
					<!-- 왼쪽영역 끝 -->
					
					<!-- 오른쪽 영역 -->
					<div class="right">
						<!-- <div class="orderBox"> -->
						<label><b>주문자 명</b></label><br>
						<input type="text" class="orderName" name="orderName" placeholder="  이름을 입력해주세요."><br><br>
						<label><b>주문자 연락처</b></label><br>
						<select class="telList" name="orderTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="orderPhone" name="orderPhone" placeholder="  숫자만 입력해주세요."><br><br>
						<label><b>주문자 이메일</b></label><br>
						<input type="email" class="orderEmail" name="orderEmail" placeholder="  이메일을 입력해주세요.">
						<br><br>
						<hr>
						<div class="orderCheckArea">
						<label for="orderCheck"><b>주문자와 동일</b></label>
						<input type="checkbox" class="orderCheck" name="orderCheck" id="orderCheck" value="orderCheck">
						</div>
						<br><br><br>
						<label><b>수령자 명</b></label><br>
						<input type="text" class="receiverName" name="receiverName" placeholder="  이름을 입력해주세요."><br><br>
						
						<label><b>수령자 성별</b></label><br><br>
						<input type="radio" name="userGender" value="M" id="male">
						<label for="male">남</label>
						<input type="radio" name="userGender" value="F" id="female">
						<label for="female">여</label>
						<br><br>
						
						<label><b>수령자 나이</b></label><br>
						<select class="receiverAge" name="receiverAge">
							<option value="">나이를 선택해주세요</option>
							<option value="10">20대 이하</option>
							<option value="20">20대</option>
							<option value="30">30대</option>
							<option value="40">40대</option>
							<option value="50">50대</option>
							<option value="60">50대 이상</option>
						</select>
						<br><br>
						<label><b>수령자 연락처</b></label><br>
						<select class="receiverTelList" name="receiverTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="receiverPhone" name="receiverPhone" placeholder="  숫자만 입력해주세요."><br><br>

						<label><b>비상 연락처</b></label><br>
						<select class="receiverSubTelList" name="receiverSubTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="receiverSubPhone" name="receiverSubPhone" placeholder="  숫자만 입력해주세요."><br><br>
						<label><b>수령자 이메일</b></label><br>
						<input type="email" class="receiverEmail" name="receiverEmail" placeholder="  이메일을 입력해주세요.">
						<br><br>

						<label><b>요청사항</b></label><br>
						<textarea rows="6" cols="39" style="resize:none;" name="requestContent"></textarea>
						<br>
						<br>
						<div class="joinTerms">
							<label><b>이용약관(필수)</b></label><br>
							<input type="checkbox" name="terms" id="terms" value="true"><label for="terms">동의 함&nbsp;&nbsp;&nbsp;</label><a href="/edr/views/user/sub/popup/paymentPop.jsp" class="termText" style="display:inline-block;">이용약관</a>
						</div>
						<!-- </div> -->
					</div>
					<!-- 오른쪽 영역 끝 -->
					<!-- 버튼 시작 -->
					<input type="hidden" name="programPrice" value="<%= resultPay %>">
					<input type="hidden" name="recPerson" value="<%= cpRequest.getCpPerson() %>">
					<input type="hidden" name="pDate" value="<%= cpRequest.getCpSdate() %>">
					<input type="hidden" name="userMno" value="<%= loginUser.getMno() %>">
					<input type="hidden" name="programName" value="<%= cpRequest.getCpTitle() %>">
					<input type="hidden" name="cpNo" value="<%= cpRequest.getCpNo() %>">
					<input type="hidden" name="cpEstNo" value="<%= cpGuideEst.getEstNo() %>">
					<!-- 버튼 끝 -->
				</div>
				<div class="btn_wrap">
					<button type="button" class="btn_com btn_white">취소하기</button>
					<button class="btn_com btn_blue" type="submit" onclick="return payment();">결제하기</button>
				</div>
			</form>
		</div>
	</div>
	</main>

<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
<!-- 팝업 관련 js -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
<script>
function termOk(){
	$("#termsCheck").attr("checked", true);
	$.magnificPopup.close();
}
	$(function() {

		$('.termText').magnificPopup({
			type : 'ajax',
		    closeOnBgClick:false
		});

		/* 
		 function termOk(){
		 $(".pop_wrap").trigger("click");
		 }; */
		$(".popUpClose").click(function() {
			$.magnificPopup.close();
		});
		 
		$("#orderCheck").click(function(){
			var checkOrder = $("#orderCheck").is(":checked");
			if(checkOrder == true){
				var name = $(".orderName").val();
				var phone = $(".orderPhone").val();
				var email = $(".orderEmail").val();
				
				$(".receiverName").val(name);
				$(".receiverPhone").val(phone);
				$(".receiverEmail").val(email);
			}else{
				$(".receiverName").val("");
				$(".receiverPhone").val("");
				$(".receiverEmail").val("");
			}
		});
			
		
	});
	
	function payment(){
		
		if($(".orderName").val() == "" ){
			alert("주문자 이름을 입력해주세요.");
			return false;
		}
		if($(".orderPhone").val() == "" ){
			alert("주문자 연락처를 입력해주세요.");
			return false;
		}
		if($(".orderEmail").val() == "" ){
			alert("주문자 이메일을 입력해주세요.");
			return false;
		}
		if($(".receiverName").val() == "" ){
			alert("수령자 이름을 입력해주세요.");
			return false;
		}
		if($(".userGender").val() == "" ){
			alert("수령자 성별을 선택해주세요.");
			return false;
		}
		if($(".receiverAge").val() == "" ){
			alert("수령자 나이를 선택해주세요.");
			return false;
		}
		if($(".receiverPhone").val() == "" ){
			alert("수령자 연락처를 입력해주세요.");
			return false;
		}
		if($(".receiverSubPhone").val() == "" ){
			alert("수령자 비상연락처를 입력해주세요.");
			return false;
		}
		if($(".receiverEmail").val() == "" ){
			alert("수령자 이메일을 입력해주세요.");
			return false;
		}
		if($("#terms").prop("checked") == false){
			alert("이용약관에 동의해주세요");
			return false;
		}
		
		return true;
		
	};
	
</script>


<%@ include file="../../../inc/user/footer.jsp"%>