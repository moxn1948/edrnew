<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.generalProgram.model.vo.*, com.edr.product.model.vo.*, com.edr.common.model.vo.*,java.text.SimpleDateFormat"%>
<%
	PaymentProduct paymentProduct = (PaymentProduct)request.getAttribute("paymentProduct");
	ArrayList<GpDetail> gpDayList = (ArrayList<GpDetail>)request.getAttribute("gpDayList");
	ArrayList<GpDetail> gpDayDetailList = (ArrayList<GpDetail>)request.getAttribute("gpDayDetailList");
	ArrayList<GpPriceDetail> gpPriceDetailList = (ArrayList<GpPriceDetail>)request.getAttribute("gpPriceDetailList");
	GpDetail gpCalendarDetail = null;
	ArrayList<GpDetail> gpCalendarDetailList = new ArrayList<>();
	String payDate = (String)request.getAttribute("payDate");
	int recPerson = (Integer)request.getAttribute("recPerson");
/* 	for(int i = 0; i < gpDetail.size(); i++ ){
		gpCalendarDetail = new GpDetail();
		gpCalendarDetail.setGpDay(gpDetail.get(i).getGpDay());
		gpCalendarDetail.setGpDaySeq(gpDetail.get(i).getGpDaySeq());
		gpCalendarDetail.setGpLocation(gpDetail.get(i).getGpLocation());
		gpCalendarDetail.setGpTime(gpDetail.get(i).getGpTime());
		
		gpCalendarDetailList.add(gpCalendarDetail);
	} */

%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
<style>
	.tableArea3 td{
		padding-left:10px;
	}
</style>


	<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_2_wrap">
			<!-- 제목 -->
			<form action="<%=request.getContextPath()%>/paymentProduct.common" method="post">
			<h1 class="main_title">프로그램 결제</h1>
			<!-- 내용 -->
				<div class="main_cnt clearfix">
					<!-- 왼쪽 영역 -->
					<div class="left">
						<label class="area"><b><%= paymentProduct.getLocalName() %></b></label><br>
						<label class="programTitle"><b><%= paymentProduct.getGpName() %></b></label>
						<hr>
						<label class="schedule"><b>상세 일정</b></label><br><br>
						<%
							ArrayList<GpDetail> day1List = new ArrayList<>();
							ArrayList<GpDetail> day2List = new ArrayList<>();
							ArrayList<GpDetail> day3List = new ArrayList<>();
							ArrayList<GpDetail> day4List = new ArrayList<>();
							ArrayList<GpDetail> day5List = new ArrayList<>();
							ArrayList<GpDetail> day6List = new ArrayList<>();
							ArrayList<GpDetail> day7List = new ArrayList<>();
							
							
						
/* 							
							
							System.out.println("day1List : " + day1List);
							System.out.println("day2List : " + day2List);
							System.out.println("day3List : " + day3List);
							System.out.println("day4List : " + day4List);
							System.out.println("day5List : " + day5List);
							System.out.println("day6List : " + day6List);
							System.out.println("day7List : " + day7List); */
							
							HashMap<String, Object> hmap = new HashMap<>();

							for (int i = 0; i < gpDayDetailList.size(); i++) {
								GpDetail gpd = new GpDetail();
								gpd = gpDayDetailList.get(i);
								if(gpd.getGpDay() == 1){
									day1List.add(gpd);
								}else if(gpd.getGpDay() == 2){
									day2List.add(gpd);
								}else if(gpd.getGpDay() == 3){
									day3List.add(gpd);
								}else if(gpd.getGpDay() == 4){
									day4List.add(gpd);
								}else if(gpd.getGpDay() == 5){
									day5List.add(gpd);
								}else if(gpd.getGpDay() == 6){
									day6List.add(gpd);
								}else if(gpd.getGpDay() == 7){
									day7List.add(gpd);
								}
							}
						

/* 							for (int i = 0; i < paymentProduct.getGpTday(); i++) {
								for(int j = 0; j < gpDayDetailList.get(i).getGpDaySeq(); j++){

								if (gpDayDetailList.get(i).getGpDay() == 1) {
									day1List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 2) {
									day2List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 3) {
									day3List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 4) {
									day4List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 5) {
									day5List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 6) {
									day6List.add(gpDayDetailList.get(i));
								}
								else if (gpDayDetailList.get(i).getGpDay() == 7) {
									day7List.add(gpDayDetailList.get(i));
								}
								}
							} */
							
							/* System.out.println("day1List : " + day1List);
							System.out.println("day2List : " + day2List);
							System.out.println("day3List : " + day3List);
							System.out.println("day4List : " + day4List);
							System.out.println("day5List : " + day5List);
							System.out.println("day6List : " + day6List);
							System.out.println("day7List : " + day7List); */
						%>
						<% if(day1List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 1</label>
							<% for(int j = 0; j < day1List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day1List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day1List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day1List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day2List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 2</label>
							<% for(int j = 0; j < day2List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day2List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day2List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day2List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day3List.size() != 0){ 
							System.out.print(day3List);
							%>
						<div class="scheduleDetail">
							<label>Day 3</label>
							<% for(int j = 0; j < day3List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day3List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day3List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day3List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day4List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 4</label>
							<% for(int j = 0; j < day4List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day4List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day4List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day4List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day5List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 5</label>
							<% for(int j = 0; j < day5List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day5List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day5List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day5List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day6List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 6</label>
							<% for(int j = 0; j < day6List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day6List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day6List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day6List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						<% if(day7List.size() != 0){ %>
						<div class="scheduleDetail">
							<label>Day 7</label>
							<% for(int j = 0; j < day7List.size(); j++ ){ %>
							<br><br>
							<p>일정 <%= day7List.get(j).getGpDaySeq() %></p>
							<p>소요시간 <%= day7List.get(j).getGpTime() %> 시간</p>
							<p>장소 : <%= day7List.get(j).getGpLocation() %></p>
							<% } %>
						</div>
						<% } %>
						
						<hr>
						 시작일 : <label class="startDay"><%= payDate %></label><br>
						<% 
							SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
							Date date = sdformat.parse(payDate);
							Calendar cal = Calendar.getInstance();
							cal.setTime(date);
							cal.add(Calendar.DATE, paymentProduct.getGpTday()-1);
							String programEndDay = sdformat.format(cal.getTime());
						%>
						 종료일 : <label class="lastDay"><%= programEndDay %></label>
						<hr>
						<table align="center" class="tableArea3">
							<tr>
								<td><b>인원추가금액</b></td>
								<% int recPay = (int)(paymentProduct.getGpCost()*(0.1*recPerson)); %>
								<td> <%= recPay %> 원</td>
							</tr>
							<tr>
								<td><b>수수료</b></td>
								<% int vat = (int)((paymentProduct.getGpCost() + recPay)*0.1); %>
								<td> <%= vat %> 원</td>
							</tr>
							<tr>
								<td><b>총비용</b></td>
								<% int realPay = paymentProduct.getGpCost() + recPay + vat; %>
								<td> <%= realPay %> 원</td>
							</tr>
						</table>
					</div>
					<!-- 왼쪽영역 끝 -->
					
					<!-- 오른쪽 영역 -->
					<div class="right">
						<!-- <div class="orderBox"> -->
						<label><b>주문자 명</b></label><br>
						<input type="text" class="orderName" name="orderName" placeholder="  이름을 입력해주세요."><br><br>
						<label><b>주문자 연락처</b></label><br>
						<select class="telList" name="orderTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="orderPhone" name="orderPhone" placeholder="  숫자만 입력해주세요."><br><br>
						<label><b>주문자 이메일</b></label><br>
						<input type="email" class="orderEmail" name="orderEmail" placeholder="  이메일을 입력해주세요.">
						<br><br>
						<hr>
						<div class="orderCheckArea">
						<label for="orderCheck"><b>주문자와 동일</b></label>
						<input type="checkbox" class="orderCheck" name="orderCheck" id="orderCheck" value="orderCheck">
						</div>
						<br><br><br>
						<label><b>수령자 명</b></label><br>
						<input type="text" class="receiverName" name="receiverName" placeholder="  이름을 입력해주세요."><br><br>
						
						<label><b>수령자 성별</b></label><br><br>
						<input type="radio" name="userGender" value="M" id="male">
						<label for="male">남</label>
						<input type="radio" name="userGender" value="F" id="female">
						<label for="female">여</label>
						<br><br>
						
						<label><b>수령자 나이</b></label><br>
						<select class="receiverAge" name="receiverAge">
							<option value="">나이를 선택해주세요</option>
							<option value="10">20대 이하</option>
							<option value="20">20대</option>
							<option value="30">30대</option>
							<option value="40">40대</option>
							<option value="50">50대</option>
							<option value="60">50대 이상</option>
						</select>
						<br><br>
						<label><b>수령자 연락처</b></label><br>
						<select class="receiverTelList" name="receiverTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="receiverPhone" name="receiverPhone" placeholder="  숫자만 입력해주세요."><br><br>

						<label><b>비상 연락처</b></label><br>
						<select class="receiverSubTelList" name="receiverSubTelList">
							 <option value="+82">+82</option>
		                     <option value="+850">+850</option>
		                     <option value="+84">+84</option>
		                     <option value="+852">+852</option>
		                     <option value="+63">+63</option>
		                     <option value="+61">+61</option>
		                     <option value="+1">+1</option>
		                     <option value="+33">+33</option>
		                     <option value="+31">+31</option>
		                     <option value="+32">+32</option>
						</select>
						<input type="tel" class="receiverSubPhone" name="receiverSubPhone" placeholder="  숫자만 입력해주세요."><br><br>
						<label><b>수령자 이메일</b></label><br>
						<input type="email" class="receiverEmail" name="receiverEmail" placeholder="  이메일을 입력해주세요.">
						<br><br>

						<label><b>요청사항</b></label><br>
						<textarea rows="6" cols="39" style="resize:none;" name="requestContent"></textarea>
						<br>
						<br>
						<div class="joinTerms">
							<label><b>이용약관(필수)</b></label><br>
							<input type="checkbox" name="terms" id="terms" value="true"><label for="terms">동의 함&nbsp;&nbsp;&nbsp;</label><a href="/edr/views/user/sub/popup/paymentPop.jsp" class="termText" style="display:inline-block;">이용약관</a>
						</div>
						<!-- </div> -->
					</div>
					<!-- 오른쪽 영역 끝 -->
					<!-- 버튼 시작 -->
					<input type="hidden" name="programPrice" value="<%= realPay %>">
					<input type="hidden" name="programPno" value="<%= paymentProduct.getPno() %>">
					<input type="hidden" name="userMno" value="<%= loginUser.getMno() %>">
					<input type="hidden" name="programName" value="<%= paymentProduct.getGpName() %>">
					<input type="hidden" name="recPerson" value="<%= recPerson %>">
					<input type="hidden" name="payDate" value="<%= payDate %>">
					<!-- 버튼 끝 -->
				</div>
				<div class="btn_wrap">
					<button type="button" class="btn_com btn_white">취소하기</button>
					<button class="btn_com btn_blue" type="submit" onclick="return payment();">결제하기</button>
				</div>
			</form>
		</div>
	</div>
	</main>

<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
<!-- 팝업 관련 js -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
<script>
function termOk(){
	$("#termsCheck").attr("checked", true);
	$.magnificPopup.close();
}
	$(function() {

		$('.termText').magnificPopup({
			type : 'ajax',
		    closeOnBgClick:false
		});

		/* 
		 function termOk(){
		 $(".pop_wrap").trigger("click");
		 }; */
		$(".popUpClose").click(function() {
			$.magnificPopup.close();
		});
		 
		$("#orderCheck").click(function(){
			var checkOrder = $("#orderCheck").is(":checked");
			if(checkOrder == true){
				var name = $(".orderName").val();
				var phone = $(".orderPhone").val();
				var email = $(".orderEmail").val();
				
				$(".receiverName").val(name);
				$(".receiverPhone").val(phone);
				$(".receiverEmail").val(email);
			}else{
				$(".receiverName").val("");
				$(".receiverPhone").val("");
				$(".receiverEmail").val("");
			}
		});
			
		
	});
	
	function payment(){
		
		if($(".orderName").val() == "" ){
			alert("주문자 이름을 입력해주세요.");
			return false;
		}
		if($(".orderPhone").val() == "" ){
			alert("주문자 연락처를 입력해주세요.");
			return false;
		}
		if($(".orderEmail").val() == "" ){
			alert("주문자 이메일을 입력해주세요.");
			return false;
		}
		if($(".receiverName").val() == "" ){
			alert("수령자 이름을 입력해주세요.");
			return false;
		}
		if($(".userGender").val() == "" ){
			alert("수령자 성별을 선택해주세요.");
			return false;
		}
		if($(".receiverAge").val() == "" ){
			alert("수령자 나이를 선택해주세요.");
			return false;
		}
		if($(".receiverPhone").val() == "" ){
			alert("수령자 연락처를 입력해주세요.");
			return false;
		}
		if($(".receiverSubPhone").val() == "" ){
			alert("수령자 비상연락처를 입력해주세요.");
			return false;
		}
		if($(".receiverEmail").val() == "" ){
			alert("수령자 이메일을 입력해주세요.");
			return false;
		}
		if($("#terms").prop("checked") == false){
			alert("이용약관에 동의해주세요");
			return false;
		}
		
		return true;
		
	};
	
</script>


<%@ include file="../../../inc/user/footer.jsp"%>