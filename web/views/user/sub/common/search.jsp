<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.Gp, com.edr.guide.model.vo.GuideDetail, com.edr.common.model.vo.Attachment"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<%
	HashMap<String, Object> list = (HashMap<String, Object>) request.getAttribute("list");
	ArrayList<HashMap<String, Object>> gpList = (ArrayList<HashMap<String, Object>>) list.get("gpList");
	ArrayList<HashMap<String, Object>> selectAllReviewCnt = (ArrayList<HashMap<String, Object>>) list.get("selectAllReviewCnt");
%>
<style>
.result {display: none;}
</style>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container search">
	<div class="inner_ct clearfix">
	
		
		<div class="layout_1_wrap">
			<!-- 내용 시작 -->
			<div class="main_cnt">
				<h2 class="search_tit">프로그램 명 겸색결과</h2>
				<!-- 작은 카드 시작 -->
				<ul class="all_card_wrap new_program_card_wrap clearfix" id="localSrchCtn">
				<!-- 작은 카드 내용  -->
			<% for(int i = 0; i < gpList.size(); i++){ %>
          <li class="card_wrap new_program_card result result<%=i %>">
            <div class="card">
	          <input type="hidden" value="">
              <a href="<%=request.getContextPath() %>/selectProgramDetail.gp?no=<%= ((Gp) gpList.get(i).get("GpObj")).getGpNo() %>">
                <span class="card_img"><img src="/edr/uploadFiles/<%= ((Attachment) gpList.get(i).get("AttaObj")).getChangeName() %>" alt=""></span>
                <div class="card_cnt_wrap">
                  <div class="top_cnt clearfix">
                      <div class="guide_cnt"><%= ((GuideDetail) gpList.get(i).get("GuideObj")).getGname() %> 가이드의</div>
                      <div class="location_cnt locationCnt">
                      <% 
                      String localName = "";
                      switch(((Gp) gpList.get(i).get("GpObj")).getLocalNo()) {	
	                    case 1: localName = "서울"; break;
	                  	case 2: localName = "경기"; break;
	                	case 3: localName = "경상"; break;
	                	case 4: localName = "강원"; break;
	                	case 5: localName = "전라"; break;
	                	case 6: localName = "충청"; break;
	                	case 7: localName = "제주"; break;
	                	case 8: localName = "부산"; break;
	                	case 9: localName = "대구"; break;
	                	case 10: localName = "대전"; break;
	                	case 11: localName = "광주"; break;
	                	case 12: localName = "인천"; break;
	                	case 13: localName = "울산"; break;
                      } %>
                      <%= localName %>
                      </div>
                    </div>
                    <div class="program_tit_cnt ellipsis"><%= ((Gp) gpList.get(i).get("GpObj")).getGpName() %></div>
                    <div class="price_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpCost() %>원</div>
                    <div class="time_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpTday() %>일</div>
                    <div class="bot_cnt clearfix">
                      <div class="grade_cnt">
                        <% if((double) (selectAllReviewCnt.get(i).get("reviewAvg")) > 0){ %>
		                  <span class="grade_tit">평점</span>
	                    <% } %>
	                    
		                  <span class="star_wrap">
		                  <%
                 			for(int j = 0; j < Integer.parseInt((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[0]); j++){
	                  	  %>
                 				<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
	                  	  <% 
                 			}

	                 		   
	                 	   if((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[1] == "5"){
	                  	   %>
	                    
	                       <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
	                  	
	                       <% }%>
		                  </span>
                      </div>
                      <div class="review_cnt">후기 <%= selectAllReviewCnt.get(i).get("reviewCount") %>개</div>
                    </div>
                </div>
              </a>
            </div>
          </li>
          <% } %>
				</ul>
				<!-- 작은 카드 끝 -->
				<!-- 버튼 시작  -->
				<div class="btn_wrap">
					<button type="submit" id="localMoreBtn" class="btn_com btn_white">프로그램 더보기</button>
				</div>
				<!-- 버튼 끝  -->
			</div>
			<!-- 내용 끝 -->
		</div>



	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
	$(function(){


		// 지역명 개수
    (function(){
			var srchCount = <%=gpList.size() %>; // 총 지역명 개수
			var programClick = 0; // 프로그램 더보기 클릭 횟수
			var resultCount = 0;
			
			if(srchCount == 0){
				$("#localSrchCtn").append("<p class='empty_search'>검색 결과가 없습니다.</p>");
			
				// 후기 버튼 삭제
				$("#localMoreBtn").remove();
			}else if(srchCount <= 8){
				// 나중에는 result 개수 만큼으로 수정
				var srchCountFix = srchCount;
				for(var i = 0; i < srchCountFix; i++){
					// $("#localSrchCtn").append(result);
					
					$("#localSrchCtn").find(".result" + resultCount).show();
					resultCount++;
					
					srchCount--;
				}
				// 후기 버튼 삭제
				$("#localMoreBtn").remove();
			}else{
				// 고정으로 8개 출력
				for(var i = 0; i < 8; i++){
					//$("#localSrchCtn").append(result);
				
					$("#localSrchCtn").find(".result" + resultCount).show();
					resultCount++;
					srchCount--;
				}
			}

			$("#localMoreBtn").on("click",function(){
				programClick++; // 몇 번째 클릭했는 지
				srchCount -= 8; // 한 번 클릭 시 출력되는 개수

				if(srchCount >= 0){
					// 8개씩 더 출력
					for(var i = 0; i < 8; i++){
						// $("#localSrchCtn").append(result);
						$("#localSrchCtn").find(".result" + resultCount).show();
						resultCount++;
					}
				}else if(srchCount < 0 && srchCount > -8){
					// 남은 개수 만큼 더 출력, 즉 (8 + srchCount) 만큼 더 출력
					for(var i = 0; i < 8 + srchCount; i++){
						// $("#localSrchCtn").append(result);
						$("#localSrchCtn").find(".result" + resultCount).show();
						resultCount++;
					}

					// 후기 버튼 삭제
					$("#localMoreBtn").remove();
				}

			});
			
    })();


	});
</script>
<%@ include file="../../../inc/user/footer.jsp" %>
