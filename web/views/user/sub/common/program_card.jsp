<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<li class="card_wrap new_program_card">
	<div class="card">
		<a href="#">
			<span class="card_img"><img src="../../../../imgs/temp_img.png" alt=""></span>
			<div class="card_cnt_wrap">
				<div class="top_cnt clearfix">
						<div class="guide_cnt">송기준 가이드의</div>
						<div class="location_cnt">서울</div>
					</div>
					<div class="program_tit_cnt ellipsis">서울의 야경 투어</div>
					<div class="price_cnt">100,000원</div>
					<div class="time_cnt">1박 2일</div>
					<div class="bot_cnt clearfix">
						<div class="grade_cnt">
							<span class="grade_tit">평점</span>
							<span class="star_wrap">
								<i><img src="../../../../imgs/star_icon.png" alt=""></i>
								<i><img src="../../../../imgs/star_icon.png" alt=""></i>
								<i><img src="../../../../imgs/star_icon.png" alt=""></i>
								<i><img src="../../../../imgs/star_icon.png" alt=""></i>
								<i><img src="../../../../imgs/star_half_icon.png" alt=""></i>
							</span>
						</div>
						<div class="review_cnt">후기 50개</div>
					</div>
			</div>
		</a>
	</div>
</li>