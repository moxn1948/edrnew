<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <li class="card_wrap guide_card_wrap new_guide_card">
   <div class="card">
     <a href="#">
       <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
       <div class="card_cnt_wrap">
           <div class="location_cnt">서울</div>
           <div class="guide_name_cnt">최원준 가이드</div>
           <div class="guide_age_cnt">남성 20대</div>
           <div class="bot_cnt clearfix">
             <div class="grade_cnt">
               <span class="grade_tit">평점</span>
               <span class="star_wrap">
                 <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                 <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                 <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                 <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                 <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
               </span>
             </div>
             <div class="review_cnt">후기 50개</div>
           </div>
       </div>
     </a>
   </div>
 </li>