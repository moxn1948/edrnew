<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String orderCode = request.getParameter("orderCode");
%>
<%@ include file="../../../inc/user/subHeader.jsp"%>

	<main class="container">
		<div class="inner_ct clearfix">
			<div class="orderComplete" style="width:600px; height:300px;margin-left:auto;margin-right:auto;">
				<h1 align="center" style="margin-top:80px;">주문이 완료되었습니다.</h1>
				<br>
				<h2 align="center"><%= orderCode %></h2>
				<br>
				<br>
				<a align="center" href="<%= request.getContextPath() %>/index">홈으로 이동</a>
			</div>
		</div>
	</main>

<%@ include file="../../../inc/user/footer.jsp"%>