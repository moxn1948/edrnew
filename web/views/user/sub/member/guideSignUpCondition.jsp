<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.edr.guide.model.vo.*, java.util.*, com.edr.common.model.vo.*" %>
<%
	GuideDetail gd = (GuideDetail)request.getAttribute("gd");
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<HashMap<String, Object>> Attachlist = (ArrayList<HashMap<String, Object>>) request.getAttribute("Attachlist");
	
	int result = (int) request.getAttribute("result");
	ArrayList<Attachment> Attachlist2 = (ArrayList<Attachment>) request.getAttribute("Attachlist2");
	int num = (int) request.getAttribute("num");
	 for (int i = 0; i < Attachlist2.size(); i++) {
		Attachment at = Attachlist2.get(i);
	}
 
%>

<%@ include file="../../../inc/user/subHeader.jsp"%>
<style>
.container .layout_1_wrap .main_cnt_list .title {
	float: left;
    width: 160px;
    font-size: 16px;
    line-height: 40px;
}
.box_txt{display: inline-block;margin-top: 10px;}
</style>
<!-- timepicker -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="/edr/css/user/jakeStyle_user.css">
<title>가이드 신청 현황</title>
	

	<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_1_wrap">
		
			<!-- 제목 -->
			<h1 class="main_title">가이드 신청 현황</h1>
			<form action="<%= request.getContextPath()%>/updateInterview.gd" method="post">
			<input type="hidden" name="mno" value="<%= loginUser.getMno()%>">
			<!-- 내용 시작 -->
			<div class="main_cnt">
				<div class="main_cnt_list clearfix">
				<%for(int i = 0; i < 1; i++) {
            		HashMap<String, Object> hmap = Attachlist.get(i);     
         	   %>
					<h2 class="title_situation">상태 : <% String str ="";
					if(hmap.get("ghState").equals("OK")) {
						str="승인완료";
							
					}else if(hmap.get("ghState").equals("FAIL")) {
						str="미승인";
					}else if(hmap.get("ghState").equals("WAIT")) {
						str="대기중";
					}%> <%= str %></h2>
					
				</div>
				 
				<div class="main_cnt_list clearfix">
					<img src="/edr/uploadFiles/<%=hmap.get("changeName")%>" id="addImg">	
					
				
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">이름</h4>
					<div class="box_input"><span class="box_txt"><%= hmap.get("gname")%></span></div>
					
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">연락처</h4>
					<div class="box_input"><span class="box_txt"><%= hmap.get("phone")%></span></div>
				</div>

			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">비상연락처</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("phone2")%></span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">카카오톡ID</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("kakaoId") %></span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">소개</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("introduce") %></span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">주소</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("address") %></span></div>				
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">활동도시</h4>
				<div class="box_input"><span class="box_txt">
				
				
				<% for(int j =0; j < Attachlist.size(); j++) {
					String test = (String) Attachlist.get(j).get("localName");
					if(j == Attachlist.size()- 1) {
				%>
					<%=test %>
				<%
					
					}	
				}
				%>
				
				
				</span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">활동도시 거주기간</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("period") %>개월</span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 경험 유무</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("expYn") %></span></div>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">가능한 언어</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("lang") %></span></div>
			</div>
				<h4 class="title">첨부 파일</h4>		
			<div class="main_cnt_list clearfix" style="width:400px;">
					
			<% for(int a = 0; a < Attachlist2.size(); a++) {
				System.out.println(Attachlist2.get(a));
				Attachment at = Attachlist2.get(a);

			%>
				<a class="txt_cnt" href="<%=request.getContextPath()%>/adGuideHistoryDown.ad?num=<%=at.getFileNo()%>"><%=at.getOriginName()%></a>	
			</div>
			<% } %>
				<div class="main_cnt_list clearfix">
				<h4 class="title">인터뷰 신청 날짜</h4>
				<div class="box_input"><span class="box_txt"><%= hmap.get("interviewD") %></span></div>
				<% }  %>
			</div>
					
			<div class="main_cnt_list clearfix">
					<h4 class="title">날짜 수정</h4>
					<div class="box_input box_select" >
					 <input type="text" name="date" id="startDateSel" class="date_pick ipt_sel" placeholder="날짜를 선택해주세요." autocomplete="off">	
					 
			<div class="sub_cnt">
						<select name="dateTime"
							class="timepicker meet_time ipt_txt add"
							placeholder="만나는 시간을 선택해주세요">
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
							<option value="13:00">13:00</option>
							<option value="14:00">14:00</option>
							<option value="15:00">15:00</option>
							<option value="16:00">16:00</option>
							<option value="17:00">17:00</option>
							
						</select>
					</div>		 									
				</div>			
							
					</div>
					
					
			
				<div class="btn_wrap">
					<button type="submit" class="btn_com btn_blue">수정하기</button>
				</div>
			</form>
			</div>

		</div>

	</main>
	<!-- timepicker -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	// 만나는 시간 timepicker
	// 만나는 시간 timepicker
		$(function() {
	
		$(".date_pick").datepicker({
		      nextText: '다음 달',
		      prevText: '이전 달',
		      dateFormat: "yy-mm-dd",
		      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
		      minDate : "+1D"
		    });
		});
	
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>
