<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String msg = (String)request.getAttribute("msg");
%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
	<!-- 팝업 관련 js -->
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
<style>
.container .layout_2_wrap{padding: 100px 0 140px;}
.container .layout_2_wrap .main_cnt{width: 470px;}
b{font-size: 16px;}
.inputBox_name {width: 381px;height: 40px;}
.inputBox_email{width: 381px;height: 40px;}
.inputBoxPwd{width: 470px;height: 40px;}
.inputBoxPwd2{width: 470px;height: 40px;}
.checkBtn_shName{line-height: 48px;}
.checkBtn_shEmail{line-height: 48px;}
.userAgeInput{margin-top:5px;width: 155px;height: 45px;}
.container .btn_wrap{padding-top: 30px;}
.joinBtn{padding: 3px 0;}
</style>	
	
	<main class="container">
	<div class="inner_ct clearfix">
	
		<div class="layout_2_wrap">
			<!-- 제목 -->
			<h1 class="main_title">회원가입</h1>
			<!-- 내용 -->
			<div class="main_cnt">
				<form action="<%= request.getContextPath() %>/join.me" method="post" class="joinForm">
				<div class="userName">
					<label><b>활동명*</b></label><br>
					<input type="text" name="userName" id="userName" class="inputBox_name" placeholder="    한글 두글자 이상" size="83" style="margin-top:6px;">
					<div style="display:inline-block; background:lightgray;margin-left: 6px;width:76px; height:47px; text-align:center;"><label class="checkBtn_shName" id="nameCheck" style="cursor:pointer;">중복확인</label></div><br>
					<p class="regCheckName">이름이 유효하지 않습니다.</p>
				</div>
				<br>
				<div class="userEmail">
					<label><b>이메일*</b></label><br>
					<input type="email" name="userEmail" id="userEmail" class="inputBox_email" placeholder="    영어 대소문자, 특수문자 ( - _ ), 숫자만 사용가능 / 4자 이상" size="83" style="margin-top:6px;">
					<div style="display:inline-block; background:lightgray;margin-left: 6px;width:76px; height:47px; text-align:center;"><label class="checkBtn_shEmail" id="emailCheck" style="cursor:pointer;">중복확인</label></div><br>
					<p class="regCheckEmail">이메일이 유효하지 않습니다.</p>
				</div>
				<br>
				<div class="userGender">
					<label><b>성별*</b></label><br>
					<input type="radio" name="userGender" value="M" id="male">
					<label for="male">남</label>
					<input type="radio" name="userGender" value="F" id="female">
					<label for="female">여</label>
				</div>
				<br>
				<div class="userAge">
					<label><b>태어난 년도*</b></label><br>
   				    <select name="userAgeList" class="userAgeInput">
   				    	<option value=""> 연도를 선택해주세요</option>
   				    <%
   				    	for(int i = 2004; i > 1900; i--){
   				    %>
   				    	<option value="<%= i %>"><%= i %> 년</option>
   				    <% 
   				    	}
   				    %>
        			</select>
				</div>
				<br>
				<div class="userPwd">
					<label><b>비밀번호*</b></label><br>
					<input type="password" name="userPwd" id="" class="inputBoxPwd" placeholder="    비밀번호를 입력해주세요" size="83" style="margin-top:6px;">
					<p class="regCheckpassword">비밀번호를 입력해주세요</p>
				</div>
				<br>
				<div class="userPwd">
					<label><b>비밀번호 확인*</b></label><br>
					<input type="password" name="userPwdCheck" id="" class="inputBoxPwd2" placeholder="    비밀번호를 다시 입력해주세요" size="83" style="margin-top:6px;">
					<p class="regCheckpasswordCheck">비밀번호가 일치하지 않습니다</p>
				</div>
				<br>
				<div class="joinTerms">
					<label><b>이용약관*</b></label><br>
					<a href="../popup/joinPop.jsp" class="termText" style="font-size: 16px;"><i>이용약관보기 →</i></a>
					<input type="checkbox" name="terms" class="termsCheck" id="termsCheck" value="true"><label for="termsCheck">동의함</label>&nbsp;&nbsp;
					<p class="regChecktermsCheck">이용약관에 동의해주세요.</p>
				</div>
				<!-- 버튼 시작 -->
				<div class="btn_wrap">
					<button class="joinBtn" onclick="return joinCheck();">회원가입</button>
				</div>
				<!-- 버튼 끝 -->
				</form>
			</div>
		</div>
	</div>
	</main>
<script type="text/javascript">
function termOk(){
	$("#termsCheck").attr("checked", true);
	$.magnificPopup.close();
}

$(function(){
	

	$('.termText').magnificPopup({ 
	     type: 'ajax',
	     closeOnBgClick:false
	});
	if(<%= msg %> != null){
		alert(<%= msg %>);
	}
	window.checkEmail = false;
	window.checkName = false;
	
	$("#nameCheck").click(function(){
		
		var userName = $("#userName").val();
		
		console.log("중복");
		
		if(userName != ""){
			
		$.ajax({
			url:"/edr/idCheck.me",
			type:"get",
			data:{
				userName:userName
			},
			success:function(data){
				if(data === "fail"){
					alert("중복된 활동명 입니다.");
				}else{
					alert("사용가능한 활동명 입니다.")
					checkName = true;
				}
			},
			error:function(){
				console.log("실패");
			}
		});
		
		}else{
			alert("활동명을 입력해주세요");
		}
		
	});
	
	//////////////////////////////////////////
	
	$("#emailCheck").click(function(){

		var userEmail = $("#userEmail").val();
		
		if(userEmail != ""){
			$.ajax({
				url:"/edr/emailCheck.me",
				type:"get",
				data:{
					userEmail:userEmail
				},
				success:function(data){
					if(data === "fail"){
						alert("중복된 이메일 입니다.");
					}else{
						alert("사용가능한 이메일 입니다.");
						checkEmail = true;
					}
				},
				error:function(){
					console.log("실패");
				}
				
			});
		}else{
			alert("이메일을 입력해주세요.");
		}
	});

});

function termzPop(){
	var term = "이용약관내용`~`afefaefasdfasdfasdfaefafasf";
			
	alert(term);
};


$(function(){
	$(".regCheckEmail").hide();
	$(".regCheckName").hide();
	$(".regCheckpasswordCheck").hide();
	$(".regCheckpassword").hide();
	$(".regChecktermsCheck").hide();
	
	var regExpName = /^[가-힝]{2,}$/;
	var regExpEmail = /\w{4,}@\w{1,}\.\w{1,3}/;
	
	
	if($(".inputBox_name").val().length > 0){
		if(!regExpName.test($(".inputBox_name").val())){
			$(".regCheckName").show();
		}else{
			$(".regCheckName").hide();
		}
	}
	
});
function joinCheck(){

		
		if($(".inputBox_name").val() == ""){
			alert("활동명을 입력해주세요");
			$(".inputBox_name").focus();
			return false;
		}
		var regExpName = /^[가-힣]{2,}|[a-zA-Z]{2,}\s[a-zA-Z]{2,}$/;
		if(!regExpName.test($(".inputBox_name").val())){
			alert("활동명을 다시 입력해주세요");
			$(".inputBox_name").focus();
			$(".inputBox_name").select();
			return false;
		}
		if($(".inputBox_email").val() == ""){
			alert("이메일을 입력해주세요");
			$(".inputBox_email").focus();
			return false;
		}
		var regExpEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if(!regExpEmail.test($(".inputBox_email").val())){
			alert("이메일을 다시 입력해주세요");
			$(".inputBox_email").focus();
			$(".inputBox_email").select();
			return false;
		}
	
		if($("input:radio[name=userGender]").is(":checked") == false){
			alert("성별을 선택해주세요");
			return false;
		}
		
		if($(".inputBox_email").val() == ""){
			alert("이메일을 입력해주세요");
			$(".inputBox_email").focus();
			return false;
		}
		if($(".userAgeInput").val() == ""){
			alert("태어난 년도를 선택해주세요");
			return false;
		}	
		var password = $(".inputBoxPwd").val();
		var password2 = $(".inputBoxPwd2").val()
		
		if( password != password2 ){
			alert("비밀번호가 일치하지 않습니다.");
			$(".inputBoxPwd2").focus();
			$(".inputBoxPwd2").select();
			return false;
		}
		
		if($(".termsCheck").prop("checked") == false){
			console.log("ddd");
			alert("이용약관에 동의해주세요");
			return false;
		}
		//////////////////////////////////////////
		

		
		///////////////////////////////////////////
		
	if(checkEmail == true && checkName == true){
		alert("해당이메일로 인증해주세요.");
		return true;
	}else{
		alert("중복체크를 해주세요.");
		return false;
	}
	
};

</script>
	<%@ include file="../../../inc/user/footer.jsp"%>