<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String userEmail = (String)request.getAttribute("userEmail");
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet" type="text/css" href="../../../../css/user/hyeonjuStyle.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style type="text/css">
	.tableArea tr{
		height : 50px;
	}
	.tableArea td{
		padding-left : 20px;
	}
</style>
</head>
<body>
<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_2_wrap" style="margin: auto;padding: 40px;width: 360px;border: 1px solid #ddd;box-shadow: 1px 1px 2px 2px rgba(100, 100, 100, 0.1);">
			<!-- 제목 -->
			<h1 class="main_title">비밀 번호 변경</h1>
			
			<!-- 내용 -->
			<form class="formtable" action="<%= request.getContextPath() %>/" method="post" >
			<div class="login_cnt">
				
				<div align="center">
						<table class="tableArea">
							<tr>

								<td><label>새로운 비밀번호  </label></td>
								<td><input type="password" name="pwd" class="idInputBox" placeholder="새로운 비밀번호를 입력해주세요"></td>

							</tr>
							<tr>
								<td><label>비밀번호 확인  </label></td>
								<td><input type="password" name="pwd2" class="PasswordBox" placeholder="비밀번호를  다시 입력해주세요."></td>
							</tr>
							<!-- 버튼 시작 -->
						</table>
						<div class="btn_wrap">
							<input type="hidden" name="userEmail" value="<%=userEmail%>">
							<button type="submit" class="confirm" style="width : 320px;line-height: 50px;border: 0;transition: 0.25s;color: #f7f7f7;background-color: #171f57;">확인</button>
						</div>

						<!-- 버튼 끝 -->
			</div>
			
			</div>
			</form>
		</div>
	</div>
	</main>
	<script>
	
	</script>
		<%@ include file="../../../inc/user/footer.jsp"%>
	
</body>
</html>