<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String msg = (String)request.getAttribute("msg");
%>
	<%@ include file="../../../inc/user/subHeader.jsp"%>
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
<style>
.container .layout_2_wrap{padding: 100px 0 160px;}
.idInputBox{padding: 0 10px;height: 40px;}
.PasswordBox{margin-top: 10px;padding: 0 10px;height: 40px;}
.container .btn_wrap{padding-top: 10px;}
.container .loginBtn{width: 344px;}
.movepasswordFind{padding-top: 8px;padding-left: 10px;}
.movesignup{padding-top: 8px;padding-right: 10px;}
.container .layout_2_wrap .login_cnt{padding: 40px 36px 56px;}
</style>
	<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_2_wrap">
			<!-- 제목 -->
			<h1 class="main_title">로그인</h1>
			<!-- 내용 -->
			<form action="<%= request.getContextPath() %>/login.me" method="post" id="test">
			<div class="login_cnt">
				<div align="center">
				<input type="text" name="userEmail" class="idInputBox" placeholder="이메일을 입력해주세요.">
				<input type="password" name="userPwd"class="PasswordBox" placeholder="비밀번호를 입력해주세요.">
				</div>
				<!-- 버튼 시작 -->
				<div class="btn_wrap">
					<button type="button" class="loginBtn">로그인</button>
				</div>  
				<span class="movepasswordFind"><a href="<%=request.getContextPath() %>/views/user/sub/member/pwdFind.jsp">비밀번호 찾기</a></span>
				<span class="movesignup"><a href="<%= request.getContextPath() %>/views/user/sub/member/join.jsp">회원가입</a></span>
				<!-- 버튼 끝 -->
			</div>
			</form>
		</div>
	</div>
	</main>
<script>
	$(function() {
		
			$(".loginBtn").click(function() {
				
				
				var userEmail = $(".idInputBox").val();
				var userPwd = $(".PasswordBox").val();
				
				console.log(userEmail);
				console.log(userPwd);
				
				if(userEmail == "" || userPwd == ""){
					alert("아이디 비밀번호를 확인해주세요");
				}else if(userEmail != "" && userPwd != ""){
					$.ajax({
						url:"/edr/idPwdCheck.me",
						type:"post",
						data:{
							userEmail:userEmail,
							userPwd:userPwd
						},
						success:function(data){
							console.log(data);
							if(data == "success3"){
								alert("탈퇴된 회원입니다.");
								
								return false;
							}else if(data == "success2"){
								alert("이메일 인증을 진행해주세요.");
								return false;
							}else if(data == "success"){
								alert("아이디 비밀번호를 확인해주세요.");
								return false;
							}else if(data == "fail"){
								alert("아이디 비밀번호를 확인해주세요.");
								return false;
							}
							
							$("#test").submit();
							
							
						},
						error:function(){
							
						}
					});		
					
				}
			//
			});
			
			
			
		<%-- var msg = '<%= msg %>';
		if (msg != 'null') {
			$(function() {
				alert(msg);
			});
		} --%>

	});
</script>
	<%@ include file="../../../inc/user/footer.jsp"%>
</body>
</html>