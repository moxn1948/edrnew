<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<!-- kakao map api link -->
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2&libraries=services"></script>
<!-- 주소 검색 api -->
<script
	src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>

 
<style>
.container #notice {
	height: 200px
}

.container #produce {
	height: 200px
}

.container #box_select {
	float: left
}

.container #box_select2 {
	float: left
}

.container #language {
	float: left
}

.container #phone {
	width: 200px;
}

.container #phone2 {
	width: 200px;
}

#titleImgArea {
	width: 350px;
	height: 200px;
	text-align: center;
	display: table-cell;
	vertical-align: middle;
}
.container .box_select select{width: 75px;}
.ipt_txt {
    float: left;
    padding: 5px;
    margin-right: 4px;
    width: 340px;
    line-height: 30px;
    border: 1px solid #ddd;
}
.ipt_add_1 {
    float: left;
    width: 146px;
}
.ipt_add_3 {
	margin-left: 120px;
}
.add_srch_wrap.btn_wrap {
    float: left;
    padding-top: 0;
    width: 159px;
}
.add_srch_btn {
	display: inline-block;
    padding: 0;
    width: 100%;
    line-height: 43px;
    text-align: center;
    color: #f7f7f7;
    background-color: #171f57;
}
.container .main_cnt_list {
    padding: 20px 0;
}
</style>

<!-- 컨텐츠가 들어가는 부분 start -->
<%
	if (loginUser != null) {
%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<main class="container">
<div class="inner_ct clearfix">
	<div class="layout_1_wrap">
		<!-- 제목 -->
		<h1 class="main_title">가이드 신청</h1>
		<!-- 내용 시작 -->
		<div class="main_cnt">
			<!-- encType="multipart/form-data" -->
			<form action="<%=request.getContextPath()%>/guideSignUp.gd"
				method="post" class="signUpForm" encType="multipart/form-data">
				<input type="hidden" name="mno" value="<%= loginUser.getMno()%>">
				<div class="main_cnt_list clearfix">
					<h4 class="title">사진*</h4>
					<div id="titleImgArea">
						<img id="titleImg" width="350" height="200px">
					</div>
					<div id="fileArea">
						<input type="file" id="profile" name="profile"
							onchange="loadImg(this, 1)" accept=".jpg, .png">
					</div>

				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">이름*</h4>
					<!-- 기본적인 input 시작 -->
					<div class="box_input">
						<input type="text" name="name" id="name" placeholder="실명을 입력해주세요">
						<!-- 기본적인 input 끝 -->
					</div>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">연락처*</h4>
					<!-- 기본적인 input 시작 -->
					<div class="box_input box_select">
						<input type="text" name="phone1" id="phone" class="phone1">

						<select name="nationNo" id="box_select">
							<option value="+82">+82</option>
							<option value="+850">+850</option>
							<option value="+84">+84</option>
							<option value="+852">+852</option>
							<option value="+63">+63</option>
							<option value="+61">+61</option>
							<option value="+1">+1</option>
							<option value="+33">+33</option>
							<option value="+31">+31</option>
							<option value="+32">+32</option>
						</select>

					</div>


				</div>

				<br>

				<div class="main_cnt_list clearfix">
					<h4 class="title">비상연락처*</h4>
					<!-- 기본적인 input 시작 -->
					<div class="box_input box_select">
						<input type="text" name="ephone1" id="phone2" class="phone4">

						<select name="nationNo2" id="box_select">
							<option value="+82">+82</option>
							<option value="+850">+850</option>
							<option value="+84">+84</option>
							<option value="+852">+852</option>
							<option value="+63">+63</option>
							<option value="+61">+61</option>
							<option value="+1">+1</option>
							<option value="+33">+33</option>
							<option value="+31">+31</option>
							<option value="+32">+32</option>
						</select>

					</div>


				</div>


				<div class="main_cnt_list clearfix">
					<h4 class="title">카카오톡 아이디</h4>
					<div class="box_input">
						<input type="text" name="kakaoId" id="kakaoId"
							placeholder="카카오톡 ID를 입력해주세요">
					</div>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">
						<label>&nbsp;&nbsp;&nbsp;</label>
					</h4>
					<div class="box_input">
						<label>※ 예약 확정 후 연락처가 공개될 경우 카카오톡으로 여행자의 문의를 받을 수 있습니다. <br>
							※ 영상인터뷰를 위해 필요합니다.
						</label>
					</div>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">소개*</h4>
					<div class="box_input">
						<textarea placeholder="소개를 입력해주세요" cols="50" rows="5"
							style="resize: none;" name="introduce" id="introduce"></textarea>
					</div>
				</div>

				<div class="sub_cnt_wrap">
					<h4 class="title">주소*</h4>
					<div class="sub_cnt">
						<div class="add_t_cnt clearfix">
							<input type="text" name="postcode" id="postcode"
								class="ipt_txt ipt_add_1" placeholder="우편번호" readonly> <input
								type="text" name="roadAddress" id="roadAddress"
								class="ipt_txt ipt_add_2" placeholder="도로명주소" readonly>
						</div>
						<div class="add_b_cnt clearfix">
							<input type="text" name="detailAddress" id="detailAddress"
								class="ipt_txt ipt_add_3" placeholder="상세주소"> <input
								type="hidden" name="addressX" id="addressX"> <input
								type="hidden" name="addressX" id="addressY">
							<div class="btn_wrap add_srch_wrap">
								<a href="javascript: void(0);" class="add_srch_btn"
									id="addressApi">검색</a>
							</div>
						</div>
					</div>
				</div>


				<div class="main_cnt_list clearfix">
					<h4 class="title">활동도시*</h4>
					<div class="box_select1 box_select">
						<select name="nation" id="box_select5" class="nation">
							<option value="1">서울</option>
							<option value="2">경기</option>
							<option value="3">경상</option>
							<option value="4">강원</option>
							<option value="5">전라</option>
							<option value="6">충청</option>
							<option value="7">제주</option>
							<option value="8">부산</option>
							<option value="9">대구</option>
							<option value="10">대전</option>
							<option value="11">광주</option>
							<option value="12">인천</option>
							<option value="13">울산</option>
						</select>
					</div>
					<a href="javascript: void(0)" onclick="cityPlus()" style="margin-top: 4px;margin-left: 120px;display: inline-block;background-color: #171f57;color: #fff;padding: 3px 0;text-align: center;width: 40px;">추가</a>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">거주기간*</h4>
					<div class="box_select">
						<select name="residence" id="box_select">
							<option value="6">6개월</option>
							<option value="7">7개월</option>
							<option value="8">8개월</option>
							<option value="9">9개월</option>
							<option value="10">10개월</option>
							<option value="11">11개월</option>
							<option value="12">12개월이상</option>
						</select>
					</div>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">가이드 경험 유무*</h4>
					<!-- 라디오 시작 -->
					<div class="box_radio">
						<input type="radio" name="exp" id="exp" value="Y"><label
							for="test1">있음</label> <input type="radio" name="exp" id="exp1"
							value="N"><label for="test2">없음</label>
					</div>
					<!-- 라디오 끝 -->
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">가능언어</h4>
					<div class="box_chk1">
						<input type="checkbox" name="language" id="language1" value="한국어">
						<label for="language1">한국어</label> 
						<input type="checkbox" name="language" id="language2" value="영어"> 
						<label for="language2">영어</label> 
						<input type="checkbox" name="language" id="language3" value="중국어"> 
						<label for="language3">중국어</label>		
						<input type="checkbox" name="language" id="language4" value="독일어">
						<label for="language4">독일어</label> <a href="javascript: void(0)"
							onclick="languagePlus()" style="margin-top: 4px;margin-left: 4px;display: inline-block;background-color: #171f57;color: #fff;padding: 3px 0;text-align: center;width: 40px;">추가</a>
					</div>
					<div class="box_input1">
						<input type="text" name="language5" id="language5"
							placeholder="그 외 언어">
					</div>
				</div>



				<div class="main_cnt_list clearfix">
					<h4 class="title">파일 추가*</h4>
					<div class="box_input2">
						<input type="file" name="necessary" id="necessary">
					</div>
					<div>
						<a href="javascript: void(0)" onclick="filePlus()"style="margin-top: 4px;display: inline-block;background-color: #171f57;color: #fff;padding: 3px 0;text-align: center;width: 40px;">추가</a>
					</div>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">해외 결격 사유*</h4>
					<div class="box_chk">
						<input type="checkbox" name="disqual" id="disqual" value="N"><label>해외
							결격 사유가 없다면 체크해주세요..</label>
									
						<div class="clearfix">
							<h4 class="title">
								<label>&nbsp;&nbsp;&nbsp;</label>
							</h4>
							<div class="box_input">
								<p style="line-height: 18px;">해외 결격 사유가 없을 시 신청 가능합니다.</p>
								<p style="line-height: 18px;">추후 결격 사유가 발견될 경우 불이익이 있을 수 있습니다.</p>
							</div>
						</div>
					</div>
				</div>


				<div class="main_cnt_list clearfix">
					<h4 class="title">인터뷰 날짜</h4>
					<div class="box_input box_select">
						<input type="text" name="date" id="startDateSel"
							class="date_pick ipt_sel" placeholder="날짜를 선택해주세요."
							autocomplete="off">
						<div class="sub_cnt">
							<select name="dateTime" class="timepicker meet_time ipt_txt add" placeholder="만나는 시간을 선택해주세요">
								<option value="10:00">10:00</option>
								<option value="11:00">11:00</option>
								<option value="12:00">12:00</option>
								<option value="13:00">13:00</option>
								<option value="14:00">14:00</option>
								<option value="15:00">15:00</option>
								<option value="16:00">16:00</option>
								<option value="17:00">17:00</option>

							</select>
						</div>
					</div>
				</div>
				<div class="btn_wrap">
					<button class="btn_com btn_white">취소하기</button>
					<button class="btn_com btn_blue"
						onclick="return signUp()">신청하기</button>
				</div>

			</form>
		</div>
		<!-- 버튼 시작  -->
		<!-- 버튼 끝  -->

	</div>



</div>

<!-- 내용 끝 -->

</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
		var i = 1;
		
		
	$(function() {
	
		$(".date_pick").datepicker({
		      nextText: '다음 달',
		      prevText: '이전 달',
		      dateFormat: "yy-mm-dd",
		      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
		      minDate : "+1D"
		    }).on("change", function(){
				var date = $('.date_pick').val();
				 $.ajax({
					url: "adTimeCheck.gd",
					data:{
						date : date,
						num : <%=loginUser.getMno()%>
					},
					
					type:"get",
					success:function(data) {
						console.log(data);
						for(var i = 0; i < data.length; i++) {
							var opIdx = $("select[name=timeP]").find("option").length;
							for(var j = 0; j < opIdx - 1; j++){
								if($("select[name=timeP]").find("option").eq(j)[0].value == data[i].interviewTime) {
									$("select[name=timeP]").find("option").eq(j)[0].remove();
								}
							}
							console.log("nn : " + data[i].interviewTime);
						}
						
						
							/* $('.timepicker').timepicker().val(data[i].interviewTime).attr('disabled', true); */
							 
						
						
						 
						
						 
					},
					error:function(error, status) {
						console.log("서버 전송 실패!");
					}
				
					
				}); 
			
			});
		
		
		
		
		
		
		
		});
		
	
		// 우편번호 검색
		(function(){
		    //주소-좌표 변환 객체를 생성
		    var geocoder = new daum.maps.services.Geocoder();
	
			$("#addressApi").on("click",function(){
				console.log("123");
			      new daum.Postcode({
			            oncomplete: function(data) {
			                var addr = data.address; // 최종 주소 변수
			                var roadAddr = data.roadAddress; // 도로명 주소 변수
							
			                // 주소 정보를 해당 필드에 넣는다.
	          				document.getElementById('postcode').value = data.zonecode;
			                document.getElementById("roadAddress").value = roadAddr;
			                // 주소로 상세 정보를 검색
			                geocoder.addressSearch(data.address, function(results, status) {
			                    // 정상적으로 검색이 완료됐으면
			                    if (status === daum.maps.services.Status.OK) {
	
			                        var result = results[0]; //첫번째 결과의 값을 활용
	
			                        // 해당 주소에 대한 좌표를 받아서
			                        var coords = new daum.maps.LatLng(result.y, result.x);
			                        
			                        $("#addressX").val(coords.Ga);
			                        $("#addressY").val(coords.Ha);
			                    }
			                });
			            }
			        }).open();
			});
			
			
			$("#postcode, #roadAddress").on("click", function(){
				$("#addressApi").trigger("click");
			});
			
		})();
		
		
	
	function loadImg(value, num) {
		if(value.files && value.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				switch(num) {
				case 1 : $("#titleImg").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}
	$(function() {
		$("#fileArea").hide();
		
		$("#titleImgArea").click(function() {
			$("#profile").click();
		});
	})


	function cityPlus() {
		console.log($(".nation .box_select1").length);
		if($(".box_select1 .nation").length < 3){
		$(".nation").clone().eq(0).appendTo(".box_select1");
			
		} 
		
	}
	function languagePlus() {
		$("<input type='text' name='language5' id='language5' placeholder='그 외 언어'>").clone().eq(0).appendTo(".box_input1");
	}
	function filePlus() {
		if($(".box_input2 #necessary").length < 3){
		$("<input type='file' name=" + ("'necessary_" + i++) +"'" + "id='necessary'>").clone().eq(0)
				.appendTo(".box_input2"); 
		}
	}
	
	
		
	


	function signUp() {
		
	var regName = /^[가-힣]{2,4}$/;
	
	if(!regName.test($("#name").val())){
		alert("이름이 잘못됬습니다");
		$("#name").focus();
		$("#name").select();
		return false;
	}
	
/* 	var regPhone1 = /[^0-9]{1, 13}/g;
	if(!regPhone1.test($(".phone1").val())) {
		alert("전화번호가 잘못됬습니다");
		$(".phone1").focus();
		$(".phone1").select();
		return false;
	}
	var regPhone2 = /[^0-9]/g;
	if(!regPhone2.test($(".phone4").val())) {
		alert("비상 연락망이 잘못됬습니다");
		$(".phone4").focus();
		$(".phone4").select();
		return false;
	} */
	
	var regKaKao = /^[0-9a-z]+$/;
	if(!regKaKao.test($("#kakaoId").val())) {
		alert("카카오ID가 잘못됬습니다");
		$("#kakaoId").focus();
		$("#kakaoId").select();
		return false();
	}
	
/* 	var regIntro = /^[가-힣]{2,50}$/;
	
	if(!regIntro.test($("#introduce").val())) {
		alert("소개가 잘못됬습니다");
		$("#introduce").focus();
		$("#introduce").select();
		return false;
	} */
	
	
	
		
	if($("#disqual").prop("checked") == false) {
		$(".btn_com btn_blue").disabled();
		return false;
		
		}
	};
	$("#titleImg").mouseenter(function() {
		   $(this).parent().css({"cursor":"pointer"});
	   })
</script>




<%
	} else {
		response.sendRedirect("../../index.jsp");
	}
%>

<%@ include file="../../../inc/user/footer.jsp"%>



