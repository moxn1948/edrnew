<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="../../../../css/user/hyeonjuStyle.css">

 <!-- 컨텐츠가 들어가는 부분 start -->
 <main class="container">
   <div class="inner_ct clearfix">
     <div class="layout_2_wrap">
       <!-- 제목 -->                                   
       <h1 class="main_title">비밀번호 찾기</h1>
       <!-- 내용 -->
       <div class="main_cnt">
       
       <form action ="<%=request.getContextPath()%>/sendChangePwd.me " method="post" class="findForm">
         <div class="main_cnt_list clearfix pwd">
           <div class="box_input findpwd" id="">아이디를 입력하세요</div>
            
         </div>
			<br><br>
         <div class="main_cnt_list clearfix pwd">
           
           <div class="box_input">
           <input type="text" name="userEmail" id="userEmail" class="send_email" placeholder="이메일을 입력하세요." ></div>
         
         
         </div>
          <!-- 버튼 시작 -->
         <div class="btn_wrap">
             <button type="submit" class="btn_com btn_blue conf" style="width:272px" onclick="return findPwd();">확인</button>
         </div>
         <!-- 버튼 끝 -->
         </form>
        
       </div>
     </div>
   </div>
 </main>
 <!-- 컨텐츠가 들어가는 부분 end -->
 <script>
	$(function () {
		$(".btn_com.btn_blue.conf").click(function () {
			//bnt클릭시 이함수 작동
			alert("입력하신 이메일로 비밀번호 변경 링크를 발송했습니다.");
		})
	})
 
 
 
   $(function(){
      $(".okBtn").click(function(){
         $.ajax({
            url:"/edr/pwdSendEmail.me", //여기를 바꿔야함
            type:"get",
            data:{
               userEmail:userEmail
            },
            success:function(data){
               if(data === "success"){
                  alert("해당아이디로 이메일을 전송하였습니다.");
                  //여기서 비밀 번호 변경 페이지로 이동
<%--                   location.href="<%= request.getContextPath() %>/sendChangePwd.me";  --%>
                  return true;
                  
                  
                  //여기서 비밀번호 변경 ???
                
                  
               }else{
                  alert("아이디를 확인해 주세요.");
                  return false;
               }
            },
            error:function(){
               console.log("실패");
            }
            
         });
      })
   })

 </script>
 
<%@ include file="../../../inc/user/footer.jsp" %>
