<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.Gp, com.edr.guide.model.vo.GuideDetail, com.edr.common.model.vo.Attachment, com.edr.common.model.vo.PageInfo"%>
<% 
	int localNo = (Integer) request.getAttribute("localNo");
	HashMap<String, Object> recomGp = (HashMap<String, Object>) request.getAttribute("recomGp"); 
	
	// 추천 프로그램 관련
	Gp recomGpObj = (Gp) recomGp.get("recomGpObj");
	GuideDetail recomGuideObj = (GuideDetail) recomGp.get("recomGuideObj");
	Attachment recomAttaObj = (Attachment) recomGp.get("recomAttaObj");
	HashMap<String, Object> recomReviewCnt = (HashMap<String, Object>) request.getAttribute("recomReviewCnt"); 
	
	int recomReviewCount = (Integer) recomReviewCnt.get("recomReviewCount");
	Double recomReviewAvg = (Double) recomReviewCnt.get("recomReviewAvg");
	int recomStar = Integer.parseInt((recomReviewAvg + "").split("\\.")[0]);
	int recomHalfStar;

	if((recomReviewAvg + "").split("\\.")[1].equals("5")){
		recomHalfStar = 5;
	}else{
		recomHalfStar = 0;
	}
 
	// 프로그램 목록 보기
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int limit = pi.getLimit();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
	
	HashMap<String, Object> list = (HashMap<String, Object>) request.getAttribute("list");
	ArrayList<HashMap<String, Object>> gpList = (ArrayList<HashMap<String, Object>>) list.get("gpList");
	ArrayList<HashMap<String, Object>> selectAllReviewCnt = (ArrayList<HashMap<String, Object>>) list.get("selectAllReviewCnt");
	
%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container areaList">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		
		<%@ include file="../../../inc/user/programSubMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit" id="localNameTitle"></h4>
			<div class="sub_ctn">
      	   <!--  <h3 class="sub_ctn_tit">서울 신규 프로그램</h3> -->
		      <!-- 큰 카드 시작 -->
		      <div class="new_wide_wrap new_program_wide">
		        <div class="new_wide">
		          <%-- <input type="hidden" value="<%=recomGpObj.getGpNo() %>"> --%>
		          <a href="<%=request.getContextPath() %>/selectProgramDetail.gp?no=<%=recomGpObj.getGpNo() %>" class="clearfix">
		            <span class="new_wide_img"><img src="/edr/uploadFiles/<%=recomAttaObj.getChangeName() %>" class="area_new_wide_img" alt="프로그램 이미지"></span>
		            <div class="new_wide_cnt_wrap">
		              <div class="top_cnt clearfix">
		                <div class="grade_cnt clearfix">
		                  <% if(recomReviewAvg > 0){ %>
		                  <span class="grade_tit">평점</span>
		                  <% } %>
		                  <span class="star_wrap">
		                  <%
                  			for(int i = 0; i < recomStar; i++){
		                  	%>
                  				<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
		                  	<% 
                  			}
		                    
		                    if(recomHalfStar == 5){
		                  	%>
		                    
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
		                  	
		                  	<% }%>
		                  </span>
		                </div>
		                <div class="review_cnt">후기 <%= recomReviewCount %>개</div>
		              </div>
		              <div class="guide_cnt"><%= recomGuideObj.getGname() %> 가이드의</div>
		              <div class="program_tit_cnt ellipsis"><%= recomGpObj.getGpName() %></div>
		              <div class="price_cnt"><%= recomGpObj.getGpCost() %>원</div>
		              <div class="desc_cnt"><%= recomGpObj.getGpDescr() %></div>
		              <div class="bot_cnt clearfix">
		                <div class="person_cnt">참가 가능 인원 <%= recomGpObj.getGpMin() %>명 ~ <%= recomGpObj.getGpMax() %>명</div>
		                <div class="time_cnt"><%= recomGpObj.getGpTday() %>일</div>
		              </div>
		            </div>
		          </a>
		        </div>
		      </div>
		      <!-- 큰 카드 끝 -->
      <!-- 작은 카드 시작 -->
      <ul class="all_card_wrap new_program_card_wrap clearfix">
      <% for(int i = 0; i < gpList.size(); i++){ %>
          <li class="card_wrap new_program_card">
            <div class="card">
	          <input type="hidden" value="">
              <a href="<%=request.getContextPath() %>/selectProgramDetail.gp?no=<%= ((Gp) gpList.get(i).get("GpObj")).getGpNo() %>">
                <span class="card_img"><img src="/edr/uploadFiles/<%= ((Attachment) gpList.get(i).get("AttaObj")).getChangeName() %>" alt=""></span>
                <div class="card_cnt_wrap">
                  <div class="top_cnt clearfix">
                      <div class="guide_cnt"><%= ((GuideDetail) gpList.get(i).get("GuideObj")).getGname() %> 가이드의</div>
                      <div class="location_cnt locationCnt"></div>
                    </div>
                    <div class="program_tit_cnt ellipsis"><%= ((Gp) gpList.get(i).get("GpObj")).getGpName() %></div>
                    <div class="price_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpCost() %>원</div>
                    <div class="time_cnt"><%= ((Gp) gpList.get(i).get("GpObj")).getGpTday() %>일</div>
                    <div class="bot_cnt clearfix">
                      <div class="grade_cnt">
                        <% if((double) (selectAllReviewCnt.get(i).get("reviewAvg")) > 0){ %>
		                  <span class="grade_tit">평점</span>
	                    <% } %>
	                    
		                  <span class="star_wrap">
		                  <%
                 			for(int j = 0; j < Integer.parseInt((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[0]); j++){
	                  	  %>
                 				<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
	                  	  <% 
                 			}

	                 		   
	                 	   if((selectAllReviewCnt.get(i).get("reviewAvg") + "").split("\\.")[1] == "5"){
	                  	   %>
	                    
	                       <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
	                  	
	                       <% }%>
		                  </span>
                      </div>
                      <div class="review_cnt">후기 <%= selectAllReviewCnt.get(i).get("reviewCount") %>개</div>
                    </div>
                </div>
              </a>
            </div>
          </li>
          <% } %>
      </ul>
      <!-- 작은 카드 끝 -->
<!--       페이저 시작
      <div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
          <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
          <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
        </ul>
      </div>
      페이저 끝 -->
      <!-- 페이저 시작 -->
      <div class="pager_wrap">
         <ul class="pager_cnt clearfix add">
            <% if(currentPage <= 1)  { %>


            <li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>


            <%} else { %>
            <li class="pager_com pager_arr prev"><a
               href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?currentPage=<%=currentPage - 1%>&localNo=<%= localNo%>">&#x003C;</a></li>

            <%} %>
            <% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
            <%} else {%>
            <li class="pager_com pager_num"><a href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?currentPage=<%=p%>&localNo=<%= localNo%>"><%=p %></a></li>
            <%} %>
            <% } %>


            <% if(currentPage >= maxPage) { %>
            <li class="pager_com pager_arr next"><a
               href="javascript: void(0);">&#x003E;</a></li>
            <%}else { %>
            <li class="pager_com pager_arr next"><a
               href="<%= request.getContextPath()%>/selectAllListWithPaging.gp?currentPage=<%=currentPage + 1%>&localNo=<%= localNo%>">&#x003E;</a></li>

            <%} %>

         </ul>
      </div>
      <!-- 페이저 끝 -->
      
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
$(function(){
	
	// 상단 지역명 처리시작
	(function(){
		var menuIdx = <%= localNo %> - 1;
	    $(".sub_menu_cnt .sub_menu_list").eq(menuIdx).addClass("on");
	    
	    var menuTit = $(".sub_menu_cnt .sub_menu_list").eq(menuIdx).find("a")[0].text;
	    $("#localNameTitle").text(menuTit);
	    $(".locationCnt").text(menuTit);
	    
	    
	})();
	// 상단 지역명 처리 끝
	
   
   
});
</script>
<%@ include file="../../../inc/user/footer.jsp" %>
