<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.*, com.edr.common.model.vo.Attachment, com.edr.product.model.vo.Product, com.edr.review.model.vo.Review"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<%
	int gpNo = (Integer) request.getAttribute("gpNo");
	Gp gpObj = (Gp) request.getAttribute("gpObj");
	ArrayList<GpPriceDetail> gpPriceList  = (ArrayList<GpPriceDetail>) request.getAttribute("gpPriceList");
	ArrayList<GpDetail> gpDetailList  = (ArrayList<GpDetail>) request.getAttribute("gpDetailList");
	ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) request.getAttribute("gpAttachmentList");
	ArrayList<GpLang> gpLangList  =  (ArrayList<GpLang>) request.getAttribute("gpLangList");
	HashMap<String, Object> gpReviewCnt = (HashMap<String, Object>) request.getAttribute("gpReviewCnt"); 
	ArrayList<Product> gpProductList  =  (ArrayList<Product>) request.getAttribute("gpProductList");
	ArrayList<HashMap<String, Object>> mainReviewCnt  =  (ArrayList<HashMap<String, Object>>) request.getAttribute("mainReviewCnt");
	
	String localName = "";
	switch (gpObj.getLocalNo()) {
	case 1: localName = "서울"; break;
	case 2: localName = "경기"; break;
	case 3: localName = "경상"; break;
	case 4: localName = "강원"; break;
	case 5: localName = "전라"; break;
	case 6: localName = "충청"; break;
	case 7: localName = "제주"; break;
	case 8: localName = "부산"; break;
	case 9: localName = "대구"; break;
	case 10: localName = "대전"; break;
	case 11: localName = "광주"; break;
	case 12: localName = "인천"; break;
	case 13: localName = "울산"; break;
	}
	
	int totalDay = gpObj.getGpTday();
	
	String lang = "";
	for(int i = 0; i < gpLangList.size(); i++){
		if(i != gpLangList.size() - 1){
			lang += gpLangList.get(i).getLang() + ", ";
		}else{
			lang += gpLangList.get(i).getLang();
		}
	}

	ArrayList<String> includeName = new ArrayList<>();
	ArrayList<String> notcludeName = new ArrayList<>();
	for(int i = 0; i < gpPriceList.size(); i++){
		if(gpPriceList.get(i).getGpInc().equals("INCLUDE")){
			includeName.add(gpPriceList.get(i).getGpCategory());
		}else{
			notcludeName.add(gpPriceList.get(i).getGpCategory());
		}
	}
	
	
	ArrayList<Integer> gpDetailCtn = new ArrayList<>();
	for(int i = 0; i < gpObj.getGpTday(); i++){
		int ctn = 0;
		for(int j = 0; j < gpDetailList.size(); j++){
			if(gpDetailList.get(j).getGpDay() == (i + 1)){
				ctn++;	
			}
		}
		gpDetailCtn.add(ctn);
	}
	
	
	Double topReviewAvg = (Double) gpReviewCnt.get("reviewAvg");

	int topReviewStar = Integer.parseInt((topReviewAvg + "").split("\\.")[0]);
	int topReviewHalfStar;
	if((topReviewAvg + "").split("\\.")[1].equals("5")){
		topReviewHalfStar = 5;
	}else{
		topReviewHalfStar = 0;
	}
 
%>

<!-- datepicker 관련 js -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- slider 관련 js -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<!-- kakao map api link -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2"></script>
<!-- 팝업 관련 css -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<meta property="og:image" content="http://127.0.0.1:8001/edr/uploadFiles/<%=gpAttachmentList.get(0).getChangeName() %>" />
<style>
.ui-widget.ui-widget-content {width: 262px;}
</style>
<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container programDetail">
	<div class="inner_ct clearfix">
    <!-- 슬라이더 start -->
		<div class="visual_wrap">
      <ul class="visual_ctn">
      <% for(int i = 0; i < gpAttachmentList.size(); i++){ %>
        <li class="visual_list"><img src="/edr/uploadFiles/<%=gpAttachmentList.get(i).getChangeName() %>" alt="프로그램 사진"></li>
      <%} %>
      </ul>
    </div>
    <!-- 슬라이더 end -->
    <!-- sticky menu start -->
    <div class="sticky_menu_wrap">
      <ul class="sticky_menu_ctn clearfix">
        <li class="sticky_menu_list"><a href="javascript: void(0);">프로그램 소개</a></li>
        <li class="sticky_menu_list"><a href="javascript: void(0);">필수 안내 사항</a></li>
        <li class="sticky_menu_list"><a href="javascript: void(0);">프로그램 코스</a></li>
        <li class="sticky_menu_list"><a href="javascript: void(0);">특이사항</a></li>
        <li class="sticky_menu_list"><a href="javascript: void(0);">후기</a></li>
      </ul>
    </div>
    <!-- sticky menu end -->
    <div class="main_ctn_wrap">
      <div class="program_ctn_wrap">
        <div class="program_ctn">
          <div class="top_cnt clearfix">
            <div class="grade_cnt clearfix">
            <% if(topReviewAvg > 0){ %>
            	<span class="grade_tit">평점</span>
            <% } %>
            	<span class="star_wrap">
           <%
          	for(int i = 0; i < topReviewStar; i++){
            %>
          		<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
            <% 
          	}
              
            if(topReviewHalfStar == 5){
            %>
              <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
            	
            <% }%>
            </div>
            <div class="review_cnt">후기 <%= gpReviewCnt.get("reviewCount") %>개</div>
          </div>
          <div class="program_list_wrap program_list_wrap_1">
            <div class="program_list">
              <div class="location_cnt"><%= localName %></div>
              <div class="program_tit_cnt"><%= gpObj.getGpName() %></div>
              <div class="desc_cnt"><%= gpObj.getGpDescr() %></div>
              <div class="program_cnt">
                <p class="cnt_tit">총 프로그램 기간</p>
                <p class="cnt"><%= totalDay %>일</p>
              </div>
              <div class="program_cnt">
                <p class="cnt_tit">참가 가능 인원</p>
                <p class="cnt">최소 <%= gpObj.getGpMin() %>명 ~ 최대 <%= gpObj.getGpMax() %>명</p>
              </div>
              <div class="program_cnt">
                <p class="cnt_tit">진행 가능 언어</p>
                <p class="cnt"><%= lang %></p>
              </div>
            </div>
          </div>
          <div class="program_list_wrap program_list_wrap_2">
            <div class="program_list">
              <div class="sub_tit">필수 안내 사항</div>
              <div class="program_cnt">
                <p class="cnt_tit">포함사항</p>
                <p class="cnt">
               	<%
					String includeStr = "";
					for(int i = 0; i < includeName.size(); i++){
						if(i == includeName.size() - 1){
							includeStr += includeName.get(i);
						}else{
							includeStr += includeName.get(i) + ", ";
						}
					}
				%>
				<%= includeStr %>
                </p>
              </div>
              <div class="program_cnt">
                <p class="cnt_tit">불포함사항</p>
                <p class="cnt">
                <%
					String notcludeStr = "";
					for(int i = 0; i < notcludeName.size(); i++){
						if(i == notcludeName.size() - 1){
							notcludeStr += notcludeName.get(i);
						}else{
							notcludeStr += notcludeName.get(i) + ", ";
						}
					}
				%>
				<%= notcludeStr %>
                </p>
              </div>
              <div class="program_cnt">
                <p class="cnt_tit">만나는 시간</p>
                <p class="cnt"><%= gpObj.getGpMeetTime() %></p>
              </div>
              <div class="program_cnt">
                <p class="cnt_tit">만나는 장소</p>
                <p class="cnt"><%= gpObj.getGpMeetArea() %></p>
                <div id="programDetail_map" class="meet_map"></div>
              </div>
            </div>
          </div>
		<div class="program_list_wrap program_list_wrap_3">
			<div class="program_list">
			<% 
			int gpDetailFor = 0;
			
			for(int i = 0; i < gpObj.getGpTday(); i++){ %>
			<div class="detail_course_wrap detail_course_wrap_1 course_wrap">
				<div class="detail_course_tit">
					<p class="tit"><%= i + 1 %>Day</p>
					<!-- <p class="total_time">총 ?시간 소요</p> -->
				</div>
				<% for(int j = 0; j < gpDetailCtn.get(i); j++){ %>
				<div class="detail_course_ctn">
					<p class="ctn_tit">일정 <%= j+1 %> : 약 <%= gpDetailList.get(gpDetailFor).getGpTime() %>시간 소요</p>
					<p class="place_tit"><%= gpDetailList.get(gpDetailFor).getGpLocation() %></p>
					<img src="/edr/uploadFiles/<%=gpAttachmentList.get(gpDetailFor).getChangeName() %>" alt="" class="place_img">
					<p class="desc"><%= gpDetailList.get(gpDetailFor).getGpCnt() %></p>
				</div>
				<% 
					gpDetailFor++;
				} %>
			</div>
			<% }%>

			</div>
		</div>
          <div class="program_list_wrap program_list_wrap_4">
            <div class="program_list">
              <div class="sub_tit">특이사항</div>
              <div class="program_cnt">
                <p class="cnt"><%= gpObj.getGpNotice() %></p>
              </div>
            </div>
          </div>
          <div class="program_list_wrap program_list_wrap_5">
            <div class="program_list">
              <div class="sub_tit">후기</div>
                <div class="review_wrap">
                  <div id="reviewCtnWrap">
                    <!-- 후기 영역 -->
                  </div>
                  <!-- 버튼 시작  -->
                  <div class="btn_wrap">
                    <button type="submit" id="reviewMoreBtn" class="btn_com btn_white">후기 더보기</button>
                  </div>
                  <!-- 버튼 끝  -->
                </div>
            </div>
          </div>
        </div> 
      </div>
      <!-- 폼태그 action 추가 -->
      <form class="impo_ctn_wrap" action="<%= request.getContextPath() %>/payment.common" method="post">
        <div class="impo_ctn">
          <div class="top_ctn clearfix">
            <p class="base_price_cnt">기본금 <%= gpObj.getGpCost() %>원</p>
            <a href="javascript:;"  class="share_cnt" id="kakao-link-btn"><img src="<%=request.getContextPath()%>/imgs/share_icon.png" alt="카카오톡으로 공유하기"></a>
          </div>
          <div class="select_ctn">
            <div class="date_ctn">
              <div class="tit">날짜</div>
              <!-- 데이트피커 -->
              <input type="text" name="payDate" id="startDateSel" class="date_pick" placeholder="날짜를 선택해주세요." autocomplete="off">
              <input type="hidden" name="productNo" value="" id="productNo">
            </div>
            <div class="person_ctn">
              <div class="tit">인원</div>
              <!-- name 추가  -->
              <select name="recPerson" id="recPerson" class="person_pick">
              	<!-- value 추가  -->
              	<%for(int i = gpObj.getGpMin(); i <= gpObj.getGpMax(); i++) {%>
	                <option value="<%= i %>"><%= i %>명</option>
                <%} %>
              </select>
            </div>
          </div>
          <div class="detail_price_ctn">
            <div class="person_price clearfix">
              <span id="per"></span>
              <span id="perCostPrice"></span>
            </div>
            <div class="service_price clearfix">
              <span>서비스수수료</span>
              <span id="servicePrice"></span>
            </div>
            <div class="total_price clearfix">
              <span>총 합계</span>
              <span id="totalPrice"></span>
            </div>
          </div>
          <div class="btn_ctn clearfix">
            <%-- <button class="wish_btn"><img src="<%=request.getContextPath()%>/imgs/wish_icon.png" alt="찜하기"></button> --%>
            <!-- 버튼 액션추가 -->
            <% if( loginUser != null ){ %>
            <button type="submit" class="buy_btn" id="buyBtn">구매하기</button>
            <% }else{ %>
            <button class="buy_btn" onclick="return paymentBtn();">구매하기</button>
            <% } %>
          </div>
          <div class="guide_ctn clearfix">
            <a href="javascript: void(0);" class="guide_name"><%= request.getAttribute("guideName") %> 가이드</a>
            <!-- <a href="../popup/ex_pop.jsp" class="send_msg">메세지 보내기</a> -->
          </div>
        </div>
      </form>
    </div>
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<!-- datepicker 관련 js -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- slider 관련 js -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- 팝업 관련 js -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>


<script>



  $(function(){

	(function(){
		  //<![CDATA[
		    // // 사용할 앱의 JavaScript 키를 설정해 주세요.
		    Kakao.init('e099dc7b4bc90cc56294bfce99d4f5b2');
		    // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
		    Kakao.Link.createDefaultButton({
		      container: '#kakao-link-btn',
		      objectType: 'feed',
		      content: {
		        title: '<%= gpObj.getGpName() %>',
		        imageUrl: 'http://127.0.0.1:8001/edr/uploadFiles/<%=gpAttachmentList.get(0).getChangeName() %>',
		        link: {
		          webUrl: document.location.href,
		          mobileWebUrl: document.location.href
		        }
		      },
		      buttons: [
		        {
		          title: 'Open!',
		          link: {
		            mobileWebUrl: document.location.href,
		            webUrl: document.location.href
		          }
		        }  
		      ]
		    });
		  //]]>
	})();
	  
	var dateDisabled = new Array(<%= gpProductList.size() %>);
	// let tdayDisabled = Array.from(Array(dateDisabled.length), () => Array());
	var tdayNum = <%= gpObj.getGpTday() %>;
	
	<%for(int i = 0; i < gpProductList.size(); i++){%>
		dateDisabled[<%= i %>] = '<%= ((Product) gpProductList.get(i)).getPdate()%>';
	<%}%>
	 
	
	/* 구매 가능한 것 없을 시 구매 버튼 막기 */
	$("#buyBtn").on("click", function(){
		<%if(gpProductList.size() == 0){%>
			alert("구매 가능한 상품이 없습니다.");
			return false;
		<%}%>
		
		if($("#startDateSel").val() == ""){
			alert("시작 날짜를 선택해주세요.");
			return false;
		}
	});
	
    /* 팝업 예시 */
   /*  $('.send_msg').magnificPopup({ 
      type: 'ajax' 
    }); */

	  /* 슬라이더 */
    $('.visual_ctn').slick();
    
    /* 데이트피커 */
    $(".date_pick").datepicker({
      nextText: '다음 달',
      prevText: '이전 달',
      dateFormat: "yy-mm-dd",
      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
      beforeShowDay: function(date) {
		    var m = date.getMonth()
		    var d = date.getDate()
		    var y = date.getFullYear();
		    for (i = 0; i < dateDisabled.length; i++) {
		    	var temp = (y + '-' + (m+1) + '-' + d);
		    	 var temp;
	    	      if((m+1) < 10 && d < 10){
	    	    	  temp = (y + '-0' + (m+1) + '-0' + d);
	    	      }else if((m+1) < 10){
	    	    	  temp = (y + '-0' + (m+1) + '-' + d);
	    	      }else if(d < 10){
	    	    	  temp = (y + '-' + (m+1) + '-0' + d);
	    	      }else{
	    	    	  temp = (y + '-' + (m+1) + '-' + d);
	    	      }
	    	      
		    	if(temp == dateDisabled[i]) {
		            return [true];
		        }

		    }
		    return [false];
		}

    });

    $(".date_pick").on("change", function(){
    	console.log($(this).val());
    	<%for(int i = 0; i < gpProductList.size(); i++){%>
    		if($(this).val() == '<%= ((Product) gpProductList.get(i)).getPdate()%>'){
    			var selPno = '<%= ((Product) gpProductList.get(i)).getPno()%>'
    			$("#productNo").val(selPno);
    			return false;
    		}
    	<%}%>
    });
    
    function priceCalc(per, base){

    	var perCost = base * (per * 0.1);
    	var serviceCha = ((perCost + base) * 0.1);
    	
    	$("#per").text(per + "명");
    	$("#perCostPrice").text(Math.floor(perCost) + "원");
    	$("#servicePrice").text(Math.floor(serviceCha) + "원");
    	$("#totalPrice").text(Math.floor(base + perCost + serviceCha) + "원");
    	
    }
    
    priceCalc(<%= gpObj.getGpMin() %>, <%= gpObj.getGpCost() %>);
    /* 가격 측정 */
    $("#recPerson").on("change", function(){
    	var per = $(this).val();
    	
    	priceCalc(per, <%= gpObj.getGpCost() %>);
    	
    });
    
    /* sticky menu style */
    if($(document).scrollTop() > $(".program_ctn_wrap").offset().top){
      	$(".sticky_menu_wrap").addClass("fix");
    }

    /* sticky menu 위치 */
    (function(){
      $(".sticky_menu_ctn").find(".sticky_menu_list").on("click", function(){
        var idx = $(this).index();
        var top = $(".program_list_wrap_"+(idx+1)).offset().top - 66;
        console.log(top);

        $('html, body').animate({
          scrollTop : top
        }, 300);

      });
    })();

    $(window).scroll(function(){
      if($(document).scrollTop() > $(".program_ctn_wrap").offset().top - 67){
        // 스티키 메뉴
      	$(".sticky_menu_wrap").addClass("fix");
        // 왼쪽 프로그램 상세 정보
      	$(".impo_ctn_wrap").addClass("fix");
      	$(".impo_ctn_wrap").css({
      		left : $(".visual_wrap").offset().left + $(".visual_wrap").width() - $(".impo_ctn_wrap").width()
      	});
      }else{
        // 스티키 메뉴
       	$(".sticky_menu_wrap").removeClass("fix");
          
        // 왼쪽 프로그램 상세 정보
        $(".impo_ctn_wrap").removeClass("fix");
        $(".impo_ctn_wrap").css({
          left : $(".visual_wrap").width() - $(".impo_ctn_wrap").width()
        });
      }
    });

    (function(){
      /* kakao map api */
      var container = document.getElementById('programDetail_map'); //지도를 담을 영역의 DOM 레퍼런스
      var options = { //지도를 생성할 때 필요한 기본 옵션
          center: new kakao.maps.LatLng(<%= gpObj.getGpMeetY() %>, <%= gpObj.getGpMeetX() %>), //지도의 중심좌표.
          level: 3 //지도의 레벨(확대, 축소 정도)
        };

      var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴

      var markerPosition  = new kakao.maps.LatLng(<%= gpObj.getGpMeetY() %>, <%= gpObj.getGpMeetX() %>); 
         var marker = new kakao.maps.Marker({
        	    position: markerPosition
        	});

         marker.setMap(map);
         

    })();

    (function(){
      	var reviewCount = <%= mainReviewCnt.size() %>; // 총 후기 개수
        var reviewClick = 0; // 후기 더보기 클릭 회수
		var revertReviewCount = 0;
   
    	let reviewCntJava = Array.from(Array(<%=mainReviewCnt.size()%>), () => Array());

    	<%for(int i=0; i < mainReviewCnt.size(); i++){ %>
    	
			reviewCntJava[<%=i%>][0] = {"reviewNo" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getReviewNo()%>'};
			reviewCntJava[<%=i%>][1] = {"guideRating" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getGuideRating()%>'};
			reviewCntJava[<%=i%>][2] = {"programRating" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getProgramRating()%>'};
			reviewCntJava[<%=i%>][3] = {"reviewCnt" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getReviewCnt()%>'};
			reviewCntJava[<%=i%>][4] = {"replyYn" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getReplyYn()%>'};
			reviewCntJava[<%=i%>][5] = {"orderDetailNo" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getOrderDetailNo()%>'};
			reviewCntJava[<%=i%>][6] = {"mno" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getMno()%>'};
			reviewCntJava[<%=i%>][7] = {"gpNo" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getGpNo()%>'};
			reviewCntJava[<%=i%>][8] = {"review_yn" : '<%=((Review) mainReviewCnt.get(i).get("reviewCnt")).getReviewYn() %>'};
			reviewCntJava[<%=i%>][9] = {"name" : '<%=mainReviewCnt.get(i).get("reviewName") %>'};
    	
		<% 

    	}%> 	
    	console.log(reviewCntJava);

        $.ajax({
          url: "<%= request.getContextPath() %>/views/user/sub/common/review_ctn.jsp",
          success:function(result) {
            if(reviewCount == 0){
              $("#reviewCtnWrap").append("<p class='empty_review'>등록된 후기가 없습니다.</p>");
            
              // 후기 버튼 삭제
              $("#reviewMoreBtn").remove();
            }else if(reviewCount <= 2){
              // 나중에는 result 개수 만큼으로 수정
              var temp = reviewCount;
              for(var i = 0; i < temp; i++){
                $("#reviewCtnWrap").append(result);

                $("#reviewCtnWrap").find("#userNameArea").prop("id", "userNameArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][9].name);
                $("#reviewCtnWrap").find("#userCntArea").prop("id", "userCntArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][3].reviewCnt);

                var starHtml = ""; 
                
                var rAvg = reviewCntJava[revertReviewCount][2].programRating;
                var rLeft = rAvg.split(".")[0];
                var rRight = rAvg.split(".")[1];
                
                for (var j = 0; j < rLeft; j++) {
                	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_icon.png' alt=''></i>";
				}
                if(rRight == 5){
                	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_half_icon.png' alt=''></i>";
                }
                $("#reviewCtnWrap").find("#reviewStarArea").prop("id", "reviewStarArea" + revertReviewCount).html(starHtml);
                
                reviewCount--;
                revertReviewCount++;
              }

              $("#reviewMoreBtn").remove();
            }else{
              // 고정으로 2개 출력
              for(var i = 0; i < 2; i++){
                $("#reviewCtnWrap").append(result);
                
                $("#reviewCtnWrap").find("#userNameArea").prop("id", "userNameArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][9].name);
                $("#reviewCtnWrap").find("#userCntArea").prop("id", "userCntArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][3].reviewCnt);
                // console.log(reviewCntJava[revertReviewCount][1].guideRating);
                

                var starHtml = ""; 
                
                var rAvg = reviewCntJava[revertReviewCount][2].programRating;
                var rLeft = rAvg.split(".")[0];
                var rRight = rAvg.split(".")[1];
                
                for (var j = 0; j < rLeft; j++) {
                	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_icon.png' alt=''></i>";
				}
                if(rRight == 5){
                	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_half_icon.png' alt=''></i>";
                }
                $("#reviewCtnWrap").find("#reviewStarArea").prop("id", "reviewStarArea" + revertReviewCount).html(starHtml);
                
                reviewCount--;
                revertReviewCount++;
              }
              //$("#reviewMoreBtn").remove();
            }

            $("#reviewMoreBtn").on("click",function(){
              reviewClick++; // 몇 번째 클릭했는 지
              reviewCount -= 5; // 한 번 클릭 시 출력되는 개수

              if(reviewCount >= 0){
                // 5개씩 더 출력
                for(var i = 0; i < 5; i++){
                  $("#reviewCtnWrap").append(result);
                  
                  $("#reviewCtnWrap").find("#userNameArea").prop("id", "userNameArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][9].name);
                  $("#reviewCtnWrap").find("#userCntArea").prop("id", "userCntArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][3].reviewCnt);

                  var starHtml = ""; 
                  
                  var rAvg = reviewCntJava[revertReviewCount][2].programRating;
                  var rLeft = rAvg.split(".")[0];
                  var rRight = rAvg.split(".")[1];
                  
                  for (var j = 0; j < rLeft; j++) {
                  	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_icon.png' alt=''></i>";
  				}
                  if(rRight == 5){
                  	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_half_icon.png' alt=''></i>";
                  }
                  $("#reviewCtnWrap").find("#reviewStarArea").prop("id", "reviewStarArea" + revertReviewCount).html(starHtml);
                  
                  revertReviewCount++;
                }
              }else if(reviewCount < 0 && reviewCount > -5){
                // 남은 개수 만큼 더 출력, 즉 (5 + reviewCount) 만큼 더 출력
                for(var i = 0; i < 5 + reviewCount; i++){
                  $("#reviewCtnWrap").append(result);
                  
                  $("#reviewCtnWrap").find("#userNameArea").prop("id", "userNameArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][9].name);
                  $("#reviewCtnWrap").find("#userCntArea").prop("id", "userCntArea" + revertReviewCount).text(reviewCntJava[revertReviewCount][3].reviewCnt);

                  var starHtml = ""; 
                  
                  var rAvg = reviewCntJava[revertReviewCount][2].programRating;
                  var rLeft = rAvg.split(".")[0];
                  var rRight = rAvg.split(".")[1];
                  
                  for (var j = 0; j < rLeft; j++) {
                  	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_icon.png' alt=''></i>";
  				}
                  if(rRight == 5){
                  	starHtml += "<i><img src='<%=request.getContextPath()%>/imgs/star_half_icon.png' alt=''></i>";
                  }
                  $("#reviewCtnWrap").find("#reviewStarArea").prop("id", "reviewStarArea" + revertReviewCount).html(starHtml);
                  
                  revertReviewCount++;
                }

                // 후기 버튼 삭제
                $("#reviewMoreBtn").remove();
              }

            });
          }
        });


    })();

  });

  function paymentBtn(){
  	alert("로그인을 해주세요.");
  	return false;
  }
   
</script>
<%@ include file="../../../inc/user/footer.jsp" %>
