<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container guideList">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/guideSubMenu.jsp" %>
		
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">서울</h4>
			<div class="sub_ctn">
          <!-- <h3 class="sub_ctn_tit">서울 신규 가이드</h3> -->
		      <!-- 큰 카드 시작 -->
		      <div class="new_wide_wrap new_guide_wide">
		        <div class="new_wide">
		          <a href="#" class="clearfix">
		            <span class="new_wide_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt="프로그램 이미지"></span>
		            <div class="new_wide_cnt_wrap">
		              <div class="top_cnt clearfix">
		                <div class="grade_cnt clearfix">
		                  <span class="grade_tit">평점</span>
		                  <span class="star_wrap">
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
		                    <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
		                  </span>
		                </div>
		                <div class="review_cnt">후기 50개</div>
		              </div>
		              <div class="guide_name_cnt">송기준 가이드</div>
		              <div class="guide_age_cnt">남성 20대</div>
		              <div class="desc_cnt">얼마나 기쁘며 얼마나 기쁘며마나 아름다우냐? 이것을 얼음 속에서 불러 내는 것이 따뜻한 봄바람이다 인생에 따뜻한 봄바람을 불어 보내는 것은 청춘의 끓는 피다
		                청춘의 피가 뜨거운지라 의 피가 뜨거운지라 인간의 동산에는 사랑의 피가의 피가 뜨거운지라 인간의 동산에는 사피가의 피가 뜨거운지라 인간의 동산에피가의 피가 뜨거운지라 인간의 동산에랑 뜨거운지라 인간의 동산에는 사랑인간의 동산에는 사랑의 풀이</div>
		            </div>
		          </a>
		        </div>
		      </div>
		      <!-- 큰 카드 끝 -->
    <!-- 작은 카드 시작 -->
    <ul class="all_card_wrap new_guide_card_wrap clearfix">
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
        <li class="card_wrap guide_card_wrap new_guide_card">
          <div class="card">
            <a href="#">
              <span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
              <div class="card_cnt_wrap">
                  <div class="location_cnt">서울</div>
                  <div class="guide_name_cnt">최원준 가이드</div>
                  <div class="guide_age_cnt">남성 20대</div>
                  <div class="bot_cnt clearfix">
                    <div class="grade_cnt">
                      <span class="grade_tit">평점</span>
                      <span class="star_wrap">
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
                        <i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
                      </span>
                    </div>
                    <div class="review_cnt">후기 50개</div>
                  </div>
              </div>
            </a>
          </div>
        </li>
    </ul>
    <!-- 작은 카드 끝 -->
      <!-- 페이저 시작 -->
      <div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
          <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
          <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
          <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
        </ul>
      </div>
      <!-- 페이저 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
$(function(){
   // 열리지 않는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
});
</script>
<%@ include file="../../../inc/user/footer.jsp" %>
