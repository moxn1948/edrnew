<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container guideInfo">
	<div class="inner_ct clearfix">
		<div class="layout_1_wrap">
			<!-- 제목 -->
			<h1 class="main_title">가이드 프로필</h1>
			<!-- 내용 시작 -->
			<div class="main_cnt clearfix">
				<div class="main_info_wrap">
					<div class="main_info_ctn">
						<div class="top_cnt clearfix">
							<div class="grade_cnt clearfix">
								<span class="grade_tit">평점</span>
								<span class="star_wrap">
									<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
									<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
									<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
									<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
									<i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
								</span>
							</div>
							<p class="review_cnt">후기 50개</p>
						</div>
						<div class="guide_img_wrap">
							<img src="<%=request.getContextPath()%>/imgs/temp_guide_img.png" alt="" class="guide_img">
						</div>
						<p class="guide_local_ctn">서울</p>
						<p class="guide_name_ctn">최원준 가이드</p>
						<p class="guide_sub_info_ctn">남성 20대</p>
						<!-- 버튼 시작  -->
						<div class="guide_msg_btn_wrap btn_wrap">
							<button type="submit" id="" class="btn_com btn_blue guide_msg_btn">메세지 보내기</button>
						</div>
						<!-- 버튼 끝  -->
					</div>
				</div>
				<div class="sub_info_wrap">
					<div class="sub_info_ctn">
						<div class="sub_info_list guide_pro_wrap">
							<div class="cnt_tit">가이드 소개</div>
							<div class="guide_prop_desc">
									<p>사막이다 보이는 끝까지 찾아다녀도 목숨이 있는 때까지 방황하여도 보이는 것은 거친 모래뿐일 것이다 이상의 꽃이 없으면</p>
									<p>행복스럽고 평화스러운 곳으로 인도하겠다는 커다란 이상을 품었기 때문이다 그러므로 그들은 길지 아니한 목숨을 사는가 싶이 살았으며 그들의 그림자는 천고에 사라지지 않는 것이다 이것은 현저하게 일월과 같은 예가 되려니와 그와 같지 못하다 할지라도 창공에 반짝이는 뭇 별과 같이 산야에 피어나는 군영과 같이 이상은</p>
									<p>가치를 주는 원질이 되는 것이다 그들은 앞이 긴지라 착목한는 곳이 원대하고 그들은 피가 더운지라 실현에 대한 자신과 용기가 있다 그러므로 그들은 이상의 보배를 능히 품으며 그들의 이상은 아름답고</div></p>
							</div>
						<div class="sub_info_list guide_program_wrap">
							<div class="cnt_tit">가이드의 프로그램</div>
							<!-- 작은 카드 시작 -->
							<ul class="all_card_wrap new_program_card_wrap clearfix">
								<li class="card_wrap new_program_card">
									<div class="card">
										<a href="../general_program/programDetail.jsp">
											<span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
											<div class="card_cnt_wrap">
												<div class="top_cnt clearfix">
														<div class="guide_cnt">송기준 가이드의</div>
														<div class="location_cnt">서울</div>
													</div>
													<div class="program_tit_cnt">서울의 야경 투어</div>
													<div class="price_cnt">100,000원</div>
													<div class="time_cnt">1박 2일</div>
													<div class="bot_cnt clearfix">
														<div class="grade_cnt">
															<span class="grade_tit">평점</span>
															<span class="star_wrap">
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
															</span>
														</div>
														<div class="review_cnt">후기 50개</div>
													</div>
											</div>
										</a>
									</div>
								</li>
								<li class="card_wrap new_program_card">
									<div class="card">
										<a href="../general_program/programDetail.jsp">
											<span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
											<div class="card_cnt_wrap">
												<div class="top_cnt clearfix">
														<div class="guide_cnt">송기준 가이드의</div>
														<div class="location_cnt">서울</div>
													</div>
													<div class="program_tit_cnt">서울의 야경 투어</div>
													<div class="price_cnt">100,000원</div>
													<div class="time_cnt">1박 2일</div>
													<div class="bot_cnt clearfix">
														<div class="grade_cnt">
															<span class="grade_tit">평점</span>
															<span class="star_wrap">
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
															</span>
														</div>
														<div class="review_cnt">후기 50개</div>
													</div>
											</div>
										</a>
									</div>
								</li>
								<li class="card_wrap new_program_card">
									<div class="card">
										<a href="../general_program/programDetail.jsp">
											<span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
											<div class="card_cnt_wrap">
												<div class="top_cnt clearfix">
														<div class="guide_cnt">송기준 가이드의</div>
														<div class="location_cnt">서울</div>
													</div>
													<div class="program_tit_cnt">서울의 야경 투어</div>
													<div class="price_cnt">100,000원</div>
													<div class="time_cnt">1박 2일</div>
													<div class="bot_cnt clearfix">
														<div class="grade_cnt">
															<span class="grade_tit">평점</span>
															<span class="star_wrap">
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
															</span>
														</div>
														<div class="review_cnt">후기 50개</div>
													</div>
											</div>
										</a>
									</div>
								</li>
								<li class="card_wrap new_program_card">
									<div class="card">
										<a href="../general_program/programDetail.jsp">
											<span class="card_img"><img src="<%=request.getContextPath()%>/imgs/temp_img.png" alt=""></span>
											<div class="card_cnt_wrap">
												<div class="top_cnt clearfix">
														<div class="guide_cnt">송기준 가이드의</div>
														<div class="location_cnt">서울</div>
													</div>
													<div class="program_tit_cnt">서울의 야경 투어</div>
													<div class="price_cnt">100,000원</div>
													<div class="time_cnt">1박 2일</div>
													<div class="bot_cnt clearfix">
														<div class="grade_cnt">
															<span class="grade_tit">평점</span>
															<span class="star_wrap">
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_icon.png" alt=""></i>
																<i><img src="<%=request.getContextPath()%>/imgs/star_half_icon.png" alt=""></i>
															</span>
														</div>
														<div class="review_cnt">후기 50개</div>
													</div>
											</div>
										</a>
									</div>
								</li>
							</ul>
							<!-- 작은 카드 끝 -->
						</div>
						<div class="sub_info_list guide_review_wrap">
							<div class="cnt_tit">가이드의 후기</div>
							<div class="review_wrap">
								<div id="reviewCtnWrap">
									<!-- 후기 영역 -->

								</div>
								<!-- 버튼 시작  -->
								<div class="btn_wrap">
									<button type="submit" id="reviewMoreBtn" class="btn_com btn_white">후기 더보기</button>
								</div>
								<!-- 버튼 끝  -->
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
$(function(){

	// 후기 시작
	(function(){
			var reviewCount = 10; // 총 후기 개수
			var reviewClick = 0; // 후기 더보기 클릭 회수

			$.ajax({
				url:"../common/review_ctn.jsp",
				success:function(result) {
					if(reviewCount == 0){
						$("#reviewCtnWrap").append("<p class='empty_review'>등록된 후기가 없습니다.</p>");
					
						// 후기 버튼 삭제
						$("#reviewMoreBtn").remove();
					}else if(reviewCount <= 2){
						// 나중에는 result 개수 만큼으로 수정
						for(var i = 0; i < 2; i++){
							$("#reviewCtnWrap").append(result);
							reviewCount--;
						}
					}else{
						// 고정으로 2개 출력
						for(var i = 0; i < 2; i++){
							$("#reviewCtnWrap").append(result);
							reviewCount--;
						}
					}

					$("#reviewMoreBtn").on("click",function(){
						reviewClick++; // 몇 번째 클릭했는 지
						reviewCount -= 5; // 한 번 클릭 시 출력되는 개수

						if(reviewCount >= 0){
							// 5개씩 더 출력
							for(var i = 0; i < 5; i++){
								$("#reviewCtnWrap").append(result);
							}
						}else if(reviewCount < 0 && reviewCount > -5){
							// 남은 개수 만큼 더 출력, 즉 (5 + reviewCount) 만큼 더 출력
							for(var i = 0; i < 5 + reviewCount; i++){
								$("#reviewCtnWrap").append(result);
							}

							// 후기 버튼 삭제
							$("#reviewMoreBtn").remove();
						}

					});
				}
			});
	})();
	// 후기 끝

});
</script>
	
<%@ include file="../../../inc/user/footer.jsp" %>