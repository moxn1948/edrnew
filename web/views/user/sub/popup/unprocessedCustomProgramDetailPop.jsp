<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- kakao map api link -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2"></script>
<style>
	.tabletd{
		font-weight: bold;
		width:100px;
	}
</style>
<div class="pop_wrap" style="width:600px; height:auto;">
	<div class="tableArea">
		<h2 align="center">일정안내</h2>
		<br>
		<table>
			<tr>
				<td class="tabletd">총 금액</td>
				<td>700,000원</td>
			</tr>
			<tr>
				<td class="tabletd">포함 사항</td>
				<td>가이드비, 교통비, 티켓비</td>
			</tr>
			<tr>
				<td class="tabletd">불포함 사항</td>
				<td>식비</td>
			</tr>
			<tr>
				<td class="tabletd">만나는 시간</td>
				<td>2019.10.11 오후 02:00</td>
			</tr>
			<tr>
				<td class="tabletd">만나는 장소</td>
				<td>경복궁앞</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td> </td>
			</tr>
			<tr>
				<td class="tabletd">Day1</td>
				<td>첫번째일정은 이러합니다이러하구요이러합니다</td>
			</tr>
			<tr>
				<td class="tabletd">Day2</td>
				<td>첫번째일정은 이러합니다이러하구요이러합니다</td>
			</tr>
			<tr>
				<td class="tabletd">특이사항</td>
				<td>공지사항입니다공지사항입니다공지사항입니다공지사항입니다공지사항입니다공지사항입니다</td>
			</tr>
			<tr>
				<td class="tabletd">파일</td>
				<td>상세일정.pdf</td>
			</tr>
			
		</table>
		<div class="btn_wrap" align="center" style="margin-top:30px;margin-bottom:30px;">
			<button style="width:150px;height:50px;border: 1px solid darkgray; transition: 0.25s;background-color: #fff;box-shadow: 2px 2px 3px 3px rgba(100, 100, 100, 0.1);">취소하기</button>
			<button style="width:150px;height:50px;border: 0; transition: 0.25s;color: #f7f7f7;background-color: #171f57;box-shadow: 2px 2px 3px 3px rgba(100, 100, 100, 0.1);">메세지보내기</button>
			<button style="width:150px;height:50px;border: 0; transition: 0.25s;color: #f7f7f7;background-color: #171f57;box-shadow: 2px 2px 3px 3px rgba(100, 100, 100, 0.1);">결제하기</button>
		</div>
	</div>
</div>