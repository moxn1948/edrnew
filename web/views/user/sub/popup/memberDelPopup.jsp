<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	int mno = Integer.parseInt(request.getParameter("mno"));
%>
    
<!-- <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/common.css"> -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">
<div class="pop_wrap member_del_pop">
  <div class="member_del_ctn">
  <form action="<%= request.getContextPath() %>/delectMember.me" method="post">
    <div class="tit_wrap">
	<button type="button" onClick="$.magnificPopup.close();" style="float:right; border: 0; transition: 0.25s; color: #000000; background-color: #fff;">X</button>
      <h1 class="pop_tit">계정 삭제</h1>
      <p class="pop_tit_desc">계정을 삭제하시면 회원 기록과 그동안의 여행기록이 모두 삭제 됩니다.</p>
    </div>
    <div class="member_del_cnt">
      <div class="cnt_tit">
        <p>계정을 삭제하시는 이유가 무엇입니까?</p>
        <p>(적어주신 의견으로 변화하는 애드립이 되겠습니다.)</p>
      </div>
      <div class="cnt_radio_wrap">
        <div class="del_reason_wrap">
          <input type="radio" name="delReason" id="delReason1" class="del_reason">
          <label for="delReason1">여행을 잘 떠나지 않아서</label>
        </div>
        <div class="del_reason_wrap">
          <input type="radio" name="delReason" id="delReason2" class="del_reason">
          <label for="delReason2">이용방법이 어려워서</label>
        </div>
        <div class="del_reason_wrap">
          <input type="radio" name="delReason" id="delReason3" class="del_reason">
          <label for="delReason3">개인정보 관련 문제가 걱정되어서</label>
        </div>
        <div class="del_reason_wrap">
          <input type="radio" name="delReason" id="delReason4" class="del_reason">
          <label for="delReason4">상품과 가격이 마음에 들지 않아서</label>
        </div>
        <div class="del_reason_wrap">
          <input type="radio" name="delReason" id="delReason5" class="del_reason">
          <label for="delReason5">기타</label>
          <textarea name="" id="" class="del_reason_txt">

          </textarea>
        </div>
      </div>
      <div class="btn_wrap">
      	<input type="hidden" name="mno" value="<%= mno %>">
        <button type="submit" class="btn_com btn_blue del_btn" onclick="return delMember();">삭제하기</button>
      </div>
    </div>
  </form>
  </div>
</div>
