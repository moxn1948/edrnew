<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.generalProgram.model.vo.*, com.edr.order.model.vo.*, java.util.*"%>
<%
	Gp gp = (Gp)request.getAttribute("gp");
	OrderDetail recPerson = (OrderDetail)request.getAttribute("recPerson");
	ArrayList<GpDetail> gpDetailList = (ArrayList)request.getAttribute("gpDetailList");
%>
<style>
	.tableArea2 td{
		padding:5px;
	}
</style>
<div class="pop_wrap" style="width:500px; height:auto;">
	<button type="button" onClick="$.magnificPopup.close();" style="float:right; border: 0; transition: 0.25s; color: #000000; background-color: #fff;">X</button>
	<!-- 팝업내용 여기에~ -->
	<div align="center" style="height:auto;">
		<br>
		<h2 align="center">상세보기</h2>
		<br>
		
		<%
		ArrayList<GpDetail> day1List = new ArrayList<>();
		ArrayList<GpDetail> day2List = new ArrayList<>();
		ArrayList<GpDetail> day3List = new ArrayList<>();
		ArrayList<GpDetail> day4List = new ArrayList<>();
		ArrayList<GpDetail> day5List = new ArrayList<>();
		ArrayList<GpDetail> day6List = new ArrayList<>();
		ArrayList<GpDetail> day7List = new ArrayList<>();

		for (int i = 0; i < gp.getGpTday(); i++) {

			if (gpDetailList.get(i).getGpDay() == 1) {
				day1List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 2) {
				day2List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 3) {
				day3List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 4) {
				day4List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 5) {
				day5List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 6) {
				day6List.add(gpDetailList.get(i));
			}
			else if (gpDetailList.get(i).getGpDay() == 7) {
				day7List.add(gpDetailList.get(i));
			}
		}
		%>
		<% if(day1List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 1</label>
		<br>
		<br>
			<% for(int j = 0; j < day1List.size(); j++ ){ %>
				<p>일정  <%= day1List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day1List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day1List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
			
		</div>
		<br>
		<% } %>
		<% if(day2List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 2</label>
		<br>
		<br>
			<% for(int j = 0; j < day2List.size(); j++ ){ %>
				<p>일정  <%= day2List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day2List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day2List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<% if(day3List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 3</label>
		<br>
		<br>
			<% for(int j = 0; j < day3List.size(); j++ ){ %>
				<p>일정  <%= day3List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day3List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day3List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<% if(day4List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 4</label>
		<br>
		<br>
			<% for(int j = 0; j < day4List.size(); j++ ){ %>
				<p>일정  <%= day4List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day4List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day4List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<% if(day5List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 5</label>
		<br>
		<br>
			<% for(int j = 0; j < day5List.size(); j++ ){ %>
				<p>일정  <%= day5List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day5List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day5List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<% if(day6List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 6</label>
		<br>
		<br>
			<% for(int j = 0; j < day6List.size(); j++ ){ %>
				<p>일정  <%= day6List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day6List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day6List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<% if(day7List.size() != 0){ %>
		<div align="center" style="border:1px solid lightgray; width:90%;">
		<label>Day 7</label>
		<br>
		<br>
			<% for(int j = 0; j < day7List.size(); j++ ){ %>
				<p>일정  <%= day7List.get(j).getGpDaySeq() %></p>			
				<p>소요시간 : <%= day7List.get(j).getGpTime() %> 시간</p>			
				<p>장소 : <%= day7List.get(j).getGpLocation() %></p>
				<br>			
			<% } %>
		</div>
		<br>
		<% } %>
		<hr width="90%">
		<br>
		<table class="tableArea2">
			<tr>
				<td><b>기본금액</b></td>
				<td><%= gp.getGpCost() %></td>
			</tr>
			<tr>
				<td><b>인원수</b></td>
				<% int personPay = (int)(gp.getGpCost()*0.1*recPerson.getRecPerson()); %>
				<td><%= personPay %></td>
			</tr>
			<tr>
				<td><b>수수료</b></td>
				<% int i = (int)((gp.getGpCost() + (gp.getGpCost()*0.1*recPerson.getRecPerson()))*0.033); %>
				<td><%= i %></td>
			</tr>
			<tr>
				<td><b>총 합계 </b></td>
				<td><%= gp.getGpCost() + personPay + i %></td>
			</tr>
		</table>
		<br>
		<br>
	</div>
</div>