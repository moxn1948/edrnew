<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	int mno = Integer.parseInt(request.getParameter("mno"));
%>
<!-- <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/common.css"> -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">
<div class="pop_wrap refund_pop_wrap">
<button type="button" onClick="$.magnificPopup.close();" style="float:right; border: 0; transition: 0.25s; color: #000000; background-color: #fff;">X</button>
<form action="<%= request.getContextPath() %>/updateMemberBank.me" method="post">
  <div class="refund_pop_ctn">
    <h1 class="pop_tit">환불계좌</h1>
    <div class="box_input">
      <input type="text" name="userName" id="userName" placeholder="본인 명의의 예금주를 입력하세요.">
    </div>
    <div class="box_select">
      <select name="bankName" id="bankName">
        <option value="">== 은행 ==</option>
        <option value="신한은행">신한은행</option>
        <option value="농협">농협</option>
        <option value="우리은행">우리은행</option>
        <option value="국민은행">국민은행</option>
        <option value="기업은행">기업은행</option>
        <option value="하나은행">하나은행</option>
        <option value="대구은행">대구은행</option>
        <option value="sc제일은행">sc제일은행</option>
        <option value="부산은행">부산은행</option>
      </select>
    </div>
    <div class="box_input">
      <input type="text" name="accountNumber" id="accountNumber" placeholder="계좌번호를 '-' 없이 입력하세요.">
      <input type="hidden" name="mno" value="<%= mno %>">
    </div>
    <div class="btn_wrap">
      <button class="btn_com btn_blue register_btn" onclick="return refundSubmit();">등록하기</button>
    </div>
  </div>
</form>
  
</div>