<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.*, com.edr.common.model.vo.Attachment"%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<%
	String state = (String) request.getParameter("state");
	int cpNo = Integer.parseInt(request.getParameter("cpNo"));

	HashMap<String, Object> cpDetail = (HashMap<String, Object>) request.getAttribute("cpDetail"); 

	CpRequest cpRe = (CpRequest) cpDetail.get("cpRe");
	ArrayList<CpLang> cpLa = (ArrayList<CpLang>) cpDetail.get("cpLa");
	
	String lang = "";
	for(int i = 0; i < cpLa.size(); i++){
		if(i != cpLa.size() - 1){
			lang += cpLa.get(i) + ", ";
		}else{
			lang += cpLa.get(i);
		}
	}
	
	String localName = "";
	switch (cpRe.getLocalNo()) {
	case 1: localName = "서울"; break;
	case 2: localName = "경기"; break;
	case 3: localName = "경상"; break;
	case 4: localName = "강원"; break;
	case 5: localName = "전라"; break;
	case 6: localName = "충청"; break;
	case 7: localName = "제주"; break;
	case 8: localName = "부산"; break;
	case 9: localName = "대구"; break;
	case 10: localName = "대전"; break;
	case 11: localName = "광주"; break;
	case 12: localName = "인천"; break;
	case 13: localName = "울산"; break;
	}
	
	switch(state){
	case "WAIT" : state = "배정중"; break; 
	case "GWAIT" : state = "배정완료"; break; 
	case "ENDPAY" : state = "결제완료"; break; 
	}
	
	
	ArrayList<CpGuideDetail> cpGdetail = (ArrayList<CpGuideDetail>) cpDetail.get("cpGdetail");
	CpGuideEst cpGest = (CpGuideEst) cpDetail.get("cpGest");
	String gname = (String) cpDetail.get("gname");
	ArrayList<CpPriceDetatil> cpPdetail = (ArrayList<CpPriceDetatil>) cpDetail.get("cpPdetail");
	Attachment cpAttr = (Attachment) cpDetail.get("cpAttr");
	
	int cpEstNo = 0;
	if(cpGest != null){
		cpEstNo = cpGest.getEstNo();
	}
	
	int include = 0;
	int notclude = 0;
	if(cpPdetail != null){
		
		for(int i = 0; i < cpPdetail.size(); i++){
			if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
				include++;
			}else{
				notclude++;
			}
		}

	}
%>


<% if(loginUser != null){ %>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
<style>
table{font-size: 16px;}
table tr{vertical-align: top;}
table td div:last-child{padding-bottom: 20px;}
.customDetailArea{padding-bottom : 20px;}
</style>
<main class="container">
	<div class="inner_ct clearfix">
	<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
	<!-- 서브메뉴 : 변경해야함 -->
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit">맞춤 프로그램 관리</h4>
		<div class="sub_ctn">
			<!-- table 시작 -->
			<div class="customDetailArea">
				<h1 align="center" class="request" style="padding:60px 0;">요청사항</h1>
				<table>
					<tr>
						<td class="customTd">신청일시</td>
						<td><%= cpRe.getCpDate() %></td>
					</tr>
					<tr>
						<td class="customTd">배정가이드</td>
						<td>
						<%
						String guideName = "";
						if(state.equals("배정중")){
							guideName = "미배정";
						}else{
							guideName = gname;
						}
						%>
						<%= guideName %>
						</td>
						
					</tr>
					<tr>
						<td class="customTd">상태</td>
						<td><%=state %></td>
					</tr>
				</table>
				<hr width="95%">
				<br>
				<table>
					<tr>
						<td class="customTd">지역</td>
						<td><%=localName %></td>
					</tr>
					<tr>
						<td class="customTd">팀 인원 수</td>
						<td><%= cpRe.getCpPerson() %>명</td>
					</tr>
					<tr>
						<td class="customTd">시작 날짜</td>
						<td><%= cpRe.getCpSdate() %></td>
					</tr>
					<tr>
						<td class="customTd">총 기간</td>
						<td><%= cpDetail.get("tday")%>일</td>
					</tr>
					<tr>
						<td class="customTd">최대 가능 금액</td>
						<td><%= cpRe.getCpCost() %>원</td>
					</tr>
					<tr>
						<td class="customTd">프로그램 진행 언어</td>
						<td><%= lang %></td>
					</tr>
					<tr>
						<td class="customTd">가이드 성별</td>
						<td>
						<%
						String gender = "";
						if(cpRe.getCpGender().equals("M")){
							gender = "남성";
						}else{
							gender = "여성";
						}
						%>
						<%= gender %>
						</td>
					</tr>
					<tr>
						<td class="customTd">가이드 연령대</td>
						<td>
						<%
						String age = "";
						if(cpRe.getCpAge() == 50){
							age = "50대 이상";
						}else{
							age = cpRe.getCpAge() + "대";
						}
						%>
						<%= age %>
						</td>
					</tr>
					<tr>
						<td class="customTd">제목</td>
						<td><%= cpRe.getCpTitle() %></td>
					</tr>
					<tr>
						<td class="customTd">내용</td>
						<td><%= cpRe.getCpCnt() %></td>
					</tr>
				</table>
			</div>
			
			<% if(!state.equals("배정중")){%>
			<div class="customDetailArea" style="margin-top: 20px;">
				<h1 align="center" class="request" style="padding:60px 0;">답변사항</h1>
				<table>
					<tr>
						<td class="customTd">총금액</td>
						<td><%= cpGest.getTotalCost() %>원</td>
					</tr>
					<tr>
						<td class="customTd">포함사항</td>
						<td>
						<% 
						String includeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
							%>
							<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
							<%
							}
						}%>
						<%= includeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">불포함사항</td>
						<td>
						
						<% 
						String notcludeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("NOTCLUDE")){
								%>
								<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
								<%
							}
						}%>
						<%= notcludeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">만나는 시간</td>
						<td><%= cpGest.getMeetTime() %></td>
					</tr>
					<tr>
						<td class="customTd">만나는 장소</td>
						<td><%= cpGest.getMeetArea() %></td>
					</tr>
					<%for(int i = 0; i < cpGdetail.size(); i ++) {%>
					<tr>
						<td class="customTd"><%= ((CpGuideDetail) cpGdetail.get(i)).getCpDay() %> DAY</td>
						<td><%= ((CpGuideDetail) cpGdetail.get(i)).getCpCnt() %></td>
					</tr>
					<%} %>
					<tr>
						<td class="customTd">특이사항</td>
						<td><%= cpGest.getUniqueness() %></td>
					</tr>
					<tr>
						<td class="customTd">첨부파일</td>
						<td><a href="<%=request.getContextPath()%>/myCpAttaDownload.cp?estNo=<%= cpEstNo %>"><%= cpAttr.getOriginName() %></a></td>
					</tr>
				</table>
			</div>
			<% }%>
			
			<% if(state.equals("배정완료")){%>
			<div class="btn_wrap">
				<a class="btn_com btn_white" style="display: inline-block;" href="<%= request.getContextPath() %>/updateMyCpBack.cp?estNo=<%= cpEstNo %>&cpNo=<%= cpNo %>&mno=<%= loginUser.getMno() %>">거절하기</a>
				<a class="btn_com btn_blue" style="display: inline-block;" href="<%= request.getContextPath() %>/customPayment.common?cpEstNo=<%= cpEstNo %>&cpNo=<%= cpNo %>&mno=<%= loginUser.getMno() %>">결제하기</a>
			</div>
			<%} %>
			
			<!-- table 끝 -->
		</div>
	</div>
	<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

	<!-- 팝업 관련 js -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

<script>
$(function(){
	// 열리는 메뉴
	$(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
 	$(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(0).addClass("on");
 		
	$('.guideProgramDetail').magnificPopup({ 
	     type: 'ajax' 
	});
});


</script>
<%@ include file="../../../inc/user/footer.jsp"%>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>
