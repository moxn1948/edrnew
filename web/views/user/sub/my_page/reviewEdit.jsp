<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="../../../../css/user/hyeonjuStyle.css">

 <!-- 컨텐츠가 들어가는 부분 start -->
 <main class="container">
   <div class="inner_ct clearfix">
     <div class="layout_2_wrap"> 
       <!-- 제목 -->
       <!-- 내용 -->
       <div class="main_cnt">
       <h1 class="main_title">후기 수정</h1>
         <div class="main_cnt_list clearfix">
           <h4 class="title">가이드 평점</h4>
           <div class="box_input rate">
           <select name="rating" class="rating">
            <option value=""  id="choose"selected disabled hidden>평점을 선택하세요</option>
            
           	<option value="5">5</option>
           	<option value="4">4</option>
           	<option value="3">3</option>
           	<option value="2">2</option>
           	<option value="1">1</option>
          
           </select>
           </div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">프로그램 평점</h4>
           <div class="box_input rate">
           <select name="rating" class="rating">
            <option value="" selected disabled hidden>평점을 선택하세요</option>
           	<option value="5">5</option>
           	<option value="4">4</option>
           	<option value="3">3</option>
           	<option value="2">2</option>
           	<option value="1">1</option>
        
           </select>
           </div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id="subject" placeholder="제목을 입력하세요"></div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">내용</h4>
           <div class="box_input"><input type="text" name="" id="contents" placeholder="내용을 입력해 주세요"></div>
         </div>
         
			<!-- 라디오 끝 -->
         </div>
         <!-- 버튼 시작 -->
         <div class="btn_wrap">
           <button type="button" class="btn_com btn_white">취소하기</button>
           <button type="submit" class="btn_com btn_blue">수정하기</button>
         </div>
         <!-- 버튼 끝 -->
       </div>
     </div>
   </div>
 </main>
 <!-- 컨텐츠가 들어가는 부분 end -->
 
<%@ include file="../../../inc/user/footer.jsp" %>
