<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.*, com.edr.common.model.vo.Attachment"%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">
<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>

<% if(loginUser != null){ %>

 <%

	HashMap<String, Object> cpDetail = (HashMap<String, Object>) request.getAttribute("cpDetail"); 

	CpRequest cpRe = (CpRequest) cpDetail.get("cpRe");
	ArrayList<CpGuideDetail> cpGdetail = (ArrayList<CpGuideDetail>) cpDetail.get("cpGdetail");
	CpGuideEst cpGest = (CpGuideEst) cpDetail.get("cpGest");
	String gname = (String) cpDetail.get("gname");
	ArrayList<CpPriceDetatil> cpPdetail = (ArrayList<CpPriceDetatil>) cpDetail.get("cpPdetail");
	Attachment cpAttr = (Attachment) cpDetail.get("cpAttr");
	
	int cpEstNo = 0;
	if(cpGest != null){
		cpEstNo = cpGest.getEstNo();
	}
	
	int include = 0;
	int notclude = 0;
	
	for(int i = 0; i < cpPdetail.size(); i++){
		if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
			include++;
		}else{
			notclude++;
		}
	}

%>
<style>
table{font-size: 16px;}
table tr{vertical-align: top;}
table td div:last-child:not(:first-child){padding-bottom: 20px;}
.customDetailArea{padding-bottom : 20px;}
.tableArea td {padding: 30px 10px;}
.tableArea tr:last-child td{padding-bottom: 50px;}
</style>
<main class="container">
	<div class="inner_ct clearfix">
	<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
	<!-- 서브메뉴 : 변경해야함 -->
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit">결제된 맞춤 프로그램</h4>
		<div class="sub_ctn">
		<div class="tableArea">
		<h1 align="center" class="customCompleteTitle" style="padding:60px 0;">일정 안내</h1>
			<table>
				<tr>
					<td class="customTd">총 금액</td>
					<td><%= cpGest.getTotalCost() %>원</td>
				</tr>
				<tr>
					<td class="customTd">포함 사항</td>
					<td>
					<% 
					String includeStr = "";
					for(int i = 0; i < cpPdetail.size(); i++){
						if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
						%>
						<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
						<%
						}
					}%>
					<%= includeStr %>
					</td>
				</tr>
				<tr>
					<td class="customTd">불포함 사항</td>
					<td>
					
					<% 
					String notcludeStr = "";
					for(int i = 0; i < cpPdetail.size(); i++){
						if(cpPdetail.get(i).getCpInc().equals("NOTCLUDE")){
							%>
							<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
							<%
						}
					}%>
					<%= notcludeStr %>
					</td>
				</tr>
				<tr>
					<td class="customTd">만나는 시간</td>
					<td><%= cpRe.getCpSdate() %> / <%= cpGest.getMeetTime() %></td>
				</tr>
				<tr>
					<td class="customTd">만나는 장소</td>
					<td><%= cpGest.getMeetArea() %></td>
				</tr>
				<%for(int i = 0; i < cpGdetail.size(); i ++) {%>
				<tr>
					<td class="customTd"><%= ((CpGuideDetail) cpGdetail.get(i)).getCpDay() %> DAY</td>
					<td><%= ((CpGuideDetail) cpGdetail.get(i)).getCpCnt() %></td>
				</tr>
				<%} %>
				<tr>
					<td class="customTd">특이사항</td>
					<td><%= cpGest.getUniqueness() %></td>
				</tr>
				<tr>
					<td class="customTd">첨부파일</td>
					<td><a href="<%=request.getContextPath()%>/myCpAttaDownload.cp?estNo=<%= cpEstNo %>"><%= cpAttr.getOriginName() %></a></td>
				</tr>
			</table>
		</div><!-- table area -->
		</div><!-- 내용영역  끝  -->
		</div>
	</div>
	<!-- 오른쪽 컨텐츠 부분 끝-->
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<script>
	$(function(){
	  		// 열리지 않는 메뉴
  			// $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
  
 			// 열리는 메뉴
  			$(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
  			$(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
	});
</script>
<%@ include file="../../../inc/user/footer.jsp"%>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>