<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.edr.member.model.vo.Member"%>
<% 
	String msg = (String)request.getAttribute("msg");
%>


<%@ include file="../../../inc/user/subHeader.jsp"%>

<!-- 팝업관련css -->
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/common/magnific-popup.css">

<link rel="stylesheet" type="text/css"
	href="../../../../css/user/hyeonjuStyle.css">

<style>
	
	
</style>



<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit">프로필 관리</h4>
		<div class="sub_ctn">
			<!-- 영역분리 시작 -->
			<div class="layout_3_wrap">
				<!-- 내용 -->


				<div class="main_cnt_list clearfix pwd">
				
			
				<form class="newPwd" action="<%= request.getContextPath()%>/pwdChange.me" method="post">
					<div class="passArea">
						<table id="tableArea" align="center">
						
							<tr>
								<td><label>새 비밀번호 : &nbsp;&nbsp; </label></td>
								<td><input type="password" name="userPwd" class="pwd1"></td>
							</tr>
							<tr>
								<td><label>비밀번호 확인 : &nbsp;&nbsp; </label></td>
								<td><input type="password" name="userPwd2" class="pwd2"></td>
							</tr>
							<tr>
								<td colspan="2"><button class="btnStyle" type="submit">확인</button></td>
							<td>
							<input type="hidden" name="userEmail" value="<%=loginUser.getEmail() %>">
							</td>
							</tr>
							
							
						</table>
					</div>
					
				</form>	
					
				</div>
 
			</div>
			<!-- 영역분리 끝 -->

		</div>
	</div>
	<!-- 오른쪽 컨텐츠 부분 끝-->
</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

<script>
	$(function() {
		// 열리지 않는 메뉴
		$(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");

		// 열리는 메뉴
		//$(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
		//$(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
	});

	$(function() {
		/* 팝업 예시 */
		$('.editProfile').magnificPopup({
			type : 'ajax'
		});
	});
	
	$(function () {
		var msg="<%=msg%>";
		console.log(msg);		
		if(msg!= "null"){
			alert(msg);
		
			 	
		}
	})
	 
	
	$(function () {
		$(".btnStyle").click(function () {
			var pwd1=$(".pwd1").val();
			var pwd2=$(".pwd2").val();
			
			if(pwd1 == pwd2){
				alert("비밀번호가 변경되었습니다. 다시  로그인해 주세요");
				return true;
			}else{
				alert("비밀번호가 일치하지 않습니다.");
				return false;
			}
			
			
			
		})
	})
	
	
	
	
	

	
</script>


<%@ include file="../../../inc/user/footer.jsp"%>
