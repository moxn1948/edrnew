<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.CpRequest, com.edr.common.model.vo.PageInfo"%>
	

<%@ include file="../../../inc/user/subHeader.jsp"%>

<% if(loginUser != null){ %>

<%
	ArrayList<HashMap<String, Object>> myCpList = (ArrayList<HashMap<String, Object>>) request.getAttribute("myCpList");

	// 프로그램 목록 보기
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int limit = pi.getLimit();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
%>
<style>
.container .tbl_wrap .tbl tr td{padding: 0;}
.container .tbl_wrap .tbl tr td a{padding: 20px 0;}
</style>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/hyeonjuStyle.css">
<main class="container">
		<div class="inner_ct clearfix">
		<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
		<!-- 서브메뉴 : 변경해야함 -->
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">진행중인 맞춤 프로그램</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="*">
							<col width="20%">
							<col width="15%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>제목</th>
							<th>신청 일시</th>
							<th>상태</th>
						</tr>
						<% for(int i = 0; i < myCpList.size(); i++) {%>
						<tr class="tbl_cnt">
							<td><a href="<%= request.getContextPath()%>/selectMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>&state=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpState()%>"><%= myCpList.get(i).get("idxnum") %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>&state=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpState()%>"><%= ((CpRequest) myCpList.get(i).get("cpList")).getCpTitle() %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>&state=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpState()%>"><%= ((CpRequest) myCpList.get(i).get("cpList")).getCpDate() %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>&state=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpState()%>"><span class="squ_tbl">
							<% 
							String stateStr = "";
							switch(((CpRequest) myCpList.get(i).get("cpList")).getCpState()){
							case "WAIT" : stateStr = "배정중"; break; 
							case "GWAIT" : stateStr = "배정완료"; break; 
							case "ENDPAY" : stateStr = "결제완료"; break; 
							}
							%>
							<%= stateStr %>
							</span></a></td>
						</tr>
						<% } %>

					</table>
				</div>
				<!-- table 끝 -->    
				     <!-- 페이저 시작 -->
			      <div class="pager_wrap">
			         <ul class="pager_cnt clearfix add">
			            <% if(currentPage <= 1)  { %>
			
			
			            <li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>
			
			
			            <%} else { %>
			            <li class="pager_com pager_arr prev"><a
			               href="<%= request.getContextPath()%>/selectMyCpList.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=currentPage - 1%>">&#x003C;</a></li>
			
			            <%} %>
			            <% for(int p = startPage; p <= endPage; p++)  {
			               if(p == currentPage) {
			            %>
			            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
			            <%} else {%>
			            <li class="pager_com pager_num"><a href="<%= request.getContextPath()%>/selectMyCpList.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=p%>"><%=p %></a></li>
			            <%} %>
			            <% } %>
			
			
			            <% if(currentPage >= maxPage) { %>
			            <li class="pager_com pager_arr next"><a
			               href="javascript: void(0);">&#x003E;</a></li>
			            <%}else { %>
			            <li class="pager_com pager_arr next"><a
			               href="<%= request.getContextPath()%>/selectMyCpList.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=currentPage + 1%>">&#x003E;</a></li>
			
			            <%} %>
			
			         </ul>
			      </div>
			      <!-- 페이저 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
		</div>
	</main>
	<!-- 컨텐츠가 들어가는 부분 end -->






<script>
	$(function(){
	
		// 열리는 메뉴
		$(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
		$(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(0).addClass("on");
		
	    // id는 테이블에 걸어주세요
	    $(".tbl td, .tbl td a").mouseenter(function() {
	       $(this).parents("tr").css({"background":"#f1f1f1", "cursor":"pointer"});
	    }).mouseout(function(){
	       $(this).parents("tr").css({"background":"white"});
	    });
	});
</script>

<%@ include file="../../../inc/user/footer.jsp"%>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>
