<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
   int mno = Integer.parseInt(request.getParameter("mno"));
   int gpNo = Integer.parseInt(request.getParameter("gpNo"));
   long orderCode = Long.parseLong(request.getParameter("orderCode"));
%>
<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="../../../../css/user/hyeonjuStyle.css">

 <!-- 컨텐츠가 들어가는 부분 start -->
 <main class="container">
   <div class="inner_ct clearfix">
     <div class="layout_2_wrap">
     <form action="<%=request.getContextPath()%>/insertReview.re" method="post">
       <!-- 제목 -->
       <!-- 내용 -->
       <div class="main_cnt">
       <h1 class="main_title">후기 작성</h1>
         <div class="main_cnt_list clearfix">
           <h4 class="title">가이드 평점</h4>
           <div class="box_input rate">
           <select name="gdRating" class="rating">
            <option value=""  id="choose"selected disabled>평점을 선택하세요</option>
              
              <option value="5.0">5.0</option>
              <option value="4.5">4.5</option>
              <option value="4">4.0</option>
              <option value="3.5">3.5</option>
              <option value="3.0">3.0</option>
              <option value="2.5">2.5</option>
              <option value="2">2.0</option>
              <option value="1.5">1.5</option>
              <option value="1.0">1.0</option>
        
           </select>
           </div>
         </div>
         <div class="main_cnt_list clearfix">
           <h4 class="title">프로그램 평점</h4>
           <div class="box_input rate">
           <select name="pRating" class="rating">
            <option value="" selected disabled>평점을 선택하세요</option>
              <option value="5.0">5.0</option>
              <option value="4.5">4.5</option>
              <option value="4">4.0</option>
              <option value="3.5">3.5</option>
              <option value="3.0">3.0</option>
              <option value="2.5">2.5</option>
              <option value="2">2.0</option>
              <option value="1.5">1.5</option>
              <option value="1.0">1.0</option>
        
           </select>
           </div>
         </div>
        <!--  <div class="main_cnt_list clearfix">
           <h4 class="title">제목</h4>
           <div class="box_input"><input type="text" name="" id="subject" placeholder="제목을 입력하세요"></div>
         </div> -->
         <div class="main_cnt_list clearfix">
           <h4 class="title">내용</h4>
           <div class="box_input "><textarea class="contents" name="rContents" placeholder="내용을 입력해 주세요"></textarea></div>
         </div>
         <input type="hidden" name="mno" value="<%= mno %>">
         <input type="hidden" name="gpNo" value="<%= gpNo %>">
         <input type="hidden" name="orderCode" value="<%=orderCode%>">
         <!-- 라디오 끝 -->
         </div>
         <!-- 버튼 시작 -->
         <div class="btn_wrap">
           <button type="button" class="btn_com btn_white">취소하기</button>
           <button type="submit" class="btn_com btn_blue">작성하기</button>
         </div>
         <!-- 버튼 끝 -->
         </form>
       </div>
     </div>
 </main>
 <!-- 컨텐츠가 들어가는 부분 end -->
 
<%@ include file="../../../inc/user/footer.jsp" %>