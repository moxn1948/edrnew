<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.edr.order.model.vo.*, com.edr.generalProgram.model.vo.*,com.edr.payment.model.vo.*,com.edr.product.model.vo.*, com.edr.common.model.vo.*, com.edr.review.model.vo.*"%>
 <%
      ArrayList<Integer> unProNo = (ArrayList)request.getAttribute("unProNo");
      ArrayList<Gp> gpNameList = (ArrayList)request.getAttribute("gpNameList");
      ArrayList<Payment> paymentDateList = (ArrayList)request.getAttribute("paymentDateList");
      ArrayList<Product> productDateList = (ArrayList)request.getAttribute("productDateList");
      ArrayList<Product> productNumber = (ArrayList)request.getAttribute("productNumber");
      ArrayList<OrderList> orderCodeList = (ArrayList)request.getAttribute("orderCodeList");
      ArrayList<Gp> gpNoList = (ArrayList)request.getAttribute("gpNoList");
      ArrayList<Review> reviewList = (ArrayList)request.getAttribute("reviewlist");
      ArrayList<Integer> reviewCount = (ArrayList<Integer>) request.getAttribute("reviewCount");
      
      PageInfo pi = (PageInfo)request.getAttribute("pi");
      int listCount = pi.getListCount();
      int currentPage = pi.getCurrentPage();
      int maxPage = pi.getMaxPage();
      int startPage = pi.getStartPage();
      int endPage = pi.getEndPage();
      
      /* System.out.println("unProNo : " + unProNo);
      System.out.println("gpNameList : " + gpNameList);
      System.out.println("paymentDateList : " + paymentDateList);
      System.out.println("productDateList : " + productDateList);
      System.out.println("paymentStateList : " + paymentStateList); */
    %>
   <%@ include file="../../../inc/user/subHeader.jsp"%>
   <!-- 팝업 관련 css -->
   <script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
   <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
   <!-- 팝업 관련 js -->
   <script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
   <link rel="stylesheet" type="text/css"
      href="<%= request.getContextPath() %>/css/user/shjeon.css">
   <script
      src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
   <style>
   .squ_tbl:last-child:hover{
   font-style: normal;
   text-decoration: none !important;
   }
   </style>
   <!-- 컨텐츠가 들어가는 부분 start -->
   <main class="container">
   <div class="inner_ct clearfix">
      <!-- 서브메뉴 : 변경해야함 -->
      <%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
      <!-- 오른쪽 컨텐츠 부분 시작-->
      <div class="sub_ctn_wrap">
         <h4 class="sub_ctn_tit">완료된 프로그램</h4>
         <div class="sub_ctn">
            <!-- table 시작 -->
            <div class="tbl_wrap">
               <table class="tbl">
                  <colgroup>
                     <col width="10%">
                     <col width="30%">
                     <col width="10%">
                     <col width="30%">
                     <col width="10%">
                     <col width="10%">
                  </colgroup>
                  <tr class="tbl_tit">
                     <th>목록</th>
                     <th>프로그램 명</th>
                     <th>결제 일시</th>
                     <th>여행 일시</th>
                     <th>상세보기</th>
                     <th>후기관리</th>
                  </tr>
                  <% for(int i = 0; i < unProNo.size(); i++){ %>
                     <tr class="tbl_cnt">
                        <td><%= unProNo.get(i) %></td> 
                        <td><%= gpNameList.get(i).getGpName() %></td>
                        <td><%= paymentDateList.get(i).getPayChange() %></td>
                        <td><%= productDateList.get(i).getPdate() %></td>
                        <td><span><a class="squ_tbl programDetailPop" href="<%= request.getContextPath() %>/selectProcessedDetail.cgp?orderCode=<%= orderCodeList.get(i).getOrderCode() %>" id="" style="margin-right:3px;">상세보기</a></span></td>
                        <% if(reviewCount.get(i) == 0) {%>
                        	<td><a class="squ_tbl" href="/edr/views/user/sub/my_page/review.jsp?mno=<%= loginUser.getMno() %>&gpNo=<%= gpNoList.get(i).getGpNo() %>&orderCode=<%= orderCodeList.get(i).getOrderCode() %>">작성하기</a></td>
                        <% }else{%>
                         	<td><a class="squ_tbl" style="background-color: #171f57; color: white;">후기작성완료</a></td> 
                        	
                        <%} %>
				<%-- 	<% if(reviewList != null) { %>
						<%for(int j=0;j < reviewList.size(); j++){ 
							if( gpNoList.get(i).getGpNo() == reviewList.get(j).getGpNo()){ %>
								<td><a class="squ_tbl" href="/edr/views/user/sub/my_page/reviewEdit.jsp?mno=<%= loginUser.getMno() %>&gpNo=<%= gpNoList.get(i).getGpNo() %>&orderCode=<%= orderCodeList.get(i).getOrderCode() %>">수정하기</a></td>
							<%} else if(j == reviewList.size() - 1) { %>
								<% System.out.println("1"); %>
	                        	<td><a class="squ_tbl" href="/edr/views/user/sub/my_page/review.jsp?mno=<%= loginUser.getMno() %>&gpNo=<%= gpNoList.get(i).getGpNo() %>&orderCode=<%= orderCodeList.get(i).getOrderCode() %>">작성하기</a></td>
                      		<% } %>
                  		<% } %>
                  	<% } else { %>
                		<td><a class="squ_tbl" href="/edr/views/user/sub/my_page/review.jsp?mno=<%= loginUser.getMno() %>&gpNo=<%= gpNoList.get(i).getGpNo() %>&orderCode=<%= orderCodeList.get(i).getOrderCode() %>">작성하기</a></td>
                  	<% } %>--%>
                  <% } %> 
                     </tr>
               </table>
            </div>
            <!-- table 끝 -->
         </div>
      </div>
      <!-- 오른쪽 컨텐츠 부분 끝-->
      <!-- 페이저 시작 -->
      <div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <% if( currentPage <= 1 ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003C;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.cgp?currentPage=<%= currentPage-1 %>&mno=<%= loginUser.getMno()%>">&#x003C;</a></li>
          <% } %>
          <% 
             
             for(int p = startPage; p <= endPage; p++ ){ 
                if(p == currentPage){
                   %>
                   <li class="pager_com pager_num on"><a href=""><%= p %></a></li>
                 <% 
                }else{
                   %>
                   <li class="pager_com pager_num"><a href="<%= request.getContextPath() %>/selectList.cgp?currentPage=<%= p %>&mno=<%= loginUser.getMno()%>"><%= p %></a></li>
                   <%
                }
                %>
          <% } %>
          <% if( currentPage >= maxPage ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003E;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.cgp?currentPage=<%= currentPage+1 %>&mno=<%= loginUser.getMno()%>">&#x003E;</a></li>
          <% } %>
        </ul>
      </div>
   </div>
   </main>
   <!-- 컨텐츠가 들어가는 부분 end -->
   <script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
   <script>
      $(function(){
         
            $('.programDetailPop').magnificPopup({ 
                 type: 'ajax' 
           });
            // 열리지 않는 메뉴
            // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
           // 열리는 메뉴
            $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
            $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(1).addClass("on");
      });
   </script>
   <%@ include file="../../../inc/user/footer.jsp"%>