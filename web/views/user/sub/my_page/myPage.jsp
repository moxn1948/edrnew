<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% 
	String msg = (String)request.getAttribute("msg");
%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<% if(loginUser != null) {%>
<!-- 팝업관련css -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/user/hyeonjuStyle.css">
<!-- 팝업 관련 css -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
  
<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
<%@ include file="../../../inc/user/myPageSubMenu.jsp" %>		
<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">프로필 관리</h4>
			<div class="sub_ctn">
        <!-- 영역분리 시작 -->
        <div class="layout_3_wrap">
            <!-- 내용 -->
            <div class="main_cnt">
              <div class="main_cnt_list clearfix">
                <h4 class="title">이름</h4>
                <div class="box_input private" id="private"><%= loginUser.getNickName() %></div>
                <div align="right">
                    <div class="btn_com btn_blue"><a href="/edr/views/user/sub/popup/profileEditPopup.jsp" class="editProfile">수정하기</a> </div>
                         
                   
                </div>
              </div>
              
              <div class="main_cnt_list clearfix">
                <h4 class="title">이메일</h4>
                 <div class="box_input private "><%= loginUser.getEmail() %></div>
              </div>
              
                 <div class="main_cnt_list clearfix">
                <h4 class="title">성별</h4>
                 <div class="box_input private ">
                 <% if(loginUser.getMgender().charAt(0) =='M'){ %>
                 	
                 <%= "남" %></div>
                 
                 <%}else{%>
                	 <%="여" %>
                <% }  %>
             
                 
              </div>
              
              <div class="main_cnt_list clearfix">
                <h4 class="title">연령대</h4>
                <div class="box_input private"><% if(loginUser.getMbirth() !=null){ 
                	//년도만 빼고싶은데
                	Calendar cal = Calendar.getInstance();
                	Calendar cal2 = new GregorianCalendar();
                	int year = cal.get(Calendar.YEAR);
                	System.out.println("year: "+year);
                int age	= Integer.parseInt(loginUser.getMbirth());
                System.out.println("age: "+age);
               int age2 = year-age+1;
                %><%= age2/10 *10 %>대</div>
                 
                <%} %>
              </div>
              <!-- <div class="main_cnt_list clearfix">
                <h4 class="title">비밀번호</h4>
                <div class="box_input private">*******</div>
              </div> -->
              <hr>
              <br><br>
              <% System.out.println("loginUserBank : " + loginUser.getBankName()); %>
              <% if(loginUser.getBankName() == null){ %>
              <div class="layout_3_wrap">
              	<h3 class="account">환불계좌</h3>
              	<div class="account number">등록된 계좌가 없습니다.</div>
              <!-- 버튼 시작 -->
              <div class="btn_wrap">
                <div align="center"><a class="btn_com btn_white" id="accountAdd" href="/edr/views/user/sub/popup/refundPopup.jsp?mno=<%= loginUser.getMno()%>">계좌 등록</a></div>
              </div>
              <% }else{ %>
              <div class="layout_3_wrap">
              	<h3 class="account">환불계좌</h3>
              	<div class="account number"><b><%= loginUser.getBankName() %></b> : <%= loginUser.getAccountNo() %></div>
              <!-- 버튼 시작 -->
              <div class="btn_wrap">
                <div align="center"><a class="btn_com btn_white" id="accountAdd" href="/edr/views/user/sub/popup/refundPopup.jsp?mno=<%= loginUser.getMno()%>">계좌 수정</a></div>
              </div>
              <% } %>
              <!-- 버튼 끝 -->
              </div>
              <br><br>
              <a style="text-align: center" class="delCnt" href="/edr/views/user/sub/popup/memberDelPopup.jsp?mno=<%= loginUser.getMno() %>">계정 삭제하기</a>
              <br>
            </div>
          </div>
				<!-- 영역분리 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
<script>

$(function(){
	// 열리지 않는 메뉴
	 $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
	
	// 열리는 메뉴
	//$(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
	//$(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
});

$(function(){
    /* 팝업 예시 */
    $('.editProfile').magnificPopup({ 
      type: 'ajax' 
    });
    $('.delCnt').magnificPopup({ 
      type: 'ajax',
      closeOnBgClick:false 
    });
    $('#accountAdd').magnificPopup({ 
      type: 'ajax',
      closeOnBgClick:false
    });
});

$(function () {
	var msg="<%=msg%>";
	console.log(msg);		
	if(msg!= "null"){
		alert(msg);
	}
	
});
function submitPwd(){
	$(".popArea").submit();	
}
function refundSubmit(){
	
	var refundName = $("#userName").val();
	var mno = <%= loginUser.getMno() %>;
	
	if($("#userName").val() == ""){
		alert("예금주명을 입력해주세요");
		$("#userName").focus();
		return false;
	}
	var regExpName = /^[가-힣]{2,}/;
	if(!regExpName.test($("#userName").val())){
		alert("예금주명을 확인해주세요");
		$("#userName").focus();
		$("#userName").select();
		return false;
	}
	
	if($("#bankName").val() == ""){
		alert("은행을 선택해주세요.");
		return false;
	}
	
	if($("#accountName").val() == ""){
		alert("계좌번호를 입력해주세요");
		$("#accountName").focus();
		return false;
	}
	var regAccountNumber = /^[0-9]{9,}/;
	if(!regAccountNumber.test($("#accountNumber").val())){
		alert("계좌번호를 확인해주세요");
		$("#accountNumber").focus();
		$("#accountNumber").select();
		return false;
	}
	

	return true;
	
}


function delMember(){
	if($("input:radio[name=delReason]").is(":checked") == false){
		alert("이유를 선택해주세요.");
		return false;
	}else{
		var del = $(".del_reason_txt").val();
		if($("#delReason5").is(":checked") == true && del == ""){
			alert("이유를 작성해주세요.");
			return false;
		}else{
			return true;
		}
		return true;
	}
}

</script>


<%@ include file="../../../inc/user/footer.jsp" %>
<% }else { %>
	<script>
		$(document).ready(function(){
			alert("잘못된 접근입니다.");
			location.href = "<%=request.getContextPath() %>/index";
		});
	</script>
<%} %>
