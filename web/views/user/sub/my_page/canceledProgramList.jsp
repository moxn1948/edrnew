<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.edr.order.model.vo.*, com.edr.generalProgram.model.vo.*,com.edr.payment.model.vo.*,com.edr.common.model.vo.*"%>
<%
	ArrayList<Integer> unProNo = (ArrayList)request.getAttribute("unProNo");
	ArrayList<Gp> gpNameList = (ArrayList)request.getAttribute("gpNameList");
	ArrayList<Payment> endPaymentDateList = (ArrayList)request.getAttribute("endPaymentDateList");
	ArrayList<Payment> paymentCostList = (ArrayList)request.getAttribute("paymentCostList");
	ArrayList<Member> bankNameAndAccountNoList = (ArrayList)request.getAttribute("bankNameAndAccountNoList");
	
	System.out.println("unProNo : " + unProNo);
	System.out.println("gpNameList : " + gpNameList);
	System.out.println("endPaymentDateList : " + endPaymentDateList);
	System.out.println("paymentCostList : " + paymentCostList);
	System.out.println("bankNameAndAccountNoList : " + bankNameAndAccountNoList);
	
	PageInfo pi = (PageInfo)request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>	
	<%@ include file="../../../inc/user/subHeader.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
	<!-- 컨텐츠가 들어가는 부분 start -->
	<main class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">취소된 프로그램</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="40%">
							<col width="10%">
							<col width="10%">
							<col width="30%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>취소 일시</th>
							<th>결제 금액</th>
							<th>환불 계좌</th>
						</tr>
						<% for(int i = 0; i < unProNo.size(); i++){ %>
						<tr class="tbl_cnt">
							<td><%= unProNo.get(i) %></td>
							<td><%= gpNameList.get(i).getGpName() %></td>
							<td><%= endPaymentDateList.get(i).getPayChange() %></td>
							<td><%= paymentCostList.get(i).getPayCost() %> 원</td>
							<td><%= bankNameAndAccountNoList.get(i).getBankName() %> <%= bankNameAndAccountNoList.get(i).getAccountNo() %> </td>
						</tr>
						<% } %>
					</table>
				</div>
				<!-- table 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
		<div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <% if( currentPage <= 1 ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003C;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.cangp?currentPage=<%= currentPage-1 %>&mno=<%= loginUser.getMno()%>">&#x003C;</a></li>
          <% } %>
          <% 
          	for(int p = startPage; p <= endPage; p++ ){ 
          		if(p == currentPage){
          			%>
			          <li class="pager_com pager_num on"><a href=""><%= p %></a></li>
			        <% 
          		}else{
          			%>
			          <li class="pager_com pager_num"><a href="<%= request.getContextPath() %>/selectList.cangp?currentPage=<%= p %>&mno=<%= loginUser.getMno()%>"><%= p %></a></li>
          			<%
          		}
  		        %>
          <% } %>
  		  <% if( currentPage >= maxPage ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003E;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.cangp?currentPage=<%= currentPage+1 %>&mno=<%= loginUser.getMno()%>">&#x003E;</a></li>
          <% } %>
        </ul>
      </div>
	</div>
	</main>
	<!-- 컨텐츠가 들어가는 부분 end -->

	<script>
		$(function(){
 	  		// 열리지 않는 메뉴
   			// $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
  			// 열리는 메뉴
   			$(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   			$(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(2).addClass("on");
		});
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>