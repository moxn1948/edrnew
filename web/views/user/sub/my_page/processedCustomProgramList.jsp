<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.CpRequest, com.edr.payment.model.vo.Payment, com.edr.common.model.vo.PageInfo"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

  
<% if(loginUser != null){ %>
  
<%
	ArrayList<HashMap<String, Object>> myCpList = (ArrayList<HashMap<String, Object>>) request.getAttribute("myCpList");

	// 프로그램 목록 보기
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int limit = pi.getLimit();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
%>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
<%@ include file="../../../inc/user/myPageSubMenu.jsp" %>		
<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h1 class="sub_ctn_tit">결제된 맞춤프로그램</h1>
        <div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="*">
							<col width="15%">
							<col width="15%">
							<col width="25%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>결제 금앧</th>
							<th>결제 일시</th>
							<th>투어 일시</th>
						</tr>
						<% for(int i = 0; i < myCpList.size(); i++) {%>
						<tr class="tbl_cnt">
							<td><a href="<%= request.getContextPath()%>/selectCompMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>"><%= myCpList.get(i).get("idxnum") %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectCompMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>"><%= ((CpRequest) myCpList.get(i).get("cpList")).getCpTitle() %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectCompMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>"><%= ((Payment) myCpList.get(i).get("pay")).getPayCost() %>원</a></td>
							<td><a href="<%= request.getContextPath()%>/selectCompMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>"><%= ((Payment) myCpList.get(i).get("pay")).getPayDate() %></a></td>
							<td><a href="<%= request.getContextPath()%>/selectCompMyCpDetail.cp?cpNo=<%=((CpRequest) myCpList.get(i).get("cpList")).getCpNo() %>"><%= ((CpRequest) myCpList.get(i).get("cpList")).getCpSdate() %> ~ <%= ((CpRequest) myCpList.get(i).get("cpList")).getCpEdate() %></a></td>
						</tr>
						<% } %>
					</table>
				</div>
				  <!-- 페이저 시작 -->
			      <div class="pager_wrap">
			         <ul class="pager_cnt clearfix add">
			            <% if(currentPage <= 1)  { %>
			
			
			            <li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>
			
			
			            <%} else { %>
			            <li class="pager_com pager_arr prev"><a
			               href="<%= request.getContextPath()%>/selectCompMyCp.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=currentPage - 1%>">&#x003C;</a></li>
			
			            <%} %>
			            <% for(int p = startPage; p <= endPage; p++)  {
			               if(p == currentPage) {
			            %>
			            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
			            <%} else {%>
			            <li class="pager_com pager_num"><a href="<%= request.getContextPath()%>/selectCompMyCp.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=p%>"><%=p %></a></li>
			            <%} %>
			            <% } %>
			
			
			            <% if(currentPage >= maxPage) { %>
			            <li class="pager_com pager_arr next"><a
			               href="javascript: void(0);">&#x003E;</a></li>
			            <%}else { %>
			            <li class="pager_com pager_arr next"><a
			               href="<%= request.getContextPath()%>/selectCompMyCp.cp?mno=<%= loginUser.getMno() %>&currentPage=<%=currentPage + 1%>">&#x003E;</a></li>
			
			            <%} %>
			
			         </ul>
			      </div>
			      <!-- 페이저 끝 -->
		</div>
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<script>
	$(function(){
	
		// 열리는 메뉴
	   $(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
	   $(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
		
	    // id는 테이블에 걸어주세요
	    $(".tbl td, .tbl td a").mouseenter(function() {
	       $(this).parents("tr").css({"background":"#f1f1f1", "cursor":"pointer"});
	    }).mouseout(function(){
	       $(this).parents("tr").css({"background":"white"});
	    });
	});
</script>

<%@ include file="../../../inc/user/footer.jsp"%>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>