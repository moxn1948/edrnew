	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.edr.order.model.vo.*, com.edr.generalProgram.model.vo.*,com.edr.payment.model.vo.*,com.edr.product.model.vo.*, com.edr.common.model.vo.*"%>
    <%
	    ArrayList<Integer> unProNo = (ArrayList)request.getAttribute("unProNo");
	    ArrayList<Gp> gpNameList = (ArrayList)request.getAttribute("gpNameList");
		ArrayList<Payment> paymentDateList = (ArrayList)request.getAttribute("paymentDateList");
		ArrayList<Product> productDateList = (ArrayList)request.getAttribute("productDateList");
		ArrayList<Payment> paymentStateList = (ArrayList)request.getAttribute("paymentStateList");
		ArrayList<Product> productNumber = (ArrayList)request.getAttribute("productNumber");
		ArrayList<OrderList> orderCodeList = (ArrayList)request.getAttribute("orderCodeList");
		
		PageInfo pi = (PageInfo)request.getAttribute("pi");
		int listCount = pi.getListCount();
		int currentPage = pi.getCurrentPage();
		int maxPage = pi.getMaxPage();
		int startPage = pi.getStartPage();
		int endPage = pi.getEndPage();
		/* System.out.println("unProNo : " + unProNo);
		System.out.println("gpNameList : " + gpNameList);
		System.out.println("paymentDateList : " + paymentDateList);
		System.out.println("productDateList : " + productDateList);
		System.out.println("paymentStateList : " + paymentStateList); */
    %>
	<%@ include file="../../../inc/user/subHeader.jsp"%>
	<!-- 팝업 관련 css -->
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
	<!-- 팝업 관련 js -->
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
	<!-- 컨텐츠가 들어가는 부분 start -->
	<link rel="stylesheet" type="text/css"href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<main class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/myPageSubMenu.jsp"%>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">준비 중인 프로그램</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="25%">
							<col width="10%">
							<col width="25%">
							<col width="10%">
							<col width="10%">
							<col width="10%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>결제 일시</th>
							<th>여행 일시</th>
							<th>상세보기</th>
							<th>주문상태</th>
							<th>예약취소</th>
						</tr>
						<% for(int i = 0; i < unProNo.size(); i++){ %>
							<tr class="tbl_cnt">
								<td><%= unProNo.get(i) %></td>
								<td><%= gpNameList.get(i).getGpName() %></td>
								<td><%= paymentDateList.get(i).getPayChange() %></td>
								<td><%= productDateList.get(i).getPdate() %></td>
								<td><span><a class="squ_tbl" href="<%= request.getContextPath() %>/selectUnprocessedDetail.ugp?orderCode=<%= orderCodeList.get(i).getOrderCode() %>" id="programDetailPop">상세보기</a></span></td>
								<td>
								<span class="squ_tbl" style="background:#171f57; color:#f7f7f7;">
									<% 
									switch(paymentStateList.get(i).getPayState()){
										case "ENDPAY" : %>
										결제완료
										<%break;
										case "WAITPAY" : %>
										결제대기
										<%break;
									}
									%>
								</span></td>
								<td><a class="squ_tbl" href="<%= request.getContextPath()%>/cancelProgram.ugp?orderCode=<%= orderCodeList.get(i).getOrderCode()%>&mno=<%= loginUser.getMno() %>" onclick="return cancelProgram();">예약취소</a></td>
							</tr>
						<% } %>
					</table>
				</div>
				<!-- table 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	<!-- 페이저 시작 -->
      <div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <% if( currentPage <= 1 ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003C;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.ugp?currentPage=<%= currentPage-1 %>&mno=<%= loginUser.getMno()%>">&#x003C;</a></li>
          <% } %>
          <% 
          	
          	for(int p = startPage; p <= endPage; p++ ){ 
          		if(p == currentPage){
          			%>
			          <li class="pager_com pager_num on"><a href=""><%= p %></a></li>
			        <% 
          		}else{
          			%>
			          <li class="pager_com pager_num"><a href="<%= request.getContextPath() %>/selectList.ugp?currentPage=<%= p %>&mno=<%= loginUser.getMno()%>"><%= p %></a></li>
          			<%
          		}
  		        %>
          <% } %>
  		  <% if( currentPage >= maxPage ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003E;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.ugp?currentPage=<%= currentPage+1 %>&mno=<%= loginUser.getMno()%>">&#x003E;</a></li>
          <% } %>
        </ul>
      </div>
	</div>
	</main>
	<!-- 컨텐츠가 들어가는 부분 end -->
	
	<!-- 팝업 관련 js -->
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
	<script>
		$(function(){
 	  		// 열리지 않는 메뉴
   			// $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   			$('#programDetailPop').magnificPopup({ 
   		      type: 'ajax',
   		   	  closeOnBgClick:false
   		    });
   
  			// 열리는 메뉴
   			$(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   			$(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(0).addClass("on");
   			
		});
		function cancelProgram(){
			console.log("접근");
			var mno = <%= loginUser.getMno() %>;
			
			temp = false;
			
			if(confirm("삭제하시겠습니까?")){
				$.ajax({
					url:"/edr/checkBank.me",
					type:"post",
					data:{
						mno:mno
					},
					success:function(data){
						if(data == "success"){
							console.log("ajax success");
							temp = true;
							return temp;
						}else if(data == "false"){
							alert("환불계좌를 등록해주세요.");
							temp = false;
							return temp;
						}
					},
					error:function(){
						
					}
				});
			}else{
			}
		return false;
			
		};
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>