<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<!-- 컨텐츠가 들어가는 부분 start -->
<%
if (loginUser != null) { 
%>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<main class="container">
	<div class="inner_ct clearfix">
		<div class="layout_1_wrap">
			<!-- 제목 -->
			<h1 class="main_title">맞춤 프로그램 신청</h1>
			<!-- 내용 시작 -->
			<div class="main_cnt">
			<form action="<%= request.getContextPath()%>/insertCp.cp" method="post" class="requestCpForm">
			<input type="hidden" name="mno" value="<%= loginUser.getMno()%>">
				<div class="main_cnt_list clearfix">
					<h4 class="title">지역</h4>
					<!-- 기본적인 input 시작 -->		
					<div class="box_select">					
					<select name="local" id="box_select">
						<option value="1">서울</option>
						<option value="2">경기</option>
						<option value="3">경상</option>
						<option value="4">강원</option>
						<option value="5">전라</option>
						<option value="6">충청</option>
						<option value="7">제주</option>
						<option value="8">부산</option>
						<option value="9">대구</option>
						<option value="10">대전</option>					
						<option value="11">광주</option>					
						<option value="12">인천</option>					
						<option value="13">울산</option>					
					</select>				
					</div>
					</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">팀 인원 수</h4>
					<div class="box_input"><input type="text" name="person" id="person" placeholder="참여하는 팀 인원 수를 작성해주세요"></div>
				</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">시작 날짜</h4>
					<div class="box_input box_select" >
					 <input type="text" name="startDate" id="startDateSel" class="date_pick ipt_sel" placeholder="날짜를 선택해주세요." autocomplete="off">										
				</div>						
					</div>
					
				<div class="main_cnt_list clearfix">
					<h4 class="title">종료 날짜</h4>
					<div class="box_input box_select" >
					<input type="text" name="endDate" id="endDateSel" class="date_pick2 ipt_sel" placeholder="날짜를 선택해주세요." autocomplete="off">														
				</div>						
					</div>
					
				<div class="main_cnt_list clearfix">
					<h4 class="title">최대 가능 금액</h4>
					<div class="box_input"><input type="text" name="money" id="money" placeholder="프로그램 진행 시 원하는 최대 금액을 작성해주세요"></div>
				</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">가능언어</h4>					
					<div class="box_chk1">
						<input type="checkbox" name="language" id="language1" value="한국어"><label for="test3">한국어</label>
						<input type="checkbox" name="language" id="language2" value="영어"><label for="test4">영어</label>
						<input type="checkbox" name="language" id="language3" value="중국어"><label for="test4">중국어</label>
						<input type="checkbox" name="language" id="language4" value="프랑스어"><label for="test4">프랑스어</label>
						
						<a href="javascript: void(0)" onclick="languagePlus()"style="margin-top: 4px;margin-left: 4px;display: inline-block;background-color: #171f57;color: #fff;padding: 3px 0;text-align: center;width: 40px;">추가</a>
					</div>				
						<div class="box_input1" ><input type="text" name="language5" id="language5" placeholder="그 외 언어" value=""> </div>	
				</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">가이드 성별</h4>
					<div class="box_radio">
						<input type="radio" name="gender" id="gender" value="M"><label for="남성">남성</label>
						<input type="radio" name="gender" id="gender1" value="F"><label for="여성">여성</label>
						<input type="radio" name="gender" id="gender1" value="ALL"><label for="무관">무관</label>
					</div>
				</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">가이드 나이대</h4>
					<div class="box_select">
					<select name="age" id="box_select">
						<option value="20">20대</option>
						<option value="30">30대</option>
						<option value="40">40대</option>
						<option value="50">50대이상</option>											
					</select>
				</div>						
			</div>			
				
				
			<div class="main_cnt_list clearfix">
					<h4 class="title">제목</h4>
					<div class="box_input"><input type="text" name="subject" id="subject" placeholder="제목을 작성해주세요"></div>
				</div>	
				
			<div class="main_cnt_list clearfix">
					<h4 class="title">내용</h4>
					<div class="box_input"><textarea placeholder="내용을 입력해주세요" rows="5" cols="50" style="resize:none;" name="cnt" id="cnt"></textarea></div>
				</div>	
				
								
															
				<div class="btn_wrap">
					<button type="submit" class="btn_com btn_white">취소</button>
					<button type="submit" class="btn_com btn_blue">신청하기</button>
				</div>
				<!-- 버튼 끝  -->
			</form>
		</div>
			<!-- 내용 끝 -->
		</div>
	</div>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	$( function() {
		 /* 데이트피커 */
	    $(".date_pick").datepicker({
	      nextText: '다음 달',
	      prevText: '이전 달',
	      dateFormat: "yy-mm-dd",
	      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
	      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
	      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
	      minDate : "+1D"
	    });

		 
	    $(".date_pick2").datepicker({
		      nextText: '다음 달',
		      prevText: '이전 달',
		      dateFormat: "yy-mm-dd",
		      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
		      minDate : "+1D"
		     
		    });
		
	
	function languagePlus() {
		$("<input type='text' name='language5' id='language5' placeholder='그 외 언어' value=''>").clone().eq(0).appendTo(".box_input1");
		
		
	}
	
	
});
	
	
	</script>
</main>
<%  
} else {
	response.sendRedirect("../../index.jsp");
}
%>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp" %>
