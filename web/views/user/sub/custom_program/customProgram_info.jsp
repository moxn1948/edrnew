<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

 <!-- 컨텐츠가 들어가는 부분 start -->
 <main class="container customProgram_info">
   <div class="inner_ct clearfix">
     <div class="layout_wrap">
       <!-- 제목 -->
       <h1 class="main_title">맞춤 프로그램 소개</h1>
       <!-- 내용 -->
       <div class="main_cnt">
         <ul class="custom_pro_wrap">
           <li class="custom_pro_list">
             <div class="custom_pro_ctn clearfix">
               <div class="custom_cnt_desc">
                 <p class="desc_tit">더 프라이빗 한 여행</p>
                 <p class="desc_sub">나의 팀만을 위한 패키지 여행을 즐기고 싶다면</p>
               </div>
               <div class="custom_cnt_img">
                 <img src="<%=request.getContextPath()%>/imgs/temp_custom_pro.png" alt="">
               </div>
             </div>
           </li>
           <li class="custom_pro_list">
             <div class="custom_pro_ctn clearfix">
               <div class="custom_cnt_desc">
                 <p class="desc_tit">내가 직접 계획하는 나만의 패키지 여행</p>
                 <p class="desc_sub">내가 원하는 장소와 원하는 시간, 원하는 일정을 직접 계획</p>
               </div>
               <div class="custom_cnt_img">
                 <img src="<%=request.getContextPath()%>/imgs/temp_custom_pro.png" alt="">
               </div>
             </div>
           </li>
           <li class="custom_pro_list">
             <div class="custom_pro_ctn clearfix">
               <div class="custom_cnt_desc">
                 <p class="desc_tit">현지 가이드가 나의 여행을 더 완벽하게</p>
                 <p class="desc_sub">나의 여행일정을 멋진 가이드가 더 완벽하게 만들어줍니다.</p>
               </div>
               <div class="custom_cnt_img">
                 <img src="<%=request.getContextPath()%>/imgs/temp_custom_pro.png" alt="">
               </div>
             </div>
           </li>
         </ul>
         <!-- 버튼 시작 -->
         <div class="btn_wrap">
           <a href="./customProgram_SignUp.jsp" class="btn_com btn_blue custom_sign_btn">신청하러 가기</a>
         </div>
         <!-- 버튼 끝 -->
       </div>
     </div>
   </div>
 </main>
 <!-- 컨텐츠가 들어가는 부분 end -->
 
<%@ include file="../../../inc/user/footer.jsp" %>
