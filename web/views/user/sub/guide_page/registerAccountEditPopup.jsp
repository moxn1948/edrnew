<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.guide.model.vo.*"%>
<%
	GuideDetail guideDetailInfo = (GuideDetail)request.getSession().getAttribute("guideDetailInfo");
%>
<%@ include file="../../../inc/user/subHeader.jsp" %>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container registerAccountPopup">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/guidePageSubMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<form action="<%= request.getContextPath() %>/updateGuideBank.gd" method="post">
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">정산 계좌 수정</h4>
			<div class="sub_ctn">
        <!-- 계좌 없는 경우 시작 -->
        <div class="layout_3_wrap accountYN_N">
          <div class="main_cnt">
            <div class="box_input">
              <input type="text" name="userName" id="userName" placeholder="본인 명의의 예금주를 입력하세요.">
            </div>
            <div class="box_select">
              <select name="bankName" id="bankName">
		        <option value="">== 은행 ==</option>
		        <option value="">== 은행 ==</option>
		        <option value="신한은행">신한은행</option>
		        <option value="농협">농협</option>
		        <option value="우리은행">우리은행</option>
		        <option value="국민은행">국민은행</option>
		        <option value="기업은행">기업은행</option>
		        <option value="하나은행">하나은행</option>
		        <option value="대구은행">대구은행</option>
		        <option value="sc제일은행">sc제일은행</option>
		        <option value="부산은행">부산은행</option>
		      </select>
            </div>
            <div class="box_input">
                <input type="text" name="accountNumber" id="accountNumber" placeholder="계좌번호를 '-' 없이 입력하세요.">
      			<input type="hidden" name="mno" value="<%= loginUser.getMno() %>">
            </div>
          </div>
          <div class="btn_wrap">
            <button class="btn_com btn_blue register_btn" onclick="return guideAccountSubmit();">저장하기</button>
          </div>
        </div>
		</div>
		</div>
        </form>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script>
  $(function(){
      
      // 열리는 메뉴
      $(".sub_menu_cnt .sub_menu_list").eq(3).addClass("on").addClass("open");
      $(".sub_menu_cnt .sub_menu_list").eq(3).find(".sub_side_list").eq(3).addClass("on");
  });
  function guideAccountSubmit(){
		
		var refundName = $("#userName").val();
		
		if($("#userName").val() == ""){
			alert("예금주명을 입력해주세요");
			$("#userName").focus();
			return false;
		}
		var regExpName = /^[가-힣]{2,}/;
		if(!regExpName.test($("#userName").val())){
			alert("예금주명을 확인해주세요");
			$("#userName").focus();
			$("#userName").select();
			return false;
		}
		
		if($("#bankName").val() == ""){
			alert("은행을 선택해주세요.");
			return false;
		}
		
		if($("#accountName").val() == ""){
			alert("계좌번호를 입력해주세요");
			$("#accountName").focus();
			return false;
		}
		var regAccountNumber = /^[0-9]{9,}/;
		if(!regAccountNumber.test($("#accountNumber").val())){
			alert("계좌번호를 확인해주세요");
			$("#accountNumber").focus();
			$("#accountNumber").select();
			return false;
		}
		

		return true;
		
	}
</script>
<%@ include file="../../../inc/user/footer.jsp" %>
