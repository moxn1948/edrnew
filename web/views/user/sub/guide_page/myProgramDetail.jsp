<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.*, com.edr.common.model.vo.Attachment"%>

<%@ include file="../../../inc/user/subHeader.jsp"%>

<% if(loginUser != null && loginUser.getMtype().equals("GUIDE")){ %>
<%
	int gpNo = (Integer) request.getAttribute("gpNo");
	Gp gpObj = (Gp) request.getAttribute("gpObj");
	ArrayList<GpPriceDetail> gpPriceList  = (ArrayList<GpPriceDetail>) request.getAttribute("gpPriceList");
	ArrayList<GpDetail> gpDetailList  = (ArrayList<GpDetail>) request.getAttribute("gpDetailList");
	ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) request.getAttribute("gpAttachmentList");
	ArrayList<GpLang> gpLangList  =  (ArrayList<GpLang>) request.getAttribute("gpLangList");
	
	String localName = "";
	switch (gpObj.getLocalNo()) {
	case 1: localName = "서울"; break;
	case 2: localName = "경기"; break;
	case 3: localName = "경상"; break;
	case 4: localName = "강원"; break;
	case 5: localName = "전라"; break;
	case 6: localName = "충청"; break;
	case 7: localName = "제주"; break;
	case 8: localName = "부산"; break;
	case 9: localName = "대구"; break;
	case 10: localName = "대전"; break;
	case 11: localName = "광주"; break;
	case 12: localName = "인천"; break;
	case 13: localName = "울산"; break;
	}
	
	int totalDay = gpObj.getGpTday();
	
	String lang = "";
	for(int i = 0; i < gpLangList.size(); i++){
		if(i != gpLangList.size() - 1){
			lang += gpLangList.get(i).getLang() + ", ";
		}else{
			lang += gpLangList.get(i).getLang();
		}
	}

	ArrayList<String> includeName = new ArrayList<>();
	ArrayList<String> notcludeName = new ArrayList<>();
	for(int i = 0; i < gpPriceList.size(); i++){
		if(gpPriceList.get(i).getGpInc().equals("INCLUDE")){
			includeName.add(gpPriceList.get(i).getGpCategory());
		}else{
			notcludeName.add(gpPriceList.get(i).getGpCategory());
		}
	}
	
	
	ArrayList<Integer> gpDetailCtn = new ArrayList<>();
	for(int i = 0; i < gpObj.getGpTday(); i++){
		int ctn = 0;
		for(int j = 0; j < gpDetailList.size(); j++){
			if(gpDetailList.get(j).getGpDay() == (i + 1)){
				ctn++;	
			}
		}
		gpDetailCtn.add(ctn);
	}
	
 
%>

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/kijoonStyle.css">
<!-- kakao map api link -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2"></script>

<style>
.price_table{font-size: 16px;}
.price_table .total{border-top: 1px solid #b0b0b0;font-weight: bold;}
#meetMap{margin-top: 10px;height: 300px;}
.noticeCnt{margin-top: 10px;width: 98%;height: 100px;resize: none;padding: 1%;border-color: #ddd;}
.payWon{text-align: right;}
#updateFin{display: none;}
</style>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<form action="<%= request.getContextPath() %>/updateMyProgramNotice.gp" method="post">
			<h4 class="sub_ctn_tit">내 프로그램 상세보기</h4>
			<div class="sub_ctn">
				<div class="program_ctn">
					<div class="program_list_wrap program_list_wrap_1">
						<div class="program_list">
							<div class="program_cnt">
								<p class="cnt_tit">프로그램 명</p>
								<p class="cnt"><%= gpObj.getGpName() %></p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">지역명</p>
								<p class="cnt"><%= localName %></p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">프로그램 설명</p>
								<p class="cnt"><%= gpObj.getGpDescr() %></p>
							</div>
							
							<hr>
							
							<div class="program_cnt">
								<p class="cnt_tit">총 프로그램 기간</p>
								<p class="cnt"><%= totalDay %>일</p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">참가 가능 인원</p>
								<p class="cnt">최소 <%= gpObj.getGpMin() %>명 ~ 최대 <%= gpObj.getGpMax() %>명</p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">프로그램 진행 가능 언어</p>
								<p class="cnt"><%= lang %></p>
							</div>
						</div>
					</div> <br>
					<hr>
					<div class="program_list_wrap program_list_wrap_2">
						<div class="program_list">
							<div class="program_cnt">
								<p class="cnt_tit">포함사항</p>
								<p class="cnt">
								<%
									String includeStr = "";
									for(int i = 0; i < includeName.size(); i++){
										if(i == includeName.size() - 1){
											includeStr += includeName.get(i);
										}else{
											includeStr += includeName.get(i) + ", ";
										}
									}
								%>
								<%= includeStr %>
								</p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">불포함사항</p>
								<p class="cnt">
								<%
									String notcludeStr = "";
									for(int i = 0; i < notcludeName.size(); i++){
										if(i == notcludeName.size() - 1){
											notcludeStr += notcludeName.get(i);
										}else{
											notcludeStr += notcludeName.get(i) + ", ";
										}
									}
								%>
								<%= notcludeStr %>
								</p>
							</div>
							
							<div class="program_cnt">
								<p class="cnt_tit">만나는 시간</p>
								<p class="cnt"><%= gpObj.getGpMeetTime() %></p>
							</div>
							<div class="program_cnt">
								<p class="cnt_tit">만나는 장소</p>
								<p class="cnt"><%= gpObj.getGpMeetArea() %></p>
								<div id="meetMap"></div>	
							</div>
						</div>
					</div>
					<br><hr><br>
					<div class="program_list_wrap program_list_wrap_3">
						<div class="program_list">
						<% 
						int gpDetailFor = 0;
						
						for(int i = 0; i < gpObj.getGpTday(); i++){ %>
						<div class="detail_course_wrap detail_course_wrap_1 course_wrap">
							<div class="detail_course_tit">
								<p class="tit"><%= i + 1 %>Day</p>
								<!-- <p class="total_time">총 ?시간 소요</p> -->
							</div>
							<% for(int j = 0; j < gpDetailCtn.get(i); j++){ %>
							<div class="detail_course_ctn">
								<p class="ctn_tit">일정 <%= j+1 %> : 약 <%= gpDetailList.get(gpDetailFor).getGpTime() %>시간 소요</p>
								<p class="place_tit"><%= gpDetailList.get(gpDetailFor).getGpLocation() %></p>
								<img src="/edr/uploadFiles/<%=gpAttachmentList.get(gpDetailFor).getChangeName() %>" alt="" class="place_img">
								<p class="desc"><%= gpDetailList.get(gpDetailFor).getGpCnt() %></p>
							</div>
							<% 
								gpDetailFor++;
							} %>
						</div>
						<% }%>
			
						</div>
					</div>
					<div class="program_list_wrap program_list_wrap_4">
						<div class="program_list">
							<div class="program_cnt">
								<p class="cnt_tit">특이사항</p>
								<textarea class="noticeCnt" name="gpNotice" readonly><%= gpObj.getGpNotice() %></textarea>
							</div>
							<input type="hidden" name="gpNo" value="<%= gpNo %>">
						</div>
					</div>
					<br><hr>
					<div class="program_list_wrap program_list_wrap_5">
						<div class="program_list">
							<div class="program_cnt">
								<p class="cnt_tit">프로그램 금액</p>
								<table class="tableArea price_table">
									<% for(int i = 0; i < gpPriceList.size(); i++){ 
										if(gpPriceList.get(i).getGpInc().equals("INCLUDE")) {
										%>
									<tr>
										<% 
											String name = gpPriceList.get(i).getGpCategory();
											int price = gpPriceList.get(i).getGpPrice();
										%>
												<!-- includeStr += includeName.get(i); -->
										<td><%= name %></td>
										<td class="payWon"><%= price %>원</td>
											
									</tr>
									<%  }	
									} %>
								
									<tr class="total">
										<td>총 합계</td>
										<td class="payWon"><%= gpObj.getGpCost() %>원</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<!-- 버튼 시작  -->
					<div class="btn_wrap">
						<button type="button" id="updateSt" class="btn_com btn_white">수정하기</button>
						<button type="submit" id="updateFin" class="btn_com btn_white">수정완료</button>
					</div>
					<!-- 버튼 끝  -->
				</div>
			</div>
		</form>
	</div>
</div>
<!-- 오른쪽 컨텐츠 부분 끝-->

</main>
<!-- 컨텐츠가 들어가는 부분 end -->



<script>
$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(2).addClass("on");
   

   (function(){
     /* kakao map api */
     var container = document.getElementById('meetMap'); //지도를 담을 영역의 DOM 레퍼런스
     var options = { //지도를 생성할 때 필요한 기본 옵션
         center: new kakao.maps.LatLng(<%= gpObj.getGpMeetY() %>, <%= gpObj.getGpMeetX() %>), //지도의 중심좌표.
         level: 3 //지도의 레벨(확대, 축소 정도)
       };

     var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴

     var markerPosition  = new kakao.maps.LatLng(<%= gpObj.getGpMeetY() %>, <%= gpObj.getGpMeetX() %>); 
     var marker = new kakao.maps.Marker({
    	    position: markerPosition
    	});

     marker.setMap(map);

   })();
   
   // 수정 버튼
   $("#updateSt").on("click",function(){
	  $(this).hide();
	  $("#updateFin").show();
	  $(".noticeCnt").prop("readonly", false);
   });
});

</script>

<%@ include file="../../../inc/user/footer.jsp"%>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>



