<%@page import="org.omg.CORBA.ACTIVITY_COMPLETED"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.guide.model.vo.*,com.edr.common.model.vo.*, java.util.*"%>
<%
	GuideDetail guideDetail = (GuideDetail) session.getAttribute("guideDetail");
	String[] langArr = (String[]) request.getAttribute("langArr");
	ArrayList<Local> localList = (ArrayList<Local>) request.getAttribute("localList");
	ArrayList<Local> localList2 = new ArrayList<>();
	int count = 0;
	for(int i = 0; i < localList.size(); i++){
		localList2.add(localList.get(i));
		if( localList2.get(0).equals(localList.get(i+1))){
			count++;
			System.out.println("localList["+i+"] : " + localList.get(i) + "localList2["+i+"] : " + localList2.get(i));
			break;
		}else{
			
		}
		
		
	}
	System.out.println("count: " + count);
	for(int i = 0; i < localList2.size(); i++){
		
		System.out.println("localList2["+i+"] : " + localList2.get(i));
		
	}
	
	Attachment at = (Attachment) request.getAttribute("at");

	String[] phoneArr = guideDetail.getPhone().split("\\)");
	String[] phoneArr2 = guideDetail.getPhone2().split("\\)");
%>
<style>
	#editBtn {
		margin: 0 auto;
	    height: 50px;
	    background-color: #171f57;
	    color: white;
	    text-align: center;
	    line-height: 50px;
	    border: none;
	}
</style>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<!-- 팝업관련css -->
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/user/hyeonjuStyle.css">
<!-- kakao map api link -->
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2&libraries=services"></script>
<!-- 주소 검색 api -->
<script
	src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- 컨텐츠가 들어가는 부분 start -->
<div class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit gset">계정 관리</h4>
			<div class="sub_ctn">
				<!-- 영역분리 시작 -->
				<div class="layout_3_wrap">
					<!-- 내용 -->
					<div class="main_cnt">
						<form action="<%=request.getContextPath()%>/guideProfileContents.gd" method="post">
							<div>
								<img src="/edr/uploadFiles/<%=at.getChangeName()%>"
									alt="My Image" style="width: 200px; height: 170px;">

							</div>
							<div class="main_cnt_list clearfix">
								<!--                mno가져오기 -->
								<input type="hidden" name="mno" value="<%=guideDetail.getMno()%>">

								<h4 class="title set">이름</h4>
								<div class="box_input tinfo"><%=guideDetail.getGname()%></div>

							</div>
							<div class="main_cnt_list clearfix">
								<h4 class="title set">연락처</h4>

								<select name="country" id="country" class="country"
									style="float: left; width: 60px; height: 46px;">
									<option value="+82">+82</option>
									<option value="+850">+850</option>
									<option value="+84">+84</option>
									<option value="+852">+852</option>
									<option value="+63">+63</option>
									<option value="+61">+61</option>
									<option value="+1">+1</option>
									<option value="+33">+33</option>
									<option value="+31">+31</option>
									<option value="+32">+32</option>

								</select> <input class="box_input tinfo" type="text" name="phonenum1"
									id="" value="<%=phoneArr[1]%>">
							</div>
							<script>
								$("#country").find("option").each(function () {
									if(<%=phoneArr[0]%> == $(this).val()){
										$(this).prop("selected","selected");
									}
								});
							</script>
							<div class="main_cnt_list clearfix">
								<h4 class="title set">비상연락처</h4>
								<select name="country2" id="country2" class="country2"
									style="float: left; width: 60px; height: 46px;">
									<option value="+82">+82</option>
									<option value="+850">+850</option>
									<option value="+84">+84</option>
									<option value="+852">+852</option>
									<option value="+63">+63</option>
									<option value="+61">+61</option>
									<option value="+1">+1</option>
									<option value="+33">+33</option>
									<option value="+31">+31</option>
									<option value="+32">+32</option>



								</select> <input class="box_input tinfo" type="text" name="phonenum2"
									id="" value="<%=phoneArr2[1]%>">


								<script>
				                  	$("#country2").find("option").each(function () {
										if(<%=phoneArr2[0]%> ==$(this).val()){
											$(this).prop("selected","selected");
										}
									});
				                  
				                  
				                  </script>
							</div>
<%-- 							<div class="main_cnt_list clearfix">
								<h4 class="title set">활동지역</h4>
								<div class="box_input tinfo">

									<%
										for (int i = 0; i < localList.size(); i++) {
									%>

									<select name="activity" id="activity<%=i%>" class="activity">
										<option value="1">서울</option>
										<option value="2">경기</option>
										<option value="3">경상</option>
										<option value="4">강원</option>
										<option value="5">전라</option>
										<option value="6">충청</option>
										<option value="7">제주</option>
										<option value="8">부산</option>
										<option value="9">대구</option>
										<option value="10">대전</option>
										<option value="11">광주</option>
										<option value="12">인천</option>
										<option value="13">울산</option>
									</select>

									<%
										}
									%>
									<script>
										var activity = $(".activity").length;
										//console.log($(".activity"));
										//console.log(activity);
										<%for (int i = 0; i < localList.size(); i++) { %>
											$(".activity").eq(<%=i%>).find("option").each(function(){
												if($(this).val() == "<%=localList.get(i).getLocalNo()%>") { //this.val()이면 숫자고
														$(this).prop("selected", true);
													}
											});
											console.log("activicy[i]: " + activity[name]);
										<%}%>
									</script>
								</div>
							</div> --%>
							
							<div class="main_cnt_list clearfix">
						<h4 class="title set">활동지역</h4>
						<div class="box_input tinfo" id="localVal">
						<% 
						String local="";
						for(int j = 0; j < localList2.size(); j++) {
								if( j == localList2.size() - 1) {
									local += localList.get(j).getLocalName();
						
								}else{
									local += localList.get(j).getLocalName() + ", ";
								}
							}
						local = local.trim();
						%>
						<%=local %>
						
						</div>
						<input type="hidden" value="" name="localhidden" id="localhidden">
						<script>
						$(function(){
							$("#localhidden").val($("#localVal").text());
						});
						</script>
					</div>
							
							
							

							<%
								String add[] = guideDetail.getAddress().split("\\/");

								for (int i = 0; i < add.length; i++) {
									System.out.println("add[" + i + "]" + add[i]);
								}
							%>
							<h3 class="sub_tit">주소</h3>
							<div class="sub_cnt">
								<div class="add_t_cnt clearfix">
									<table>
										<tr>
											<td class="postco"><label>우편번호 </label></td>
											<!-- 									placeholder="우편번호" -->
											<td><input type="text" name="postcode" id="postcode"
												class="ipt_txt ipt_add_1" value="<%=add[0]%>" readonly>
											</td>
											<td><label>도로명 주소</label></td>
											<td><input type="text" name="roadAddress"
												id="roadAddress" class="ipt_txt ipt_add_2"
												value="<%=add[1]%>" readonly></td>
										</tr>
										<tr>
											<td><label>상세주소</label></td>
											<td><input type="text" name="detailAddress"
												id="detailAddress" class="ipt_txt ipt_add_3"
												value="<%=add[2]%>"> <input type="hidden"
												name="addressX" id="addressX"> <input type="hidden"
												name="addressY" id="addressY"></td>
											<td><a href="javascript: void(0);"
												class="btn_com btn_blue add_srch_btn" id="addressApi">검색</a>
											</td>
										</tr>
									</table>
									<div class="btn_wrap add_srch_wrap"></div>
								</div>
							</div>
							<div class="main_cnt_list clearfix">
								<h4 class="title set">카카오톡 ID</h4>
								<%--                   <div class="box_input tinfo"><%= guideDetail.getKakaoId() %></div> --%>
								<input class="box_input tinfo" type="text" name="kakaoId" id=""
									value="<%=guideDetail.getKakaoId()%>">
							</div>
							<div class="main_cnt_list clearfix">
								<h4 class="title set">가능 언어</h4>
								<!-- 체크박스 시작 -->
								<div class="box_chk lang">
									<%=guideDetail.getLang()%>
								</div>
								<br> <br>
								<div class="main_cnt_list clearfix">
									<div class="lang_etc_wrap">
		
										<%
											String[] lan = guideDetail.getLang().split(",");
										%>
		
										<%
											for (int i = 0; i < lan.length; i++) {
		
												System.out.println("lan[" + i + "]" + lan[i]);
		
											}
										%>
									</div>
								</div>
							</div>

							<div class="main_cnt_list clearfix">
								<h4 class="title set">소개</h4>
								<div class="box_inputtinfo">
		
									<textarea name="intro" rows="" cols=""
										style="width: 600px; height: 200px;"><%=guideDetail.getIntroduce()%></textarea>
								</div>
							</div>

							<div align="center">
								<div class="btn_com btn_white"
									style="float: left; height: 50px; width: 160px; margin-left: 23%;">취소</div>
								<div class="btn_com btn_blue" style="margin-left: 6%; width: 156px; height: 50px;">
									<input type="submit" id="editBtn" value="수정하기">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- 영역분리 끝 -->

	<!-- 오른쪽 컨텐츠 부분 끝-->
	</div></div>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

	<script>
		/* $(document).ready(function()){
		   $('.img_btn').on("click",function(){
		      $('.cloneOther').clone(true).appendTo('h1');
		   });
		} */

		$(document).ready(
				function() {
					$(".img_btn").click(
							function() { //click this

								//자바스크립트영역
								$("<input type='text' class='ipt_txt_lang'>")
										.clone().appendTo('.here');

								//$('.ipt_txt_lang:first').clone(false).appendTo('.here');

							})

				});
	</script>
	<!-- 컨텐츠가 들어가는 부분 end -->
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>
	<script
		src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	<script>
		$(function() {
			// 열리지 않는 메뉴
			$(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");

			// 열리는 메뉴
			//             $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
			//             $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(4).addClass("on");
		});

		$(function() {
			/* 팝업 예시 */
			$('.editProfile').magnificPopup({
				type : 'ajax'
			});
		});

		// 우편번호 검색
		(function() {
			//주소-좌표 변환 객체를 생성
			var geocoder = new daum.maps.services.Geocoder();

			$("#addressApi")
					.on(
							"click",
							function() {
								console.log("123");
								new daum.Postcode(
										{
											oncomplete : function(data) {
												var addr = data.address; // 최종 주소 변수
												var roadAddr = data.roadAddress; // 도로명 주소 변수

												// 주소 정보를 해당 필드에 넣는다.
												document
														.getElementById('postcode').value = data.zonecode;
												document
														.getElementById("roadAddress").value = roadAddr;
												// 주소로 상세 정보를 검색
												geocoder
														.addressSearch(
																data.address,
																function(
																		results,
																		status) {
																	// 정상적으로 검색이 완료됐으면
																	if (status === daum.maps.services.Status.OK) {

																		var result = results[0]; //첫번째 결과의 값을 활용

																		// 해당 주소에 대한 좌표를 받아서
																		var coords = new daum.maps.LatLng(
																				result.y,
																				result.x);

																		$(
																				"#addressX")
																				.val(
																						coords.Ga);
																		$(
																				"#addressY")
																				.val(
																						coords.Ha);
																	}
																});
											}
										}).open();
							});

			$("#postcode, #roadAddress").on("click", function() {
				$("#addressApi").trigger("click");
			});

		})();
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>