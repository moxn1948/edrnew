<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.common.model.vo.*, java.util.*, com.edr.customProgram.model.vo.*"%>

<%
	
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
 	PageInfo pi = (PageInfo) request.getAttribute("pi");
 	String str =(String)request.getAttribute("str");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	System.out.println("필터jsp : " + str);
	if(str.equals("WAIT")) {
		str="대기중";
	}else if(str.equals("GWAIT")) {
		str="가이드배정중";
	}else if(str.equals("ENDPAY")) {
		str="결제완료";
	}
	 
%>

 <%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css"
	href="../../../../css/user/kijoonStyle.css">
 

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/guidePageSubMenu.jsp" %> 
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">맞춤 프로그램 목록</h4>
			<input type="hidden" name="mno" value="<%= loginUser.getMno()%>">
        <div class="filter">
			<select name="listfil" id="listfil" class="listfil">
				<option selected disabled hidden><%= str %></option>
				<option value="전체보기">전체 보기</option>
				<option value="WAIT">대기중</option>
				<option value="GWAIT">가이드배정중</option>
				<option value="ENDPAY">결제완료</option>				

			</select>
		</div>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
				
					<table class="tbl" id="listArea">
						<colgroup>
							<col width="10%">
							<col width="30%">
							<col width="10%">
							<col width="20%">
							<col width="20%">
							<col width="10%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>가이드 기준</th>
							<th>최대 금액</th>
							<th>신청 일시</th>
							<th>상태</th>
						</tr>
						<% for (int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i);
        %>
        	<tr class="tbl_cnt">

						<td><%= hmap.get("idxnum") %></td>
						<td><%= hmap.get("cpName") %></td>
						<td><%= hmap.get("age") %>/<% String str1 = "";
														if(hmap.get("gender").equals("M")) {
															str1="남자";
														}else if(hmap.get("gender").equals("F")) {
															str1="여자";
														}else if(hmap.get("gender").equals("ALL")) {
															str1="무관";
														}%>
														<%= str1 %></td>
						<td><%= hmap.get("cost") %></td>
						<td><%= hmap.get("cpDate") %></td>
						<td><% String str2=""; 
								if(hmap.get("state").equals("WAIT")) {
									str2="대기중";
								} else if(hmap.get("state").equals("GWAIT")) {
									str2="가이드배정중";
									
								}else if(hmap.get("state").equals("ENDPAY")){
									str2="결제완료";
								}%>
								<%= str2 %>
						<input type="hidden" id="cpNo" name="cpNo" value="<%= hmap.get("cpNo")%>"></td>
						

									
							<% }%>

					</table>
				</div>
				<!-- table 끝 -->
				
			</div>
			
			  <!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%= request.getContextPath()%>/selectCp.cp?currentPage=<%=currentPage - 1%>&mno=<%= loginUser.getMno() %>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%= request.getContextPath()%>/selectCp.cp?currentPage=<%=p%>&mno=<%= loginUser.getMno() %>"><%=p %></a></li>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/selectCp.cp?currentPage=<%=currentPage + 1%>&mno=<%= loginUser.getMno() %>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<script>
$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(0).addClass("on");
   
   // id는 테이블에 걸어주세요
   $("#listArea td").mouseenter(function() {
	   $(this).parent().css({"background":"#f1f1f1", "cursor":"pointer"});
   }).mouseout(function(){
	   $(this).parent().css({"background":"white"})
   }).click(function(){
	   // 상세보기로 이동이니 사용안하려면 삭제하세요!
	   var cpNo = $(this).parent().children().find("#cpNo").val()
	   location.href="<%= request.getContextPath()%>/selectOne.cp?cpNo=" + cpNo;
   });
   
   
   
   
   
});
<%-- 	$(".filter").change(function() {

		$(this).prop("selected", true);
		var str = $(this).val();
		console.log(str);
		if(str == "전체보기") {
			location.href="<%=request.getContextPath()%>/selectCp.cp?mno=<%=  loginUser.getMno()%>";
		}else {
			
		}
	})
	
	$(".listfil").change(function(){
  		var str = $(this).val();
  		location.href="<%=request.getContextPath()%>/selectCpFilter.cp?str=" +str +"&mno=<%= loginUser.getMno()%>";
   }); --%>
   
   
   $(".listfil").change(function(){
  	 $(this).prop("selected", true);
 		var str = $(this).val();
 		console.log(str);
 		if(str == "전체보기") {
 			location.href="<%=request.getContextPath()%>/selectCp.cp?mno=<%=  loginUser.getMno()%>";
 		}else {
 			location.href="<%=request.getContextPath()%>/selectCpFilter.cp?str=" +str +"&mno=<%= loginUser.getMno()%>";	
 		}
 		
  });
   
</script>


<%@ include file="../../../inc/user/footer.jsp" %>
