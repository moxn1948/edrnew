<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.*, com.edr.common.model.vo.Attachment" %>


<%
	CpRequest cr = (CpRequest) request.getAttribute("cr");
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<CpLang> listLang = (ArrayList<CpLang>) request.getAttribute("listLang");
	

	/* int result = (Integer) request.getAttribute("result") + 1; */
	int cpNo = (Integer) request.getAttribute("cpNo");
	/* int estNo = (Integer) request.getAttribute("estNo"); */
	
	
	
	ArrayList<CpGuideEst> estList = (ArrayList<CpGuideEst>) request.getAttribute("estList");
	ArrayList<CpPriceDetatil> priceList = (ArrayList<CpPriceDetatil>) request.getAttribute("priceList");

	
	String state = (String) request.getParameter("state");

	HashMap<String, Object> cpDetail = (HashMap<String, Object>) request.getAttribute("cpDetail"); 

	CpRequest cpRe = (CpRequest) cpDetail.get("cpRe");
	ArrayList<CpLang> cpLa = (ArrayList<CpLang>) cpDetail.get("cpLa");
	
	String lang = "";
	for(int i = 0; i < cpLa.size(); i++){
		if(i != cpLa.size() - 1){
			lang += cpLa.get(i) + ", ";
		}else{
			lang += cpLa.get(i);
		}
	}
	
	String localName = "";
	switch (cpRe.getLocalNo()) {
	case 1: localName = "서울"; break;
	case 2: localName = "경기"; break;
	case 3: localName = "경상"; break;
	case 4: localName = "강원"; break;
	case 5: localName = "전라"; break;
	case 6: localName = "충청"; break;
	case 7: localName = "제주"; break;
	case 8: localName = "부산"; break;
	case 9: localName = "대구"; break;
	case 10: localName = "대전"; break;
	case 11: localName = "광주"; break;
	case 12: localName = "인천"; break;
	case 13: localName = "울산"; break;
	}
	
	switch(state){
	case "WAIT" : state = "배정중"; break; 
	case "GWAIT" : state = "배정완료"; break; 
	case "ENDPAY" : state = "결제완료"; break; 
	}
	
	
	ArrayList<CpGuideDetail> cpGdetail = (ArrayList<CpGuideDetail>) cpDetail.get("cpGdetail");
	CpGuideEst cpGest = (CpGuideEst) cpDetail.get("cpGest");
	String gname = (String) cpDetail.get("gname");
	ArrayList<CpPriceDetatil> cpPdetail = (ArrayList<CpPriceDetatil>) cpDetail.get("cpPdetail");
	Attachment cpAttr = (Attachment) cpDetail.get("cpAttr");
	
	int cpEstNo = 0;
	if(cpGest != null){
		cpEstNo = cpGest.getEstNo();
	}
	
	int include = 0;
	int notclude = 0;
	if(cpPdetail != null){
		
		for(int i = 0; i < cpPdetail.size(); i++){
			if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
				include++;
			}else{
				notclude++;
			}
		}

	}
	
%>



<%@ include file="../../../inc/user/subHeader.jsp"%>
<!-- timepicker -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/shjeon.css">

<style>
.container #calendar {
	height: 150px;
}

.container #notice {
	height: 150px;
} 
table{font-size: 16px;}
table tr{vertical-align: top;}
table td div:last-child{padding-bottom: 20px;}
.customDetailArea{padding-bottom : 20px;}
</style>




<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>

	<div class="sub_ctn_wrap">

		<h4 class="sub_ctn_tit">배정된 맞춤 프로그램</h4>
		<div class="sub_ctn">

			<div class="layout_3_wrap">
				<h1 class="main_title" align="center">투어객 요청사항</h1>
				<div class="main_cnt">
					<div class="main_cnt_list clearfix">
				<h4 class="title">지역</h4>
				<div class="box_input">
					<span class="box_txt"><%=localName %></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">팀인원</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpPerson() %>명</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">시작일자</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpSdate() %></span>
				</div>
			</div>

			<br>
			<div class="main_cnt_list clearfix">
				<h4 class="title">종료일자</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpEdate() %></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">총기간</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpDetail.get("tday") %>일</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">최대 가능 금액</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpCost() %>원</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">요구언어</h4>
				<div class="box_input">
					<span class="box_txt"> 
					<%= lang %>
					</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 성별</h4>
				<div class="box_input">
					<span class="box_txt">
						<%
						String gender = "";
						if(cpRe.getCpGender().equals("M")){
							gender = "남성";
						}else{
							gender = "여성";
						}
						%>
						<%= gender %>
					</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">제목</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpTitle() %></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">내용</h4>
				<div class="box_input">
					<span class="box_txt"><%= cpRe.getCpCnt() %></span>
				</div>
			</div>
			
		</div>
		<hr>

		
			<div class="customDetailArea" style="margin-top: 20px;">
				<h1 align="center" class="request" style="padding:60px 0;">답변사항</h1>
				<table>
					<tr>
						<td class="customTd">총금액</td>
						 <td><%= cpGest.getTotalCost() %>원</td>
					</tr>
					<tr>
						<td class="customTd">포함사항</td>
						<td>
						<% 
						String includeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
							%>
							<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
							<%
							}
						}%>
						<%= includeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">불포함사항</td>
						<td>
						
						<% 
						String notcludeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("NOTCLUDE")){
								%>
								<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
								<%
							}
						}%>
						<%= notcludeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">만나는 시간</td>
						<td><%= cpGest.getMeetTime() %></td>
					</tr>
					<tr>
						<td class="customTd">만나는 장소</td>
						<td><%= cpGest.getMeetArea() %></td>
					</tr>
					<%for(int i = 0; i < cpGdetail.size(); i ++) {%>
					<tr>
						<td class="customTd"><%= ((CpGuideDetail) cpGdetail.get(i)).getCpDay() %> DAY</td>
						<td><%= ((CpGuideDetail) cpGdetail.get(i)).getCpCnt() %></td>
					</tr>
					<%} %>
					<tr>
						<td class="customTd">특이사항</td>
						<td><%= cpGest.getUniqueness() %></td>
					</tr>
					<tr>
						<td class="customTd">첨부파일</td>
						<td><a href="<%=request.getContextPath()%>/myCpAttaDownload.cp?estNo=<%= cpEstNo %>"><%= cpAttr.getOriginName() %></a></td>
					</tr>
				</table>
			</div>
		
			
			
			<div class="btn_wrap">
				<%-- <a class="btn_com btn_white" style="display: inline-block;" href="<%= request.getContextPath() %>/updateMyCpBack.cp?estNo=<%= estNo %>&cpNo=<%= cpNo %>&mno=<%= loginUser.getMno() %>">거절하기</a> --%>
				<button type="button" class="btn_com btn_blue">결제하기</button>
			</div>
		
			
			<!-- table 끝 -->
		</div>
	</div>
	<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<!-- timepicker -->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
	function inclusion() {
		$("#inclusionAdd").clone().eq(0).appendTo(".box_input1");

	}
	function notInclusion() {
		$("#notInclusionAdd").clone().eq(0).appendTo(".box_input2");
	}
	
	function filePlus() {
		$("#fileAdd").clone().eq(0).appendTo(".box_input3");
	}

$(function(){
	   // 열리지 않는 메뉴
	   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
	   
	   // 열리는 메뉴
	   $(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
	   $(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
	   

       // 만나는 시간 timepicker
       $('.timepicker').timepicker({
          timeFormat: 'h:mm p',
          interval: 60,
          dynamic: false,
          dropdown: true,
          scrollbar: true,
          change: function(time) {
             
                // 타임피커 변경되는 부분
                var element = $(this), text;
                console.log(time);
                
                // get access to this Timepicker instance
                /* var timepicker = element.timepicker();
                text = 'Selected time is: ' + timepicker.format(time);
                element.siblings('span.help-line').text(text); */
            }
       });
	   
	});
	
	
</script>

</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp"%>
