<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.product.model.vo.*, com.edr.generalProgram.model.vo.*, com.edr.calculate.model.vo.*,com.edr.payment.model.vo.*, com.edr.common.model.vo.*"%>
<%
	ArrayList<Integer> calNo = (ArrayList)request.getAttribute("calNo");
	ArrayList<Product> productEpiList = (ArrayList)request.getAttribute("productEpiList");
	ArrayList<Gp> gpNameList = (ArrayList)request.getAttribute("gpNameList");
	ArrayList<Payment> costList = (ArrayList)request.getAttribute("costList");
	ArrayList<Product> productDateList = (ArrayList)request.getAttribute("productDateList");
	
	PageInfo pi = (PageInfo)request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
	<%@ include file="../../../inc/user/subHeader.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
	<!-- 컨텐츠가 들어가는 부분 start -->
	<main class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">정산 대기 내역</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="10%">
							<col width="20%">
							<col width="25%">
							<col width="15%">
							<col width="20%">
							<col width="10%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>회차</th>
							<th>프로그램 명</th>
							<th>정산 금액</th>
							<th>투어 일시</th>
							<th>상태</th>
						</tr>
						<% for(int i = 0; i < calNo.size(); i++ ){ %>
						<tr class="tbl_cnt">
							<td><%= calNo.get(i) %></td>
							<td><%= productEpiList.get(i).getEpi() %></td>
							<td><%= gpNameList.get(i).getGpName() %></td>
							<td><%= costList.get(i).getPayCost() %></td>
							<td><%= productDateList.get(i).getPdate() %></td>
							<td><span class="squ_tbl">대기중</span></td>
						</tr>
						<% } %>
					</table>
				</div>
				<!-- table 끝 -->
			</div>
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
		<!-- 페이저 시작 -->
      <div class="pager_wrap">
        <ul class="pager_cnt clearfix">
          <% if( currentPage <= 1 ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003C;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.wcalc?currentPage=<%= currentPage-1 %>&mno=<%= loginUser.getMno()%>">&#x003C;</a></li>
          <% } %>
          <% 
          	
          	for(int p = startPage; p <= endPage; p++ ){ 
          		if(p == currentPage){
          			%>
			          <li class="pager_com pager_num on"><a href=""><%= p %></a></li>
			        <% 
          		}else{
          			%>
			          <li class="pager_com pager_num"><a href="<%= request.getContextPath() %>/selectList.wcalc?currentPage=<%= p %>&mno=<%= loginUser.getMno()%>"><%= p %></a></li>
          			<%
          		}
  		        %>
          <% } %>
  		  <% if( currentPage >= maxPage ){ %>
          <li class="pager_com pager_arr prev"><a href="">&#x003E;</a></li>
          <% }else{ %>
          <li class="pager_com pager_arr prev"><a href="<%= request.getContextPath() %>/selectList.wcalc?currentPage=<%= currentPage+1 %>&mno=<%= loginUser.getMno()%>">&#x003E;</a></li>
          <% } %>
        </ul>
      </div>
	</div>
	</main>
	<!-- 컨텐츠가 들어가는 부분 end -->
	<script>
	$(function(){
	   // 열리지 않는 메뉴
	   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
	   
	   // 열리는 메뉴
	   $(".sub_menu_cnt .sub_menu_list").eq(3).addClass("on").addClass("open");
	   $(".sub_menu_cnt .sub_menu_list").eq(3).find(".sub_side_list").eq(1).addClass("on");
	});
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>