<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>


<% if(loginUser != null && loginUser.getMtype().equals("GUIDE")){ %>

<!-- timepicker -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">

<!-- kakao map api link -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2&libraries=services"></script>
<!-- 주소 검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>

<main class="container programAdd">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/guidePageSubMenu.jsp" %>
		<form id="formSubmit" action="<%= request.getContextPath()%>/insertNew.gp" method="post" encType="multipart/form-data" onsubmit="return txtFieldCheck()">
			<!-- 오른쪽 컨텐츠 부분 시작-->
			<input type="hidden" name="mno" value="<%= loginUser.getMno() %>">
			<div class="sub_ctn_wrap">
				<h4 class="sub_ctn_tit">프로그램 신청</h4>
				<div class="sub_ctn">
					<div class="sub_ctn_list">
						<div class="sub_cnt_wrap">
							<p class="sub_tit">프로그램 명</p>
							<div class="sub_cnt">
								<label style="display:none;" for="gpName">프로그램 명</label><input type="text" name="gpName" id="gpName" class="iptArea ipt_txt" placeholder="30자 이내로 입력해주세요." maxlength="30">
							</div>
						</div>
						<div class="sub_cnt_wrap">
							<p class="sub_tit">프로그램 지역</p>
							<div class="sub_cnt">
								<label style="display:none;" for="guideLocal">프로그램 지역</label>
								<select name="localNo" id="guideLocal" class="ipt_sel gp_local_sel">
									<option value="" selected hidden>지역 선택</option>
								</select>
							</div>
						</div>
						<div class="sub_cnt_wrap">
							<p class="sub_tit">프로그램 설명</p>
							<div class="sub_cnt">
								<label style="display:none;" for="gpDescr">프로그램 설명</label><textarea name="gpDescr" id="gpDescr iptArea2" class="iptArea ipt_txt_box" placeholder="100자 이내로 프로그램에 대한 설명을 작성해주세요." maxlength="100"></textarea>
							</div>
						</div>
					</div>
					<div class="sub_ctn_list">
						<div class="sub_cnt_wrap">
							<p class="sub_tit">총 프로그램 기간</p>
							<div class="sub_cnt">
								<select name="gpTday" id="gpTday" class="ipt_sel total_program_day">
									<option value="" hidden>기간 선택</option>
									<option value="1">1일</option>
									<option value="2">2일</option>
									<option value="3">3일</option>
									<option value="4">4일</option>
									<option value="5">5일</option>
									<option value="6">6일</option>
									<option value="7">7일</option>
								</select>
							</div>
						</div>
						<div class="sub_cnt_wrap">
							<p class="sub_tit">참가 가능 인원</p>
							<div class="sub_cnt">
								<select name="gpMin" id="gpMin" class="ipt_sel ipt_min_per">
									<option value="" hidden>최소 인원</option>
									<option value="1">1명</option>
									<option value="2">2명</option>
									<option value="3">3명</option>
									<option value="4">4명</option>
									<option value="5">5명</option>
									<option value="6">6명</option>
									<option value="7">7명</option>
									<option value="8">8명</option>
									<option value="9">9명</option>
									<option value="10">10명</option>
									<option value="11">11명</option>
									<option value="12">12명</option>
									<option value="13">13명</option>
									<option value="14">14명</option>
									<option value="15">15명</option>
								</select>
								<select name="gpMax" id="gpMax" class="ipt_sel ipt_max_per max_person_sel">
									<option value="" hidden>최대 인원</option>
								</select>
							</div>
							<div class="sub_cnt_wrap">
								<p class="sub_tit">프로그램 진행 가능 언어</p>
								<div class="sub_cnt">
									<div class="chk_wrap">
										<input type="checkbox" class="ipt_chk" name="gpLang" id="ko" value="한국어" checked onclick="return false;"><label for="ko">한국어</label>
										<input type="checkbox" class="ipt_chk" name="gpLang" id="en" value="영어"><label for="en">영어</label>
										<input type="checkbox" class="ipt_chk" name="gpLang" id="ch" value="중국어"><label for="ch">중국어</label>
										<input type="checkbox" class="ipt_chk" name="gpLang" id="ger" value="독일어"><label for="ger">독일어</label>
									</div>
									<div class="lang_etc_wrap">
										<span>그 외</span>
										<div class="lang_etc_cnt">
											<input type="text" name="gpLangEtc" id="" class="ipt_txt ipt_lang_etc">
										</div>
										<a href="javascript: void(0);" class="add_btn lang_add_btn">+</a>
										<a href="javascript: void(0);" class="add_btn minus_btn lang_minus_btn">-</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					 <div class="sub_ctn_list">
						<div class="sub_cnt_wrap">
							<p class="sub_tit">만나는 시간</p>
							<div class="sub_cnt">
								<input type="text" name="gpMeetTime" class="timepicker meet_time ipt_txt" placeholder="만나는 시간을 선택해주세요" autocomplete="off">
							</div>
						</div>
						<div class="sub_cnt_wrap">
							<p class="sub_tit">만나는 장소</p>
							<div class="sub_cnt">
								<div class="add_t_cnt clearfix">
									<input type="text" name="postcode" id="postcode" class="ipt_txt ipt_add_1" placeholder="우편번호" readonly>
									<input type="text" name="roadAddress" id="roadAddress" class="ipt_txt ipt_add_2" placeholder="도로명주소" readonly>
								</div>
								<div class="add_b_cnt clearfix">
									<input type="text" name="detailAddress" id="detailAddress" class="ipt_txt ipt_add_3" placeholder="상세주소">
									<input type="hidden" name="addressX" id="addressX">
									<input type="hidden" name="addressY" id="addressY">
									<div class="btn_wrap add_srch_wrap">
										<a href="javascript: void(0);" class="btn_com btn_blue add_srch_btn" id="addressApi">검색</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sub_ctn_list day_cnt_list" id="dayCntList">
						<!-- 일정 추가 : common/day_ctn.jsp -->
					</div>
					<div class="sub_ctn_list">
						<div class="sub_cnt_wrap notice_wrap">
							<p class="sub_tit">특이사항</p>
							<div class="sub_cnt">
								<textarea name="gpNotice" id="" class="ipt_txt_box ipt_txt_notice" placeholder="특이사항을 작성해주세요. 해당 내용은 추후에 수정 가능합니다."></textarea>
							</div>
						</div>
					</div>
					<div class="sub_ctn_list">
						<div class="sub_cnt_wrap price_wrap">
							<p class="sub_tit">프로그램 금액</p>
							<div class="sub_cnt price_ctn">
								<div class="price_ctn_wrap">
									<div class="price_list price_default_list">
										<select class="ipt_sel" name="includeYn0">
											<option class="" value="include" >포함사항</option>
										</select>
										<input type="text" name="gpCategory" class="ipt_txt ipt_price_tit" placeholder="" value="가이드비" readonly>
										<input type="text" name="gpPrice" class="ipt_txt ipt_price_num" placeholder="금액" onKeyup="totalPrice(this);" value="0">
									</div>
									<div class="price_list">
										<select class="ipt_sel ipt_clude_sel" name="includeYn1">
											<option class="" value="include">포함사항</option>
											<option class="" value="notclude">불포함사항</option>
										</select>
										<input type="text" name="gpCategory" class="ipt_txt ipt_price_tit" placeholder="항목 작성">
										<input type="text" name="gpPrice" class="ipt_txt ipt_price_num" placeholder="금액" onKeyup="totalPrice(this);" value="0">
									</div>
								</div>
								<a href="javascript: void(0);" class="add_btn price_btn">+</a>
								<a href="javascript: void(0);" class="add_btn minus_btn price_minus_btn">-</a>
								<ul class="total_price_wrap clearfix">
									<li class="total_price_cnt">총 금액</li>
									<li class="total_price_cnt" id="totalPrice"></li>
								</ul>
								<input type="hidden" name="gpCost" id="totalPriceHidden">
							</div>
						</div>
					</div>
			         <!-- 버튼 시작 -->
	    		     <div class="btn_wrap">
						<button type="button" class="btn_com btn_white">취소하기</button>
						<button type="submit" id="submitBtn" class="btn_com btn_blue">신청하기</button>
					</div>
					<!-- 버튼 끝 -->
				</div>
			</div>
			<!-- 오른쪽 컨텐츠 부분 끝-->
		</form>
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<!-- timepicker -->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
	/* $("#submitBtn").click(function(){
	
		var result = txtFieldCheck() == true ? $("#formSubmit").submit() : false;
	

	});
 */


	// 열리는 메뉴
	$(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
	$(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(0).addClass("on");

	// 프로그램 진행 가능 언어 추가
	$(".lang_add_btn").on("click", function(){
		var cloneLang = $(".ipt_lang_etc").eq(0).clone().val("");

		$(".lang_etc_cnt").append(cloneLang);
	});

	
	// 프로그램 진행 가능 언어 삭제
	$(".lang_minus_btn").on("click", function(){
		var idx = $(".lang_etc_cnt").find("input").length;
		if(idx > 1){
			$(".lang_etc_cnt").find("input").eq(idx-1).remove();
		}
	});

	/* // 포함 사항  추가
	$(".in_y_btn").on("click", function(){
		var cloneLang = $(".ipt_in_y").eq(0).clone().val("");

		$(".in_y_cnt").append(cloneLang);
	});
	// 포함 사항  삭제
	$(".in_y_minus_btn").on("click", function(){
		var idx = $(".in_y_cnt").find("input").length;

		if(idx > 1){
			$(".in_y_cnt").find("input").eq(idx-1).remove();
		}
	});
	
	// 불 포함 사항  추가
	$(".in_n_btn").on("click", function(){
		var cloneLang = $(".ipt_in_n").eq(0).clone().val("");

		$(".in_n_cnt").append(cloneLang);
	});
	// 불포함 사항 삭제
	$(".in_n_minus_btn").on("click", function(){
		var idx = $(".in_n_cnt").find("input").length;

		if(idx > 1){
			$(".in_n_cnt").find("input").eq(idx-1).remove();
		}
	}); */
	
	// 만나는 시간 timepicker
	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 60,
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});
	
	var priceIdx = 1;
	
	// 프로그램 금액 추가
	$('.price_btn').on("click",function(){
		priceIdx++;
		
		var cloneLang = $(".price_list").eq(1).clone();
		cloneLang.find(".ipt_sel").prop("name", "includeYn"+priceIdx);
		cloneLang.find(".ipt_txt").eq(0).val("");
		cloneLang.find(".ipt_txt").eq(1).val("");
		cloneLang.find(".ipt_txt").eq(1)[0].readOnly = false;
		cloneLang.find(".ipt_txt").eq(1)[0].value = 0;

		$(".price_ctn_wrap").append(cloneLang);

	});

	// 프로그램 금액 삭제
	$(".price_minus_btn").on("click", function(){
		priceIdx--;
		
		var idx = $(".price_ctn_wrap").find(".price_list").length;
		if(idx > 2){
			$(".price_ctn_wrap").find(".price_list").eq(idx-1).remove();
		}
	});

	// 사진 등록 input trigger
	$(document).on("click", ".file_btn", function(){
		$(this).siblings(".ipt_file").trigger("click");
	});

	function loadImg(value){
		var fileName;
		if(value.files && value.files[0]){ // 파일이 업로드 되었는지? , 파일이 업로드 되면 value.files 가 생김
			var reader = new FileReader();
			reader.onload = function(e){ // 스트림이 생성되어서 이벤트가 연결된것 그 스트림으로 부터 경로가  e.target.result

				fileName = value.files[0].name;
			}
			$(value).siblings(".fileArea")[0].innerHTML = value.files[0].name;
			
		}

	}
	
	// 일정 추가
	(function(){
		
		var dayDetail = new Array(0, 0, 0, 0, 0, 0, 0);

		// 기간 만큼 추가
		$.ajax({
    			url:"../common/day_ctn.jsp",
       			success:function(result) {
				var day = $(".total_program_day").val();
				$("#dayCntList").append("<p class='empty_day'>기간을 선택해주세요.</p>");
				
				$(".total_program_day").on("change", function(){
				
				$("#dayCntList").text("");
   				day = $(this).val();
				// dayDetail = new Array(0, 0);
				for(var i = 0; i < day; i++){
					$("#dayCntList").append(result);
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".sub_tit").text("Day" + (i+1));
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".placeDay").prop("name", ("placeDayDetail"+(i+1)));
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".timeDay").prop("name", ("timeDayDetail"+(i+1)));
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".descDay").prop("name", ("descDayDetail"+(i+1)));
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".pictureDay").prop("name", ("pictureAtt"+ (i+1) + "_1"));
					$("#dayCntList").find(".day_cnt_wrap").eq(i).find(".pictureDay").addClass("pictureAtt"+ (i+1) + "_1");
				}
				
       	    });
       	  }
       	});

	
		// 일정 개수 추가
		$(document).on("click",".day_add_btn",function(){

			var cloneCom = $(".day_cnt_desc").eq(0).clone();
			cloneCom.find("input").val("");
			cloneCom.find("textarea").val("");

			var idx = $(this).parents(".day_cnt_wrap").index();
			dayDetail[idx]++;
			console.log(dayDetail);
			
			cloneCom.find(".day_cnt_tit").text("일정" + (dayDetail[idx] + 1));
			cloneCom.find(".placeDay").prop("name", ("placeDayDetail"+(idx+1)));
			cloneCom.find(".timeDay").prop("name", ("timeDayDetail"+(idx+1)));
			cloneCom.find(".descDay").prop("name", ("descDayDetail"+(idx+1)));
			cloneCom.find(".pictureDay").prop("name", ("pictureAtt"+ (idx+1) + "_" + (dayDetail[idx] + 1)));
			cloneCom.find(".pictureDay").addClass("pictureAtt"+ (idx+1) + "_" + (dayDetail[idx] + 1));
			cloneCom.find(".fileArea")[0].innerHTML = "";
			$(".day_cnt_wrap .day_main_cnt").eq(idx).append(cloneCom);

			if(dayDetail[idx] == 6){
				$(this).parent(".btn_wrap").remove();
			}
		});
	})();
	
	//  최소 최대 인원
	$(".ipt_min_per").on("change", function(){
		var min = $(this).val();
		
		$(".ipt_max_per").text("");
		$(".ipt_max_per").append("<option value='' hidden>최대 인원</option>");
		 for (var i = min; i <= 15; i++) {
			$(".ipt_max_per").append("<option value=" + i + ">" + i + "명</option>");
			
		}
	});
	

	
	// 지역 ajax
	(function(){
		$.ajax({
			url : "<%=request.getContextPath() %>/selectLocal.gp?mno=<%=loginUser.getMno()%>",
			type : "get",
			success : function(data){
				console.log(data);
				$select = $("#guideLocal");
				$select.find("option").remove();
				
				$select.append("<option value='' hidden>지역 선택</option>");
				for(var key in data){
					var $option = $("<option>");
					$option.val(data[key].localNo);
					$option.text(data[key].localName);
					$select.append($option);
				}
			},
			error : function(data){
				console.log("지역 불러오기 error");
			}
		});
	
	})();
	
	// 우편번호 검색
	(function(){
	    //주소-좌표 변환 객체를 생성
	    var geocoder = new daum.maps.services.Geocoder();

		$("#addressApi").on("click",function(){
		      new daum.Postcode({
		            oncomplete: function(data) {
		                var addr = data.address; // 최종 주소 변수
		                var roadAddr = data.roadAddress; // 도로명 주소 변수

		                // 주소 정보를 해당 필드에 넣는다.
          				    document.getElementById('postcode').value = data.zonecode;
		                document.getElementById("roadAddress").value = roadAddr;
		                // 주소로 상세 정보를 검색
		                geocoder.addressSearch(data.address, function(results, status) {
		                    // 정상적으로 검색이 완료됐으면
		                    if (status === daum.maps.services.Status.OK) {

		                        var result = results[0]; //첫번째 결과의 값을 활용

		                        // 해당 주소에 대한 좌표를 받아서
		                        var coords = new daum.maps.LatLng(result.y, result.x);
		                        
		                        $("#addressX").val(coords.Ga);
		                        $("#addressY").val(coords.Ha);
		                        
		                        console.log(coords.Ga);
		                        console.log(coords.Ha);
		                    }
		                });
		            }
		        }).open();
		});
		
		
		$("#postcode, #roadAddress").on("click", function(){
			$("#addressApi").trigger("click");
		});
		
	})();
	
	$(document).on("change", ".ipt_clude_sel", function(){
	 	if($(this).val() == "notclude"){
	 		console.log($(this).siblings(".ipt_price_num")[0]);
	 		$(this).siblings(".ipt_price_num")[0].readOnly = true;
	 		$(this).siblings(".ipt_price_num")[0].value = 0;
		} else{
	 		$(this).siblings(".ipt_price_num")[0].readOnly = false;
		}
	});
	

	
	// 총 금액 변경
	function totalPrice(thisVal){
		// 가격 input:text에 숫자만 입력
		thisVal.value=thisVal.value.replace(/[^0-9]/g,'');
		var sum = 0;
		for(var i = 0; i < $(".ipt_price_num").length; i++){
			console.log(parseInt($(".ipt_price_num").eq(i).val()));
			
			sum += parseInt($(".ipt_price_num").eq(i).val());
			
		}
		$("#totalPrice").html(sum);
		$("#totalPriceHidden").val(sum);
		
	}

	// 총 금액 변경
	function timeDayNum(thisVal){
		// 가격 input:text에 숫자만 입력
		thisVal.value=thisVal.value.replace(/[^0-9]/g,'');
		
		if(thisVal.value > 24){
			thisVal.value = "";
		}
		
	}

	function txtFieldCheck(){

		// form안의 모든 text type 조회
	
		var txtEle = $(".iptArea");
	
		  
	
		for(var i = 0; i < txtEle.length; i ++){
		
			if("" == $(txtEle[i]).val() || null == $(txtEle[i]).val() || "undefined" == $(txtEle[i]).val() || " " == $(txtEle[i]).val() ){
		
				var ele_id = $(txtEle[i]).attr("id");
			
				var label_txt = $("label[for='" + ele_id +"']").text();
				console.log("id : " + ele_id + ", label : " + label_txt);



				showAlert(ele_id, label_txt);
			
				return false;
		
			}
	
		}
		
		return true;

	}



	function showAlert(ele_id, label_txt){

		alert(label_txt + "을(를) 작성해주세요.");

		$("#" + ele_id).focus();
		
	}



</script>

<%@ include file="../../../inc/user/footer.jsp" %>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>

