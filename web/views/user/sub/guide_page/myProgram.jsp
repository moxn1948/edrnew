<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.Gp, com.edr.common.model.vo.PageInfo"%>

	
<%@ include file="../../../inc/user/subHeader.jsp"%>

<% if(loginUser != null && loginUser.getMtype().equals("GUIDE")){ %>

<%
	ArrayList<HashMap<String, Object>> programList = (ArrayList<HashMap<String, Object>>) ((HashMap<String, Object>) request.getAttribute("programMap")).get("programList");
	ArrayList<HashMap<String, Object>> programAllList = (ArrayList<HashMap<String, Object>>) ((HashMap<String, Object>) request.getAttribute("programMap")).get("programAllList");
	
	// 프로그램 목록 보기
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int limit = pi.getLimit();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
%>
<style>
.container .tbl_wrap .tbl tr td{padding: 0;}
.container .tbl_wrap .tbl tr td a{padding: 20px 0;}
</style>
<!-- datepicker 관련 js -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.searchDiv {margin: 20px 0;border: 1px solid lightgray;height: 130px;padding: 0 20px;}
.searchPro {display: inline-block;line-height: 40px;padding:20px 10px;}
.searchDate {display: inline-block;line-height: 40px;	padding:20px 10px;}
.ipt_sel{width: 290px;line-height: 40px;height: 40px;}
.ui-widget.ui-widget-content {width: 284px;}
.ipt_sel.date_pick{padding-left: 4px;width: 282px;height: 34px;}
.btn_wrap_my{display: inline-block;}
.container .btn_wrap_my .btn_blue{display: inline-block;margin-left: 13px;height: 40px;line-height: 40px;}
</style>

<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h1 class="myProgram">내 프로그램</h1>
			<form class="insertProgram" action="<%=request.getContextPath()%>/insertMyProgram.gp" method="post">
				<div class="searchDiv">
					<div class="searchPro">
						<h3>프로그램 명</h3>
						<select class="ipt_sel" name="myProgramSel" id="myProramName">
							<option selected hidden>프로그램명을 선택해주세요</option>
							<% for(int i = 0; i < programAllList.size(); i++){ %>
							<option value="<%= ((Gp) programAllList.get(i).get("myGp")).getGpNo() %>"><%= ((Gp) programAllList.get(i).get("myGp")).getGpName() %></option>
							<% } %>
						</select>
					</div>
		
					<div class="searchDate">
						<h3>시작 날짜</h3>
			            <!-- 데이트피커 -->
			            <input type="text" name="startDate" id="startDateSel" class="date_pick ipt_sel" placeholder="날짜를 선택해주세요." autocomplete="off" disabled>
					</div>
					<div class="btn_wrap_my">
						<input type="hidden" name="mno" value="<%= loginUser.getMno() %>">
						<button type="submit" class="btn_com btn_blue my">프로그램 등록</button>
					</div>
				</div>
			</form>
					<!-- table 시작 -->
					<div class="tbl_wrap">
						<table class="tbl">
							<colgroup>
								<col width="10%">
								<col width="90%">
							</colgroup>
							<tr class="tbl_tit">
								<th>목록</th>
								<th>프로그램 명</th>
							</tr>
							<% for(int i = 0; i < programList.size(); i++){ %>
							<tr class="tbl_cnt">
								<td><%= programList.get(i).get("idx") %></td>
								<td>
									<a href="<%= request.getContextPath() %>/selectMyProgramDetail.gp?no=<%= ((Gp) programList.get(i).get("myGp")).getGpNo() %>"><%= ((Gp) programList.get(i).get("myGp")).getGpName() %></a>
								</td>
							</tr>
							<% } %>
						</table>
				</div>
			      <!-- 페이저 시작 -->
			      <div class="pager_wrap">
			         <ul class="pager_cnt clearfix add">
			            <% if(currentPage <= 1)  { %>
			
			
			            <li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>
			
			
			            <%} else { %>
			            <li class="pager_com pager_arr prev"><a
			               href="<%= request.getContextPath()%>/selectMyProgramList.gp?mno=<%= mno %>&currentPage=<%=currentPage - 1%>">&#x003C;</a></li>
			
			            <%} %>
			            <% for(int p = startPage; p <= endPage; p++)  {
			               if(p == currentPage) {
			            %>
			            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
			            <%} else {%>
			            <li class="pager_com pager_num"><a href="<%= request.getContextPath()%>/selectMyProgramList.gp?mno=<%= mno %>&currentPage=<%=p%>"><%=p %></a></li>
			            <%} %>
			            <% } %>
			
			
			            <% if(currentPage >= maxPage) { %>
			            <li class="pager_com pager_arr next"><a
			               href="javascript: void(0);">&#x003E;</a></li>
			            <%}else { %>
			            <li class="pager_com pager_arr next"><a
			               href="<%= request.getContextPath()%>/selectMyProgramList.gp?mno=<%= mno %>&currentPage=<%=currentPage + 1%>">&#x003E;</a></li>
			
			            <%} %>
			
			         </ul>
			      </div>
			      <!-- 페이저 끝 -->
		</div>
</div>
</main>

<!-- datepicker 관련 js -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		var dateDisabled = new Array();
		let tdayDisabled ;
		// const tdayDisabled = Array.from(Array(4), () => Array());

		var tdayNum;
	    // id는 테이블에 걸어주세요
	    $(".tbl td, .tbl td a").mouseenter(function() {
	       $(this).parents("tr").css({"background":"#f1f1f1", "cursor":"pointer"});
	    }).mouseout(function(){
	       $(this).parents("tr").css({"background":"white"});
	    });
	    
	    /* 데이트피커 */
	    $(".date_pick").datepicker({
	      nextText: '다음 달',
	      prevText: '이전 달',
	      dateFormat: "yy-mm-dd",
	      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
	      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
	      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
	      minDate : "+1D",
	      beforeShowDay: function(date) {
			    var m = date.getMonth()
			    var d = date.getDate()
			    var y = date.getFullYear();
	    	  //	var b = true;
	    	  	//console.log(date);
	    	  	for(var i = 0; i < dateDisabled.length; i++){

		    	    for(var j = 0; j < tdayNum; j++){
			    	      console.log(tdayDisabled[i][j]);
			    	      var temp;
			    	      if((m+1) < 10 && d < 10){
			    	    	  temp = ('0' + (m+1) + '월 0' + d + ', ' + y);
			    	      }else if((m+1) < 10){
			    	    	  temp = ('0' + (m+1) + '월 ' + d + ', ' + y);
			    	      }else if(d < 10){
			    	    	  temp = ((m+1) + '월 0' + d + ', ' + y);
			    	      }else{
			    	    	  temp = ((m+1) + '월 ' + d + ', ' + y);
			    	      }
			    	      
		    			  if(temp == tdayDisabled[i][j]) {
					          return [false];
					      }

				        
		    	    }
	    	  	}	   
	    
			    return [true];

			}
	    });

		// 열리는 메뉴
		$(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
		$(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(2).addClass("on");
		
		// 프로그램 등록 - 회차 등록된 날짜
		$("#myProramName").on("change", function(){
			var selVal = $(this).val();

			$("#startDateSel").prop("disabled", true);
			
			
			$.ajax({
				url : "<%=request.getContextPath() %>/selectMyProgramDate.gp?gpNo=" + selVal,
				type : "get",
				success : function(data){
					dateDisabled = data.myProgramDateList;
					// tdayDisabled = data.tday;
					tdayNum = data.tday;
				    tdayDisabled = Array.from(Array(dateDisabled.length), () => Array());
					
					for(var i = 0; i < dateDisabled.length; i++){
						for(var j = 0; j < tdayNum; j++){
							
							tdayDisabled[i][j] = date_add(dateDisabled[i], j);
							
						}
					}
					

					$("#startDateSel").prop("disabled", false);
					
					console.log(dateDisabled);
					console.log(tdayDisabled);
					console.log("data.tday : " + data.tday);
				},
				error : function(data){
					console.log("날짜 불러오기 error");
				}
			});
		
			
		});
		
	/* 	function disableAllTheseDays(date) {
		    var m = date.getMonth()
		    var d = date.getDate()
		    var y = date.getFullYear();
		    
		    for (i = 0; i < dateDisabled.length; i++) {
		        if($.inArray((m+1) + '월 ' + d + ', ' + y, disabledDays) != -1) {
		            return [false];
		        }
		    }
		    return [true];
		} */
	});
	

	function date_add(sDate, nDays) {
		console.log("sDate : " + sDate);
		var tempDate = sDate.split("월 ");
	    var yy = parseInt(tempDate[1].split(",")[1]);
	    var mm = parseInt(tempDate[0]);
	    var dd = parseInt(tempDate[1].split(",")[0]);
	    
	    d = new Date(yy, mm - 1, dd + nDays);

	    yy = d.getFullYear();
	    mm = d.getMonth() + 1; mm = (mm < 10) ? '0' + mm : mm;
	    dd = d.getDate(); dd = (dd < 10) ? '0' + dd : dd;

		return mm + '월 ' + dd + ', ' + yy;
		
	}
</script>

<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>

<%@ include file="../../../inc/user/footer.jsp"%>
