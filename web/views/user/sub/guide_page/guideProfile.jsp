<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.guide.model.vo.*,com.edr.common.model.vo.*, java.util.*"%>
<%
	GuideDetail guideDetail = (GuideDetail) session.getAttribute("guideDetail");
	String[] langArr = (String[]) request.getAttribute("langArr");
	ArrayList<Local> localList = (ArrayList) request.getAttribute("localList");
	ArrayList<Local> localList2 = new ArrayList<>();
	
	int count = 0;
	for(int i = 0; i < localList.size(); i++){
		localList2.add(localList.get(i));
		if( localList2.get(0).equals(localList.get(i+1))){
			count++;
			System.out.println("localList["+i+"] : " + localList.get(i) + "localList2["+i+"] : " + localList2.get(i));
			break;
		}else{
			
		}
		
		
	}
	System.out.println("count: " + count);
	for(int i = 0; i < localList2.size(); i++){
		
		System.out.println("localList2["+i+"] : " + localList2.get(i));
		
	}
	
	Attachment at = (Attachment) request.getAttribute("at");
	    
%>
<%@ include file="../../../inc/user/subHeader.jsp"%>
<!-- 팝업관련css -->
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/common/magnific-popup.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/user/hyeonjuStyle.css">
<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit gset">계정 관리</h4>
		<div class="sub_ctn">
			<!-- 영역분리 시작 -->
			<div class="layout_3_wrap">
				<!-- 내용 -->
				<div class="main_cnt">
					<div style="width: 100%; height: 170px">


						<div class="btn_com btn_blue"
							style="margin-left: 2%; float: right">
							<a href="/edr/views/user/sub/popup/guidePwdPopup.jsp"
								class="editProfile">수정하기</a>
						</div>
						<img src="/edr/uploadFiles/<%=at.getChangeName()%>" alt="My Image"
							style="width: 200px; height: 170px;">

					</div>  

					<div class="main_cnt_list clearfix">
						<input type="hidden">
						<h4 class="title set">이름</h4>
						<div class="box_input tinfo"><%=guideDetail.getGname()%></div>
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">연락처</h4>
						<div class="box_input tinfo"><%=guideDetail.getPhone()%></div>
						<!--                  <input type="text" name="" id="" placeholder="010-1234-5678"> -->
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">비상연락처</h4>
						<div class="box_input tinfo"><%=guideDetail.getPhone2()%></div>
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">활동지역</h4>
						<div class="box_input tinfo">
						<% for(int j = 0; j < localList2.size(); j++) {
								if( j == localList2.size() - 1) {	%>
								<%=localList.get(j).getLocalName() %>
						<%
								}else{ %>
								<%=localList.get(j).getLocalName() %>,&nbsp;
						<% 		}
							}
						%>
						
							
						</div>
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">주소</h4>
						<div class="box_input tinfo"><%=guideDetail.getAddress()%></div>
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">카카오톡 ID</h4>
						<div class="box_input tinfo"><%=guideDetail.getKakaoId()%></div>
					</div>
					<div class="main_cnt_list clearfix">
						<h4 class="title set">가능 언어</h4>
						<!-- 체크박스 시작 -->
						<div class="box_chk lang">
							<%
								for (int i = 0; i < langArr.length; i++) {
									if (i == langArr.length - 1) {
							%>
							<%=langArr[i]%>
							<%
								} else {
							%>
							<%=langArr[i]%>,&nbsp;
							<%
								}
								}
							%>
						</div>
						<br> <br>

					</div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title set">소개</h4>
					<div class="box_inputtinfo" style="vertical-align: middle;">
						<%=guideDetail.getIntroduce()%>
					</div>
				</div>
			</div>
			<!-- <div align="center">
            <div class="btn_com btn_white" style="float:left; height:50px; width:160px; margin-left: 23%;">취소</div>
            <div class="btn_com btn_blue" style="margin-left: 2%;"><a href="/edr/views/user/sub/popup/guidePwdPopup.jsp" class="editProfile">수정하기</a> </div>
			</div> -->
		</div>

	</div>
	<!-- 영역분리 끝 -->
</div>
<!-- 오른쪽 컨텐츠 부분 끝--> </main>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

<script>
	/* $(document).ready(function()){
	   $('.img_btn').on("click",function(){
	      $('.cloneOther').clone(true).appendTo('h1');
	   });
	} */

	$(document).ready(
			function() {
				$(".img_btn").click(
						function() { //click this

							//자바스크립트영역
							$("<input type='text' class='ipt_txt_lang'>")
									.clone().appendTo('.here');

							//$('.ipt_txt_lang:first').clone(false).appendTo('.here');

						})

			});
</script>
<!-- 컨텐츠가 들어가는 부분 end -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

<script>
	$(function() {
		// 열리지 않는 메뉴
		$(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");

		// 열리는 메뉴
		//             $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
		//             $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(4).addClass("on");
	});

	$(function() {
		/* 팝업 예시 */
		$('.editProfile').magnificPopup({
			type : 'ajax'
		});
	});
</script>
<%@ include file="../../../inc/user/footer.jsp"%>