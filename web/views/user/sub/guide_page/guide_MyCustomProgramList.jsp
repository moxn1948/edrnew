<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.common.model.vo.*, java.util.*, com.edr.customProgram.model.vo.*"%>
<%
ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
int listCount = pi.getListCount();
int currentPage = pi.getCurrentPage();
int maxPage = pi.getMaxPage();
int startPage = pi.getStartPage();
int endPage = pi.getEndPage(); 
%>

 <%@ include file="../../../inc/user/subHeader.jsp" %>
<link rel="stylesheet" type="text/css" href="../../../../css/user/kijoonStyle.css">
 


<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/guidePageSubMenu.jsp" %> 
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">배정된 맞춤 프로그램</h4>
			<input type="hidden" name="mno" value="<%= loginUser.getMno()%>">
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl" id="listArea">
						<colgroup>
							<col width="10%">
							<col width="30%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>신청자 명</th>
							<th>신청 일시</th>
							<th>상태</th>
						</tr>
						<% for (int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i); %>
        		<tr class="tbl_cnt">
        				<td><%= hmap.get("rNum") %></td>
        				<td><%= hmap.get("cpTitle") %></td>
        				<td><%= hmap.get("nickName") %></td>
        				<td><%= hmap.get("cDate") %></td>
						<td><%String str = "";
							if(hmap.get("status").equals("GWAIT")) {
								str = "가이드 배정중";
							}else if(hmap.get("status").equals("FAIL")){
								str = "미채택";
							}
							else if(hmap.get("status").equals("OK")){
								str = "채택";
							}%> <%= str %></td>


		<td><input type="hidden" id="cpNo" name="cpNo" value="<%= hmap.get("cpNo")%>"></td>						
		<td><input type="hidden" id="mno" name="mno" value="<%= loginUser.getMno()%>"></td>						
		<td><input type="hidden" id="status" name="status" value="<%= hmap.get("status")%>"></td>						
								
							
					<% }%>
						</tr>
					</table>
				</div>
				<!-- table 끝 -->
			</div>
			<!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%= request.getContextPath()%>/selectMCP.cp?currentPage=<%=currentPage - 1%>&mno=<%= loginUser.getMno() %>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%= request.getContextPath()%>/selectMCP.cp?currentPage=<%=p%>&mno=<%= loginUser.getMno() %>"><%=p %></a></li>
				<%} %>
				<% } %>
 

				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/selectMCP.cp?currentPage=<%=currentPage + 1%>&mno=<%= loginUser.getMno() %>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(1).addClass("on");
   
   $("#listArea td").mouseenter(function() {
	   $(this).parent().css({"background":"#f1f1f1", "cursor":"pointer"});
   }).mouseout(function(){
	   $(this).parent().css({"background":"white"})
   }).click(function(){
	   var cpNo = $(this).parent().children().find("#cpNo").val()
	   var state = $(this).parent().children().find("#status").val()
	  
	  location.href="<%= request.getContextPath()%>/selectOneMCP.cp?cpNo=" + cpNo + "&state="+ state;
   }) 
   
   	
		$(".date_pick").datepicker({
		      nextText: '다음 달',
		      prevText: '이전 달',
		      dateFormat: "yy-mm-dd",
		      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
		      minDate : "+1D"
		    });
		
		
   
});

</script>	
<%@ include file="../../../inc/user/footer.jsp" %>

