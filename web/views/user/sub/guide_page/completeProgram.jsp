<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.Gp, com.edr.payment.model.vo.Payment, com.edr.product.model.vo.Product, com.edr.common.model.vo.PageInfo"%>

<%@ include file="../../../inc/user/subHeader.jsp"%>


<% if(loginUser != null && loginUser.getMtype().equals("GUIDE")){ %>

<% 
	// 프로그램 목록 보기
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int limit = pi.getLimit();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
	
	ArrayList<HashMap<String, Object>> compList = (ArrayList<HashMap<String, Object>>) request.getAttribute("compList");
%>

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/user/kijoonStyle.css">

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp" %>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit">완료된 프로그램</h4>
		<div class="sub_ctn">
			<!-- 오른쪽 컨텐츠 부분 시작-->
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl" id="listArea">
						<colgroup>
							<col width="10%">
							<col width="20%">
							<col width="*">
							<col width="25%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>회차</th>
							<th>프로그램 명</th>
							<th>투어 일시</th>
						</tr>
						<% for(int i = 0; i < compList.size(); i++) {%>
						<tr class="tbl_cnt">
							<td><%= compList.get(i).get("idxnum") %></td>
							<td><%= ((Product) compList.get(i).get("pdComp")).getEpi() %>회차</td>
							<td><%= ((Gp) compList.get(i).get("gpComp")).getGpName() %></td>
							<td><%= ((Product) compList.get(i).get("pdComp")).getPdate() %> ~ <%= compList.get(i).get("fday") %></td>
						</tr>
						<% } %>

					</table>
				</div>
				<!-- table 끝 -->
			</div>
	

      <!-- 페이저 시작 -->
      <div class="pager_wrap">
         <ul class="pager_cnt clearfix add">
            <% if(currentPage <= 1)  { %>
            <li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>
            <%} else { %>
            <li class="pager_com pager_arr prev"><a
               href="<%= request.getContextPath()%>/selectCompGp.gd?currentPage=<%=currentPage - 1%>&mno=<%= loginUser.getMno() %>">&#x003C;</a></li>
            <%} %>
            <% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
            <%} else {%>
            <li class="pager_com pager_num"><a href="<%= request.getContextPath()%>/selectCompGp.gd?currentPage=<%=p%>&mno=<%= loginUser.getMno() %>"><%=p %></a></li>
            <%} %>
            <% } %>

            <% if(currentPage >= maxPage) { %>
            <li class="pager_com pager_arr next"><a
               href="javascript: void(0);">&#x003E;</a></li>
            <%}else { %>
            <li class="pager_com pager_arr next"><a
               href="<%= request.getContextPath()%>/selectCompGp.gd?currentPage=<%=currentPage + 1%>&mno=<%= loginUser.getMno() %>">&#x003E;</a></li>

            <%} %>

         </ul>
      </div>
      <!-- 페이저 끝 -->
      
	</div>
</div>

<!-- 오른쪽 컨텐츠 부분 끝--> 
<!-- 컨텐츠가 들어가는 부분 end -->
</main>


<script>
$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(4).addClass("on");
   
   
});
</script>

<%@ include file="../../../inc/user/footer.jsp"%>


<% } else {
	response.sendRedirect(request.getContextPath()+"/index");
} %>


