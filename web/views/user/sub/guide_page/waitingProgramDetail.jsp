<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.generalProgram.model.vo.*, com.edr.common.model.vo.Attachment"%>
<%
	int gpNo = (Integer) request.getAttribute("gpNo");
	Gp gpObj = (Gp) request.getAttribute("gpObj");
	ArrayList<GpPriceDetail> gpPriceList  = (ArrayList<GpPriceDetail>) request.getAttribute("gpPriceList");
	ArrayList<GpDetail> gpDetailList  = (ArrayList<GpDetail>) request.getAttribute("gpDetailList");
	ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) request.getAttribute("gpAttachmentList");
	ArrayList<GpLang> gpLangList  =  (ArrayList<GpLang>) request.getAttribute("gpLangList");
	
	int totalDay = gpObj.getGpTday();
	
	String lang = "";
	for(int i = 0; i < gpLangList.size(); i++){
		if(i != gpLangList.size() - 1){
			lang += gpLangList.get(i).getLang() + ", ";
		}else{
			lang += gpLangList.get(i).getLang();
		}
	}

	ArrayList<String> includeName = new ArrayList<>();
	ArrayList<String> notcludeName = new ArrayList<>();
	for(int i = 0; i < gpPriceList.size(); i++){
		if(gpPriceList.get(i).getGpInc().equals("INCLUDE")){
			includeName.add(gpPriceList.get(i).getGpCategory());
		}else{
			notcludeName.add(gpPriceList.get(i).getGpCategory());
		}
	}
	
	
	ArrayList<Integer> gpDetailCtn = new ArrayList<>();
	for(int i = 0; i < gpObj.getGpTday(); i++){
		int ctn = 0;
		for(int j = 0; j < gpDetailList.size(); j++){
			if(gpDetailList.get(j).getGpDay() == (i + 1)){
				ctn++;	
			}
		}
		gpDetailCtn.add(ctn);
	}
%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/user/mjStyle.css">
<%@ include file="../../../inc/user/subHeader.jsp" %>
<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/guidePageSubMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<div class="program_list_wrap program_list_wrap_1">
			<h1>승인 대기중인 프로그램 상세보기</h1>
			<br>
			<h3><b>프로그램 명</b></h3>
         	<label class="program_tit_cnt"><%= gpObj.getGpName() %></label>
         	<br><br>
         	<h3><b>프로그램 설명</b></h3>
			<label><%= gpObj.getGpDescr() %></label>
			<br><br>
         	<h3><b>프로그램 가격</b></h3>
			<label><%= gpObj.getGpCost() %> 원</label>
			<br><br>
			<hr style="border:1px solid darkgray;">
			<br>
			<h3><b>총 프로그램 기간</b></h3>
			<label><%= totalDay %> 일</label>
			<br><br>
			<h3><b>참가 가능 인원</b></h3>
			<label>최소  <%= gpObj.getGpMin() %>명 ~ 최대  <%= gpObj.getGpMax() %>명</label>
			<br><br>		
			<h3><b>프로그램 진행 가능 언어</b></h3>
			<label><%= lang %></label>
			<br><br>
			<hr style="border:1px solid darkgray;">
          </div>
          
          <div class="program_list_wrap program_list_wrap_2">
            <div class="program_list">
           	<h3><b>필수 안내 사항</b></h3>
           	<br>
			<h3><b>포함사항</b></h3>
               	<%
					String includeStr = "";
					for(int i = 0; i < includeName.size(); i++){
						if(i == includeName.size() - 1){
							includeStr += includeName.get(i);
						}else{
							includeStr += includeName.get(i) + ", ";
						}
					}
				%>
				<%= includeStr %>
			<br><br>
            <h3><b>불포함사항</b></h3> 
                <%
					String notcludeStr = "";
					for(int i = 0; i < notcludeName.size(); i++){
						if(i == notcludeName.size() - 1){
							notcludeStr += notcludeName.get(i);
						}else{
							notcludeStr += notcludeName.get(i) + ", ";
						}
					}
				%>
				<%= notcludeStr %>
			<br><br>
			<h3><b>만나는 시간</b></h3>
            <label><%= gpObj.getGpMeetTime() %></label>
            <br><br>
			<h3><b>만나는 장소</b></h3>
			<label><%= gpObj.getGpMeetArea() %></label>
            <div id="programDetail_map" class="meet_map"></div>
            </div>
          </div>
          <hr style="border:1px solid darkgray;">
		<div class="program_list_wrap program_list_wrap_3" style="margin-top: 10px;margin-bottom: 20px;padding: 40px 20px;border: 1px solid #ddd; text-align:center;">
			<div class="program_list">
			<% 
			int gpDetailFor = 0;
			
			for(int i = 0; i < gpObj.getGpTday(); i++){ %>
			<div class="detail_course_wrap detail_course_wrap_1 course_wrap">
				<div class="detail_course_tit">
					<p class="tit" style="font-size:20px;font-weight:bold;"><%= i + 1 %>Day</p>
					<!-- <p class="total_time">총 ?시간 소요</p> -->
				</div>
				<% for(int j = 0; j < gpDetailCtn.get(i); j++){ %>
				<div class="detail_course_ctn">
					<p class="ctn_tit" style="font-size:15px;font-weight:bold;">일정 <%= j+1 %> : 약 <%= gpDetailList.get(gpDetailFor).getGpTime() %>시간 소요</p>
					<p class="place_tit" style="font-size:15px;font-weight:bold;"><%= gpDetailList.get(gpDetailFor).getGpLocation() %></p>
					<img src="/edr/uploadFiles/<%=gpAttachmentList.get(gpDetailFor).getChangeName() %>" alt="" class="place_img" style="width: 100%;">
					<p class="desc" style="font-size:20px; text-align:left;"><%= gpDetailList.get(gpDetailFor).getGpCnt() %></p>
				</div>
				<% 
					gpDetailFor++;
				} %>
			</div>
			<% }%>

			</div>
		</div>
          <div class="program_list_wrap program_list_wrap_4">
            <div class="program_list">
            <h3><b>특이사항</b></h3>
            <label><%= gpObj.getGpNotice() %></label>
            </div>
          </div>
        	
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
	<script>
	$(function(){
	   // 열리지 않는 메뉴
	   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
	   
	   // 열리는 메뉴
	   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
	   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(1).addClass("on");
	});
	</script>
<%@ include file="../../../inc/user/footer.jsp" %>
