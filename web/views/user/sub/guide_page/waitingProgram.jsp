<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.common.model.vo.*, java.util.*, com.edr.generalProgram.model.vo.*"%>
    
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
 	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();  
	
 
%>    
<%@ include file="../../../inc/user/subHeader.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/user/shjeon.css">
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/user/scriptSh.js"></script>
	<!-- 컨텐츠가 들어가는 부분 start -->
	<main class="container">
	<div class="inner_ct clearfix">
		<!-- 서브메뉴 : 변경해야함 -->
		<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">승인 대기중인 프로그램</h4>
			<div class="sub_ctn">
				<!-- table 시작 -->
				<div class="tbl_wrap">
					<table class="tbl" id="listarea">
						<colgroup>
							<col width="10%">
							<col width="40%">
							<col width="20%">
							<col width="15%">
							<col width="15%">
						</colgroup>
						<tr class="tbl_tit">
							<th>목록</th>
							<th>프로그램 명</th>
							<th>등록일</th>
							<th>상태</th>
							<th>삭제</th>
						</tr>
						<% for (int i = 0; i < list.size(); i++) {
        					HashMap<String, Object> hmap = list.get(i);
        				%>
						<tr class="tbl_cnt">
							<td><%= hmap.get("rnum")%></td>
							<td><a href="<%= request.getContextPath() %>/selectOneWaitingProgram.gp?mno=<%= hmap.get("mno") %>&gpNo=<%= hmap.get("gpno") %>"><%= hmap.get("gpName") %></a></td>
							<td><%= hmap.get("gpRday") %></td>
							<td><%  
								String str = "";
								if(hmap.get("allow").equals("N")) {
									str = "대기중";
								}
								%>
							<%= str  %>
							</td>
					
						<td>
						
						<input type="hidden" name="mno" value="<%= hmap.get("mno")%>">
						<input type="hidden" id="gpnoHid" name="gpno" value="<%= hmap.get("gpno")%>">
						<button value="삭제" onclick="allowDel(this)">삭제</button></td>
					</tr>


					<% }%>
						
					</table>
				</div>
				<!-- table 끝 -->
			</div>
			     <!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%= request.getContextPath()%>/allowList.gp?currentPage=<%=currentPage - 1%>&mno=<%= loginUser.getMno() %>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%= request.getContextPath()%>/allowList.gp?currentPage=<%=p%>&mno=<%= loginUser.getMno() %>"><%=p %></a></li>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/allowList.gp?currentPage=<%=currentPage + 1%>&mno=<%= loginUser.getMno() %>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
	</main>
	<!-- 컨텐츠가 들어가는 부분 end -->
	<script>
	$(function(){
	   // 열리지 않는 메뉴
	   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
	   
	   // 열리는 메뉴
	   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
	   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(1).addClass("on");
	});
	
	 <%-- $(".tbl_cnt").click(function(){
	     var str =  $(this).children().eq(0).text();
	     var num = $(this).children("input").val();
	     
	      console.log(num);
	      location.href="<%= request.getContextPath()%>/adGuideDetail.gd?num=" + num;
	   }) --%>
	function allowDel(val) {
		var gpno = $(val).parent().find("#gpnoHid").val();
		
		console.log(gpno);
		
		
		location.href="<%= request.getContextPath()%>/allowDel.gp?mno=<%= loginUser.getMno()%>&gpno=" +gpno;
		
		$("#listArea td").mouseenter(function() {
		       $(this).parents("tr").css({"background":"#f1f1f1", "cursor":"pointer"});
		}).mouseout(function(){
		       $(this).parents("tr").css({"background":"white"});
		});
	}
	</script>
	<%@ include file="../../../inc/user/footer.jsp"%>