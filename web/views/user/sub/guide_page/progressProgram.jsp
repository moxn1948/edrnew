<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.common.model.vo.*, java.util.*, com.edr.generalProgram.model.vo.*"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
 	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();  
	 	

%>

<%@ include file="../../../inc/user/subHeader.jsp"%>

<link rel="stylesheet" type="text/css"
	href="/edr/css/user/jakeStyle_user.css">



<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
<div class="inner_ct clearfix">

	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap">
		<h4 class="sub_ctn_tit">진행 중인 프로그램</h4>
		<div class="sub_ctn">
			<!-- table 시작 -->
			<div class="tbl_wrap">
				<table class="tbl"id="listArea">
					<colgroup>
						<col width="10%">
						<col width="10%">
						<col width="30%">
						<col width="20%">
						<col width="20%">
						<col width="10%">
					</colgroup>
					<tr class="tbl_tit">
						<th>목록</th>
						<th>회차</th>
						<th>프로그램 명</th>
						<th>투어 일시</th>
						<th>상태</th>
						<th>삭제</th>
					</tr>
					<% for (int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i);
        %>
					<tr class="tbl_cnt">
						
						<td><%= hmap.get("rnum")%></td>
						<td><%= hmap.get("epi") %></td>
						<td><%= hmap.get("gpname") %></td>
						<td><%= hmap.get("pdate") %></td>
						<td><%  
							String str = "";
							if(hmap.get("paystate").equals("ENDPAY")) {
								str = "판매완료";
							} else {
								str = "판매중";
							}
						%>
						<%= str  %>
						</td>
					
						<td>
						<input type="hidden" name="mno" value="<%= hmap.get("mno")%>">
						<input type="hidden" id="pnoHid"name="pno" value="<%= hmap.get("pno")%>">
						
						<button value="삭제" onclick="progressDel(this)" <% if(hmap.get("paystate").equals("ENDPAY")) { %>
							disabled
					<% 	} %>><% if(hmap.get("paystate").equals("ENDPAY")) {%>
							삭제불가
						<% } else { %>
								삭제
						<% }%> 
					</button></td>
					</tr>


					<% }%>
				</table>
			</div>
			<!-- table 끝 -->
		</div>
		  <!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%= request.getContextPath()%>/processList.gp?currentPage=<%=currentPage - 1%>&mno=<%= loginUser.getMno() %>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%= request.getContextPath()%>/processList.gp?currentPage=<%=p%>&mno=<%= loginUser.getMno() %>"><%=p %></a></li>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/processList.gp?currentPage=<%=currentPage + 1%>&mno=<%= loginUser.getMno() %>">&#x003E;</a></li>

				<%} %>
 
			</ul>
		</div>
		<!-- 페이저 끝 -->
	</div>
	<!-- 오른쪽 컨텐츠 부분 끝-->
	<script>
	$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(3).addClass("on");
});
	function progressDel(val) {
		var pno = $(val).parent().find("#pnoHid").val();
		var epi = $(val).parent().find("#epiHid").val();
		
		console.log(epi);
		
		location.href="<%= request.getContextPath()%>/progressDel.gp?mno=<%= loginUser.getMno()%>&pno=" + pno;
	}
	
	$("#listArea td").mouseenter(function() {
	       $(this).parents("tr").css({"background":"#f1f1f1", "cursor":"pointer"});
	}).mouseout(function(){
	       $(this).parents("tr").css({"background":"white"});
	});
</script>
</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->


<%@ include file="../../../inc/user/footer.jsp"%>