<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.customProgram.model.vo.*, java.util.*"%>

<%
	CpRequest cr = (CpRequest) request.getAttribute("cr");
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<CpLang> listLang = (ArrayList<CpLang>) request.getAttribute("listLang");
	int result = (Integer) request.getAttribute("result") + 1;
	int cpNo = (Integer) request.getAttribute("cpNo");

	
%>
 
<%@ include file="../../../inc/user/subHeader.jsp"%>
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e099dc7b4bc90cc56294bfce99d4f5b2&libraries=services"></script>
<!-- 주소 검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>

<%-- <%@ include file="../../../css/user/kijoonStyle.css" %> --%>

<style>
.ipt_sel {
	float: left;    
	width: 160px;
    padding: 9px 5px;
    line-height: 30px;
    border: 1px solid #ddd;}
.box_txt {
	display: inline-block;
	margin-top: 10px;
}
.sub_cnt_wrap{padding-bottom: 20px;}
.ipt_txt {
    float: left;
    padding: 5px;
    margin-right: 4px;
    width: 340px;
    line-height: 30px;
    border: 1px solid #ddd;
}
.ipt_add_1 {
    float: left;
    width: 146px;
}
.ipt_add_3 {
	margin-left: 120px;
}
.add_srch_wrap.btn_wrap {
    float: left;
    padding-top: 0;
    width: 159px;
}
.add_srch_btn {
	display: inline-block;
    padding: 0;
    width: 100%;
    line-height: 43px;
    text-align: center;
    color: #f7f7f7;
    background-color: #171f57;
}
.add_btn{ 
	display: inline-block;
	margin: 10px 0;
	width: 16px;
	height: 31px;
    padding: 8px 14px 8px;
    font-weight: bold;
    font-size: 20px;
    color: #fff;
    background-color: #171f57;
    border-radius: 50%;}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<main class="container">
<div class="inner_ct clearfix">
	<!-- 서브메뉴 : 변경해야함 -->
	<%@ include file="../../../inc/user/guidePageSubMenu.jsp"%>
	<!-- 오른쪽 컨텐츠 부분 시작-->
	<div class="sub_ctn_wrap ce clearfix">
		<%for(int i = 0; i < 1; i++) {
            		HashMap<String, Object> hmap = list.get(i);     
         	   %>
		<h4 class="sub_ctn_tit">맞춤프로그램 상세보기</h4>

		<div class="sub_ctn">


			<div class="main_cnt_list clearfix">
				<h4 class="title">상태</h4>
				<div class="box_input">
					<span class="box_txt">
						<% String str2 = "";
					if(hmap.get("state").equals("WAIT")) {
							str2="대기중";								
					}
					else if(hmap.get("state").equals("ENDPAY")) {
							str2="결제완료";
					} else if(hmap.get("state").equals("GWAIT")) {
							str2="가이드 배정중";

					} 					
					%> <%= str2 %>


					</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">신청일자</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("cpDate")%></span>
				</div>
			</div>

			<hr>

			<div class="main_cnt_list clearfix">
				<h4 class="title">지역</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("local")%></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">팀인원</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("person")%></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">시작일자</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("sDate")%></span>
				</div>
			</div>

			<br>
			<div class="main_cnt_list clearfix">
				<h4 class="title">종료일자</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("eDate")%></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">총기간</h4>
				<div class="box_input">
					<span class="box_txt"><%= result %>일</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">최대 가능 금액</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("cost")%></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">요구언어</h4>
				<div class="box_input">
					<span class="box_txt"> <%for(int j = 0; j < listLang.size(); j++)  { %>
						<%=listLang.get(j).getLang()%>, <% } %>
					</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 성별</h4>
				<div class="box_input">
					<span class="box_txt">
						<% String str = "";
												if(hmap.get("gender").equals("M")) {
													str="남자";
												}else if(hmap.get("gender").equals("F")) {
													str="여자";
												}else if(hmap.get("gender").equals("ALL")) {
													str="무관";
												}%> <%= str %>
					</span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">제목</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("title")%></span>
				</div>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">내용</h4>
				<div class="box_input">
					<span class="box_txt"><%= hmap.get("cnt")%></span>
				</div>
			</div>
			<% } %>
		</div>
				<hr>

		<div class="sub_ctn">
			
			<div class="layout_3_wrap">
				<h1 class="main_title" align="center">답변달기</h1>
				<form id="customForm"action="<%= request.getContextPath()%>/insertMCP.cp?result=<%= result %>" method="post" encType="multipart/form-data">
				<input type="hidden" name="cpNo" value=<%= cpNo %>>
				<input type="hidden" name="mno" value=<%= loginUser.getMno() %>>
				
				<div class="main_cnt">
					<div class="main_cnt_list clearfix">
						<h4 class="title">총금액</h4>
						<div class="box_input">
							<input type="text" name="pay" id="pay"
								placeholder="수수료를 포함한 총 금액">
								
				<div class="sub_ctn_list">
				
						<div class="sub_cnt_wrap price_wrap">
						<h4 class="title" style="display: block;float: none;">상세 금액</h4>
							<div class="sub_cnt price_ctn">
								<div class="price_ctn_wrap">
									<div class="price_list price_default_list">
										<select class="ipt_sel" name="includeYn0">
											<option class="" value="include" >포함사항</option>
										</select>
										<input type="text" name="cpCategory" class="ipt_txt ipt_price_tit" placeholder="" value="가이드비" readonly>
										<input type="text" name="cpPrice" class="ipt_txt ipt_price_num" placeholder="금액" onKeyup="totalPrice(this);" value="0">
									</div>
									<div class="price_list">
										<select class="ipt_sel ipt_clude_sel" name="includeYn1">
											<option class="" value="include">포함사항</option>
											<option class="" value="notclude">불포함사항</option>
										</select>
										<input type="text" name="cpCategory" class="ipt_txt ipt_price_tit" placeholder="항목 작성">
										<input type="text" name="cpPrice" class="ipt_txt ipt_price_num" placeholder="금액" onKeyup="totalPrice(this);" value="0">
									</div>
								</div>
								<a href="javascript: void(0);" class="add_btn price_btn">+</a>
				
								<ul class="total_price_wrap clearfix">
									<li class="total_price_cnt">총 금액</li>
									<li class="total_price_cnt" id="totalPrice"></li>
								</ul>
								<input type="hidden" name="gpCost" id="totalPriceHidden">
							</div>
						</div>
					</div>
					</div>
				
 
					<div class="main_cnt_list clearfix">
					<h4 class="title">만나는 시간</h4>
					<div class="box_input box_select">					
						<div class="sub_cnt">
							<select name="dateTime" class="timepicker meet_time ipt_txt add" placeholder="만나는 시간을 선택해주세요">
								<option value="10:00">10:00</option>
								<option value="11:00">11:00</option>
								<option value="12:00">12:00</option>
								<option value="13:00">13:00</option>
								<option value="14:00">14:00</option>
								<option value="15:00">15:00</option>
								<option value="16:00">16:00</option>
								<option value="17:00">17:00</option>

							</select>
						</div>
					</div>
				</div>
							<div class="sub_cnt_wrap">
					<h4 class="title">주소</h4>
					<div class="sub_cnt">
						<div class="add_t_cnt clearfix">
							<input type="text" name="postcode" id="postcode"
								class="ipt_txt ipt_add_1" placeholder="우편번호" readonly> <input
								type="text" name="roadAddress" id="roadAddress"
								class="ipt_txt ipt_add_2" placeholder="도로명주소" readonly>
						<div class="add_b_cnt clearfix">
							<input type="text" name="detailAddress" id="detailAddress"
								class="ipt_txt ipt_add_3" placeholder="상세주소"> <input
								type="hidden" name="addressX" id="addressX"> <input
								type="hidden" name="addressX" id="addressY">
							<div class="btn_wrap add_srch_wrap">
								<a href="javascript: void(0);" class="add_srch_btn"
									id="addressApi">검색</a>
							</div>
						</div>
						</div>
					</div>
				</div>


			<% for(int i = 0; i < result; i++) {%>
					<div class="main_cnt_list clearfix">
						<h4 class="title">day <%= i+1 %></h4>
						<div class="box_input">
							<textarea name="dayCnt" placeholder="일정내용을 입력해주세요" rows="5" cols="50" style="resize:none;"></textarea>
						</div>
					</div>
				<%} %>
					<div class="main_cnt_list clearfix">
						<h4 class="title">내용(특이사항)</h4>
						<div class="box_input">
							<textarea name="notice" placeholder="공지사항을 입력해주세요" rows="5" cols="50" style="resize:none;"></textarea>
						</div>
					</div>



							<div class="main_cnt_list clearfix">
					<h4 class="title">파일 추가*</h4>
					<div class="box_input2">
						<input type="file" name="necessary" id="necessary">
					</div>
				</div>


				</div>
	<div class="btn_wrap">

		<button type="submit" class="btn_com btn_blue">배정받기</button>
	</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(2).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(2).find(".sub_side_list").eq(0).addClass("on");
   
   $(".date_pick").datepicker({
	      nextText: '다음 달',
	      prevText: '이전 달',
	      dateFormat: "yy-mm-dd",
	      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
	      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
	      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
	      minDate : "+1D"
	    })
});

//우편번호 검색
(function(){
    //주소-좌표 변환 객체를 생성
    
 	   
    
    var geocoder = new daum.maps.services.Geocoder();

	$("#addressApi").on("click",function(){
		console.log("123");
	      new daum.Postcode({
	            oncomplete: function(data) {
	                var addr = data.address; // 최종 주소 변수
	                var roadAddr = data.roadAddress; // 도로명 주소 변수
					
	                // 주소 정보를 해당 필드에 넣는다.
      				    document.getElementById('postcode').value = data.zonecode;
	                document.getElementById("roadAddress").value = roadAddr;
	                // 주소로 상세 정보를 검색
	                geocoder.addressSearch(data.address, function(results, status) {
	                    // 정상적으로 검색이 완료됐으면
	                    if (status === daum.maps.services.Status.OK) {

	                        var result = results[0]; //첫번째 결과의 값을 활용

	                        // 해당 주소에 대한 좌표를 받아서
	                        var coords = new daum.maps.LatLng(result.y, result.x);
	                        
	                        $("#addressX").val(coords.Ga);
	                        $("#addressY").val(coords.Ha);
	                    }
	                });
	            }
	        }).open();
	});
	
	
	$("#postcode, #roadAddress").on("click", function(){
		$("#addressApi").trigger("click");
	});
	
})();

function includePlus() {
	$("<input type='text' name='include' id='include'>").clone().eq(0).appendTo(".box_input3");
}
function notCludePlus() {
	$("<input type='text' name='notClude' id='notClude'>").clone().eq(0).appendTo(".box_input4");
	
}

var priceIdx = 1;

// 프로그램 금액 추가
$('.price_btn').on("click",function(){
	priceIdx++;
	
	var cloneLang = $(".price_list").eq(1).clone();
	cloneLang.find(".ipt_sel").prop("name", "includeYn"+priceIdx);
	cloneLang.find(".ipt_txt").eq(0).val("");
	cloneLang.find(".ipt_txt").eq(1).val("");
	cloneLang.find(".ipt_txt").eq(1)[0].readOnly = false;
	cloneLang.find(".ipt_txt").eq(1)[0].value = 0;

	$(".price_ctn_wrap").append(cloneLang);

});



function totalPrice(thisVal){
	// 가격 input:text에 숫자만 입력
	thisVal.value=thisVal.value.replace(/[^0-9]/g,'');
	var sum = 0;
	for(var i = 0; i < $(".ipt_price_num").length; i++){
		console.log(parseInt($(".ipt_price_num").eq(i).val()));
		
		sum += parseInt($(".ipt_price_num").eq(i).val());
		
	}
	$("#totalPrice").html(sum);
	$("#totalPriceHidden").val(sum);
	
}

$(document).on("change", ".ipt_clude_sel", function(){
 	if($(this).val() == "notclude"){
 		console.log($(this).siblings(".ipt_price_num")[0]);
 		$(this).siblings(".ipt_price_num")[0].readOnly = true;
 		$(this).siblings(".ipt_price_num")[0].value = 0;
	} else{
 		$(this).siblings(".ipt_price_num")[0].readOnly = false;
	}
});

// 총 금액 변경
function timeDayNum(thisVal){
	// 가격 input:text에 숫자만 입력
	thisVal.value=thisVal.value.replace(/[^0-9]/g,'');
	
	if(thisVal.value > 24){
		thisVal.value = "";
	}
	
}

</script>


<%@ include file="../../../inc/user/footer.jsp"%>