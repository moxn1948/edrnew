<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../../../inc/user/subHeader.jsp" %>

<!-- 컨텐츠가 들어가는 부분 start -->
<main class="container">
	<div class="inner_ct clearfix">
    <!-- 서브메뉴 : 변경해야함 -->
    <%@ include file="../../../inc/user/guidePageSubMenu.jsp" %>
		<!-- 오른쪽 컨텐츠 부분 시작-->
		<div class="sub_ctn_wrap">
			<h4 class="sub_ctn_tit">진행중인 프로그램 상세보기</h4>
			<div class="sub_ctn">
			
			<div class="main_cnt_list clearfix">
           <h4 class="title">회차 정보</h4>
         </div>
           <label id="episode" class="episode">5</label>
			
			<div class="main_cnt_list clearfix">
           <h4 class="title">지역명</h4>
         </div>
           <label>서울</label>
												
       	<div class="main_cnt_list clearfix">
           <h4 class="title">프로그램명</h4>
         </div>
           <label>5대 고궁 투어</label>
           
           <hr>
           
       	<div class="main_cnt_list clearfix">
           <h4 class="title">수령자명</h4>
         </div>
           <label>송기준</label>
			
       	<div class="main_cnt_list clearfix">
           <h4 class="title">수령자 연락처</h4>
         </div>
           <label>010-1234-5678</label>
           
       	<div class="main_cnt_list clearfix">
           <h4 class="title">수령자 비상 연락처</h4>
         </div>
           <label>010-9876-5432</label>
           
       	<div class="main_cnt_list clearfix">
           <h4 class="title">수령자 성별</h4>
         </div>
           <label>남성</label>
						
			<div class="main_cnt_list clearfix">
           <h4 class="title">수령자 나이</h4>
         </div>
           <label>30대</label>
           
			<div class="main_cnt_list clearfix">
           <h4 class="title">수령자 이메일</h4>
         </div>
           <label>bap@bird.com</label>
           
			<div class="main_cnt_list clearfix">
           <h4 class="title">요청 사항</h4>
         </div>
           <label>풀먹기 싫어요</label>
           
           <hr>
           
           <div class="main_cnt_list clearfix">
           <h4 class="title">총 결제 금액</h4>
         </div>
           <label>168000원</label>
			
			</div>	
        	
		</div>
		<!-- 오른쪽 컨텐츠 부분 끝-->
	</div>
	<script>
$(function(){
   // 열리지 않는 메뉴
   // $(".sub_menu_cnt .sub_menu_list").eq(0).addClass("on");
   
   // 열리는 메뉴
   $(".sub_menu_cnt .sub_menu_list").eq(1).addClass("on").addClass("open");
   $(".sub_menu_cnt .sub_menu_list").eq(1).find(".sub_side_list").eq(1).addClass("on");
});
</script>
</main>
<!-- 컨텐츠가 들어가는 부분 end -->

<%@ include file="../../../inc/user/footer.jsp" %>
