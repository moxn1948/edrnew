<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>가이드 문의내역 상세관리</title>

	<%@ include file="../../../inc/admin/header.jsp"%>
	<link rel="stylesheet" type="text/css" href="/edr/css/admin/jakeStyle_admin.css">
	<main class="container">
	<div class="inner_ct">
		<div class="main_cnt">
			<!-- 요소 시작 -->
			<div class="main_cnt_list2 clearfix">
				<div class="main_cnt_list3 clearfix">
					<br>
					<h1>1:1 문의 내역</h1>
				</div>
				<div class="main_cnt_list clearfix">
					<br>
					<h4 class="title">제목</h4>
					<!-- 기본적인 txt 시작 -->
					<p class="txt_cnt">정산신청을 했는데 왜 아직...</p>
					<!-- 기본적인 txt 끝 -->
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">일시</h4>
					<p class="txt_cnt">2019-10-21 12:12:30</p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">작성자</h4>
					<p class="txt_cnt">김진호</p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">문의 내역</h4>
				</div>
				<div class="main_cnt_list clearfix questionContent">정산 신청을 했는데
					아직 안됬습니다 첨부파일 참조 부탁드립니다</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">첨부파일</h4>
					<h2>정산신청내역.PDF</h2>
				</div>
				<div>
					<h1>답변하기</h1>
					<textarea type="text" class="answerText">내용을 입력해주세요.</textarea>
				</div>

				<div class="btn_wrap">
					<button type="submit" class="btn_com4">전송</button>
					<button type="submit" class="btn_com4">닫기</button>
				</div>

				<!-- 요소 끝 -->
				<script>
					$(function() {
						// 열리는 메뉴
						$(".nav_list").eq(7).addClass("on").addClass("open");
						$(".nav_list").eq(7).find(".nav_cnt_list").eq(0).addClass("on");
					});
				</script>
			</div>
		</div>
	</main>
	<%@ include file="../../../inc/admin/footer.jsp"%>