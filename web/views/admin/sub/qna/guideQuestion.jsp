<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>가이드 문의하기 관리</title>

	<%@ include file="../../../inc/admin/header.jsp"%>

	<main class="container">
	<div class="inner_ct">

		<!-- 테이블 위 영역 -->
		<div class="top_wrap clearfix">
			<!-- 검색폼 시작 -->
			<form action="" method="" class="srch_form">
				<div class="box_select">
					<select name="" id="">
						<option value="">No</option>
						<option value="">일시</option>
						<option value="">작성자</option>
					</select>
				</div>
				<div class="srch_wrap">
					<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
						class="srch_img"> <input type="text" name="" id=""
						class="srch_ipt">
				</div>
			</form>
			<!-- 검색폼 끝 -->
			<!-- 검색 오른쪽 영역 시작  -->
			<div class="srch_r_wrap">

				<!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->
				<div class="box_select">
					<select name="" id="">
						<option value="">미완료</option>
						<option value="">답변완료</option>

					</select>
				</div>
				<!-- select 박스 끝 -->
			</div>
			<!-- 검색 오른쪽 영역 끝  -->
		</div>
		<!-- 테이블 위 검색, 정렬 끝 -->
		<!-- table 시작 -->
		<div class="tbl_wrap">
			<table class="tbl">
				<colgroup>
					<col width="10%">
					<col width="40%">
					<col width="20%">
					<col width="20%">
					<col width="10%">
				</colgroup>
				<tr class="tbl_tit">
					<th>No</th>
					<th>제목</th>
					<th>일시</th>
					<th>작성자</th>
					<th>상태</th>
				</tr>
				<tr class="tbl_cnt">
					<td>5</td>
					<td><a href="#">정산신청 했는데 왜아직....</a></td>
					<td>2019-10-12</td>
					<td>김진호</td>
					<td>미완료</td>
				</tr>
				<tr class="tbl_cnt">
					<td>4</td>
					<td><a href="#">투어객이 이상해요</a></td>
					<td>2019-10-11</td>
					<td>김진호</td>
					<td>답변완료</td>
				</tr>
				<tr class="tbl_cnt">
					<td>3</td>
					<td><a href="#">오늘은 꿀꿀한데 쉬어도 되나요?</a></td>
					<td>2019-10-10</td>
					<td>김진호</td>
					<td>답변완료</td>
				</tr>
				<tr class="tbl_cnt">
					<td>2</td>
					<td><a href="#">가이드 못해먹겠습니다..</a></td>
					<td>2019-10-02</td>
					<td>김진호</td>
					<td>답변완료</td>
				</tr>
				<tr class="tbl_cnt">
					<td>1</td>
					<td><a href="#">플랫폼에서 지원가능한건 무엇이있나요?</a></td>
					<td>2019-10-01</td>
					<td>김진호</td>
					<td>답변완료</td>
				</tr>
			</table>
		</div>
		<!-- table 끝 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix">
				<li class="pager_com pager_arr prev"><a
					href="javascrpt: void(0);">&#x003C;</a></li>
				<li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
				<li class="pager_com pager_num on"><a
					href="javascrpt: void(0);">2</a></li>
				<li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
				<li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
				<li class="pager_com pager_arr next"><a
					href="javascrpt: void(0);">&#x003E;</a></li>
			</ul>
		</div>
		<script>
			$(function() {
				// 열리는 메뉴
				$(".nav_list").eq(7).addClass("on").addClass("open");
				$(".nav_list").eq(7).find(".nav_cnt_list").eq(1).addClass("on");
			});
		</script>
	</div>
	</main>

	<%@ include file="../../../inc/admin/footer.jsp"%>

