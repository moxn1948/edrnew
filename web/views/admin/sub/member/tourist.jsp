<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.common.model.vo.PageInfo"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<title>투어객 관리</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">
<main class="container">
<div class="inner_ct">
	<!-- 제목 -->
	<h2 class="main_tit"></h2>
	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath()%>/searchMember.ad"
			method="post" class="srch_form">
			<div class="box_select">
				<select name="searchVal" id="searchVal">
					<option value="id">ID</option>
					<option value="name">활동명</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="searchText"
					id="srch_ipt" value="" class="srch_ipt">
			</div>
			<button type="submit" id="btn_Search">검색</button>
		</form>
		<!-- 검색폼 끝 -->
		<div class="">
			<select name="memberfil" id="memberfil" class="memberfil">
				<option selected disabled hidden>이용자 분류</option>
				<option value="N">전체보기</option>
				<option value="TOURIST">투어객</option>
				<option value="GUIDE">가이드</option>
				<option value="Y">탈퇴한 회원</option>
			</select>
		</div>
		<!-- table 시작 -->
		<div class="tbl_wrap">
			<table id="listArea" class="tbl">
				<colgroup>
					<col width="25%">
					<col width="25%">
					<col width="25%">
					<col width="25%">
				</colgroup>
				<tr class="tbl_tit">
					<th>NO</th>
					<th>ID</th>
					<th>활동명</th>
					<th>가이드 여부</th>
				</tr>
				<%
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, Object> hmap = list.get(i);
				%>
				<tr class="tbl_cnt">
					<input type="hidden" name="mno" value="<%=hmap.get("mno")%>">
					<td><%=hmap.get("rnum")%></td>
					<td><%=hmap.get("email")%></td>
					<td><%=hmap.get("nickname")%></td>
					<td><%=hmap.get("mtype")%></td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	</div>

	<!-- table 끝 -->

	<!-- 페이저 시작 -->
	<div class="pager_wrap">
		<ul class="pager_cnt clearfix add">
			<%
					if (currentPage <= 1) {
				%>


			<li class="pager_com pager_arr prev"><a
				href="javascirpt: void(0);">&#x003C;</a></li>


			<%
					} else {
				%>
			<li class="pager_com pager_arr prev"><a
				href="<%=request.getContextPath()%>/selectMember.ad?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

			<%
					}
				%>
			<%
					for (int p = startPage; p <= endPage; p++) {
						if (p == currentPage) {
				%>
			<li class="pager_com pager_num on"><a
				href="javascript: void(0);"><%=p%></a></li>
			<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
			<%
					} else {
				%>
			<li class="pager_com pager_num"><a
				href="<%=request.getContextPath()%>/selectMember.ad?currentPage=<%=p%>"><%=p%></a></li>
			<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
			<%
					}
				%>
			<%
					}
				%>


			<%
					if (currentPage >= maxPage) {
				%>
			<li class="pager_com pager_arr next"><a
				href="javascript: void(0);">&#x003E;</a></li>
			<%
					} else {
				%>
			<li class="pager_com pager_arr next"><a
				href="<%=request.getContextPath()%>/selectMember.ad?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

			<%
					}
				%>

		</ul>
	</div>
	<!-- 페이저 끝 -->
</div>

</main>
<script>
		
			$(function() {
				// 열리는 메뉴
				$(".nav_list").eq(0).addClass("on").addClass("open");
				$(".nav_list").eq(0).find(".nav_cnt_list").eq(0).addClass("on");
			});
			
			$(function(){
				
			$(".tbl_cnt").click(function(){
				var num =$(this).children().eq(0).text();
				var str = $(this).children("input").val();
				console.log(str);	
				
				location.href= "<%=request.getContextPath()%>/selectOneMember.ad?str="+ str;
						})
		
		$(".memberfil").change(function(){
			 $(this).prop("selected", true);		
		   		var str = $(this).val();
		   		var num = $(".tbl_cnt").children("input").val();
		   			   		
		   		if(str == "전체보기") {
		   			location.href="<%=request.getContextPath()%>/selectMember.ad";
		   		}else {
		   			location.href="<%=request.getContextPath()%>/memberfilter.ad?str="+str;
		   		}
		   		
		});
			
			
		   		
	
		   
	    $("#listArea td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
	    
			});
	   
	
</script>
<%@ include file="../../../inc/admin/footer.jsp"%>




