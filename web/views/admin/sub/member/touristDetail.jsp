<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%
HashMap<String, Object> hmap = (HashMap<String, Object>) request.getAttribute("hmap");
%>
<title>투어객 상세관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/user/jakeStyle_user.css">
<main class="container">

<div class="inner_ct">
	<div class="main_cnt">
		<!-- 버튼 시작  -->
		<form id="updateForm" method="post">
			<div class="main_cnt_list clearfix">
				<div class="btn_wrap">
					<button type="submit" class="btn_com btn_white"
						onclick="complete()">저장</button>
					<button type="submit" class="btn_com btn_white"
						onclick="deleteMember()">삭제</button>
				</div>
			</div>

			<!-- 버튼 끝  -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list2 clearfix">
				<div class="main_cnt_list3 clearfix">
					<br>
					<h2>회원 정보</h2>
				</div>
				<div class="main_cnt_list clearfix">
					<br> 
					<input type="hidden" name="mno" value="<%=hmap.get("mno")%>">
					<h4 class="title">ID</h4>
					<!-- 기본적인 txt 시작 -->
					<p class="txt_cnt"><%=hmap.get("email")%></p>
					<!-- 기본적인 txt 끝 -->
					
					
					
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">성별</h4>
					<p class="txt_cnt"><%=hmap.get("mgender")%></p>
					<h4 class="title boolean">가이드 여부</h4>
					<p class="txt_cnt"><%=hmap.get("mtype")%></p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">나이</h4>
					<p class="txt_cnt"><%=hmap.get("mbirth")%>대</p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">가입일</h4>
					<p class="txt_cnt"><%=hmap.get("enrolldate")%></p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">활동명</h4>
					<h4 class="title account">환불 계좌</h4>
				</div>
				<div class="main_cnt_list clearfix">
					<input class="txt_cnt22" name="nickname"
						value="<%=hmap.get("nickname")%>">
					<p class="txt_cnt22 refund"><%=hmap.get("accountname")%></p>
					<p class="txt_cnt22"><%=hmap.get("accountno")%></p>
				</div>
				<br>

				<!-- 요소 끝 -->
				<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(0).addClass("on").addClass("open");
   $(".nav_list").eq(0).find(".nav_cnt_list").eq(0).addClass("on");
});
function complete(){
	$("#updateForm").attr("action", "<%=request.getContextPath()%>/updateMember.ad?num=<%=hmap.get("mno")%>");
}
function deleteMember(){
	$("#updateForm").attr("action", "<%=request.getContextPath()%>/deleteMember.ad?num=<%=hmap.get("mno")%>");
					}
				</script>

			</div>
		</form>
	</div>

</div>

</main>

<%@ include file="../../../inc/admin/footer.jsp"%>

