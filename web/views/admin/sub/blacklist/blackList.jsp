<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>블랙리스트 관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css" href="/edr/css/admin/jakeStyle_admin.css">
<!-- 팝업 관련 css -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/magnific-popup.css">

<main class="container">
<div class="inner_ct">

	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="" method="" class="srch_form">
			<div class="box_select">
				<select name="" id="">
					<option value="">no</option>
					<option value="">ID</option>
					<option value="">이름</option>
					<option value="">사유</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="" id=""
					class="srch_ipt">
			</div>
		</form>
		<!-- 검색폼 끝 -->
		<!-- 검색 오른쪽 영역 시작  -->
		<div class="srch_r_wrap">
			<!-- 버튼 시작  -->
			<div class="btn_wrap">
				<a class="btn_white detailPop btnAdd" href="../popup/addBlackListPop.jsp">추가</a>
				<button type="submit" class="btn_com btn_blue">가이드</button>
				<button type="submit" class="btn_com btn_white">투어객</button>
			</div>
			<!-- 버튼 끝  -->
			<!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->
			<!-- 			<div class="box_select">
				<select name="" id="">
					<option value="">01</option>
					<option value="">02</option>
					<option value="">03</option>
					<option value="">04</option>
				</select>		
			</div> -->
			<!-- select 박스 끝 -->
		</div>
		<!-- 검색 오른쪽 영역 끝  -->
	</div>
	<!-- 테이블 위 검색, 정렬 끝 -->
	<!-- table 시작 -->
	<div class="tbl_wrap">
		<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="40%">
				<col width="20%">
				<col width="20%">
				<col width="10%">
			</colgroup>
			<tr class="tbl_tit">
				<th>no</th>
				<th>ID</th>
				<th>이름</th>
				<th>사유</th>
				<th></th>
			</tr>
			<tr class="tbl_cnt">
				<td>5</td>
				<td><a href="../popup/blackListDetailPop.jsp" class="blackListPop">iamjqueryking@kh.co.kr</a></td>
				<td>쿼리킹</td>
				<td>욕설</td>
				<td><button>삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>4</td>
				<td><a href="#">iamjqueryking@kh.co.kr</a></td>
				<td>자바킹</td>
				<td>욕설</td>
				<td><button>삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>3</td>
				<td><a href="#">iamjqueryking@kh.co.kr</a></td>
				<td>디비킹</td>
				<td>범죄</td>
				<td><button>삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>2</td>
				<td><a href="#">iamjqueryking@kh.co.kr</a></td>
				<td>연결킹</td>
				<td>욕설</td>
				<td><button>삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>1</td>
				<td><a href="#">iamjqueryking@kh.co.kr</a></td>
				<td>스크킹</td>
				<td>음란물유포</td>
				<td><button>삭제</button></td>
			</tr>
		</table>
	</div>
	<!-- table 끝 -->
	<div class="pager_wrap">
		<ul class="pager_cnt clearfix">
			<li class="pager_com pager_arr prev"><a
				href="javascrpt: void(0);">&#x003C;</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
			<li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
			<li class="pager_com pager_arr next"><a
				href="javascrpt: void(0);">&#x003E;</a></li>
		</ul>
	</div>
	
	<!-- 팝업 관련 js -->
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common/jquery.magnific-popup.min.js"></script>

	<script>
			$(function() {
				/* 팝업 예시 */
				$('.blackListPop').magnificPopup({
					type : 'ajax',
					callbacks : {
						/*  beforeOpen : function(){
					      console.log("beforeOpen");   
					   },
					 parseAjax : function(mfpResponse){
					    console.log("parseAjax");     
					    console.log('Ajax content loaded:', mfpResponse);
					    
					},
					change :  function(){
					   
					  },
					close : function(){
					    console.log("close");     
					},
					open : function(){
					    console.log("open");     
					    

					}, */
					ajaxContentAdded : function() {

					}
				}
				
				});
				
				$('.detailPop').magnificPopup({
					type: 'ajax',callbacks : {
						/*  beforeOpen : function(){
					      console.log("beforeOpen");   
					   },
					 parseAjax : function(mfpResponse){
					    console.log("parseAjax");     
					    console.log('Ajax content loaded:', mfpResponse);
					    
					},
					change :  function(){
					   
					  },
					close : function(){
					    console.log("close");     
					},
					open : function(){
					    console.log("open");     
					    

					}, */
					ajaxContentAdded : function() {

					}
				}
				
				});
				
				// 열리는 메뉴
				$(".nav_list").eq(6).addClass("on").addClass("open");
				$(".nav_list").eq(6).find(".nav_cnt_list").eq(2).addClass("on");
			});
		</script>
</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>

