<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.common.model.vo.Attachment, com.edr.generalProgram.model.vo.Gp"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<Attachment> list2 = (ArrayList<Attachment>) request.getAttribute("list2");
	ArrayList<HashMap<String, Object>> list3 = (ArrayList<HashMap<String, Object>>) request.getAttribute("list3");
	ArrayList<HashMap<String, Object>> list4 = (ArrayList<HashMap<String, Object>>) request.getAttribute("list4");
	int num = (int) request.getAttribute("num");
	
%>
<title>가이드 상세관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/user/jakeStyle_user.css">
<style>
.img_wrap{overflow: hidden;width: 400px;height: 300px;}
.img_wrap img{width: 100%;}
</style>
<main class="container">
<div class="inner_ct">
	<div class="main_cnt">
		<!-- 버튼 시작  -->
		<form id="updateForm" method="post">
			<div class="main_cnt_list clearfix">
				<div class="btn_wrap">
					<button type="submit" class="btn_com btn_white"
						onclick="complete()">저장</button>
					<!-- <button type="submit" class="btn_com btn_white"
						onclick="deleteGuide()">삭제</button> -->
				</div>
			</div>
			<!-- 버튼 끝  -->
			<!-- 요소 시작 -->
					
			<%
				for (int i = 0; i < 1; i++) {
					HashMap<String, Object> hmap = list.get(i);
					HashMap<String, Object> hmap2 = list3.get(i);
					HashMap<String, Object> hmap3 = list4.get(i);

			%>
			<div class="main_cnt_list2 clearfix">
				<div class="main_cnt_list3 clearfix">
					<br>
					<h2>회원 정보</h2>
					<div class="img_wrap">
						<img src="/edr/uploadFiles/<%=hmap2.get("changename")%>" id="imgtag">
					</div>
				</div>
				<div class="main_cnt_list clearfix">
					<br> <input type="hidden" name="mno"
						value="<%=hmap.get("mno")%>">
					<h4 class="title">ID</h4>
					<!-- 기본적인 txt 시작 -->
					<p class="txt_cnt"><%=hmap.get("email")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">카카오톡 ID</h4>
					<p class="txt_cnt"><%=hmap.get("kakaoid")%></p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">가이드 신청일</h4>
					<p class="txt_cnt"><%=hmap.get("ghdate")%></p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">이름</h4>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div>
					<input class="txt_cnt cnt" name="gname"
						value="<%=hmap.get("gname")%>">
				</div>
				<br>
				<div class="main_cnt_list clearfix">
					<h4 class="title">활동도시</h4>
				</div>
				<div>
					<p class="txt_cnt"><%=hmap.get("localname")%></p>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">가능언어</h4>
					
					<div class="box_chk">
						<p class="txt_cnt cnt3"><%=hmap.get("lang") %></p>
					</div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">그 외</h4>
					<a href="javascript: void(0);" class="lang_add_btn"></a>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">전화번호</h4>
					<h4 class="title cnt2">비상연락망</h4>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div>
					<p class="txt_cnt cnt3"><%=hmap.get("phone")%></p>
					<p class="txt_cnt cnt3 cnt4"><%=hmap.get("phone2")%></p>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">주소</h4>
				</div>
				<div class="main_cnt_list clearfix">
					<div class="box_input">
						<p class="txt_cnt" id="address1"><%=hmap.get("address")%></p>
						<!-- <p class="txt_cnt" id="address2"></p>
						<p class="txt_cnt" id="address3"></p> -->
					</div>
				</div>
				
						
				<div class="tbl_wrap">
					<table class="tbl">
						<colgroup>
							<col width="20%">
							<col width="80%">
						</colgroup>
						<tr class="tbl_tit">
							<th>NO.</th>
							<th>프로그램명</th>
						</tr>
						<tr class="tbl_cnt">
							<td id="gpno"><%=hmap.get("gpno")%></td>
							<td id="gpname"><%=hmap.get("gpname")%></td>
						</tr>
					</table>
				</div>
					
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">소개</h4>
				</div>
				<div class="main_cnt_list clearfix">
					<div class="introduction"><%=hmap.get("introduce")%></div>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">첨부파일</h4>
				</div>
				<div class="">
					<table>
						<%
							for (int j = 0; j < list2.size(); j++) {
									Attachment at = list2.get(j);
						%>
						<tr>
							<td><a class="txt_cnt"
								href="<%=request.getContextPath()%>/selectOneGuideDownload.ad?num=<%=at.getFileNo()%>"><%=at.getOriginName()%></a></td>
						</tr>
						<%
							}
						%>
					</table>
				</div>
				<br> <br>
			</div>
		</form>
	</div>



	<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(1).addClass("on").addClass("open");
   $(".nav_list").eq(1).find(".nav_cnt_list").eq(1).addClass("on");
});


/* $(function(){
	
	var gpno = document.getElementById("gpno").value;
	var gpname = document.getElementById("gpname").value;
	
	console.log(gpno);
	console.log(gpname);
	
	}); */
function complete(){
	$("#updateForm").attr("action", "<%=request.getContextPath()%>/updateGuide.ad?num=<%=hmap.get("mno")%>");
		}
	</script>
	<%
		}
	%>


</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>


