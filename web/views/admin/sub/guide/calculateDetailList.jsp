<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideDetail, com.edr.common.model.vo.*"%>

<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

	int num = (int) request.getAttribute("num");
	
%>

<style>
	.cal_detail {
		margin-left:220px;
		margin-top:-25px;
		line-height:120px;
	}
	button {
		background-color:white;
		width:50px;
		height:30px;
	}
	.btnArea {
		margin-top:-700px;
		float:right;	
	}
</style>


<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 


<main class="container">
<div class="inner_ct">
	<!-- 제목 -->
	<h2 class="main_tit">가이드 정산신청 신청</h2>
	<div class="main_cnt">
		<!-- 요소 시작 -->
		<% for(int i = 0; i < list.size(); i++)  {
			
			HashMap<String, Object> hmap = list.get(i);
			
			if(hmap.get("pkind").equals("GP")) {
		%>
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">ID</h4>
			<!-- 기본적인 txt 시작 -->
			<p class="txt_cnt"><%=hmap.get("email") %></p>
			<!-- 기본적인 txt 끝 -->
		</div>

		<div class="main_cnt_list clearfix">
			<h4 class="title cal">이름</h4>
			<!-- 기본적인 input 시작 -->
			<p class="txt_cnt"><%=hmap.get("gname") %></p>
			<!-- 기본적인 input 끝 -->

		</div>
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산금지급계좌</h4>
			<p class="txt_cnt"><%=hmap.get("bankName")%>  <%=hmap.get("accountNo") %></p>
		</div>

		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산 신청일</h4>
			<!-- 체크박스 시작 -->
			<p class="txt_cnt"><%=hmap.get("calcDate") %></p>
			<!-- 체크박스 끝 -->
		</div>
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산 지급일</h4>
			<%if(hmap.get("calcPayment") == null) { %>
			<p class="txt_cnt">미지급</p>
			<%}else { %>
			<p class="txt_cnt"><%=hmap.get("calcPayment") %></p>
			<%} %>
		</div>
	</div>

	<h4 class="title cal">정산 요청 프로그램</h4>
	<div class="tbl_wrap">
	<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="10%">
				<col width="20%">
				
			</colgroup>
			<tr class="tbl_tit">
				<th>NO</th>
				<th>프로그램명</th>
				<th>회차</th>
				<th>금액</th>
				
			</tr>
			
			<tr class="tbl_cnt">
				<input type="hidden" value="">
				<td>i</td>
				<td><%= hmap.get("gpName")%></td>
				<td><%= hmap.get("epi") %></td>
				<td><%= hmap.get("gpCost")%></td>
				
				
				
			</tr>
			
		</table>
		</div>
	<!-- <div class="tbl_wrap wait cal">
		<table class="tbl">
			<colgroup>
				<col width="100%">
			</colgroup>
			<tr class="tbl_tit">
				<th>day1</th>
			</tr>
			<tr class="tbl_cnt wait">
				<td>cnt1</td>
			</tr>
		</table> -->
		


		<br>
		<br>
		<br>

	</div>

	<div class="title cal2 pay">
		<h4 class="title last2">정산 요청 금액</h4>
		
		<h4 class="title last2">정산 금액 지급 상태</h4>
	</div>
	
	<div class="cal_detail">
		<p class="txt_cnt"><%=hmap.get("calcCost") %> 원</p>
		<%if(hmap.get("calcState").equals("승인 대기중")) {
		%>
		<select class="cal_pay2">
			<option selected>승인 대기중</option>
			<option>지급 완료</option>
			<option>승인  보류</option>
		</select>
	
	<%}else if(hmap.get("calcState").equals("지급완료")){
	
	%>
	<select class="cal_pay2">
			<option>승인 대기중</option>
			<option selected>지급 완료</option>
			<option>승인  보류</option>
		</select>
	
	<%} else {%>
	<select class="cal_pay2Select">
			<option >승인 대기중</option>
			<option>지급 완료</option>
			<option selected>승인  보류</option>
		</select>
	<%} %>
	</div>
	<%}else { %>
	<!-- CP일때 출력 -->
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">ID</h4>
			<!-- 기본적인 txt 시작 -->
			<p class="txt_cnt"><%=hmap.get("email") %></p>
			<!-- 기본적인 txt 끝 -->
		</div>

		<div class="main_cnt_list clearfix">
			<h4 class="title cal">이름</h4>
			<!-- 기본적인 input 시작 -->
			<p class="txt_cnt"><%=hmap.get("gname") %></p>
			<!-- 기본적인 input 끝 -->

		</div>
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산금지급계좌</h4>
			<p class="txt_cnt"><%=hmap.get("bankName")%>  <%=hmap.get("accountNo") %></p>
		</div>

		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산 신청일</h4>
			<!-- 체크박스 시작 -->
			<p class="txt_cnt"><%=hmap.get("calcDate") %></p>
			<!-- 체크박스 끝 -->
		</div>
		<div class="main_cnt_list clearfix">
			<h4 class="title cal">정산 지급일</h4>
			<p class="txt_cnt"><%=hmap.get("calcPayment") %></p>
		</div>

	<h4 class="title cal">정산 요청 프로그램</h4>
	<div class="tbl_wrap">
	<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="10%">
				<col width="20%">
				
			</colgroup>
			<tr class="tbl_tit">
				<th>NO</th>
				<th>프로그램명</th>
				<th>회차</th>
				<th>금액</th>
				
			</tr>
			
			
			<tr class="tbl_cnt">
				<input type="hidden" value="">
				<td>i</td>
				<td><%=hmap.get("cpTitle") %></td>
				<td><%=hmap.get("epi") %></td>
				<td><%=hmap.get("cpCost") %></td>
				
				
				
			</tr>
			
		</table>
		</div>
	<!-- <div class="tbl_wrap wait cal">
		<table class="tbl">
			<colgroup>
				<col width="100%">
			</colgroup>
			<tr class="tbl_tit">
				<th>day1</th>
			</tr>
			<tr class="tbl_cnt wait">
				<td>cnt1</td>
			</tr>
		</table> -->
		


		<br>
		<br>
		<br>


	<div class="title cal2">
		<h4 class="title last2">정산 요청 금액</h4>
		
		<h4 class="title last2">정산 금액 지급 상태</h4>
	</div>
	
	<div class="cal_detail">
		<p class="txt_cnt"><%=hmap.get("calcCost") %> 원</p>
		<%if(hmap.get("calcState").equals("승인 대기중")) {
		%>
		<select class="cal_pay2">
			<option selected>승인 대기중</option>
			<option>지급 완료</option>
			<option>승인  보류</option>
		</select>
	
	<%}else if(hmap.get("calcState").equals("지급완료")){
	
	%>
	<select class="cal_pay2Select">
			<option>승인 대기중</option>
			<option selected>지급 완료</option>
			<option>승인  보류</option>
		</select>
	
	<%} else {%>
	<select class="cal_pay2">
			<option >승인 대기중</option>
			<option>지급 완료</option>
			<option selected>승인  보류</option>
		</select>
	<%} %>
	</div>
	
	
	
	
	
	
	
	<%} %>
	<%} %>
	<div class="btnArea">
		<button onclick="submit();">저장</button>
		<button onclick="">취소</button>
	</div>
	
	
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>




	<!-- 버튼 끝  -->

</main>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(1).addClass("on").addClass("open");
   $(".nav_list").eq(1).find(".nav_cnt_list").eq(2).addClass("on");
   
   

   
});
function submit() {
	var str = $(".cal_pay2Select option:selected").text();
	console.log(str);
	location.href="<%=request.getContextPath()%>/adCalculateUpdate.gd?num=<%=num%>&str="+str;	
}
</script>

<%@ include file="../../../inc/admin/footer.jsp"%>
