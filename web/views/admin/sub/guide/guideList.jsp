<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.guide.model.vo.GuideDetail, com.edr.common.model.vo.*"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>

<title>가이드 관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">
<main class="container">
<div class="inner_ct">
	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath()%>/searchGuide.ad"
			method="get" class="srch_form">
			<div class="box_select">
				<select name="searchVal" id="searchVal">
					<option value="id">ID</option>
					<option value="name">이름</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="searchText"
					value="" id="srch_ipt" class="srch_ipt">
			</div>
			<button type="submit" id="btn_Search">검색</button>
		</form>
		<!-- 검색폼 끝 -->


		<!-- 테이블 위 검색, 정렬 끝 -->
		<!-- table 시작 -->
		<div class="tbl_wrap">
			<table id="listArea" class="tbl">

				<colgroup>
					<col width="10%">
					<col width="40%">
					<col width="20%">
					<col width="20%">
					<col width="10%">
				</colgroup>
				<tr class="tbl_tit">
					<th>No</th>
					<th>ID</th>
					<th>이름</th>
					<th>핸드폰 번호</th>
					<th>활동도시</th>
				</tr>
				<%
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, Object> hmap = list.get(i);
				%>
				<tr class="tbl_cnt">
					<input type="hidden" value="<%=hmap.get("mno")%>">
					<%-- <input type="hidden" value="<%=hmap.get("gpno")%>"> --%>
					<td><%=hmap.get("rnum")%></td>
					<td><%=hmap.get("email")%></td>
					<td><%=hmap.get("gname")%></td>
					<td><%=hmap.get("phone")%></td>
					<td><%=hmap.get("localname")%></td>
				</tr>

				<%
					}
				%>
			</table>
		</div>
		<!-- table 끝 -->
		<!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<%
					if (currentPage <= 1) {
				%>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%
					} else {
				%>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/selectGuide.ad?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

				<%
					}
				%>
				<%
					for (int p = startPage; p <= endPage; p++) {
						if (p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p%></a></li>

				<%
					} else {
				%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/selectGuide.ad?currentPage=<%=p%>"><%=p%></a></li>

				<%
					}
				%>
				<%
					}
				%>


				<%
					if (currentPage >= maxPage) {
				%>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%
					} else {
				%>
				<li class="pager_com pager_arr next"><a
					href="<%=request.getContextPath()%>/selectGuide.ad?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

				<%
					}
				%>

			</ul>
		</div>
		<!-- 페이저 끝 -->
		<script>
	$(function() {
		// 열리는 메뉴
		$(".nav_list").eq(1).addClass("on").addClass("open");
		$(".nav_list").eq(1).find(".nav_cnt_list").eq(1).addClass("on");
	});
	$(function(){
		
		$(".tbl_cnt").click(function(){
			var str =  $(this).children().eq(0).val();
			//  var gpno = $(this).children().eq(1).val();
			  var num = $(this).children("input").val();
			  
			  console.log("str : " + str);
			  // console.log("gpno : " + gpno);
			
			location.href= "<%=request.getContextPath()%>/selectOneGuide.ad?num="+str;
								});

			});
	
	   $("#listArea td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
		</script>
	</div>
</main>
<%@ include file="../../../inc/admin/footer.jsp"%>


