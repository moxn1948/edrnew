<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>

<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

 	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
	/* String searchTxdt = (String) request.getAttribute("searchTxt");
	String category = (String) request.getAttribute("category"); */
%>



<%@ include file="../../../inc/admin/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<main class="container">
  <div class="inner_ct">
    <!-- 제목 -->
    <h2 class="main_tit">가이드 정산신청 관리</h2>
    <!-- 테이블 위 영역 -->
    <div class="top_wrap clearfix">
      <!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath() %>/adsearhCalculate.gd" method="get" class="srch_form">
			<div class="box_select">
				<select name="category" id="category">
					<option selected disabled hidden>카테고리</option>
					<option value="gname">활동명</option>
					<option value="id">ID</option>
				</select>		
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt="" class="srch_img">
				<input type="text" name="searchTxt" id="" class="srch_ipt" >
			</div>
		</form>
		<!-- 검색폼 끝 -->
		<!-- 검색 오른쪽 영역 시작  -->
		<div class="srch_r_wrap">
			<!-- 버튼 시작  -->
<!-- 			<div class="btn_wrap">
				<button type="submit" class="btn_com btn_white">신청하기</button>
				<button type="submit" class="btn_com btn_blue">신청하기</button>
			</div> -->
			<!-- 버튼 끝  -->
			<!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->
 			<div class="box_select">
				<select name="" id="filter">
					<option selected dlsabled hidden>정산상태 </option>
					<option value="HOLD">HOLD</option>
					<option value="PAYMENT">PAYMENT</option>
					<option value="WAIT">WAIT</option>
					<!-- <option value="">03</option>
					<option value="">04</option> -->
				</select>		
			</div>
			<!-- select 박스 끝 -->
		</div>
		<!-- 검색 오른쪽 영역 끝  -->
    </div>
    <!-- 테이블 위 검색, 정렬 끝 -->
    <!-- table 시작 -->
    <div class="tbl_wrap">
      <table class="tbl">
        <colgroup>
          <col width="10%">
          <col width="20%">
          <col width="10%">
          <col width="20%">
          <col width="10%">
          <col width="20%">
          <col width="10%">
        </colgroup>
        <tr class="tbl_tit">
          <th>NO</th>
          <th>ID</th>
          <th>이름</th>
          <th>정산 신청 일자</th>
          <th>정산 요청 금액</th>
          <th>정산 지급 일시</th>
          <th>정산 상태</th>
        </tr>
        <%for(int i = 0; i < list.size(); i++) {
       		HashMap<String, Object> hmap = list.get(i); 	
       
        	%>
        
        <tr class="tbl_cnt">
        <input type="hidden" value="<%=hmap.get("calcNo")%>">
          <td><%=hmap.get("rnum") %></td>
          <td><%=hmap.get("email") %></td>
          <td><%=hmap.get("gname") %></td>
          <td><%=hmap.get("calcDate") %></td>
          <td><%=hmap.get("calcCost") %></td>
          <td><%=hmap.get("calcPay") %></td>
          <td><%=hmap.get("calcState") %></td>
        </tr>
        <%} %>
        
      </table>
    </div>
    <!-- table 끝 -->
    <!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev on"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev "><a
					href="<%=request.getContextPath()%>/adGuideCalculate.gd?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a href="javascript: void(0);"
					><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/adGuideCalculate.gd?currentPage=<%=p%>"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next on"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/adGuideCalculate.gd?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
  </div>
</main>
<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(1).addClass("on").addClass("open");
   $(".nav_list").eq(1).find(".nav_cnt_list").eq(2).addClass("on");
   
   
   $(".tbl_cnt").click(function(){
		  var str =  $(this).children().eq(0).text();
		  var num = $(this).children("input").val();
		  
		   console.log(num);
		   location.href="<%= request.getContextPath()%>/adGuideCalculateDetail.gd?num=" + num;
	   })
	   

	    $(".tbl_cnt td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
   
   
});

$("#filter").change(function(){
	var str = $(this).val();
		console.log(str);
		location.href="<%=request.getContextPath()%>/adGuideCalFilter.gd?str="+str;
});
</script>

<%@ include file="../../../inc/admin/footer.jsp" %>
