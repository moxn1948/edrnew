<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>FAQ</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">

<main class="container">
<div class="inner_ct">

	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="" method="" class="srch_form">
			<div class="box_select">
				<select name="" id="">
					<option value="">No</option>
					<option value="">질문</option>
					<option value="">답변내용</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="" id=""
					class="srch_ipt">
			</div>
		</form>
		<!-- 검색폼 끝 -->
		<!-- 검색 오른쪽 영역 시작  -->
		<div class="srch_r_wrap">
			<button class="btn_add">추가</button>
		</div>
		<!-- 검색 오른쪽 영역 끝  -->
	</div>
	<!-- 테이블 위 검색, 정렬 끝 -->
	<!-- table 시작 -->
	<div class="tbl_wrap">
		<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="40%">
				<col width="10%">
				<col width="10%">
			</colgroup>
			<tr class="tbl_tit">
				<th>No</th>
				<th>질문</th>
				<th>답변내용</th>
				<th></th>
				<th></th>
			</tr>
			<tr class="tbl_cnt">
				<td>5</td>
				<td>아이디 찾기가 안되요</td>
				<td><a href="#">플랫폼으로 전화 문의 부탁드립니다. </a></td>
				<td><button class="btn_modi">수정</button></td>
				<td><button class="btn_del">삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>4</td>
				<td>가이드가 이상해요</td>
				<td><a href="#">플랫폼으로 전화 문의 부탁드립니다. </a></td>
				<td><button class="btn_modi">수정</button></td>
				<td><button class="btn_del">삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>3</td>
				<td>주문상세 페이지가 안들어가져요</td>
				<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
				<td><button class="btn_modi">수정</button></td>
				<td><button class="btn_del">삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>2</td>
				<td>결제가 안돼요</td>
				<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
				<td><button class="btn_modi">수정</button></td>
				<td><button class="btn_del">삭제</button></td>
			</tr>
			<tr class="tbl_cnt">
				<td>1</td>
				<td>가이드 신청을 했는데 언제처리되나요?</td>
				<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
				<td><button class="btn_modi">수정</button></td>
				<td><button class="btn_del">삭제</button></td>
			</tr>
		</table>
	</div>
	<!-- table 끝 -->
	<!-- 페이저 시작 -->
	<div class="pager_wrap">
		<ul class="pager_cnt clearfix">
			<li class="pager_com pager_arr prev"><a
				href="javascrpt: void(0);">&#x003C;</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
			<li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
			<li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
			<li class="pager_com pager_arr next"><a
				href="javascrpt: void(0);">&#x003E;</a></li>
		</ul>
	</div>
	<!-- 페이저 끝 -->
	<script>
		$(function() {
			// 열리는 메뉴
			$(".nav_list").eq(8).addClass("on").addClass("open");
			$(".nav_list").eq(8).find(".nav_cnt_list").eq(0).addClass("on");
		});
	</script>
</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>
