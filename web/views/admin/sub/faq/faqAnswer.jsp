<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<title>FAQ 답변달기</title>

	<%@ include file="../../../inc/admin/header.jsp"%>
	<link rel="stylesheet" type="text/css" href="/edr/css/admin/jakeStyle_admin.css">

	<main class="container">
	<div class="inner_ct">

		<!-- 테이블 위 영역 -->
		<div class="top_wrap clearfix">
			<!-- 검색폼 시작 -->
			<form action="" method="" class="srch_form">
				<div class="box_select">
					<select name="" id="">
						<option value="">질문</option>
						<option value="">답변내용</option>
					</select>
				</div>
				<div class="srch_wrap">
					<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
						class="srch_img"> <input type="text" name="" id=""
						class="srch_ipt">
				</div>
			</form>
			<!-- 검색폼 끝 -->
			<!-- 검색 오른쪽 영역 시작  -->
			<div class="srch_r_wrap">
				<button class="btn_add">추가</button>
			</div>
			<!-- 검색 오른쪽 영역 끝  -->
		</div>
		<!-- 테이블 위 검색, 정렬 끝 -->
		<!-- table 시작 -->
		<div class="tbl_wrap">
			<table class="tbl">
				<colgroup>
					<col width="40%">
					<col width="40%">
					<col width="10%">
					<col width="10%">
				</colgroup>
				<tr class="tbl_tit">
					<th>질문</th>
					<th>답변내용</th>
					<th></th>
					<th></th>
				</tr>
				<tr class="tbl_cnt">
					<td>아이디 찾기가 안되요</td>
					<td><a href="#">플랫폼으로 전화 문의 부탁드립니다. </a></td>
					<td><button class="btn_modi">수정</button></td>
					<td><button class="btn_del">삭제</button></td>
				</tr>
				<tr class="tbl_cnt">
					<td>가이드가 이상해요</td>
					<td><a href="#">플랫폼으로 전화 문의 부탁드립니다. </a></td>
					<td><button class="btn_modi">수정</button></td>
					<td><button class="btn_del">삭제</button></td>
				</tr>
				<tr class="tbl_cnt">
					<td>주문상세 페이지가 안들어가져요</td>
					<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
					<td><button class="btn_modi">수정</button></td>
					<td><button class="btn_del">삭제</button></td>
				</tr>
				<tr class="tbl_cnt">
					<td>결제가 안돼요</td>
					<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
					<td><button class="btn_modi">수정</button></td>
					<td><button class="btn_del">삭제</button></td>
				</tr>
				<tr class="tbl_cnt">
					<td>가이드 신청을 했는데 언제처리되나요?</td>
					<td><a href="#">플랫폼으로 전화 문의 부탁드립니다.</a></td>
					<td><button class="btn_modi">수정</button></td>
					<td><button class="btn_del">삭제</button></td>
				</tr>
			</table>
		</div>
		<!-- table 끝 -->
		<div class="secondDiv2">
			<div class="main_cnt_list clearfix">
				<h2 class="title">질문</h2>
			</div>
			<div class="main_cnt_list clearfix">
				<h2 class="title">회원 ID</h2>
				<div class="box_input">
					<input type="text" name="" id="" placeholder="ID 찾기가 안되요.">
				</div>
			</div>
			<div class="main_cnt_list clearfix">
				<h2 class="title">답변</h2>				
			</div>
			<div class="main_cnt_list clearfix">
				<input type="text" class="answertext" placeholder="수정부분 입니다.">
			</div>
		</div>
	</main>
	<%@ include file="../../../inc/admin/footer.jsp"%>
