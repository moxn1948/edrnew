<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.review.model.vo.*,com.edr.common.model.vo.PageInfo"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	Review r = (Review) request.getAttribute("r");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<title>후기관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">

<main class="container">
<div class="inner_ct">

	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath()%>/searchReview.ad"
			method="post" class="srch_form">
			<div class="box_select">
				<select name="searchVal" id="searchVal">
					<option value="pname">프로그램명</option>
					<option value="name">구매자명</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="searchText"
					id="searchText" class="srch_ipt">
			</div>
			<button type="submit" id="btn_Search">검색</button>
		</form>
		<!-- 검색폼 끝 -->
		<!-- 검색 오른쪽 영역 시작  -->
		<div class="srch_r_wrap">

			<!-- 버튼 끝  -->
			<!-- <!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->

			<!-- 검색 오른쪽 영역 끝  -->
		</div>
		<!-- 테이블 위 검색, 정렬 끝 -->
		<!-- table 시작 -->
		<form id="updateForm" method="post">
			<div class="tbl_wrap">
				<table class="tbl" id="listArea">
					<colgroup>
						<col width="20%">
						<col width="40%">
						<col width="10%">
						<col width="15%">
						<col width="15%">
					</colgroup>
					<tr class="tbl_tit">
						<th>리뷰 번호</th>
						<th>프로그램명</th>
						<th>구매자명</th>
						<th>가이드 평점</th>
						<th>프로그램 평점</th>
					</tr>
					<%
						for (int i = 0; i < list.size(); i++) {
							HashMap<String, Object> hmap = list.get(i);
					%>
					<tr class="tbl_cnt">
						<input type="hidden" name="reviewno"
							value="<%=hmap.get("reviewno")%>">
						<td><%=hmap.get("rnum")%></td>
						<td><%=hmap.get("gpname")%></td>
						<td><%=hmap.get("ordername")%></td>
						<td><%=hmap.get("guiderating")%></td>
						<td><%=hmap.get("programrating")%></td>
					</tr>
					<%
						}
					%>

				</table>
			</div>
		</form>
		<!-- table 끝 -->
		<!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<%
					if (currentPage <= 1) {
				%>


				<li class="pager_com pager_arr prev"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%
					} else {
				%>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/selectReview.ad?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

				<%
					}
				%>
				<%
					for (int p = startPage; p <= endPage; p++) {
						if (p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p%></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%
					} else {
				%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/selectReview.ad?currentPage=<%=p%>"><%=p%></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%
					}
				%>
				<%
					}
				%>
				<%
					if (currentPage >= maxPage) {
				%>
				<li class="pager_com pager_arr next"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%
					} else {
				%>
				<li class="pager_com pager_arr next"><a
					href="<%=request.getContextPath()%>/selectReview.ad?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

				<%
					}
				%>

			</ul>
		</div>
		<!-- 페이저 끝 -->
	
		
		<script>
		$(function() {	
		
				
				$(".tbl_cnt").click(function(){
					var num =$(this).children().eq(0).text();
					var str = $(this).children("input").val();
					console.log(str);	
					
					location.href= "<%=request.getContextPath()%>/selectOneReview.ad?str="+ str;
							})
			
			// 열리는 메뉴
			$(".nav_list").eq(5).addClass("on").addClass("open");
			$(".nav_list").eq(5).find(".nav_cnt_list").eq(0).addClass("on");
		
			$("#listArea td").mouseenter(function() {
		        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
		     }).mouseout(function(){
		        $(this).parent().css({"background":"white", "color":"#333"});
		     });
		
		});
		
		
		
		</script>

	</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>
