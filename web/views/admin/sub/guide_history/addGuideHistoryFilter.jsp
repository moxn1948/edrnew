<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>


<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

 	PageInfo pi = (PageInfo) request.getAttribute("pi");
 	String str = (String)request.getAttribute("str");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
	/* int num2 = 0;
	if(num2 != 0) {
		for(int i = 0; i < 1; i++) {
				num2 = (int) list.get(i).get("mno");
		}
	} */
	
	//int num = (int)request.getAttribute("num");
	//int num2 = (int)request.getAttribute("num2");
			
%>



<style>
#searchVal {
	height: 43px;
	position: relative;
	left: -260px;
}

#btn_addSearch {
	background: white;
	width: 50px;
	height: 40px;
}

.srch_wrap.ad {
	position: relative;
	left: 55px;
}

.srch_form.add {
	
}

#btn_addSearch {
	margin-left: 20px;
}

.filter {
	float: right;
	margin-top: 20px;
}
</style>


<%@ include file="../../../inc/admin/header.jsp"%>



<main class="container">
<div class="inner_ct">

	<!-- 검색폼 시작 -->
	<form action="<%=request.getContextPath() %>/searchNameAddG.gd"
		method="post" class="srch_form add">

		<select name="searchVal" id="searchVal">


			<option value="findId">ID</option>
		</select>





		<div class="srch_wrap ad">
			<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
				class="srch_img"> <input type="text" name="searchText"
				class="srch_ipt">
		</div>

		<button type="submit" id="btn_addSearch">검색</button>




	</form>
	<div>
		<select class="filter">
			<option selected disabled hidden><%=str %></option>
			<option>전체보기</option>
			<option>WAIT</option>
			<option>FAIL</option>
			
		</select>
	</div>
	<!-- 제목 -->

	<div class="tbl_wrap">
		<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="10%">
				<col width="20%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
			</colgroup>
			<tr class="tbl_tit">
				<th>NO</th>
				<th>ID</th>
				<th>인터뷰 날짜</th>
				<th>인터뷰 여부</th>
				<th>신청일자</th>
				<th>인터뷰 결과</th>
			</tr>
			<% for (int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i);
        %>
			<tr class="tbl_cnt">
				<input type="hidden" value="<%=hmap.get("gh_no")%>">
				<input type="hidden" value="<%=hmap.get("mno")%>">

				<td><%= hmap.get("rnum")%></td>
				<td><%= hmap.get("id") %></td>
				<td><%= hmap.get("interviewDate") %></td>
				<td><%= hmap.get("interviewYn") %></td>
				<td><%= hmap.get("ghDate") %></td>
				<td><%= hmap.get("ghState") %></td>


			</tr>
			<% } %>

		</table>
		<!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev on"><a
					href="javascirpt: void(0);">&#x003C;</a></li>

				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/adWaitPrigramFilter.gd?currentPage=<%=currentPage - 1%>&str=<%=str%>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/adWaitPrigramFilter.gd?currentPage=<%=p%>&str=<%=str%>"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next on"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/adWaitPrigramFilter.gd?currentPage=<%=currentPage + 1%>&str=<%=str%>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
	</div>


</div>


</main>
<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(1).addClass("on").addClass("open");
   $(".nav_list").eq(1).find(".nav_cnt_list").eq(0).addClass("on");
   
   

   $(".tbl_cnt").click(function(){
	  var str =  $(this).children().eq(0).val();
	  var mno = $(this).children().eq(1).val();
	  var num = $(this).children("input").val();
	  
	   console.log("ghno : " + str);
	   console.log("mno : " + mno);
	   location.href="<%= request.getContextPath()%>/adGuideDetail.gd?num="+str + "&num2=" + mno;
   })
   
   /* $(".filter").change(function(){
	   var str = $(this).val();
	   console.log(str);
	   $.ajax({
		 url: "adWaitPrigramFilter.gd",
		 data: {
			 str : str
		 },
		 type:"get",
		 success:function(data) {
			 
		 },
		 error:function(error, status) {
			 console.log("서버 전송 실패!");
		 }
		 
	  }); 
   });
    */
    
   $(".filter").change(function(){
  	 $(this).prop("selected", true);
 		var str = $(this).val();
   		var num = $(".tbl_cnt").children("input").val();

 		console.log(str);
 		if(str == "전체보기") {
 			location.href="<%=request.getContextPath()%>/adGuideHistory.gd";
 		}else {
   			location.href="<%=request.getContextPath()%>/adWaitPrigramFilter.gd?str="+str;	
 		}
 		
  });

   $(".tbl_cnt td").mouseenter(function() {
       $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
    }).mouseout(function(){
       $(this).parent().css({"background":"white", "color":"#333"});
    });
    
   
});
</script>

<%@ include file="../../../inc/admin/footer.jsp"%>
