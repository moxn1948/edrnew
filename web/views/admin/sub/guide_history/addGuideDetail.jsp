<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.common.model.vo.Attachment"%>

<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<Attachment> list2 = (ArrayList<Attachment>) request.getAttribute("list2");
	ArrayList<HashMap<String, Object>> list3 = (ArrayList<HashMap<String, Object>>) request.getAttribute("list3");
	ArrayList<HashMap<String, Object>> list4 = (ArrayList<HashMap<String, Object>>) request.getAttribute("list4");
	ArrayList<HashMap<String, Object>> listLocal = (ArrayList<HashMap<String, Object>>) request.getAttribute("listLocal");
	String localNa = (String) request.getAttribute("localNa");
	int num = (int) request.getAttribute("num");
	int num2 = (int) request.getAttribute("num2");
	//ArrayList<HashMap<String, Object>> list3 = (ArrayList<HashMap<String, Object>>) request.getAttribute("list3");
	for (int i = 0; i < list2.size(); i++) {
		Attachment at = list2.get(i);

		System.out.println(at.getChangeName());
		System.out.println(at.getFileNo());
		System.out.println(at.getChangeName());
	}
%>



<%@ include file="../../../inc/admin/header.jsp"%>
<!-- datepicker 관련 js -->
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/admin/kijoonStyle2.css">

<style>
.txt_cnt.add {
	width: 200px;
	height: 200px;
}

#addImg {
	width: 100%;
}

.inner_add_area {
	border: 1px solid lightgray;
	padding-left: 40px;
}

.please {
	position: relative;
	left: -159px;
	top: 60px;
}

.title.add, .title.time {
	width: 150px;
}

.main_cnt_list.clearfix.add {
	margin-left: 400px;
	margin-top: -100px;
}

.txt_cnt.date {
	height: 20px;
	margin-left: 100px;
	margin-top: 5px;
}

.timepicker.meet_time.ipt_txt.add {
	margin-left: 7px;
	margin-top: 10px;
	height: 40px;
	width: 74px;
}

.main_cnt_list.clearfix.result {
	margin-left: 300px;
	margin-top: -115px;
}

.container .main_cnt_listadd .btn_com {
	background-color: white;
	border: 1px solid;
}

.container .main_cnt_list .txt_cnt {
	margin-left: 100px;
	line-height: 27px;
}

.main_cnt_list.clearfix.time {
	margin-left: -400px;
	margin-top: 100px;
}

.txt_cnt {
	margin-left: 100px;
	margin-top: 10px;
}

.container .title {
	line-height: 40px;
	margin-top: 15px;
}

.answer {
	text-align: right;
	margin-top: 2px;
}

.datBtn {
	margin-left: 480px;
	margin-top: -40px;
	background-color:;
}

button {
	background-color: white;
	width: 60px;
	height: 40px;
	border: 1px solid rgb(169, 169, 169);
}

.inter {
	margin-left: 300px;
	margin-top: -65px;
}

.YorN {
	margin-left: 300px;
	margin-top: -70px;
}

.container .main_cnt_list .txt_cnt.img_wrap{overflow: hidden;width: 200px;height: 200px;}
.container .main_cnt_list .txt_cnt.img_wrap img{width: 100%;}
</style>

<main class="container">
<div class="inner_ct add">
	<div class="inner_add_area">
		<!-- 제목 -->
		<h2 class="main_tit">가이드 신청내역 상세관리</h2>
		<div class="main_cnt">
			<!-- 요소 시작 -->
			<%
				for (int i = 0; i < 1; i++) {
					HashMap<String, Object> hmap = list.get(i);
					HashMap<String, Object> hmap2 = list3.get(i);
					HashMap<String, Object> hmap3 = list4.get(i);
					HashMap<String, Object> hmapLo = listLocal.get(i);
			%>

			<div class="main_cnt_list clearfix">
				<h4 class="title">사진</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="txt_cnt add img_wrap">
					<img src="/edr/uploadFiles/<%=hmap3.get("changeName")%>"
						id="addImg">
				</div>
				<!-- 기본적인 txt 끝 -->
			</div>
		</div>
		<!-- 요소 끝 -->

		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">이름*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("gname")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">연락처*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("phone")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">비상연락처*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("phone2")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">카카오톡ID*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("kakaoId")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">소개*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("introduce")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">주소*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("address")%></p>
					<!-- 기본적인 txt 시작 -->
				</div>
				<!-- 기본적인 txt 끝 -->
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">활동도시*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=localNa%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title time">활동도시 거주기간*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("period")%>개월 이상
					</p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title add">가이드 경험 유무*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("expYn")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">가능한 언어*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap2.get("lang")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<%
			}
		%>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<h4 class="title">서류*</h4>
				<!-- 기본적인 txt 시작 -->
				<%-- <%for(int j = 0; j < list3.size() - 1; j++) {
				HashMap<String, Object> hmap2 = list3.get(i);
				%> --%>

				<div class="answer">
					<table>
						<%
							for (int j = 0; j < list2.size(); j++) {
								Attachment at = list2.get(j);
						%>
						<tr>
							<td><a class="txt_cnt"
								href="<%=request.getContextPath()%>/adGuideHistoryDown.ad?num=<%=at.getFileNo()%>"><%=at.getOriginName()%></a></td>
						</tr>
						<%
							}
						%>
					</table>
				</div>


				<!-- 기본적인 txt 끝 -->
			</div>
		</div>

		<!-- 요소 끝 -->

		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix">
			<div class="main_cnt_list clearfix">
				<%
					for (int i = 0; i < 1; i++) {
						HashMap<String, Object> hmap = list.get(i);
				%>
				<h4 class="title">인터뷰 날짜*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("interviewD")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<div class="main_cnt_list clearfix add">
			<div class="main_cnt_list clearfix time">
				<h4 class="title ">인터뷰 시간*</h4>
				<!-- 기본적인 txt 시작 -->
				<div class="answer">
					<p class="txt_cnt"><%=hmap.get("interviewT")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
			</div>
		</div>
		<%
			}
		%>
		<!-- 요소 끝 -->
		<hr>
		<br>


		<!-- 변경시작 -->


		<!-- 요소 시작 -->
		<form
			action="<%=request.getContextPath()%>/adGuideHistoryDetailUp.gd?num=<%=num%>&num2=<%=num2%>"
			method="post">
			<div class="main_cnt_list clearfix">
				<div class="main_cnt_list clearfix">
					<h4 class="title">인터뷰 날짜수정</h4>
					<!-- 기본적인 txt 시작 -->
					<input type="text" name="payDate" id="" class="date_pick txt_cnt"
						placeholder="날짜를 선택해주세요." autocomplete="off"> <input
						type="hidden" name="gpNo" value="1">
					<!-- 기본적인 txt 끝 -->
					<div class="sub_cnt">
						<select name="timeP" class="timepicker meet_time ipt_txt add"
							placeholder="만나는 시간을 선택해주세요" autocomplete="off">
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
							<option value="13:00">13:00</option>
							<option value="14:00">14:00</option>
							<option value="15:00">15:00</option>
							<option value="16:00">16:00</option>
							<option value="17:00">17:00</option>

						</select>
						<div class="datBtn">
							<button onclick="daySubmit();" class="daySubmit">저장</button>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<form
			action="<%=request.getContextPath()%>/adGuiideDetailUpdateInterview.gd?num=<%=num%>&num2=<%=num2%>"
			method="post">
			<div class="main_cnt_list clearfix">
				<div class="main_cnt_list clearfix">
					<h4 class="title">인터뷰 여부*</h4>
					<!-- 기본적인 txt 시작 -->
					<%
						for (int q = 0; q < 1; q++) {
							HashMap<String, Object> hmap2 = list.get(q);
					%>
					<select class="txt_cnt" name="interYn">
						<option value="" selected disabled hidden><%=hmap2.get("interviewYn")%></option>
						<option>Y</option>
						<option>N</option>
					</select>
					<!-- 기본적인 txt 끝 -->
				</div>
				<%
					}
				%>
				<div class="inter">
					<button onclick="interviewSubmit();" class="daySubmit">저장</button>
				</div>
			</div>
		</form>
		<!-- 요소 끝 -->
		<!-- 요소 시작 -->
		<form
			action="<%=request.getContextPath()%>/adGuideHistoryDetailInterviewRe.gd?num=<%=num%>&num2=<%=num2%>"
			method="post">
			<div class="main_cnt_list clearfix">
				<div class="main_cnt_list clearfix">
					<h4 class="title">인터뷰 결과*</h4>
					<!-- 기본적인 txt 시작 -->
					<select class="txt_cnt" name="interResult">
						<option value="" selected disabled hidden>N/A</option>
						<option>승인</option>
						<option>거절</option>
					</select>
					<!-- 기본적인 txt 끝 -->
				</div>
				<div class="YorN">
					<button onclick="YornSubmit();" class="daySubmit">저장</button>
				</div>
			</div>
		</form>
		<!-- 요소 끝 -->



		<!-- 버튼 시작  -->


		<!-- 버튼 끝  -->

	</div>
</div>
<div style="height: 300px;"></div>
</main>



<!-- 아래 공간 띄우기 위한 div -->
<div class="guideDetail2"></div>
<!-- 아래 공간 띄우기 위한 div -->
<!-- datepicker 관련 js -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- timepicker -->
<script>
	$(function() {
		// 열리는 메뉴
		
		var time = "";
		console.log(time);
		
		$(".nav_list").eq(1).addClass("on").addClass("open");
		$(".nav_list").eq(1).find(".nav_cnt_list").eq(0).addClass("on");

		// 만나는 시간 timepicker
		<%-- .on("change", function(){
			var time = $('.timepicker').val();
			console.log(time);
			 $.ajax({
				url: "adTimeCheck.gd",
				data:{
					time : time,
					num : <%=num%>	
				},
				
				type:"get",
				success:function(data) {
					
				},
				error:function(error, status) {
					console.log("서버 전송 실패!");
				}
			
				
			});  --%>
		
		
		
		$(".date_pick").datepicker({
		      nextText: '다음 달',
		      prevText: '이전 달',
		      dateFormat: "yy-mm-dd",
		      showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
		      dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
		      monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'], // 월의 한글 형식.
			  minDate : "+1D"  
		}).on("change", function(){
				var date = $('.date_pick').val();
				 $.ajax({
					url: "adTimeCheck.gd",
					data:{
						date : date,
						num : <%=num%>	
					},
					
					type:"get",
					success:function(data) {
						console.log(data);
						for(var i = 0; i < data.length; i++) {
							var opIdx = $("select[name=timeP]").find("option").length;
							for(var j = 0; j < opIdx - 1; j++){
								if($("select[name=timeP]").find("option").eq(j)[0].value == data[i].interviewTime) {
									$("select[name=timeP]").find("option").eq(j)[0].remove();
								}
							}
							console.log("nn : " + data[i].interviewTime);
						}
						
						
							/* $('.timepicker').timepicker().val(data[i].interviewTime).attr('disabled', true); */
							 
						
						
						 
						
						 
					},
					error:function(error, status) {
						console.log("서버 전송 실패!");
					}
				
					
				}); 
			
			});
		});
		<%-- function daySubmit() {
			
			console.log("ml??");
			
			 var str = $("select[name=timeP]").text();
			console.log(str);
		
			location.href="<%=request.getContextPath()%>/adGuideHistoryDetailUp.gd?num="+num; 
			
			
		}--%>
			
			
			
		function interviewSubmit() {
			
			
			}
		function YornSubmit() {
			var interYn = $("select[name=interYn]").text();
			
			console.log(interYn);
			
			if(interYn == "N") {
				alert("인터뷰를 진행하지 않았습니다.");
				return false;
			}else {
				
			}
			$(".pass").submit();
	
		} 
		

	
</script>




<%@ include file="../../../inc/admin/footer.jsp"%>

