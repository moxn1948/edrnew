<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>


<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
 	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	String searchText = (String) request.getAttribute("searchText");
	String searchVal = (String) request.getAttribute("searchVal");
%>

<%-- <%@ include file="../../../css/user/kijoonStyle.css" %> --%>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css">

<style>
#searchVal {
	height: 43px;
	position: relative;
	left: -260px;
}

#btn_addSearch {
	background: white;
	width: 50px;
	height: 40px;
}

.srch_wrap.ad {
	position: relative;
	left: 55px;
}

.srch_form.add {
	
}

#btn_addSearch {
	margin-left: 20px;
}

.filter {
	float: right;
	margin-top: 20px;
}
</style>


<%@ include file="../../../inc/admin/header.jsp"%>



<main class="container">
<div class="inner_ct">

	<!-- 검색폼 시작 -->
	<form
		action="<%=request.getContextPath() %>/searchNameAddG.gd?<%-- num=<%=num2 %> --%>"
		method="post" class="srch_form">

		<select name="searchVal" id="searchVal">


			<option value="findId">ID</option>
		</select>





		<div class="srch_wrap ad">
			<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
				class="srch_img"> <input type="text" name="searchText"
				class="srch_ipt">
		</div>

		<button onclick="search();" id="btn_addSearch">검색</button>




	</form>
	<div>
		<select class="filter">
			<option selected disabled hidden>인터뷰 결과</option>
			<option>전체보기</option>
			<option>WAIT</option>
			<option>FAIL</option>
			
		</select>
	</div>
	<!-- 제목 -->

	<div class="tbl_wrap">
		<table class="tbl">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="10%">
				<col width="20%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
			</colgroup>
			<tr class="tbl_tit">
				<th>NO</th>
				<th>ID</th>
				<th>이름</th>
				<th>전화번호</th>
				<th>인터뷰 날짜</th>
				<th>인터뷰 여부</th>
				<th>인터뷰 결과</th>
			</tr>
			<% for (int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i);
        %>
			<tr class="tbl_cnt">
				<input type="hidden" value="<%=hmap.get("gh_no")%>">
				<input type="hidden" value="<%=hmap.get("mno")%>">

				<td><%= hmap.get("rnum")%></td>
				<td><%= hmap.get("id") %></td>
				<td><%= hmap.get("gname") %></td>
				<td><%= hmap.get("phone") %></td>
				<td><%= hmap.get("interviewDate") %></td>
				<td><%= hmap.get("interviewYn") %></td>
				<td><%= hmap.get("ghState") %></td>
			</tr>
			<% } %>

		</table>
		<!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev on"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/searchNameAddG.gd?currentPage=<%=currentPage - 1%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a
					href="javascript: void(0);"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/searchNameAddG.gd?currentPage=<%=p%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next on"><a
					href="javascirpt: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/searchNameAddG.gd?currentPage=<%=currentPage + 1%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
	</div>


</div>


</main>
<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(1).addClass("on").addClass("open");
   $(".nav_list").eq(1).find(".nav_cnt_list").eq(0).addClass("on");
   


   $(".tbl_cnt").click(function(){
	  var str =  $(this).children().eq(0).val();
	  var mno = $(this).children().eq(1).val();
	  var num = $(this).children("input").val();
	  
	   console.log("ghno : " + str);
	   console.log("mno : " + mno);
	   location.href="<%= request.getContextPath()%>/adGuideDetail.gd?num="+str + "&num2=" + mno;
   })
	   $(".filter").change(function(){
	    	 $(this).prop("selected", true);
	   		var str = $(this).val();
	   		console.log(str);
	   		if(ster == "전체보기") {
	   			location.href="<%=request.getContextPath()%>/adGuideHistory.gd";
	   		}else {
	   			location.href="<%=request.getContextPath()%>/adWaitPrigramFilter.gd?str="+str;	
	   		}
	   		
	    });

	    $(".tbl_cnt td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
	   
 
   
});



function search() {
	$(".srch_form").submit();
	
}

	
	
</script>

<%@ include file="../../../inc/admin/footer.jsp"%>
