<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>
    
  <%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

 	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage(); 
	String category = (String) request.getAttribute("category");
	String searchArea = (String) request.getAttribute("searchArea");
	
%>  
    
    
<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 

<style>
	.filter {
	position:relative;
	top:-10px;
	float:right;
	height:30px;
	
}
	
</style>    
    

	<main class="container">
<div class="inner_ct">
   <!-- 제목 -->
		<form action="<%=request.getContextPath() %>/adSearchOrder.od" method="post" class="srch_formWaite">
			<div class="box_select order">
				<select name="category" id="category">
					<option value="ORDER_NO" name="ORDER_NO">주문번호</option>
					<option value="ORDER_NAME" name="ORDER_NAME">주문자 명</option>
				</select>		
			</div>
			<div class="srch_wrap order">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt="" class="srch_img">
				<input type="text" name="searchArea" id="searchArea" class="srch_ipt" >
			</div>
			
			<button type="submit" class="btn_search3 order">검색</button>
			
		</form>
		
		<div>
		<select class="filter">
			<option selected disabled hidden>인터뷰 결과</option>
			<option>결제완료</option>
			<option>환불완료</option>
			<option>결제취소</option>
			<option>환불취소</option>
			<option>전체보기</option>
		</select>
		</div>
		
		
</div>
    <div class="tbl_wrap">
      <table class="tbl">
        <colgroup>
          <col width="10%">
          <col width="10%">
          <col width="10%">
          <col width="20%">
          <col width="10%">
          <col width="10%">
          <col width="10%">
          <col width="10%">
          <col width="10%">
        </colgroup>
        <tr class="tbl_tit">
          <th>NO</th>
          <th>주문번호</th>
          <th>회차</th>
          <th>프로그램 명</th>
          <th>주문자 명</th>
          <th>가격</th>
          <th>일시</th>
          <th>결제 수단</th>
          <th>결제상태</th>
        </tr>
        <%for(int i = 0; i < list.size(); i++)  {
        	HashMap<String, Object> hmap = list.get(i);
        		if(list.get(i).get("payKind").equals("GP")) {
        %>
        <tr class="tbl_cnt" onclick="location.href='orderDetail.jsp'">
          <input type="hidden" value="<%=hmap.get("orderCode")%>">
          <td><%=hmap.get("idxnum") %></td>
          <td><%=hmap.get("orderCode") %></td>
          <td><%=hmap.get("epi") %></td>
          <td><%=hmap.get("gpName") %></td>
          <td><%=hmap.get("orderName") %></td>
          <td><%=hmap.get("pcost") %></td>
          <td><%=hmap.get("payChange") %></td>
          <td><%=hmap.get("payk") %></td>
          <td><%=hmap.get("payState") %></td>
        </tr>
        
        <%}else { %>
         <tr class="tbl_cnt" onclick="location.href='orderDetail.jsp'">
		  <input type="hidden" value="<%=hmap.get("orderCode")%>">
          <td><%=hmap.get("idxnum") %></td>
          <td><%=hmap.get("orderCode") %></td>
          <td><%=hmap.get("cpTitle") %></td>
          <td><%=hmap.get("orderName") %></td>
          <td><%=hmap.get("pcost") %></td>
          <td><%=hmap.get("payChange") %></td>
          <td><%=hmap.get("payk") %></td>
          <td><%=hmap.get("payState") %></td>
        </tr>
        
        
        
        <%} %>
        <%} %>
       
      </table>
       <!-- 페이저 시작 -->
		<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev on"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/adSearchOrder.od?currentPage=<%=currentPage - 1%>&category=<%=category%>&searchArea=<%=searchArea%>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/adSearchOrder.od?currentPage=<%=p%>&category=<%=category%>&searchArea=<%=searchArea%>"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next on"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next on"><a
					href="<%= request.getContextPath()%>/adSearchOrder.od?currentPage=<%=currentPage + 1%>&category=<%=category%>&searchArea=<%=searchArea%>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
		<!-- 페이저 끝 -->
    </div>
   


</main>

<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(4).addClass("on").addClass("open");
   $(".nav_list").eq(4).find(".nav_cnt_list").eq(0).addClass("on");
   
   $(".tbl_cnt").click(function(){
		  var str =  $(this).children().eq(9).text();
		  var num = $(this).children("input").val();
		  
		   console.log(num);
		   location.href="<%= request.getContextPath()%>/adOrderDetail.od?num=" + num + "&str="+str;
	   })
   
   $(".filter").change(function(){
	   $(this).prop("selected", true);
  		var str = $(this).val();
  		console.log(str);
  		if(str == "전체보기") {
  			console.log("너는 뭐니??")
  			location.href="<%=request.getContextPath()%>/adOrderList.od";
  		}else {
  			location.href="<%=request.getContextPath()%>/adOrderFilter.od?str="+str;	
  		}
  		
   });
   

   $(".tbl_cnt td").mouseenter(function() {
       $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
    }).mouseout(function(){
       $(this).parent().css({"background":"white", "color":"#333"});
    });
   
});
</script>

<%@ include file="../../../inc/admin/footer.jsp"%>
	
	
	
