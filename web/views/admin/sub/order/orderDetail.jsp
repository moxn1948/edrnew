<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>
 
 <%
 	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
 	Long num = (Long) request.getAttribute("num");
 	int ono = (int) request.getAttribute("ono");
 			
 %>
 
 <style>
 .but.order button {
 	background-color:white;
 	width:50px;
 	height:30px;
 	float:right;
 }
 </style>
 
 
<%@ include file="../../../inc/admin/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 


<main class="container">
  
	<div class="waitDetail order">
	
		<h1 id="info">주문 상세내역</h1>
		<%for(int i = 0; i < list.size(); i++)  {
		HashMap<String, Object> hmap = list.get(i);
	%>
		<h4 id="name">주문번호</h4>	
		<h4 id="name2"><%=hmap.get("orderNo") %></h4>
		
		<h4 id="order_pro">프로그램 명</h4>
		<%if(hmap.get("gpName") != null) {%>
		<h4 id="g_id2"><%=hmap.get("gpName") %></h4>
		<%}else { %>
		<h4 id="g_id2"><%=hmap.get("cpTitle") %></h4>
		<%} %>
		<%if(hmap.get("epi") != null) {%>
		<h4 id="order_cnt">회차</h4>
		<h4 id="langu2"><%=hmap.get("epi") %></h4>
		<%}else { %>
		
		<%} %>
		<h4 id="pro">투어일시</h4>
		<h4 id="pro2"><%=hmap.get("pDate") %></h4>
		
		<h4 id="proInfo">주문자 명</h4>
		<h4 id="order_name"><%=hmap.get("orderName") %></h4>
		
		<h4 id="cnt1">주문자 연락처</h4>
		<h4 id="cnt2"><%=hmap.get("orderPhone") %></h4>

		
		
		<h4 id="pro_day1">주문자 이메일</h4>
		<h4 id="order_mail"><%=hmap.get("orderEmail") %></h4>
		
		<h4 id="waitD">결제 일시</h4>
		<h4 id="waitD2"><%=hmap.get("payChange") %></h4>
		
		<h4 id="g_pay">구매자 요청사항</h4><br>
		<%if(hmap.get("receRe") == null)  { %>
		<textarea class="orderText" style="resize:none;"></textarea>
		<%}else { %>
		<textarea class="orderText" style="resize:none;"><%=hmap.get("receRe") %></textarea>
		<%} %>
		
		
		<h4 id="t_pay">수령인 이름</h4>
		<h4 id="t_pay2"><%=hmap.get("reName") %></h4>
		
		<h4 id="order_phone">수령인 연락처</h4>
		<h4 id="order_phone2"><%=hmap.get("rePhone") %></h4>
		<br>
		
		
		<h4 id="order_emerPhone">수령인 비상연락망</h4>
		<h4 id="order_emerPhone2"><%=hmap.get("rePhone2") %></h4>
		
		  
		<h4 id="order_email">수령인 이메일</h4>
		<h4 id="order_email2"><%=hmap.get("reEmail") %></h4>
		
		
		<div class="but order">
			<!-- <button onclick="reset();" id="">취소</button> -->
			<%if(hmap.get("payState").equals("결제완료")) { %>
			<button onclick="reset();" id="">취소</button>
			<!-- <button onclick="" id="reset">환불</button> -->
			<%}else { %>
			
			<%} %>
		</div>
		<%} %>
	</div>
	
	

</main>	

<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(4).addClass("on").addClass("open");
   $(".nav_list").eq(4).find(".nav_cnt_list").eq(0).addClass("on");
});
function reset() {
	location.href="<%=request.getContextPath()%>/adOrderReset.od?num=<%=num%>&ono=<%=ono%>";
}

</script>
	
	
	
	
	
	
<%@ include file="../../../inc/admin/footer.jsp" %>
