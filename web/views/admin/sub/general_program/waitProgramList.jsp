`<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>
    
<%
ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

	PageInfo pi = (PageInfo) request.getAttribute("pi");
int listCount = pi.getListCount();
int currentPage = pi.getCurrentPage();
int maxPage = pi.getMaxPage();
int startPage = pi.getStartPage();
int endPage = pi.getEndPage(); 
%>

<style>
	#btn_search2 {
		background:white;
		width:55px;
		height:42px;
	}
</style>


<%@ include file="../../../inc/admin/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 

<main class="container">
  <div class="inner_ct">
    <!-- 제목 -->
    <h2 class="main_tit">승인 대기 프로그램</h2>
    <!-- 테이블 위 영역 -->
    <div class="top_wrap clearfix">
      <!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath() %>/adSearchWait.gp" method="post" class="srch_form">
			<div class="box_select">
				<select name="waitCategory" id="">
					<option value="name">이름</option>
					<option value="id">ID</option>
				</select>
					
			</div>
			
			
			
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt="" class="srch_img">
				<input type="text" name="waitSearch" id="" class="srch_ipt" >
			</div>
			
			<button type="submit" id="btn_search2">검색</button>
			
			
			
			
			
		</form>
		<!-- 검색폼 끝 -->
		<!-- 검색 오른쪽 영역 시작  -->
		<div class="srch_r_wrap">

			<!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->
<!-- 			<div class="box_select">
				<select name="" id="">
					<option value="">01</option>
					<option value="">02</option>
					<option value="">03</option>
					<option value="">04</option>
				</select>		
			</div> -->
			<!-- select 박스 끝 -->
		</div>
		<!-- 검색 오른쪽 영역 끝  -->
    </div>
    <!-- 테이블 위 검색, 정렬 끝 -->
    <!-- table 시작 -->
    <div class="tbl_wrap">
      <table class="tbl">
        <colgroup>
          <col width="10%">
          <col width="20%">
          <col width="10%">
          <col width="40%">
          <col width="20%">
        </colgroup>
        <tr class="tbl_tit">
          <th>NO</th>
          <th>ID</th>
          <th>이름</th>
          <th>프로그램 명</th>
          <th>프로그램 금액</th>
        </tr>
        <% for(int i = 0; i < list.size(); i++) {
        	HashMap<String, Object> hmap = list.get(i);
        %>
        <tr class="tbl_cnt">
          <input type="hidden" value="<%=hmap.get("gpno")%>">
          <td><%=hmap.get("rnum") %></td>
          <td><%=hmap.get("id") %></td>
          <td><%=hmap.get("name") %></td>
          <td><%=hmap.get("gpName") %></td>
          <td><%=hmap.get("cost") %></td>
        </tr>
        <%} %>
      </table>
    </div>
    <!-- table 끝 -->
    <!-- 페이저 시작 -->
			<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
				<% if(currentPage <= 1)  { %>


				<li class="pager_com pager_arr prev on"><a
					href="javascirpt: void(0);">&#x003C;</a></li>


				<%} else { %>
				<li class="pager_com pager_arr prev"><a
					href="<%=request.getContextPath()%>/adWaitProgramList.gp?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
				<li class="pager_com pager_num on"><a href="javascript: void(0);"
					><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
				<%} else {%>
				<li class="pager_com pager_num"><a
					href="<%=request.getContextPath()%>/adWaitProgramList.gp?currentPage=<%=p%>"><%=p %></a></li>
				<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
				<%} %>
				<% } %>


				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next on"><a
					href="javascript: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a
					href="<%= request.getContextPath()%>/adWaitProgramList.gp?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

				<%} %>

			</ul>
		</div>
			<!-- 페이저 끝 -->
  </div>
</main>
<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(3).addClass("on").addClass("open");
   $(".nav_list").eq(3).find(".nav_cnt_list").eq(0).addClass("on");
   
   
   $(".tbl_cnt").click(function(){
		  var str =  $(this).children().eq(0).text();
		  var num = $(this).children("input").val();
		  
		   console.log(num);
		   location.href="<%= request.getContextPath()%>/adSelectOneWaitProgramList.gp?num=" + num;
	   })

	    $(".tbl_cnt td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
   
   
   
});
</script>

<%@ include file="../../../inc/admin/footer.jsp" %>
