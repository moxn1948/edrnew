<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.guide.model.vo.*, com.edr.common.model.vo.*, java.util.*, com.edr.generalProgram.model.vo.*,com.edr.member.model.vo.Member, com.edr.order.model.vo.OrderDetail"%>
<%
	String state = (String) request.getParameter("state");

	HashMap<String, Object> epiDetail = (HashMap<String, Object>) request.getAttribute("epiDetail");
	OrderDetail od = (OrderDetail) epiDetail.get("od");
	ArrayList<GpPriceDetail> listPrice = (ArrayList<GpPriceDetail>) request.getAttribute("listPrice");
	
/* 	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<GpPriceDetail> listPrice = (ArrayList<GpPriceDetail>) request.getAttribute("listPrice");

	String include = (String) request.getAttribute("include");
	String notClude = (String) request.getAttribute("notClude");
	int sum = (int) request.getAttribute("sum");
	int num = (int) request.getAttribute("num"); */
	//int num2 = (int) request.getAttribute("num2");
	
	
%>


<title>가이드 상세관리</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/user/jakeStyle_user.css">
<main class="container">
<div class="inner_ct">
	<div class="main_cnt">

		<!-- 요소 시작 -->
		<div class="main_cnt_list2 clearfix">
			<div class="main_cnt_list3 clearfix">
				<br>
				<h2>프로그램 상세내역(회차)</h2>
			</div>

			<div class="main_cnt_list clearfix">
				<%-- <br> <input type="hidden" value=<%=hmap.get("gpno")%>> --%>
				<h4 class="title">이름</h4>
				<p class="txt_cnt"><%= epiDetail.get("gdname") %></p>
			</div>
			<!-- 요소 끝 -->
	
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 명</h4>
				<h4 class="txt_cnt"><%= epiDetail.get("gpname")  %></h4>
			</div>
			
			<!-- 요소 끝 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 회차</h4>
				<p class="txt_cnt"><%= epiDetail.get("epi") %>회차</p>
			</div>
			
			
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 인원</h4>
				<p class="txt_cnt"><%=  od.getRecPerson() %>명</p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">주문자 명</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= epiDetail.get("oname") %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">주문자 연락처</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= epiDetail.get("ophone") %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">주문자 이메일</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= epiDetail.get("oemail") %></p>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자명</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= od.getReceName() %></p>
			</div>

			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자 성별</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%
					String gender = "";
					if(od.getReceGender().equals("F")){
						gender = "여성";
					}else{
						gender = "남성";
					}
					%>
					<%= gender %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자 나이</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%
					String age = "";
					if(od.getReceAge().equals("50")){
						age += od.getReceAge() + "대 이상";
					}else{
						age += od.getReceAge() + "대";
					}
					%>
					<%= age %>
				</p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자 연락처</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= od.getRecePhone() %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자 비상 연락처</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= od.getRecePhone2() %></p>
			</div>
		
			<div class="main_cnt_list clearfix">
				<h4 class="title">수령자 이메일</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= od.getReceEmail() %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">요청 사항</h4>
				<p class="txt_cnt"><%= od.getReceRequest() %></p>
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 기간</h4>
				<p class="txt_cnt">
					&emsp;&emsp;
					<%= epiDetail.get("tday") %>일
				</p>
			</div>
			
			
	<%-- 		<%for(int a = 0; a < listPrice.size(); a++) {
   
   				%>
			<div class="main_cnt_list clearfix">
				<h4 class="title"><%=listPrice.get(a).getGpCategory() %>
				</h4>
				<p class="txt_cnt2">
					&emsp;&emsp;
					<%=listPrice.get(a).getGpPrice() %>원
				</p>
			</div>
			<%} %>  --%>
			<!-- 요소 끝 -->
			<%--  <div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 비용</h4>
				<p class="txt_cnt">
					&emsp;&emsp;인당
					<%=hmap.get("gpcost")%>원
				</p>
			</div> --%>
		<%-- 	<%
				}
			%> --%>
			
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 총비용</h4>
				<p class="txt_cnt">	&emsp;&emsp;<%= epiDetail.get("cost") %>원
				</p>
			</div>

			<%-- 	<!-- 버튼 시작  -->
			<div class="main_cnt_list clearfix">
				<div class="btn_wrap btnaddd">
					<button type="submit" class="btn_com btnad" onclick="location.href= '<%=request.getContextPath()%>/selectGPNo.ad'">닫기</button>
				</div>
			</div>
			<!-- 버튼 끝  --> --%>
		</div>
	</div>

	<script>
		$(function() {
			// 열리는 메뉴
			$(".nav_list").eq(3).addClass("on").addClass("open");
			$(".nav_list").eq(3).find(".nav_cnt_list").eq(1).addClass("on");
		});
	</script>

</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>


