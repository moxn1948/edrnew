<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.generalProgram.model.vo.Gp ,com.edr.common.model.vo.PageInfo"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	String searchText = (String) request.getAttribute("searchText");
	String searchVal = (String) request.getAttribute("searchVal");
%>

<title>모든 프로그램 관리</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">
<main class="container">
<div class="inner_ct">

	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath()%>/searchGP.ad"
			method="get" class="srch_form">
			<div class="box_select">
				<select name="searchVal" id="searchVal">
					<option value="pname">프로그램명</option>
					<option value="name">이름</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="searchText"
					id="searchText" class="srch_ipt">
			</div>
			<button onclick="search();" id="btn_Search">검색</button>

		</form>
		<!-- 검색폼 끝 -->

		<!-- table 시작 -->
		<div class="tbl_wrap">
			<table id="listArea" class="tbl">
				<colgroup>
					<col width="10%">
					<col width="40%">
					<col width="10%">
					<col width="10%">
					<col width="15%">
					<col width="15%">
				</colgroup>
				<tr class="tbl_tit">
					<th>NO</th>
					<th>프로그램명</th>
					<th>이름</th>
					<th></th>
					<th></th>
				</tr>
				<%
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, Object> hmap = list.get(i);
				%>
				<tr class="tbl_cnt">
					<input type="hidden" value="<%= hmap.get("gpno")%>">
					<td><%=hmap.get("rnum")%></td>
					<td><%=hmap.get("gpname")%></td>
					<td><%=hmap.get("gname")%></td>
					<td><button onclick="location.href='<%=request.getContextPath()%>/selectOneGP.ad?num=<%=hmap.get("gpno")%>'">상세보기</button></td>
					<td><button	onclick="location.href='<%=request.getContextPath()%>/selectGPNo.ad'">회차정보</button></td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
		<!-- table 끝 -->
		<!-- 페이저 시작 -->
			<div class="pager_wrap">
			<ul class="pager_cnt clearfix add">
			<% if(currentPage <= 1)  { %>
				
				
					<li class="pager_com pager_arr prev"><a href="javascirpt: void(0);">&#x003C;</a></li>
					
				
				<%} else { %>
					<li class="pager_com pager_arr prev"><a href="<%=request.getContextPath()%>/searchGP.ad?currentPage=<%=currentPage - 1%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>">&#x003C;</a></li>
					
				<%} %>
				<% for(int p = startPage; p <= endPage; p++)  {
					if(p == currentPage) {
				%>
					<li class="pager_com pager_num no"><a href="javascript: void(0);"><%=p %></a></li>
					<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
					<%} else {%>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/searchGP.ad?currentPage=<%=p%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>"><%=p %></a></li>
					<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
					<%} %>
				<% } %>
				
				
				<% if(currentPage >= maxPage) { %>
				<li class="pager_com pager_arr next"><a href="javascirpt: void(0);">&#x003E;</a></li>
				<%}else { %>
				<li class="pager_com pager_arr next"><a href="<%= request.getContextPath()%>/searchGP.ad?currentPage=<%=currentPage + 1%>&searchText=<%=searchText%>&searchVal=<%=searchVal %>">&#x003E;</a></li>
				
				<%} %>
				
				</ul>
			</div>
			<!-- 페이저 끝 --> 
    </div>
		<script>
			$(function() {
				// 열리는 메뉴
				$(".nav_list").eq(3).addClass("on").addClass("open");
				$(".nav_list").eq(3).find(".nav_cnt_list").eq(1).addClass("on");
			});
		
			function search(){
				$(".srch_from").submit();
			}
			
			$("#listArea td").mouseenter(function() {
		        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
		     }).mouseout(function(){
		        $(this).parent().css({"background":"white", "color":"#333"});
		     });
		</script>
	</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>
