<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*, com.edr.generalProgram.model.vo.Gp ,com.edr.common.model.vo.PageInfo"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>


<title>모든 프로그램 회차정보</title>

<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css" 	href="/edr/css/admin/jakeStyle_admin.css">
<main class="container">
<div class="inner_ct">

	<!-- 테이블 위 영역 -->
	<div class="top_wrap clearfix">
		<!-- 검색폼 시작 -->
		<form action="<%=request.getContextPath()%>/searchGPNo.ad"
			method="get" class="srch_form">
			<div class="box_select">
				<select name="searchVal" id="searchVal">
					<option value="pname">프로그램명</option>
					<option value="name">이름</option>
				</select>
			</div>
			<div class="srch_wrap">
				<img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt=""
					class="srch_img"> <input type="text" name="searchText" id=""
					class="srch_ipt">
			</div>
			<button type="submit" id="btn_Search">검색</button>
		</form>
		<!-- 검색폼 끝 -->
		<%-- <div class="">
			<select name="allPfil" id="allPfil" class="allPfil">
				<option selected disabled hidden>필터링</option>
				<option value="WAITTOUR">대기 중인 투어</option>
				<option value="ENDTOUR">종료된 투어</option>
			</select>
		</div>--%>
	</div>
	<!-- 테이블 위 검색, 정렬 끝 -->
	<!-- table 시작 -->
	<div class="tbl_wrap">
		<table id="listArea" class="tbl">
			<colgroup>
				<col width="10%">
				<col width="*">
				<col width="10%">
				<col width="20%">
				<col width="20%">
			</colgroup>
			<tr class="tbl_tit">
				<th>NO</th>
				<th>프로그램명</th>
				<th>회차</th>
				<th>가이드</th>
				<th>진행 여부</th>
			</tr>
			<%
				for (int i = 0; i < list.size(); i++) {
					HashMap<String, Object> hmap = list.get(i);
			%>
			<tr class="tbl_cnt">
				<input type="hidden" value="<%=hmap.get("gpno")%>">
				<input type="hidden" value="<%=hmap.get("pno")%>">
				<td><%=hmap.get("rnum")%></td>
				<td><%=hmap.get("gpname")%></td>
				<td><%=hmap.get("epi")%></td>
				<td><%=hmap.get("gname")%></td>
				<td><%=hmap.get("tourstate")%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<!-- table 끝 -->
	<!-- 페이저 시작 -->
	<div class="pager_wrap">
		<ul class="pager_cnt clearfix add">
			<%
					if (currentPage <= 1) {
				%>


			<li class="pager_com pager_arr prev"><a
				href="javascirpt: void(0);">&#x003C;</a></li>


			<%
					} else {
				%>
			<li class="pager_com pager_arr prev"><a
				href="<%=request.getContextPath()%>/selectGPNo.ad?currentPage=<%=currentPage - 1%>">&#x003C;</a></li>

			<%
					}
				%>
			<%
					for (int p = startPage; p <= endPage; p++) {
						if (p == currentPage) {
				%>
			<li class="pager_com pager_num on"><a href="javascript: void(0);"
				><%=p%></a></li>
			<%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
			<%
					} else {
				%>
			<li class="pager_com pager_num"><a
				href="<%=request.getContextPath()%>/selectGPNo.ad?currentPage=<%=p%>"><%=p%></a></li>
			<%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
					<li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
			<%
					}
				%>
			<%
					}
				%>
			<%
					if (currentPage >= maxPage) {
				%>
			<li class="pager_com pager_arr next"><a
				href="javascript: void(0);">&#x003E;</a></li>
			<%
					} else {
				%>
			<li class="pager_com pager_arr next"><a
				href="<%=request.getContextPath()%>/selectGPNo.ad?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

			<%
					}
				%>

		</ul>
	</div>
	<!-- 페이저 끝 -->

	<script>
		$(function() {
			// 열리는 메뉴
			$(".nav_list").eq(3).addClass("on").addClass("open");
			$(".nav_list").eq(3).find(".nav_cnt_list").eq(1).addClass("on");

		});
		$(function(){
			
			$(".tbl_cnt").click(function(){
				var num =$(this).children().eq(0).text();
				var str = $(this).children("input").eq(0).val();
				var pno = $(this).children("input").eq(1).val();
				var state = $(this).children("td").eq(4).text();
				
				if(state == "결제되지 않은 투어"){
					state = "nopay";
					alert("결제되지 않은 투어입니다.");
				}else{
				 	location.href= "<%=request.getContextPath()%>/selectOneGPNo.ad?str=" + str + "&pno=" + pno;
				}
				
			});
			
		}); 
		$("#listArea td").mouseenter(function() {
	        $(this).parent().css({"background":"rgb(94, 94, 94)","color":"white", "cursor":"pointer"});
	     }).mouseout(function(){
	        $(this).parent().css({"background":"white", "color":"#333"});
	     });
		<%--$(".allPfil").change(function(){
	   		var str = $(this).val();
	   		console.log(str);
	   		location.href="<%=request.getContextPath()%>/GPNofilter.ad?str="+str;
	    });--%>
	</script>
</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>
