<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*, 
   com.edr.generalProgram.model.vo.*"%>
   
<%
   ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
   ArrayList<GpLang> listLang = (ArrayList<GpLang>)request.getAttribute("listLange");
   ArrayList<GpDetail> listGpDetail = (ArrayList<GpDetail>) request.getAttribute("listGpDetail");
   ArrayList<GpPriceDetail> listPrice = (ArrayList<GpPriceDetail>) request.getAttribute("listPrice");
   String include = (String) request.getAttribute("include");
   String notClude = (String) request.getAttribute("notClude");
   int sum = (int) request.getAttribute("sum");
   int num = (int) request.getAttribute("num");
   for(int i = 0; i < list.size(); i++) {
      HashMap<String, Object> hmap = list.get(i);
   }
   for(int j = 0; j < listLang.size(); j++) {
   }
   
   
   
   
%>   

   
<%@ include file="../../../inc/admin/header.jsp"%>
 <link rel="stylesheet" type="text/css"
   href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css">

<style>
   #proInfo3 {
   border:1px solid lightgray;
      width:700px;
      height:200px;
      word-break:normal;
      /* height:auto; */
   }
   #s_pay {
		margin-top:-40px;
	}
	
	.include_list.not {
		margin-left:0px;
	}
	.includePay {
		color:blue;
	}
	.pay td{
 		 width : 173px;
 		 margin-left:40px;
 		 padding:20px 0;
	}
	#clude {
		width:1500px;
	}
	.tableArea {
	margin-left:25px;
	margin-top:400px;
	font-weight:bold;
	}
	.secondArea {
		margin-top:-440px;
		width:500px;
	}
	.notice {
	/*  border:1px solid red; */
	width:600px;
	height:auto;
	word-break:normal; 
	}
</style>

<main class="container">
<div class="waitDetail">
   <h1 id="info">프로그램 상세내역</h1>

   <% for(int i = 0; i < list.size(); i++) {
      HashMap<String, Object> hmap = list.get(i);   
   %>
   
   <h4 id="name">이름 : </h4>
   <h4 id="name2"><%=hmap.get("gname") %></h4>

   <h4 id="g_id">ID : </h4>
   <h4 id="g_id2"><%=hmap.get("email") %></h4>

    <h4 id="langu">언어 : </h4>
    
   
   <h4 id="langu2">
   <%for(int j = 0; j < listLang.size(); j++)  { %>
   <%=listLang.get(j).getLang()%>,
   <% } %> 
   </h4>
   
   <h4 id="pro">프로그램 명 : </h4>
   <h4 id="pro2"><%=hmap.get("proName") %></h4>

   <h4 id="proInfo">프로그램 소개</h4><br>
   <br>
   <div id="proInfo3">
      <h4 id="">
         <%=hmap.get("gpDescr") %>
      </h4>
   </div>
      <br>
      <br>
   
   <h4 id="cnt1">프로그램 최소인원 : </h4>
   <h4 id="cnt2"><%=hmap.get("gpmin") %>명</h4><br>
   <h4 id="cnt1">프로그램 최대인원 : </h4>
   <h4 id="cnt2"><%=hmap.get("gpmax") %>명</h4>

   <h4 id="waitD">프로그램 일정</h4>
   <h4 id="waitD2"><%=hmap.get("gpTday") %>일</h4><br><br><br>

   <h4 id="pro_day1">프로그램 일정</h4>
   <div class="tbl_wrap wait">
   <%for(int a = 0; a < listGpDetail.size(); a++)  {
   
      %>
      <table class="tbl">
         <colgroup>
            <col width="100%">
         </colgroup>
         <%if(a != 0)  {%>
         <%if(listGpDetail.get(a).getGpDay() != listGpDetail.get(a-1).getGpDay())  {%>
         <tr class="tbl_tit">
            <th><%= listGpDetail.get(a).getGpDay()%> day</th>
         </tr>
         <%}else { %>

         <%} %>



         <%if(listGpDetail.get(a).getGpDaySeq() != listGpDetail.get(a-1).getGpDaySeq() || listGpDetail.get(a).getGpDay() != listGpDetail.get(a-1).getGpDay()) {%>
         <tr class="tbl_cnt wait">
            <td>일정 <%= listGpDetail.get(a).getGpDaySeq()%></td>
         </tr>
         <tr class="tbl_cnt wait">
            <td><%= listGpDetail.get(a).getGpCnt()%></td>
         </tr>

         <%}else { %>
         <tr class="tbl_tit">
            <td><%= listGpDetail.get(a).getGpDay()%> day</td>
         </tr>
         <%} %>
      </table> <br>


      
      <%}else {%>
      <table class="tbl">
         <colgroup>
            <col width="100%">
         </colgroup>
         <tr class="tbl_tit">
            <th><%= listGpDetail.get(a).getGpDay()%> day</th>
         </tr>

         <tr class="tbl_cnt wait">
            <td>일정 <%= listGpDetail.get(a).getGpDaySeq()%></td>
         </tr>
         <tr class="tbl_cnt wait">
            <td><%= listGpDetail.get(a).getGpCnt()%></td>
         </tr>
      </table> <br>


      <%} %>
      <%} %>
      <br>


   </div>
   <br>
   <br>

   
   <%for(int b = 0; b < listPrice.size(); b++) {
   
   %>
   <h4 id="g_pay"><%=listPrice.get(b).getGpCategory() %> :  </h4>
   <h4 id="g_pay2"><%=listPrice.get(b).getGpPrice() %> 원</h4>
   <br>
   
   <%} %>

   <h4 id="s_pay">총 프로그램 비용 : </h4>

   <h2 id="s_pay2"><%=sum %> 원</h2><br>
   
   <div class="tableArea">
   <%-- <%for(int c = 0; c < listPrice.size(); c++)  {
   	if(listPrice.get(c).getGpInc().equals("포함사항")) {
   %> --%>
   <table class="pay">
   	<tr>
   		<td>포함사항 : </td>
   		<td id="clude"><%=include %></td>
   	</tr>
   </table>
   
   <%-- <%}else { %> --%>
   <%if(hmap.get("notClude") != null) { %>
   <table class="pay">
   	<tr>
   		<td>불포함사항 : </td>
   		<td id="clude"><%=notClude %></td>
   	</tr>
   </table>
   <%}else { %>
   
   <%} %>
   
   <%-- <%} %>
  <%} %> --%>
  <%for(int d = 0; d < list.size(); d++)  {%>
   </div>
   <div class="secondArea">
   <h4 id="g_info">공지사항</h4>
   <div class="notice">
   <h4 id="g_info2">
      <%=list.get(d).get("gpNotice") %>   </h4>
</div>
   <h4 id="time">만나는 시간</h4>
   <h4 id="time2"><%=list.get(d).get("meetTime") %></h4>

   <h4 id="con_area1">만나는 장소</h4>
   <h4 id="con_area2"><%=list.get(d).get("meetArea") %></h4>
   <%} %>
   <%} %>

   <div class="but submit">
      <button onclick="approval();" id="commit">승인</button>
      <button onclick="notApproval();" id="reset">거절</button>
   </div>

</div>
</div>



<!-- 아래 공간 띄우기 위한 div -->
<!-- 아래 공간 띄우기 위한 div --> <!-- 저장, 삭제 버튼 시작 --> <!-- 저장, 삭제 버튼 끝 --> </main>

<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(3).addClass("on").addClass("open");
   $(".nav_list").eq(3).find(".nav_cnt_list").eq(0).addClass("on");

});
function approval() {
	   if(confirm("승인 하시겠습니까?") == true) {
		   alert("등록되었습니다.");
		   var str = "Y";
		   location.href="<%=request.getContextPath()%>/adWaitProgramApproval.gp?str=Y&num=<%=num%>";
	   }else {
		   
	   }
}
function notApproval() {
	if(confirm("삭제하시겠습니까?") == true) {
		alert("삭제되었습니다.");
		   var str = "N";
		   location.href="<%=request.getContextPath()%>/adWaitProgramApproval.gp?str="+ str +"&num=<%=num%>";
	}
	
}
</script>

<%@ include file="../../../inc/admin/footer.jsp"%>