<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.edr.guide.model.vo.*, com.edr.common.model.vo.*,java.util.*,com.edr.generalProgram.model.vo.*,com.edr.member.model.vo.Member"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
	ArrayList<GpPriceDetail> listPrice = (ArrayList<GpPriceDetail>) request.getAttribute("listPrice");
	String include = (String) request.getAttribute("include");
	String notClude = (String) request.getAttribute("notClude");
	int sum = (int) request.getAttribute("sum");
	int num = (int) request.getAttribute("num");
%>
<title>모든 프로그램 상세관리</title>



<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/user/jakeStyle_user.css">
<main class="container">
<div class="inner_ct">
	<div class="main_cnt">
		<form id="updateForm" method="post">
			<!-- 요소 시작 -->
			<div class="main_cnt_list2 clearfix">
				<div class="main_cnt_list3 clearfix">
					<br>
					<h2>프로그램 상세내역</h2>
				</div>


		
				<%
					for (int i = 0; i < 1; i++) {
						HashMap<String, Object> hmap = list.get(i);
				%>

				<div class="main_cnt_list clearfix">
					<br>
					<h4 class="title">이름</h4>
					<!-- 기본적인 txt 시작 -->
					<p class="txt_cnt"><%=hmap.get("gname")%></p>
					<!-- 기본적인 txt 끝 -->
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h2 class="title">ID</h2>
					<p class="txt_cnt"><%=hmap.get("email")%></p>
				</div>

				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">언어</h4>
					<p class="txt_cnt"><%=hmap.get("lang")%></p>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div class="main_cnt_list clearfix">
					<h4 class="title">프로그램 명</h4>
					<h4 class="txt_cnt"><%=hmap.get("gpname")%></h4>
				</div>
				<!-- 요소 끝 -->
				<!-- 요소 시작 -->
				<div>
					<h4 class="title">프로그램 소개</h4>
				</div>
				<div>
					<p class="txt_cnt"><%=hmap.get("gpdescr")%></p>
				</div>
				<br> <br>
				<div class="main_cnt_list clearfix">
					<h4 class="title">프로그램 인원</h4>
					<p class="txt_cnt">
						최소<%=hmap.get("gpmin")%>명 ~ 최대<%=hmap.get("gpmax")%>명
					</p>
				</div>
				<div>
					<h4 class="title">프로그램 일정</h4>
				</div>

				<div class="tbl_wrap">
					<table class="tbl">
						<%
							for (int j = 0; j < list.size(); j++) {
									HashMap<String, Object> hmap2 = list.get(j);
						%>
						<colgroup>
							<col width="100%">
						</colgroup>
						<tr class="tbl_tit">
							<th><%=hmap2.get("gpday")%>일차</th>
						</tr>

						<tr class="tbl_cnt">
							<td><p class="plan"><%=hmap2.get("gpcnt")%></p></td>
						</tr>
						<%
							}
						%>
					</table>
				</div>

				<div class="main_cnt_list clearfix">
					<h4 class="title">프로그램 기간</h4>
					<p class="txt_cnt">
						&nbsp;&nbsp;<%=hmap.get("gptday")%>일
					</p>
				</div>
				<%for(int a = 0; a < listPrice.size(); a++) {
   
   				%>
				<div class="main_cnt_list clearfix">
					<h4 class="title"><%=listPrice.get(a).getGpCategory() %></h4>
					<p class="txt_cnt2"><%=listPrice.get(a).getGpPrice() %> 원</p>
				</div>
				<%} %>
				<div class="main_cnt_list clearfix">
					<h4 class="title">총 프로그램비용</h4>
					<p class="txt_cnt">
						&nbsp;<%=sum%>원
					</p>
				</div>
				
				<div class="main_cnt_list clearfix">
					<h4 class="title">포함 사항</h4>
					<p class="inc txt_cnt3"><%=include %></p>
					
				</div>
				<%if(hmap.get("notClude") != null) { %>
				<div class="main_cnt_list clearfix">
					<h4 class="title incl">불 포함 사항</h4>
					<p class="inc txt_cnt3"><%=notClude %></p>
				</div>
				<%}else{ %>
				
				<%} %>
				<div>
					<h4 class="title">특이사항</h4>
					<p class="txt_cnt"><%=hmap.get("gpnotice")%></p>
				</div>
				<br> <br>
				<div class="main_cnt_list clearfix">
					<h4 class="title">만나는 시간</h4>
					<p class="txt_cnt2"><%=hmap.get("gpmeettime")%></p>
				</div>
				<div class="main_cnt_list clearfix">
					<h4 class="title">만나는 장소</h4>
					<p class="txt_cnt2"><%=hmap.get("gpmeetarea")%></p>
				</div>
				<!-- 버튼 시작  -->

				<div class="main_cnt_list clearfix">
					<div class="btn_wrap btnaddd">
						<button type="submit" class="btn_com btnad" onclick="deleteGP()">삭제</button>
					</div>
				</div>
				<!-- 버튼 끝  -->
			</div>


			<script>
		$(function() {
			// 열리는 메뉴
			$(".nav_list").eq(3).addClass("on").addClass("open");
			$(".nav_list").eq(3).find(".nav_cnt_list").eq(1).addClass("on");
		});
		function deleteGP() {
			$("#updateForm").attr("action",	"<%=request.getContextPath()%>/deleteGP.ad?num=<%=hmap.get("gpno")%>");
				}
			
		<%} %>
		
			</script>
		
			
		</form>
	</div>
</div>
</main>

<%@ include file="../../../inc/admin/footer.jsp"%>


