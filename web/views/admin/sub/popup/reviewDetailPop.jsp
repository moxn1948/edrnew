<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");
%>
<%@ include file="../../../inc/admin/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">

<main class="container">

<div class="inner_ct">
	<div class="main_cnt">
		<form id="updateForm" method="post">
			<div class="main_cnt_list clearfix">

				<h2>리뷰 상세</h2>
			</div>
			<br>
			<%
				for (int i = 0; i < 1; i++) {
					HashMap<String, Object> hmap = list.get(i);
			%>

			<div class="main_cnt_list clearfix">
				<input type="hidden" name="reviewno"
					value="<%=hmap.get("reviewno")%>">
				<h4 class="title">프로그램명</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=hmap.get("gpname")%></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<br>
			   
			<div class="main_cnt_list clearfix">
				<h4 class="title">구매자명</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=hmap.get("ordername")%></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<br>
			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 평점</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=hmap.get("guiderating")%></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<br>
			<div class="main_cnt_list clearfix">
				<h4 class="title">프로그램 평점</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=hmap.get("programrating")%></p>
				<!-- 기본적인 txt 끝 -->
			</div>

			<div class="main_cnt_list clearfix">
				<h2 class="title">리뷰 내용</h2>
			</div>
			<br>

			<div class="main_cnt_list clearfix reviewContent"><%=hmap.get("reviewcnt")%></div>
			<br>

			<button type="submit" class="dele" onclick="deleteReview()">삭제</button>
		</form>
		<!--  <button type="submit" class="close"	onclick="location.href='<%=request.getContextPath()%>/selectReview.ad'">닫기</button>
-->
		<script>
		function deleteReview(){
			$("#updateForm").attr("action", "<%=request.getContextPath()%>/deleteReview.ad?num=<%=hmap.get("reviewno")%>");
	}
		$(function() {
			// 열리는 메뉴
			$(".nav_list").eq(5).addClass("on").addClass("open");
			$(".nav_list").eq(5).find(".nav_cnt_list").eq(0).addClass("on");
		});
<%}%>
	</script>

	</div>
</div>

</main>

<%@ include file="../../../inc/admin/footer.jsp"%>