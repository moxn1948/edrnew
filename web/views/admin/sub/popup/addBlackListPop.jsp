<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css"
	href="/edr/css/admin/jakeStyle_admin.css">
<div class="pop_wrap popupBlackList">
	<div class="popupdiv">
		<div class="main_cnt_list clearfix">
			<h2>블랙리스트 추가</h2>
		</div>
		<br>
		<div class="box_select">
			<h2 class="box_select">회원 구분&emsp;&emsp;&emsp;</h2>
			<div class="box_select">
				<div class="box_select">
					<select class="box_select" name="" id="">
						<option value="">==선택==</option>
						<option value="">가이드</option>
						<option value="">투어객</option>
					</select>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="box_input2">
			<h2 class="box_input">회원 ID&emsp;&emsp;&emsp;&emsp;</h2>
			<div class="box_input">
				<input class="box_input" type="text" name="" id=""
					placeholder="ID를 입력해주세요.">
			</div>
		</div>
		<br>
		<div class="main_cnt_list">
			<h2 class="box_select2">신고 항목&emsp;&emsp;&emsp;</h2>
			<div class="box_select2 clearfix">
				<div class="box_select2">
					<select name="" id="">
						<option value="">==선택==</option>
						<option value="">욕설/ 비방</option>
						<option value="">음란물 유포</option>
						<option value="">기타 범죄</option>
					</select>
				</div>
			</div>
		</div>
		<br>
		<div class="main_cnt_list clearfix">
			<h2 class="title">신고 제목&emsp;&emsp;&emsp;</h2>
			<div class="title">
				<input class="title" type="text" name="" id=""
					placeholder="제목을 입력해주세요.">
			</div>
		</div>
		<br>
		<div class="main_cnt_list clearfix">
			<h2 class="title">신고 내용</h2>
		</div>
		<br>
		<textarea class="main_cnt_list clearfix blackListContent">신고 내용을 입력 해주세요.</textarea>

		<div class="main_cnt_list clearfix">
			<div class="btn_wrap">
				<button type="submit" class="btn_com2 btn_white">등록</button>
				<button type="submit" class="btn_com2 btn_white">취소</button>
			</div>
		</div>
	</div>
	<button type="button" onClick="$.magnificPopup.close();">Close</button>
</div>