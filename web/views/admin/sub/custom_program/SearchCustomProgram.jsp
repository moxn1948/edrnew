<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, com.edr.guide.model.vo.GuideHistory, com.edr.common.model.vo.*"%>
    
      <%
   ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

    PageInfo pi = (PageInfo) request.getAttribute("pi");
    String category = (String) request.getAttribute("category");
    String searchTxt = (String) request.getAttribute("searchTxt");
          
   int listCount = pi.getListCount();
   int currentPage = pi.getCurrentPage();
   int maxPage = pi.getMaxPage();
   int startPage = pi.getStartPage();
   int endPage = pi.getEndPage(); 
   
%>  
<style>
.filter {
   float:right;
   margin-top:20px;
}
</style>    

<%@ include file="../../../inc/admin/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> 

<main class="container">
  <div class="inner_ct">
    <!-- 제목 -->
    <h2 class="main_tit">맞춤 프로그램 관리</h2>
    <!-- 테이블 위 영역 -->
    <div class="top_wrap clearfix">
      <!-- 검색폼 시작 -->
      <form action="<%=request.getContextPath() %>/adSearchCustom.cp" method="post" class="srch_form">
         <div class="box_select">
            <select name="category" id="category">
               <option value="order">신청자</option>
               <option value="pName">제목</option>
            </select>
               
         </div>
         
         
         
         <div class="srch_wrap">
            <img src="<%=request.getContextPath()%>/imgs/srch_icon.png" alt="" class="srch_img">
            <input type="text" name="searchTxt" class="srch_ipt" >
         </div>
         
         <button type="submit" id="btn_search2">검색</button>
         
         
         
         
         
      </form>
      <div>
      <select class="filter">
         <option selected disabled hidden>현재상태</option>
         <option>WAIT</option>
         <option>ENDPAY</option>
         <option>GWAIT</option>
         <option>전체보기</option>
      </select>
   </div>
      <!-- 검색폼 끝 -->
      <!-- 검색 오른쪽 영역 시작  -->
      <div class="srch_r_wrap">

         <!-- select 박스 시작  : 있는 경우 주석 풀어서 사용-->
<!--          <div class="box_select">
            <select name="" id="">
               <option value="">01</option>
               <option value="">02</option>
               <option value="">03</option>
               <option value="">04</option>
            </select>      
         </div> -->
         <!-- select 박스 끝 -->
      </div>
      <!-- 검색 오른쪽 영역 끝  -->
    </div>
    <!-- 테이블 위 검색, 정렬 끝 -->
    <!-- table 시작 -->
    <div class="tbl_wrap">
      <table class="tbl">
        <colgroup>
          <col width="5%">
          <col width="20%">
          <col width="10%">
          <col width="20%">
          <col width="20%">
          <col width="10%">
        </colgroup>
        <tr class="tbl_tit">
          <th>NO</th>
          <th>제목</th>
          <th>신청자</th>
          <th>신청일시</th>
          <th>배정날짜</th>
          <th>현재상태</th>          
        <%for(int i = 0; i < list.size(); i++) {
           HashMap<String, Object> hmap = list.get(i);   
           %>
        
        <tr class="tbl_cnt">
        <%if(hmap.get("cpEstNo") != null) { %>
           <input type="hidden" value="<%=hmap.get("cpEstNo")%>">
        <%} else {%>
           <input type="hidden" value="0">
           
        <%} %>
        <input type="hidden" value="<%=hmap.get("cpNo")%>">
          <td><%=hmap.get("idxnum") %></td>
          <td><%=hmap.get("cpTitle") %></td>
          <td><%=hmap.get("orderName") %></td>
          <td><%=hmap.get("cpDate") %></td>
          <td><%=hmap.get("cpDate") %></td>
          <td><%=hmap.get("cpState") %></td>
        </tr>
        <%} %>
      </table>
    </div>
    <!-- table 끝 -->
   <!-- 페이저 시작 -->
      <div class="pager_wrap">
         <ul class="pager_cnt clearfix add">
            <% if(currentPage <= 1)  { %>


            <li class="pager_com pager_arr prev on"><a
               href="javascirpt: void(0);">&#x003C;</a></li>


            <%} else { %>
            <li class="pager_com pager_arr prev"><a
               href="<%=request.getContextPath()%>/adCustomProgram.cp?currentPage=<%=currentPage - 1%>&category=<%=category%>&searchTxt=<%=searchTxt%>">&#x003C;</a></li>

            <%} %>
            <% for(int p = startPage; p <= endPage; p++)  {
               if(p == currentPage) {
            %>
            <li class="pager_com pager_num on"><a href="javascript: void(0);"><%=p %></a></li>
            <%-- <li class="pager_com pager_num on"><a href="javascrpt: void(0);"><%=p %></a></li>
               <li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li>
               <li class="pager_com pager_num"><a href="javascrpt: void(0);"><%=p %></a></li> --%>
            <%} else {%>
            <li class="pager_com pager_num"><a
               href="<%=request.getContextPath()%>/adCustomProgram.cp?currentPage=<%=p%>&category=<%=category%>&searchTxt=<%=searchTxt%>"><%=p %></a></li>
            <%-- <li class="pager_com pager_num on"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
               <li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li>
               <li class="pager_com pager_num"><a href="<%=request.getContextPath()%>/adGuideHistory.ad?currentPage=<%=p%>"><%=p %></a></li> --%>
            <%} %>
            <% } %>


            <% if(currentPage >= maxPage) { %>
            <li class="pager_com pager_arr next on"><a
               href="javascript: void(0);">&#x003E;</a></li>
            <%}else { %>
            <li class="pager_com pager_arr next on"><a
               href="<%= request.getContextPath()%>/adCustomProgram.cp?currentPage=<%=currentPage + 1%>">&#x003E;</a></li>

            <%} %>

         </ul>
      </div>
      <!-- 페이저 끝 -->
  </div>
</main>

<script>
$(function(){
   // 열리는 메뉴
   $(".nav_list").eq(2).addClass("on").addClass("open");
   $(".nav_list").eq(2).find(".nav_cnt_list").eq(0).addClass("on");
   
   $(".filter").change(function(){
      $(this).prop("selected", true);
        var str = $(this).val();
       console.log(str);
       
       
       if(str == "전체보기") {
          location.href="<%=request.getContextPath()%>/adCustomProgram.cp";
       }else {
          location.href="<%=request.getContextPath()%>/adCustomFilter.cp?str="+str;   
       }
       
  });
   
   $(".tbl_cnt").click(function(){
         
        var cp_no =  $(this).children().eq(0).text();
        var cpEstNo = $(this).children("input").eq(0).val();
        var cpNo = $(this).children("input").eq(1).val();
        var cpState = $(this).children().eq(7).text();
        
        location.href="<%= request.getContextPath()%>/adCustomProgramDetail.cp?cpNo=" + cpNo + "&cpEstNo=" + cpEstNo+"&cpState="+cpState;
        <%-- location.href="<%= request.getContextPath()%>/adCustomProgramDetail.cp?cpNo=" + cpNo+"&cpEstNo="cpEstNo; --%>
       
        
      });
   
});
</script>

<%@ include file="../../../inc/admin/footer.jsp" %>