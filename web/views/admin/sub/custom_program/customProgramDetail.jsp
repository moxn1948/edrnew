<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.HashMap, java.util.ArrayList, com.edr.customProgram.model.vo.*, com.edr.common.model.vo.Attachment"%>

<%
	
	String state = (String) request.getParameter("cpState");
	int cpNo = Integer.parseInt(request.getParameter("cpNo"));

	HashMap<String, Object> cpDetail = (HashMap<String, Object>) request.getAttribute("cpDetail"); 

	CpRequest cpRe = (CpRequest) cpDetail.get("cpRe");
	ArrayList<CpLang> cpLa = (ArrayList<CpLang>) cpDetail.get("cpLa");
	
	String lang = "";
	for(int i = 0; i < cpLa.size(); i++){
		if(i != cpLa.size() - 1){
			lang += cpLa.get(i) + ", ";
		}else{
			lang += cpLa.get(i);
		}
	}
	
	String localName = "";
	switch (cpRe.getLocalNo()) {
	case 1: localName = "서울"; break;
	case 2: localName = "경기"; break;
	case 3: localName = "경상"; break;
	case 4: localName = "강원"; break;
	case 5: localName = "전라"; break;
	case 6: localName = "충청"; break;
	case 7: localName = "제주"; break;
	case 8: localName = "부산"; break;
	case 9: localName = "대구"; break;
	case 10: localName = "대전"; break;
	case 11: localName = "광주"; break;
	case 12: localName = "인천"; break;
	case 13: localName = "울산"; break;
	}
	
	/* switch(state){
	case "WAIT" : state = "배정중"; break; 
	case "GWAIT" : state = "배정완료"; break; 
	case "ENDPAY" : state = "결제완료"; break; 
	} */
	
	System.out.println(state + "기준아 ");
	
	ArrayList<CpGuideDetail> cpGdetail = (ArrayList<CpGuideDetail>) cpDetail.get("cpGdetail");
	CpGuideEst cpGest = (CpGuideEst) cpDetail.get("cpGest");
	String gname = (String) cpDetail.get("gname");
	ArrayList<CpPriceDetatil> cpPdetail = (ArrayList<CpPriceDetatil>) cpDetail.get("cpPdetail");
	Attachment cpAttr = (Attachment) cpDetail.get("cpAttr");
	
	int cpEstNo = 0;
	if(cpGest != null){
		cpEstNo = cpGest.getEstNo();
	}
	
	int include = 0;
	int notclude = 0;
	if(cpPdetail != null){
		
		for(int i = 0; i < cpPdetail.size(); i++){
			if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
				include++;
			}else{
				notclude++;
			}
		}

	}
%>

<%@ include file="../../../inc/admin/header.jsp"%>
<%-- <link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath() %>/css/admin/kijoonStyle2.css"> --%>

<style>
.container .inner_ct.cus .cusArea {
	/* border: 1px solid black;
	padding: 10px 16px;
	border-radius: 10px; */
	width:100%;
	background:#fff;
	box-shadow: 2px 2px 3px 3px rgba(100, 100, 100, 0.1);
}

.container .main_cnt_list.if {
	border: 1px solid black;
	padding: 10px 16px;
	border-radius: 10px;
	margin-top: 40px;
}

.container .title.cus {
	width: 140px;
}
.ifH3 {
	font-size:30px;
}
.txt_cnt.table td{
	align:left;
	padding:5px;
}
.container .main_cnt_list .txt_cnt {
	margin-left:150px;
}
.customDetailArea{padding-bottom : 20px;}

.customDetailArea{
	width:100%;
	background:#fff;
	box-shadow: 2px 2px 3px 3px rgba(100, 100, 100, 0.1);
}
.customDetailArea{
	margin-left:10px;	
}
.customDetailArea td{
	padding-left:30px;
}
.customDetailArea tr{
	height:50px;
}
.customDetailArea td{
	padding-left:30px;
}
.customDetailArea tr{
	height:50px;
}

</style>

<main class="container">
<div class="inner_ct cus">
	<!-- 제목 -->
	<div class="cusArea">
		<h2 class="main_tit">맞춤 프로그램 신청</h2>
		<div class="main_cnt">

			<!-- 요소 시작 -->
			<!-- <div class="main_cnt_list clearfix">
				<h4 class="title">활동명</h4>
				<p class="txt_cnt"></p>
			</div> -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">배정가이드</h4>
				<p class="txt_cnt">
				<%
				String guideName = "";
				if(state.equals("배정중")){
					guideName = "미배정";
				}else{
					guideName = gname;
				}
				%>
				<%= guideName %>
				</p>
			</div>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<%-- <div class="main_cnt_list clearfix">
				<h4 class="title">email</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=hmap.get("email") %></p>
				<!-- 기본적인 txt 끝 -->
			</div> --%>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">신청일시</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpRe.getCpDate() %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">상태</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=state %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<hr>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">지역</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=localName %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">팀 인원수</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpRe.getCpPerson() %>명</p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<!-- 요소 끝 -->
			<!-- 요소 시작 -->
			<div class="main_cnt_list clearfix">
				<h4 class="title">시작 날짜</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpRe.getCpSdate() %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">총 기간</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpDetail.get("tday")%>일</p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">최대 가능 금액</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpRe.getCpCost() %>원</p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title cus">프로그램 진행언어</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%=lang%></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 성별</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%
						String gender = "";
						if(cpRe.getCpGender().equals("M")){
							gender = "남성";
						}else{
							gender = "여성";
						}
						%>
						<%= gender %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">가이드 나이대</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%
						String age = "";
						if(cpRe.getCpAge() == 50){
							age = "50대 이상";
						}else{
							age = cpRe.getCpAge() + "대";
						}
						%>
						<%= age %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">제목</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt"><%= cpRe.getCpTitle() %></p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<div class="main_cnt_list clearfix">
				<h4 class="title">내용</h4>
				<!-- 기본적인 txt 시작 -->
				<p class="txt_cnt">
					<%= cpRe.getCpCnt() %>
				</p>
				<!-- 기본적인 txt 끝 -->
			</div>
			<!-- 요소 끝 -->
		</div>
	</div>


	<% if(!state.equals("배정중")){%>
			<div class="customDetailArea" style="margin-top: 20px;">
				<h1 align="center" class="request" style="padding:60px 0;">답변사항</h1>
				<table>
					<tr>
						<td class="customTd">총금액</td>
						<td><%= cpGest.getTotalCost() %></td>
					</tr>
					<tr>
						<td class="customTd">포함사항</td>
						<td>
						<% 
						String includeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("INCLUDE")){
							%>
							<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
							<%
							}
						}%>
						<%= includeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">불포함사항</td>
						<td>
						<%
						String notcludeStr = "";
						for(int i = 0; i < cpPdetail.size(); i++){
							if(cpPdetail.get(i).getCpInc().equals("NOTCLUDE")){
								%>
								<div><%= cpPdetail.get(i).getCpCategory()%>, <%=cpPdetail.get(i).getCpPrice() %> </div>
								<%
							}
						}%>
						<%= notcludeStr %>
						</td>
					</tr>
					<tr>
						<td class="customTd">만나는 시간</td>
						<td><%= cpGest.getMeetTime() %></td>
					</tr>
					<tr>
						<td class="customTd">주소</td>
						<td><%= cpGest.getMeetArea() %></td>
					</tr>
					<%for(int i = 0; i < cpGdetail.size(); i ++) {%>
					<tr>
						<td class="customTd"><%= ((CpGuideDetail) cpGdetail.get(i)).getCpDay() %> DAY</td>
						<td><%= ((CpGuideDetail) cpGdetail.get(i)).getCpCnt() %></td>
					</tr>
					<%} %>
					<tr>
						<td class="customTd">특이사항</td>
						<td><%= cpGest.getUniqueness() %></td>
					</tr>
					<tr>
						<td class="customTd">첨부파일</td>
						<%if(cpAttr.getOriginName() != null) { %>
						<td><a href="<%=request.getContextPath()%>/adCustomProDown.cp?estNo=<%= cpEstNo %>"><%= cpAttr.getOriginName() %></a></td>
						<%} else {%>
						
						<%} %>
					</tr>
				</table>
			</div>
			<% }%>
			
			
		
</div>
</main>


<!-- 지워 -->


<script>
	$(function() {
		// 열리는 메뉴
		$(".nav_list").eq(2).addClass("on").addClass("open");
		$(".nav_list").eq(2).find(".nav_cnt_list").eq(0).addClass("on");
		
	});
</script>






<%@ include file="../../../inc/admin/footer.jsp"%>
