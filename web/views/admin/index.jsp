<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.edr.member.model.vo.Member"%>

<% 
	Member loginAdmin = (Member) session.getAttribute("loginAdmin");
%>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/admin/kijoonStyle2.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	.loginAd{
		/* margin-left:auto;
		margin-right:auto; */
		text-align:center;
	}
	body, html{margin: 0;padding: 0;}
	.container .main_login{text-align: center;}
	.container .main_login img {padding-top: 360px;position: static;}
	.ipt_txt {margin: 3px 0;padding-left: 4px;
    width: 97%;
    line-height: 30px;}
    .loginAd{    
    		padding-top:15px;
    		width: 230px;
		    height: 34px;
		    background-color: rgb(51, 51, 51);
		    text-align: center;
		    font-size: 14px;
		    color: #f7f7f7;
		    border: none;
		    cursor: pointer;
	}
</style>

 <% if(loginAdmin == null) { %>

<main class="container">

<div class="main_login">

	<img src="../../imgs/logo_white.png">

</div>


<div class="main_tex">
	<form id="loginForm" action="<%= request.getContextPath()%>/adLogin.me" method="post" name="">
		<input id="log" class="ipt_txt" type="text" placeholder="아이디를 입력하세요" name="adminId">
		<input id="pass" class="ipt_txt" type="password" placeholder="Password" name="userPwd">
		<div onclick="login();" class="loginAd">login</div>
	</form>
	
</div>

</main> 


<%}else { %> 

<%@ include file="../inc/admin/header.jsp"%>
<%@ include file="../inc/admin/footer.jsp"%>
	


<% } %> 
 <script>
function login(){
	var adminId = $("#log").val();
	var userPwd = $("#pass").val();
	
	//console.log(str);
	//console.log(str2);
	if(adminId == "" || userPwd == "") {
		alert("회원 정보를 입력해주세요!");
		return false;
	}else if(adminId != "" && userPwd != ""){
		 /* $("#loginForm").submit();  */
		 $.ajax({
			 url:"/edr/adLoginCheck.me",
		 	type:"post",
		 	data:{
		 		adminId:adminId,
		 		userPwd:userPwd
		 	},
		 	success:function(data) {
		 		if(data == "fail") {
		 			alert("관리자 정보가 일치하지 않습니다.");
		 			return false;
		 		}
		 		$("#loginForm").submit();
		 	},
		 	error:function(){
		 		
		 	}
		 	
		 })
		
	}
}
</script> 

	
	
	
