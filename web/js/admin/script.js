$(function(){

  $(".nav_list>a").on("click", function(event){
    event.stopPropagation(); 
    if($(this).parent(".nav_list").hasClass("open")){
      $(this).parent(".nav_list").removeClass("open");
    }else{
      $(this).parent(".nav_list").removeClass("open");
      $(this).parent(".nav_list").siblings(".nav_list").removeClass("open");
      $(this).parent(".nav_list").addClass("open");
    }
    $(this).parent(".nav_list").siblings(".nav_list").find(".nav_cnt").slideUp();
    $(this).parent(".nav_list").find(".nav_cnt ").slideToggle();
  });


});