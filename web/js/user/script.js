$(function(){

  // 회원 gnb의 내 정보 보기, 알림 박스
  $(".box_wrap>a").on("click", function(e){
    e.stopPropagation();
    if($(this).parent(".box_wrap").hasClass("on")){
      $(this).parent(".box_wrap").removeClass("on");
    }else{
      $(".box_wrap").removeClass("on");
      $(this).parent(".box_wrap").addClass("on");
    }
  });

  $("body").on("click", function(e){
    // 박스 이외의 곳을 눌렀을 때
    if($(".box_wrap").hasClass("on")){
      if(!$(".box_wrap").has(e.target).length){
        $(".box_wrap").removeClass("on");
      }
    }

    // 알림 지우기 버튼 눌렀을 때
    if($(e.target)[0].className == "notice_del"){
      $(".notice_del").siblings().remove();
      $(".notice_del").parent().append("<p style='text-align: center;'>알림이 없습니다.</p>");
      $(".notice_del").addClass("empty_notice");
    }
  });

  $(".sub_menu_list.add>a").on("click", function(){
      if($(this).parent(".sub_menu_list.add").hasClass("open")){
        $(this).parent(".sub_menu_list.add").removeClass("open");
      }else{
        $(this).parent(".sub_menu_list.add").removeClass("open");
        $(this).parent(".sub_menu_list.add").siblings(".sub_menu_list").removeClass("open");
        $(this).parent(".sub_menu_list.add").addClass("open");
      }
      $(this).parent(".sub_menu_list.add").siblings(".sub_menu_list").find(".sub_side_wrap").slideUp();
      $(this).parent(".sub_menu_list.add").find(".sub_side_wrap ").slideToggle();
  });
  
});