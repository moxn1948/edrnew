package com.edr.guide.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuideHistory implements java.io.Serializable{
	private int ghNo;				// 신청이력번호
	private int mno;				// 회원번호
	private Date ghDate;			// 신청일
	private String interviewYn;		// 인터뷰여부
	private Date interviewDate;	// 인터뷰날짜
	private String interviewTime;	// 인터뷰시간
	private String ghState;			// 신청상태
	
}
