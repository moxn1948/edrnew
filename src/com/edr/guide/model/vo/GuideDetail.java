package com.edr.guide.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "Gp")
public class GuideDetail implements java.io.Serializable{
	private int mno;			// 회원번호
	private String gname;		// 가이드이름
	private String kakaoId;		// 카카오ID
	private String phone;		// 전화번호
	private String phone2;		// 비상연락망
	private String address;		// 주소
	private String lang;		// 가능언어
	private int period;			// 거주기간
	private String introduce;	// 소개
	private String accountNo;	// 계좌번호
	private String accountName;	// 예금주명
	private String bankName;	// 은행명
	private String expYn;		// 가이드경험유무
	private String tbBan;		// 해외결격사유
	
}
