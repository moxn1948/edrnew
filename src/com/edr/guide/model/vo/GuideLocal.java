package com.edr.guide.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuideLocal implements java.io.Serializable{
	private int mno;			// 회원번호
	private int localNo;		// 지역번호
}
