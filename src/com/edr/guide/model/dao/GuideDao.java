package com.edr.guide.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.guide.model.vo.GuideLocal;

public class GuideDao {
	Properties prop = new Properties();
	public GuideDao() {
		String fileName = GuideDao.class.getResource("/sql/guide/guide-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} 
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 
	 * @param con
	 * @param gd
	 * @return         :
	 */
	public int insertGuide(Connection con, GuideDetail gd) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("insertGuide");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gd.getMno());
			pstmt.setString(2, gd.getGname());
			pstmt.setString(3, gd.getKakaoId());
			pstmt.setString(4, gd.getPhone());
			pstmt.setString(5, gd.getPhone2());
			pstmt.setString(6, gd.getAddress());
			pstmt.setString(7, gd.getLang());
			pstmt.setInt(8, gd.getPeriod());
			pstmt.setString(9, gd.getIntroduce());
			pstmt.setString(10, gd.getExpYn());
			pstmt.setString(11, gd.getTbBan());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			
		}
		
		
		
		return result;
	}
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 지역테이블에 삽입
	 * @param con
	 * @param gl
	 * @return         :
	 */
	public int insertLocal(Connection con, GuideLocal gl) {
		PreparedStatement pstmt = null;
		int result2 = 0;
		String query = prop.getProperty("insertLocal");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gl.getMno());
			pstmt.setInt(2, gl.getLocalNo());
			
			result2 = pstmt.executeUpdate(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result2;
	}
	public int insertHistory(Connection con, GuideHistory gh) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result3 = 0;
		int result = 0;
		String query = prop.getProperty("insertHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gh.getMno());
			pstmt.setDate(2, gh.getInterviewDate());
			pstmt.setString(3, gh.getInterviewTime());
		
			result = pstmt.executeUpdate();
			
			result3 = selectGhNo(con, gh);
			
			System.out.println("dao :" + result3);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result3;
	}
	
	public int selectGhNo(Connection con, GuideHistory gh) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectGhNo");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gh.getMno());
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
				System.out.println("dao : " + rset.getInt(1));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		
		
		return result;
		
	}
	
	public int insertAttachment(Connection con, ArrayList<Attachment> fileList, int result3) {
		PreparedStatement pstmt = null;
		int result4 = 0;
		String query = prop.getProperty("insertAttachment");
		
		try {
			
			for(int i=0; i <fileList.size(); i++) {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, fileList.get(i).getOriginName());
				pstmt.setString(2, fileList.get(i).getChangeName());
				pstmt.setString(3, fileList.get(i).getFilePath());
				pstmt.setInt(4, fileList.get(i).getMno());
				pstmt.setInt(5, fileList.get(i).getGhNo());
				pstmt.setString(6, fileList.get(i).getProfileYn());
				
				result4 = pstmt.executeUpdate();
				System.out.println("result4 : " + result4);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result4;
	}
	public ArrayList<HashMap<String, Object>> selectOneGuide(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		Attachment at = null;
		ArrayList<Attachment> list2 = null;
		
		String query = prop.getProperty("selectOneGuide");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				
				hmap.put("filePath", rset.getString("FILE_PATH"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("kakaoId", rset.getString("KAKAO_ID"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("phone2", rset.getString("PHONE2"));
				hmap.put("address", rset.getString("ADDRESS"));
				hmap.put("localName", rset.getString("LOCAL_NAME"));
				hmap.put("period", rset.getInt("PERIOD"));
				hmap.put("expYn", rset.getString("EXP_YN"));
				hmap.put("lang", rset.getString("LANG"));
				hmap.put("introduce", rset.getString("INTRODUCE"));
				hmap.put("interviewD", rset.getDate("INTERVIEW_DATE"));
				hmap.put("interviewT", rset.getString("INTERVIEW_TIME"));
				hmap.put("ghState", rset.getString("GH_STATE"));
				
				list.add(hmap);
				
			}
		/*	for (int j = 0; j < list.size()/2; j++) {

			}*/
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}
	public ArrayList<Attachment> selectAttachment(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list = null;
		
		String query = prop.getProperty("selectAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Attachment>();
			
			while(rset.next()) {
				Attachment at = new Attachment();
				at.setFileNo(rset.getInt("FILE_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				
				list.add(at);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	public int updateHistory(Connection con, GuideHistory gh) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("updateHistory");
		
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setDate(1, gh.getInterviewDate());
			pstmt.setString(2, gh.getInterviewTime());
			pstmt.setInt(3, gh.getMno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		System.out.println("dao : " + result);
		
		return result;
	}
	public int countAttachment(Connection con, int num) {
		PreparedStatement pstmt = null;
		int result = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("countAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			System.out.println(result);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		System.out.println("dao : " + result);
		
		return result;
	}
	public ArrayList<Attachment> selectAttachmentNo(Connection con, int num) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list2 = null;
		Attachment at = null;


		String query = prop.getProperty("SelectAttachmentNo");
		//System.out.println("query : " + query);

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<Attachment>();

			while(rset.next()) {
				at = new Attachment();
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				list2.add(at);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		System.out.println("dao123456 : " + list2);
		
		return list2;
		
	}

}
