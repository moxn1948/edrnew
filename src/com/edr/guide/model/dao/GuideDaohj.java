package com.edr.guide.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideLocal;
import com.edr.member.model.vo.Member;

import static com.edr.common.JDBCTemplate.*;
public class GuideDaohj {
	
	Properties prop = new Properties();
	
	public GuideDaohj(){
		
		
		String fileName = GuideDaohj.class.getResource("/sql/guide/guide-query.properties").getPath();
		
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		  
		
	}
	
	

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 15. 
	 * @Description  : hashmap으로 guideDetail정보랑 Local에서 이름가져옴 /가이드 정보 출력
	 * @param   :con,mno
	 * @return    : HashMap 타입 guideInfo
	 */
	public HashMap<String, Object> guideSelectOne(Connection con, int mno) {
		
		PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		HashMap<String, Object> guideInfo = null;
		ArrayList<Local> localList = null; // 지
		GuideDetail gd = null;
		ArrayList<GuideDetail> gdList = null;
		Local lc = null;
		Attachment at =null;
		
		String query =prop.getProperty("guideSelectOne");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			
			rset =pstmt.executeQuery();
			
			//localList 는 ArrayList타입
			localList = new ArrayList<>();
			
			while(rset.next()) {
				gd  = new GuideDetail(); // 그때마다 새로 생성
				lc = new Local();
				at = new Attachment();
				
				at.setChangeName(rset.getString("CHANGE_NAME"));
				
				gd.setMno(rset.getInt("MNO"));
				gd.setGname(rset.getString("GNAME"));
				gd.setKakaoId(rset.getString("KAKAO_ID"));
				gd.setPhone(rset.getString("PHONE"));
				gd.setPhone2(rset.getString("PHONE2"));
				gd.setAddress(rset.getString("ADDRESS"));
				gd.setLang(rset.getString("LANG"));
				gd.setPeriod(rset.getInt("PERIOD"));
				gd.setIntroduce(rset.getString("INTRODUCE"));
				gd.setExpYn(rset.getString("EXP_YN"));	
				gd.setTbBan(rset.getString("TB_BAN"));
				gd.setAccountNo(rset.getString("ACCOUNT_NO"));
				gd.setAccountName(rset.getString("ACCOUNT_NAME"));
				gd.setBankName(rset.getString("BANK_NAME"));
				
				
				lc.setLocalName(rset.getString("LOCAL_NAME"));
				lc.setLocalNo(rset.getInt("LOCAL_NO"));
				
				localList.add(lc);
				
			}//반복문 종료
			
			guideInfo = new HashMap<>();
			String[] langArr = gd.getLang().split(", ");
			guideInfo.put("guideDetail", gd);
			guideInfo.put("langArr", langArr);//gd라는 String 키값으로 gd넣음
			guideInfo.put("lcList", localList);
			guideInfo.put("at", at);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		
		return guideInfo;
	}



	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 19. 
	 * @Description  : 가이드 프로필 수정클릭후에  비밀번호 체크하는 메소드
	 * @param   : con,requestMember
	 * @return    :
	 */
	public int checkGdPwd(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result =0;
		   
		String query= prop.getProperty("checkGdPwd");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getEmail());
			pstmt.setString(2, requestMember.getMpwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result=rset.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return result;
	}



	
//update는 delete와 insert ,update 세가지 메소드 작용한다.

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 22. 
	 * @Description  :가이드 로컬 지우기
	 * @param   :con, mno
	 * @return    :
	 */
	public int deleteguidelocal(Connection con, int mno) {
		PreparedStatement pstmt =null;
		int result =0;
		
		String query=prop.getProperty("deleteGuideLocal");
		
		try {
			pstmt =con.prepareStatement(query);
		
			pstmt.setInt(1, mno);
			
			result =pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		
		
		return result;
	}
	
	public int insertguidelocal(Connection con, int mno, Local local) {
		PreparedStatement pstmt = null;
		int result =0;
		
		String query = prop.getProperty("insertGuideLocal");
		
		try {
			pstmt =con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			pstmt.setInt(2, local.getLocalNo());
			
			result =pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}



	public int updateProfile(Connection con, GuideDetail gd) {
		PreparedStatement pstmt = null;
		int result =0;
		
		
		String query = prop.getProperty("updateProfile");
		
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, gd.getKakaoId());
			pstmt.setString(2, gd.getPhone());
			pstmt.setString(3, gd.getPhone2());
			pstmt.setString(4, gd.getAddress());
			pstmt.setString(5, gd.getIntroduce());
			pstmt.setInt(6, gd.getMno());
			
			result =pstmt.executeUpdate();
			
			
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		
		return result;
	}

}
