package com.edr.guide.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.omg.Messaging.SyncScopeHelper;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.vo.GuideHistory;


/**
 * @Author : kijoon
 * @CreateDate : 2019. 12. 8.
 * @Description : :
 */
public class AdGuideDao {
	private Properties prop = new Properties();

	public AdGuideDao() {
		String fileName = AdGuideDao.class.getResource("/sql/guide/guide-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 8.
	 * @Description : adGuideHistoryCount
	 * @param con
	 * @return : int listCount
	 */
	public int adGuideHistoryCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adlistCount");

		try {

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}
		System.out.println("listCount : " + listCount);
		return listCount;
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 8.
	 * @Description : adGuideHistory
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adGuideHistory(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adGuideHistory");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;


		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();


			list = new ArrayList<HashMap<String, Object>>();
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gh_no", rset.getInt("GH_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("interviewYn", rset.getString("INTERVIEW_YN"));
				
				hmap.put("interviewDate", rset.getDate("INTERVIEW_DATE"));
				hmap.put("ghDate", rset.getDate("GH_DATE"));
				if(rset.getString("GH_STATE").equals("OK")) {
					hmap.put("ghState", "통과");
				}else if(rset.getString("GH_STATE").equals("FAIL")){
					hmap.put("ghState", "불통과");
				}else {
					hmap.put("ghState", "심사중");
				}



				list.add(hmap);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		//("list : " + list);

		return list;
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 9.
	 * @Description : 가이드 신청내역 이름 검색
	 * @param con
	 * @param userName
	 * @return : 가이드 신청내역의 검색 내용 갯수
	 */
	public int adSearchNameGuideHistoryCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNamelistCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 9.
	 * @Description : 가이드 신청내역 아이디검색 갯수
	 * @param con
	 * @param userId
	 * @return : 검색어에 대한 목록 갯수
	 */
	public int adSearchIdGuideHistoryCount(Connection con, String searchText) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchIdlistCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		//("listCount : " + listCount);
		return listCount;
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 9.
	 * @Description : 이름 검색에 대한 목록
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @param userName
	 * @return : 검색에 대한 결과 목록
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameGuideHistory(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchName");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gh_no", rset.getInt("GH_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("interviewDate", rset.getDate("INTERVIEW_DATE"));
				hmap.put("interviewYn", rset.getString("INTERVIEW_YN"));
				if(rset.getString("GH_STATE").equals("OK")) {
					hmap.put("ghState", "통과");
				}else if(rset.getString("GH_STATE").equals("FAIL")){
					hmap.put("ghState", "불통과");
				}else {
					hmap.put("ghState", "심사중");
				}

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : kijoon
	 * @CreateDate : 2019. 12. 9.
	 * @Description : 가이드 신청내력 아이디 검색에 대한 결과 목록
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @param userId
	 * @return : 가이드 신청내역 아이디 검색에 대한 list
	 */
	public ArrayList<HashMap<String, Object>> adSearchIdGuideHistory(Connection con, int currentPage, int limit,
			String searchText) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchId");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			//();
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gh_no", rset.getInt("GH_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("interviewDate", rset.getDate("INTERVIEW_DATE"));
				hmap.put("interviewYn", rset.getString("INTERVIEW_YN"));
				if(rset.getString("GH_STATE").equals("OK")) {
					hmap.put("ghState", "통과");
				}else if(rset.getString("GH_STATE").equals("FAIL")){
					hmap.put("ghState", "불통과");
				}else {
					hmap.put("ghState", "심사중");
				}

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;

	}


	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 선택한 한개의 가이드 신청내역 가져오기
	 * @param con
	 * @param num
	 * @return         : 선택한 가이드의 신청내역 상세정보
	 */
	public ArrayList<HashMap<String, Object>> selectOneGuideHistory(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		Attachment at = null;
		ArrayList<Attachment> list2 = null;


		String query = prop.getProperty("selectOneGuideHistory");



		try {


			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			//("rset : " + rset);


			while(rset.next()) {

				//("whileRset : " + rset);
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				/*hmap.put("fileNo", rset.getInt("FILE_NO"));
				hmap.put("filePath", rset.getString("FILE_PATH"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));*/
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("kakaoId", rset.getString("KAKAO_ID"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("phone2", rset.getString("PHONE2"));
				hmap.put("address", rset.getString("ADDRESS"));
				/*hmap.put("localName", rset.getString("LOCAL_NAME"));*/
				hmap.put("period", rset.getInt("PERIOD"));
				if(rset.getString("EXP_YN").equals("Y")) {
					hmap.put("expYn",  "유");
				}else {
					hmap.put("expYn",  "무");
				}
				hmap.put("lang", rset.getString("LANG"));
				hmap.put("localName", rset.getString("LOCAL_NAME"));
				hmap.put("introduce", rset.getString("INTRODUCE"));
				hmap.put("interviewD", rset.getDate("INTERVIEW_DATE"));
				hmap.put("interviewT", rset.getString("INTERVIEW_TIME"));
				hmap.put("interviewYn", rset.getString("INTERVIEW_YN"));



				list.add(hmap);


			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		//("오늘list : " + list);

		return list;
	}

	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 가이드 신청내역 인터뷰날짜, 시간, 인터뷰 여부, 결과 업데이트
	 * @param con
	 * @param gh
	 * @param num 
	 * @return         :신청결과
	 */
	public int updateGuideHistory(Connection con, GuideHistory gh, int num) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateGuideHistory");

		//("여기 는?? : " + gh.getInterviewDate());
		//(gh.getInterviewTime());
		//(gh.getInterviewPassYn());
		//("제일중요 : " + gh.getInterviewYn());


		try {

			pstmt = con.prepareStatement(query);
			pstmt.setDate(1, gh.getInterviewDate());
			pstmt.setString(2, gh.getInterviewTime());
			//pstmt.setString(3, gh.getInterviewYn());
			pstmt.setInt(3, num);


			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		//("result : " + result);

		return result;
	}


	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 해당 신청 가이드의 첨부파일
	 * @param con
	 * @param num
	 * @return         : 해당 신청 가이드의 첨부파일
	 */
	public ArrayList<Attachment> selectAttachment(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list2 = null;
		Attachment at = null;


		String query = prop.getProperty("adSelectAttachment");
		//("query : " + query);

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<Attachment>();

			while(rset.next()) {
				at = new Attachment();
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				list2.add(at);

			}







		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list2;
	}

	/**
	 * @Author         : KIJOON
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 가이드 신청내역 상세보기 첨부파일
	 * @param con
	 * @param num
	 * @return         : 신청 가이드의 첨부파일 목록
	 */
	public ArrayList<Attachment> adselectAttachmentHistory(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list2 = null;
		Attachment at = null;


		String query = prop.getProperty("adSelectAttachmentHistory");
		//("query : " + query);

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<Attachment>();

			while(rset.next()) {
				at = new Attachment();
				at.setFileNo(rset.getInt("FILE_NO"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));

				list2.add(at);
				//("여기는???? : " + list2);

			}







		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list2;
	}

	public ArrayList<HashMap<String, Object>> adGuideTimeCheck(Connection con, String day2) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adGuideTimeCheck");
		int num2 = 0;

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, day2);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();



			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("interviewTime", rset.getString("INTERVIEW_TIME"));
				num2 = rset.getInt(2);
				hmap.put("count", num2);



				list.add(hmap);
			}






		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}




		return list;
	}

	public ArrayList<HashMap<String, Object>> adGuideHistoryFileter(Connection con, String str, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adGuideHistoryFilter");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {

				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gh_no", rset.getInt("GH_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("interviewYn", rset.getString("INTERVIEW_YN"));
				hmap.put("interviewDate", rset.getDate("INTERVIEW_DATE"));
				hmap.put("ghDate", rset.getDate("GH_DATE"));
				if(rset.getString("GH_STATE").equals("OK")) {
					hmap.put("ghState", "통과");
				}else if(rset.getString("GH_STATE").equals("FAIL")){
					hmap.put("ghState", "불통과");
				}else {
					hmap.put("ghState", "심사중");
				}


				list.add(hmap);

			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}

	public int adGuideCalculateCount(Connection con) {

		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adCalculateCount");

		try {

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}

		return listCount;


	}


	public int adGuideHistoryFilterCount(Connection con, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adlistFilterCount");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			rset = pstmt.executeQuery();


			if (rset.next()) {
				listCount = rset.getInt(1);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}

		return listCount;

	}

	public ArrayList<HashMap<String, Object>> adGuideCalculate(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adGuideCal");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("calcNo", rset.getInt("CALC_NO"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("calcDate", rset.getDate("CALC_DATE"));
				hmap.put("calcPay", rset.getDate("CALC_PAYMENT"));
				hmap.put("calcCost", rset.getInt("CALC_COST"));
				hmap.put("calcState", rset.getString("CALC_STATE"));
				
				list.add(hmap);
			}
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return list;
	}

	public int adSearchNameCalculateCount(Connection con, String searchTxt) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameCalCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchTxt);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchNameCalculate(Connection con, String searchTxt, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameCal");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchTxt);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("calcNo", rset.getInt("CALC_NO"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("calcDate", rset.getDate("CALC_DATE"));
				hmap.put("calcCost", rset.getInt("CALC_COST"));
				hmap.put("calcPayment", rset.getDate("CALC_PAYMENT"));
				hmap.put("calcState", rset.getString("CALC_STATE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public int adSearchIdCalCount(Connection con, String searchTxt) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;
		
		String query = prop.getProperty("adSearchCalIdCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchTxt);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchIdCal(Connection con, String searchTxt, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adSearchIdCal");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchTxt);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("calcNo", rset.getInt("CALC_NO"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("calcDate", rset.getDate("CALC_DATE"));
				hmap.put("calcCost", rset.getInt("CALC_COST"));
				hmap.put("calcPayment", rset.getDate("CALC_PAYMENT"));
				hmap.put("calcState", rset.getString("CALC_STATE"));

				list.add(hmap);
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		return list;
	}

	public int adGuideCalFilterCount(Connection con, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;
		
		String query = prop.getProperty("adGuideCalFilterCount");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adGuideCalFilter(Connection con, int currentPage, int limit, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adGuideCalFilter");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, str);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("calcNo", rset.getInt("CALC_NO"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("calcDate", rset.getDate("CALC_DATE"));
				hmap.put("calcCost", rset.getInt("CALC_COST"));
				hmap.put("calcPayment", rset.getDate("CALC_PAYMENT"));
				hmap.put("calcState", rset.getString("CALC_STATE"));

				list.add(hmap);
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> adGuideCalSelectOne(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adGuideCalSelectOne");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();
			list = new ArrayList<HashMap<String, Object>>();
			
			if(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				if(rset.getString("PKIND").equals("GP")) {
					hmap.put("pkind", rset.getString("PKIND"));
					hmap.put("calcNo", rset.getInt("CALC_NO"));
					hmap.put("gpNo", rset.getInt("GP_NO"));
					hmap.put("email", rset.getString("EMAIL"));
					hmap.put("gname", rset.getString("GNAME"));
					hmap.put("accountNo", rset.getString("ACCOUNT_NO"));
					hmap.put("accountName", rset.getString("ACCOUNT_NAME"));
					hmap.put("bankName", rset.getString("BANK_NAME"));
					hmap.put("calcDate", rset.getDate("CALC_DATE"));
					hmap.put("calcPayment", rset.getDate("CALC_PAYMENT"));
					hmap.put("epi", rset.getInt("EPI"));
					hmap.put("ghState", rset.getString("GH_STATE"));
					hmap.put("mno", rset.getInt("MNO"));
					hmap.put("calcCost", rset.getInt("CALC_COST"));
					hmap.put("calcState", rset.getString("CALC_STATE"));
					hmap.put("gpName", rset.getString("GP_NAME"));
					hmap.put("gpDescr", rset.getString("GP_DESCR"));
					hmap.put("epi", rset.getInt("EPI"));
					hmap.put("gpCost", rset.getInt("GP_COST"));
					
					list.add(hmap);
					
				}else {
					PreparedStatement pstmt2 = null;
					ResultSet rset2 = null;
					
					String query2 = prop.getProperty("adGuideCalSelctOneCp");
					
					pstmt2 = con.prepareStatement(query2);
					pstmt2.setInt(1, num);
					rset2 = pstmt2.executeQuery();
					
					while(rset2.next()) {
						hmap.put("pkind", rset2.getString("PKIND"));
						hmap.put("calcNo", rset.getInt("CALC_NO"));
						hmap.put("cpNo", rset2.getInt("CP_NO"));
						hmap.put("email", rset2.getString("EMAIL"));
						hmap.put("gname", rset2.getString("GNAME"));
						hmap.put("accountNo", rset2.getString("ACCOUNT_NO"));
						hmap.put("epi", rset2.getInt("EPI"));
						hmap.put("accountName", rset.getString("ACCOUNT_NAME"));
						hmap.put("bankName", rset.getString("BANK_NAME"));
						hmap.put("calcDate", rset2.getDate("CALC_DATE"));
						hmap.put("calcPay", rset2.getDate("CALC_PAYMENT"));
						hmap.put("ghState", rset2.getString("GH_STATE"));
						hmap.put("mno", rset2.getInt("MNO"));
						hmap.put("calcCost", rset2.getInt("CALC_COST"));
						hmap.put("calcState", rset2.getString("CALC_STATE"));
						hmap.put("cpDate", rset2.getDate("CP_DATE"));
						hmap.put("cpTitle", rset2.getString("CP_TITLE"));
						hmap.put("cpCnt", rset2.getInt("CP_CNT"));
						hmap.put("cpCost", rset2.getInt("CP_COST"));
						
						list.add(hmap);
						
						
						
						
					}
					
					
				}
				
				
				
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		System.out.println("컬큘레이트 list : " + list);
		return list;
	}

	public int adUpdateCalculate(Connection con, int num, String str) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("adUpdateCal");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			pstmt.setInt(2, num);
			
			result = pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			
		}
		
		
		
		return result;
	}

	public int adUpdateGuideHistoryInterYn(Connection con, int num, GuideHistory gh) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateGuideHistoryInterview");

		//("여기 는?? : " + gh.getInterviewDate());
		//(gh.getInterviewTime());
		//(gh.getInterviewPassYn());
		//("제일중요 : " + gh.getInterviewYn());


		try {

			pstmt = con.prepareStatement(query);
			//pstmt.setDate(1, gh.getInterviewDate());
			//pstmt.setString(2, gh.getInterviewTime());
			pstmt.setString(1, gh.getInterviewYn());
			pstmt.setInt(2, num);


			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}


		return result;
	}

	public int adUpdateGuideHistoryInterResult(Connection con, int num, GuideHistory gh) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateGuideHistoryInterviewResult");
		
		try {

			pstmt = con.prepareStatement(query);
			//pstmt.setDate(1, gh.getInterviewDate());
			//pstmt.setString(2, gh.getInterviewTime());
			pstmt.setString(1, gh.getGhState());
			pstmt.setInt(2, num);


			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}


		return result;
		
	}

	public ArrayList<HashMap<String, Object>> adSelectOneGuideHistoryDetailLang(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list3 = null;
		
		String query = prop.getProperty("adGuideDetailLang");
		
		try {

			pstmt = con.prepareStatement(query);
			//pstmt.setDate(1, gh.getInterviewDate());
			//pstmt.setString(2, gh.getInterviewTime());
			pstmt.setInt(1, num);
			
			
			list3 = new ArrayList<HashMap<String, Object>>();
			rset = pstmt.executeQuery();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("lang", rset.getString("LANG"));
				
				list3.add(hmap);
			}
			


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}


		return list3;
	}

	public int adUpdateGuideHistoryInterResultOk(Connection con, int num, GuideHistory gh) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateGuideHistoryInterviewResultOK");
		
		try {

			pstmt = con.prepareStatement(query);
			//pstmt.setDate(1, gh.getInterviewDate());
			//pstmt.setString(2, gh.getInterviewTime());
			pstmt.setString(1, gh.getGhState());
			pstmt.setInt(2, num);


			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}


		return result;
	}

	public int adUpdateGuideHistoryInterResultOkTotal(Connection con, int num2) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateGuideHistoryInterviewResultOKTotal");
		
		try {

			pstmt = con.prepareStatement(query);
			//pstmt.setDate(1, gh.getInterviewDate());
			//pstmt.setString(2, gh.getInterviewTime());
			pstmt.setInt(1, num2);


			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}


		return result;
	}

	public ArrayList<HashMap<String, Object>> adselectAttachmentHistoryPro(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list2 = null;
		Attachment at = null;
		
		System.out.println("gh_no : " + num);

		String query = prop.getProperty("adSelectAttachmentHistoryPro");
		//("query : " + query);

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("fileNp", rset.getInt("FILE_NO"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));
				hmap.put("filePath", rset.getString("FILE_PATH"));

				list2.add(hmap);
				//("여기는???? : " + list2);

			}







		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list2;







}

	public ArrayList<HashMap<String, Object>> selectOneGuideHistoryLocal(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;


		String query = prop.getProperty("selectOneGuideHistoryLocal");



		try {


			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			//("rset : " + rset);


			while(rset.next()) {

				//("whileRset : " + rset);
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				/*hmap.put("fileNo", rset.getInt("FILE_NO"));
				hmap.put("filePath", rset.getString("FILE_PATH"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));*/
				hmap.put("localName", rset.getString("LOCAL_NAME"));



				list.add(hmap);


			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		//("오늘list : " + list);

		return list;
	}
}
