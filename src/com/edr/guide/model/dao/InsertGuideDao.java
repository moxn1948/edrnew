package com.edr.guide.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.guide.model.vo.GuideLocal;




public class InsertGuideDao {
	Properties prop = new Properties();
	public InsertGuideDao() {
		String fileName = InsertGuideDao.class.getResource("/sql/guide/guide-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		
	}
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 
	 * @param con
	 * @param gd
	 * @return         :
	 */
	public int insertGuide(Connection con, GuideDetail gd) {
		PreparedStatement pstmt = null;
		int result = 0;
		

		String query = prop.getProperty("insertGuide");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gd.getMno());
			pstmt.setString(2, gd.getGname());
			pstmt.setString(3, gd.getKakaoId());
			pstmt.setString(4, gd.getPhone());
			pstmt.setString(5, gd.getPhone2());
			pstmt.setString(6, gd.getAddress());
			pstmt.setString(7, gd.getLang());
			pstmt.setInt(8, gd.getPeriod());
			pstmt.setString(9, gd.getIntroduce());
			pstmt.setString(10, gd.getExpYn());
			pstmt.setString(11, gd.getTbBan());

			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			
		}
		

		
		return result;
	}
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 지역테이블에 삽입
	 * @param con
	 * @param gl
	 * @return         :
	 */
	public int insertLocal(Connection con, GuideLocal gl) {
		PreparedStatement pstmt = null;
		int result2 = 0;
		String query = prop.getProperty("insertLocal");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gl.getMno());
			pstmt.setInt(2, gl.getLocalNo());
			

			result2 = pstmt.executeUpdate(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result2;
	}
	
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 10.
	 * @Description      : 신청내역 저장 
	 * @param con
	 * @param gh
	 * @return         :
	 */
	public int insertHistory(Connection con, GuideHistory gh) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result3 = 0;
		int result = 0;
		String query = prop.getProperty("insertHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gh.getMno());
			pstmt.setDate(2, gh.getInterviewDate());
			pstmt.setString(3, gh.getInterviewTime());
		
			result = pstmt.executeUpdate();
			
			result3 = selectGhNo(con, gh);
			
			System.out.println("dao :" + result3);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result3;
	}
	
	public int selectGhNo(Connection con, GuideHistory gh) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectGhNo");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, gh.getMno());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
				System.out.println("dao : " + rset.getInt(1));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		
		
		return result;
		
	}
	public int insertAttachment(Connection con, ArrayList<Attachment> fileList, int result3) {
		PreparedStatement pstmt = null;
		int result4 = 0;
		String query = prop.getProperty("insertAttachment");
		
		try {
			
			for(int i=0; i <fileList.size(); i++) {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, fileList.get(i).getOriginName());
				pstmt.setString(2, fileList.get(i).getChangeName());
				pstmt.setString(3, fileList.get(i).getFilePath());
				pstmt.setInt(4, fileList.get(i).getGhNo());
				pstmt.setString(5, fileList.get(i).getProfileYn());
				
				result4 += pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result4;
	}
	

}
