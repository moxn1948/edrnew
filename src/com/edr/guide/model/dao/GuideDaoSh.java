package com.edr.guide.model.dao;
import static com.edr.common.JDBCTemplate.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.vo.Member;
public class GuideDaoSh {

	Properties prop = new Properties();
	
	public GuideDaoSh() {
		
		String fileName = GuideDaoSh.class.getResource("/sql/guide/guide-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public int updateGuideBank(Connection con, GuideDetail requestGuide) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("updateGuideBank");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestGuide.getBankName());
			pstmt.setString(2, requestGuide.getAccountName());
			pstmt.setString(3, requestGuide.getAccountNo());
			pstmt.setInt(4, requestGuide.getMno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public GuideDetail guideDetailInfo(Connection con, Member loginUser) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		GuideDetail guideDetailInfo = null;
		
		String query = prop.getProperty("getGuideDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, loginUser.getMno());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				guideDetailInfo = new GuideDetail();
				guideDetailInfo.setMno(rset.getInt("MNO"));
				guideDetailInfo.setGname(rset.getString("GNAME"));
				guideDetailInfo.setKakaoId(rset.getString("KAKAO_ID"));
				guideDetailInfo.setPhone(rset.getString("PHONE"));
				guideDetailInfo.setPhone2(rset.getString("PHONE2"));
				guideDetailInfo.setAddress(rset.getString("ADDRESS"));
				guideDetailInfo.setLang(rset.getString("LANG"));
				guideDetailInfo.setPeriod(rset.getInt("PERIOD"));
				guideDetailInfo.setIntroduce(rset.getString("INTRODUCE"));
				guideDetailInfo.setExpYn(rset.getString("EXP_YN"));
				guideDetailInfo.setTbBan(rset.getString("TB_BAN"));
				guideDetailInfo.setAccountNo(rset.getString("ACCOUNT_NO"));
				guideDetailInfo.setAccountName(rset.getString("ACCOUNT_NAME"));
				guideDetailInfo.setBankName(rset.getString("BANK_NAME"));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return guideDetailInfo;
	}
	
	
}
