package com.edr.guide.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.generalProgram.model.vo.Gp;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

public class CompGpDao {

	Properties prop = new Properties();
	
	public CompGpDao() {
		String fileName = GuideDaoSh.class.getResource("/sql/guide/guide-query.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 19.
	 * @Description      : 가이드 완료된 프로그램 
	 * @param con
	 * @param mno
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> selectCompGp(Connection con, int currentPage, int limit, int mno) {
		ArrayList<HashMap<String, Object>> compList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCompGp");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit -1;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			compList = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				int rnum = rset.getInt("RNUM");
				Date fday = rset.getDate("FDAY");
				int idxnum = rset.getInt("IDXNUM");
				
				Gp gpComp = new Gp();
				gpComp.setGpName(rset.getString("GPNAME"));
				gpComp.setGpNo(rset.getInt("GNO"));
				gpComp.setGpTday(rset.getInt("GTDAY"));
				
				Product pdComp = new Product();
				pdComp.setEpi(rset.getInt("PEPI"));
				pdComp.setPdate(rset.getDate("PPDATE"));
				pdComp.setPno(rset.getInt("PPNO"));
				
				Payment pmComp = new Payment();
				pmComp.setPayState(rset.getString("PAYSTATE"));
				
				map.put("gpComp", gpComp);
				map.put("pdComp", pdComp);
				map.put("pmComp", pmComp);
				map.put("rnum", rnum);
				map.put("fday", fday);
				map.put("idxnum", idxnum);
				
				compList.add(map);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return compList;
	}

	public int selectCompGpCount(Connection con, int mno) {
		int result = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCompGpCount");
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return result;
	}

}
