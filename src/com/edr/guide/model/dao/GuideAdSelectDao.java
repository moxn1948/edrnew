package com.edr.guide.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.member.model.dao.MemberDao;
import com.edr.member.model.vo.Member;

public class GuideAdSelectDao {
	Properties prop = new Properties();

	public GuideAdSelectDao() {

		String fileName = MemberDao.class.getResource("/sql/guide/guide-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Guide list select Dao
	 * @param con
	 * @return : list
	 */
	public ArrayList<GuideDetail> selectGuideList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<GuideDetail> list = null;

		String query = prop.getProperty("selectGuideList");

		try {
			stmt = con.createStatement();

			rset = stmt.executeQuery(query);
			list = new ArrayList<>();

			while (rset.next()) {
				GuideDetail gd = new GuideDetail();
				gd.setMno(rset.getInt("MNO"));
				gd.setGname(rset.getString("GNAME"));
				gd.setKakaoId(rset.getString("KAKAO_ID"));
				gd.setPhone(rset.getString("PHONE"));
				gd.setPhone2(rset.getString("PHONE2"));
				gd.setAddress(rset.getString("ADDRESS"));
				gd.setLang(rset.getString("LANG"));
				gd.setPeriod(rset.getInt("PERIOD"));
				gd.setIntroduce(rset.getString("INTRODUCE"));
				gd.setExpYn(rset.getString("EXP_YN"));
				gd.setTbBan(rset.getString("TB_BAN"));
				gd.setAccountNo(rset.getString("ACCOUNT_NO"));
				gd.setAccountName(rset.getString("ACCOUNT_NAME"));
				gd.setBankName(rset.getString("BANK_NAME"));

				list.add(gd);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Guide list Count Dao
	 * @param con
	 * @return : listCount
	 */
	public int getlistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("listCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			close(stmt);
			close(rset);

		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Guide list select Paging Dao
	 * @param con,
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectListWithPaging(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("selectListWithPaging");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("mno", rset.getInt("MNO"));
				//hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("localname", rset.getString("LOCAL_NAME"));
				/*
				 * hmap.put("phone2", rset.getString("PHONE2")); hmap.put("address",
				 * rset.getString("ADDRESS")); hmap.put("lang", rset.getString("LANG"));
				 * hmap.put("period", rset.getInt("PERIOD")); hmap.put("introduce",
				 * rset.getString("INTRODUCE")); hmap.put("expyn", rset.getString("EXP_YN"));
				 * hmap.put("tbban", rset.getString("TB_BAN")); hmap.put("kakaoid",
				 * rset.getString("KAKAO_ID")); hmap.put("accountno",
				 * rset.getString("ACCOUNT_NO")); hmap.put("accountname",
				 * rset.getString("ACCOUNT_NAME")); hmap.put("bankname",
				 * rset.getString("BANK_NAME")); hmap.put("localno", rset.getInt("LOCAL_NO"));
				 */

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 11.
	 * @Description : Guide list Detail select Dao
	 * @param con,
	 *            num
	 * @return : hmap
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneGuide(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSelectOneGuide");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("changename", rset.getString("CHANGE_NAME"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("kakaoid", rset.getString("KAKAO_ID"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("phone2", rset.getString("PHONE2"));
				hmap.put("address", rset.getString("ADDRESS"));
				hmap.put("introduce", rset.getString("INTRODUCE"));
				hmap.put("lang", rset.getString("LANG"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("gpno", rset.getInt("GP_NO"));

				// hmap.put("wcount", rset.getInt("WCOUNT"));

				hmap.put("gpname", rset.getString("GP_NAME"));

				// hmap.put("openyn", rset.getString("OPEN_YN"));
				// hmap.put("localno", rset.getInt("LOCAL_NO"));
				hmap.put("localname", rset.getString("LOCAL_NAME"));
				hmap.put("originname", rset.getString("ORIGIN_NAME"));
				hmap.put("ghdate", rset.getDate("GH_DATE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search Id Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchIdGuideCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchIdGuideCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search Id Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchIdGuide(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchIdGuide");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("localname", rset.getString("LOCAL_NAME"));
				// hmap.put("mtype", rset.getString("MTYPE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search Name Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchNameGuideCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameGuideCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search Name Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameGuide(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameGuide");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("localname", rset.getString("LOCAL_NAME"));
				// hmap.put("mtype", rset.getString("MTYPE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search City Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchCityGuideCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchCityGuideCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Guide list Search City Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchCityGuide(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchCityGuide");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("localname", rset.getString("LOCAL_NAME"));
				// hmap.put("mtype", rset.getString("MTYPE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Guide list Update Dao
	 * @param :con,
	 *            m
	 * @return : result
	 */
	public int adUpdateGuide(Connection con, GuideDetail gd) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("adUpdateGuide");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, gd.getGname());
			pstmt.setInt(2, gd.getMno());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 18.
	 * @Description : Ad Guide Select One Attachment Download Dao
	 * @param :num
	 * @return : list2
	 */
	public ArrayList<Attachment> adSelectOneAttachment(Connection con, int num) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list2 = null;
		Attachment at = null;

		String query = prop.getProperty("adSelectOneAttachment");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<Attachment>();

			while (rset.next()) {
				at = new Attachment();
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				list2.add(at);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list2;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 18.
	 * @Description : Ad Guide Select Attachment Download Dao
	 * @param :num
	 * @return : list2
	 */
	public ArrayList<Attachment> adSelectGuideAttachment(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list2 = null;
		Attachment at = null;

		String query = prop.getProperty("adSelectGuideAttachment");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list2 = new ArrayList<Attachment>();

			while (rset.next()) {
				at = new Attachment();
				at.setFileNo(rset.getInt("FILE_NO"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));

				list2.add(at);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list2;
	}

	public ArrayList<HashMap<String, Object>> adSelectGuideAttachmentPro(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list3 = null;
		Attachment at = null;

		String query = prop.getProperty("adSelectGuideAttachmentPro");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list3 = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("fileno", rset.getInt("FILE_NO"));
				hmap.put("changename", rset.getString("CHANGE_NAME"));
				hmap.put("originname", rset.getString("ORIGIN_NAME"));
				hmap.put("filepath", rset.getString("FILE_PATH"));

				list3.add(hmap);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list3;

	}

	public ArrayList<HashMap<String, Object>> adSelectGuideGP(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list4 = null;
		
		String query = prop.getProperty("adSelectGuideGP");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			list4 = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
			
				list4.add(hmap);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list4;

	}

}
