package com.edr.guide.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.dao.AdGuideDao;
import com.edr.guide.model.vo.GuideHistory;

public class AdGuideService {

	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : GuideHistoryCount
	 * @return         : listCount
	 */
	public int adGuideHistoryCount() {
		Connection con = getConnection();

		int listCount = new AdGuideDao().adGuideHistoryCount(con);

		close(con);

		return listCount;
	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : GuideHistoryService(with paging)
	 * @param currentPage
	 * @param limit
	 * @return         : ArrayList<HashMap<String Object>>
	 */
	public ArrayList<HashMap<String, Object>> adGuideHistory(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideHistory(con, currentPage, limit);


		close(con);

		return list;
	}





	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 가이드 신청내역 이름검색 service
	 * @param userName
	 * @param currentPage
	 * @param limit
	 * @return         : 가이드 신청내역 이름검색에 대한 ArrayList
	 */
	public ArrayList<HashMap<String, Object>> AdSearchGuideHistoryName(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adSearchNameGuideHistory(con, currentPage, limit, searchText);

		close(con);

		return list;

	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 가이드 신청내역 아이디 검색 serivce
	 * @param userId
	 * @param currentPage
	 * @param limit
	 * @return         : 가이드 신청내역 아이디 검색에 대한 ArrayList
	 */
	public ArrayList<HashMap<String, Object>> AdSearchGuideHistoryId(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adSearchIdGuideHistory(con, currentPage, limit, searchText);

		close(con);

		return list;

	}





	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 해당 이름 검색에 대한 총 갯수
	 * @param userName
	 * @return         : 해당 이름 검색 결과에 대한 총갯수 int listCount
	 */
	public int adSearchNameGuideHistoryCount(String searchText) {

		Connection con = getConnection();

		int listCount = new AdGuideDao().adSearchNameGuideHistoryCount(con,searchText);

		close(con);

		return listCount;

	}



	/**
	 * @Author         : kijoin
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 가이드 신청내역 아이디 검색 결과 갯수 service
	 * @param userId
	 * @return         : 가이드 신청내역 아이디 검색 결과 갯수 int listCount
	 */
	public int adSearchIdGuideHistoryCount(String searchText) {

		Connection con = getConnection();

		int listCount = new AdGuideDao().adSearchIdGuideHistoryCount(con, searchText);

		close(con);

		return listCount;

	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 가이드 신청내역 상세보기
	 * @param num
	 * @return         : 가이드 신청내역 상세보기 목록
	 */
	public ArrayList<HashMap<String, Object>> selectOneGuideHistory(int num) {

		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().selectOneGuideHistory(con, num);

		close(con);

		return list;
	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 가이드 신청내역 상세보기 정보업데이트
	 * @param gh
	 * @param num 
	 * @return         : 가이드 신청내역 상세보기 정보 업데이트 
	 */
	public int updateGuideHistory(GuideHistory gh, int num) {
		Connection con = getConnection();

		int result = new AdGuideDao().updateGuideHistory(con, gh, num);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 가이드 신청내역 상세보기 첨부파일
	 * @param num
	 * @return         :가이드 신청내역 상세보기 첨부파일 목록
	 */
	public ArrayList<Attachment> selectAttachMent(int num) {
		Connection con = getConnection();

		ArrayList<Attachment> list2 = new AdGuideDao().selectAttachment(con, num);

		close(con);

		return list2;
	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 가이드 첨부파일 상세보기 목록
	 * @param num
	 * @return         :가이드 첨부파일 상세보기 목록
	 */
	public ArrayList<Attachment> adSelectAttachmentHistory(int num) {
		Connection con = getConnection();

		ArrayList<Attachment> list2 = new AdGuideDao().adselectAttachmentHistory(con, num);

		close(con);

		return list2;



	}



	public ArrayList<HashMap<String, Object>> adTimeCountCheck(String day2) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideTimeCheck(con, day2);
		
		close(con);
		
		
		return list;
	}



	public ArrayList<HashMap<String, Object>> adGuideHistoryFileter(int currentPage, int limit, String str) {
		
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideHistoryFileter(con, str,currentPage, limit);
		
		close(con);
		
		return list;
	}



	public int adGuideCalculateCount() {
		
		Connection con = getConnection();

		int listCount = new AdGuideDao().adGuideCalculateCount(con);

		close(con);

		return listCount;
		
	}



	



	public int adGuideHistoryFilterCount(String str) {
		Connection con = getConnection();

		int listCount = new AdGuideDao().adGuideHistoryFilterCount(con, str);

		close(con);

		return listCount;
		
	}



	public ArrayList<HashMap<String, Object>> adGuideCalculate(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideCalculate(con, currentPage, limit);


		close(con);

		return list;
	}



	public int adSearchNameCalculateCount(String searchTxt) {
		Connection con = getConnection();
		int listCount = new AdGuideDao().adSearchNameCalculateCount(con, searchTxt);
		
		close(con);
		
		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adSearchNameCalculate(String searchTxt, int currentPage, int limit) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adSearchNameCalculate(con, searchTxt, currentPage, limit);
		close(con);
		
		return list;
	}



	public int adSearchIdCalCount(String searchTxt) {
		Connection con = getConnection();
		
		int listCount = new AdGuideDao().adSearchIdCalCount(con, searchTxt);
		
		close(con);
		
		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adSearchIdCal(String searchTxt, int currentPage, int limit) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adSearchIdCal(con, searchTxt, currentPage, limit);
		
		close(con);
		
		return list;
	}



	public int adGuideCalFilterCount(String str) {
		Connection con = getConnection();
		int listCount = new AdGuideDao().adGuideCalFilterCount(con, str);
		close(con);
		
		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adGuideCalFileter(int currentPage, int limit, String str) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideCalFilter(con, currentPage, limit, str);
		
		close(con);
		
		return list;
	}



	public ArrayList<HashMap<String, Object>> adGuideCalSelectOne(int num) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().adGuideCalSelectOne(con, num);
		
		close(con);
		
		return list;
	}





	public int adUpdateCalculate(int num, String str) {
		Connection con = getConnection();
		int result = new AdGuideDao().adUpdateCalculate(con, num, str);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}



	public int updateGuideHistoryinterYn(GuideHistory gh, int num) {
		Connection con = getConnection();
		int result = new AdGuideDao().adUpdateGuideHistoryInterYn(con, num, gh);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	public int updateGuideHistoryinterResult(GuideHistory gh, int num) {
		Connection con = getConnection();
		int result = new AdGuideDao().adUpdateGuideHistoryInterResult(con, num, gh);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	public ArrayList<HashMap<String, Object>> adSelectOneGuideHistoryDetailLang(int num) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list3 = new AdGuideDao().adSelectOneGuideHistoryDetailLang(con, num);
		close(con);
		
		return list3;
	}



	public int updateGuideHistoryinterResultOk(GuideHistory gh, int num) {
		Connection con = getConnection();
		int result = new AdGuideDao().adUpdateGuideHistoryInterResultOk(con, num, gh);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	public int updateGuideHistoryMtypeUpdate(int num2) {
		Connection con = getConnection();
		int result = new AdGuideDao().adUpdateGuideHistoryInterResultOkTotal(con, num2);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	public ArrayList<HashMap<String, Object>> adSelectAttachmentHistoryPro(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list2 = new AdGuideDao().adselectAttachmentHistoryPro(con, num);

		close(con);

		return list2;
	}



	public ArrayList<HashMap<String, Object>> adSelectOneGuideLocal(int num) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdGuideDao().selectOneGuideHistoryLocal(con, num);

		close(con);

		return list;
	}
















}
