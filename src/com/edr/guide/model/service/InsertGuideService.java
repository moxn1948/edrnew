package com.edr.guide.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.dao.InsertGuideDao;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.guide.model.vo.GuideLocal;

public class InsertGuideService {
 
	/**
	 * @Author         : cwj
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 
	 * @param gd
	 * @return         :
	 */
	public int insertGuide(GuideDetail gd) {
		Connection con = getConnection();
		
		int result = new InsertGuideDao().insertGuide(con, gd);
		
		if(result >0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int insertLocal(GuideLocal gl) {
		Connection con = getConnection();
		
		int result2 = new InsertGuideDao().insertLocal(con, gl);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result2;
	}

	public int insertHistory(GuideHistory gh) {
		Connection con = getConnection();
		
		int result3 = new InsertGuideDao().insertHistory(con, gh);
		
		if(result3 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result3;
	}

	public int insertAttachment(ArrayList<Attachment> fileList, int result3) {
		Connection con = getConnection();
		
		
		
		int result4 = new InsertGuideDao().insertAttachment(con, fileList, result3);
		
		if(result4 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result4;
	}

}
