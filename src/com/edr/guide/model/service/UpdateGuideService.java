package com.edr.guide.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.dao.GuideDao;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.guide.model.vo.GuideLocal;

public class UpdateGuideService {
 
	/**
	 * @Author         : cwj
	 * @CreateDate       : 2019. 12. 8.
	 * @Description      : 
	 * @param gd
	 * @return         :
	 */
	public int insertGuide(GuideDetail gd) {
		Connection con = getConnection();
		
		int result = new GuideDao().insertGuide(con, gd);
		
		if(result >0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int insertLocal(GuideLocal gl) {
		Connection con = getConnection();
		
		int result2 = new GuideDao().insertLocal(con, gl);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result2;
	}

	public ArrayList<HashMap<String, Object>> selectOneGuide(int num) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new GuideDao().selectOneGuide(con, num);
		
		close(con);
		return list;
	}

	public ArrayList<Attachment> selectAttachment(int num) {
		Connection con = getConnection();
		
		ArrayList<Attachment> list2 = new GuideDao().selectAttachment(con, num);
		
		close(con);
		
		return list2;
	}

	public int updateHistory(GuideHistory gh) {
		Connection con = getConnection();
		
		int result = new GuideDao().updateHistory(con, gh);
		
		if(result > 0) {
			commit(con);
			
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

}
