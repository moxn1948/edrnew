package com.edr.guide.model.service;

import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.guide.model.dao.CompGpDao;

public class CompGpService {

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 19.
	 * @Description      : 가이드 완료된 프로그램
	 * @param mno
	 * @param mno2 
	 * @param limit 
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> selectCompGp(int currentPage, int limit, int mno) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> compList = new CompGpDao().selectCompGp(con, currentPage, limit, mno);
		
		return compList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 19.
	 * @Description      : 가이드 완료된 프로그램 - 카운트
	 * @param mno
	 * @return         :
	 */
	public int selectCompGpCount(int mno) {
		Connection con = getConnection();
		
		int result = new CompGpDao().selectCompGpCount(con, mno);
		
		return result;
	}

}
