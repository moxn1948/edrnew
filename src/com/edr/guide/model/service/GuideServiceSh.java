package com.edr.guide.model.service;

import com.edr.guide.model.dao.GuideDaoSh;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.vo.Member;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;

public class GuideServiceSh {

	public int updateGuideBank(GuideDetail requestGuide) {

		Connection con = getConnection();
		
		int result = new GuideDaoSh().updateGuideBank(con,requestGuide);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public GuideDetail guideDetailInfo(Member loginUser) {

		Connection con = getConnection();
		
		GuideDetail guideDetailInfo = new GuideDaoSh().guideDetailInfo(con,loginUser);
		
		close(con);
		
		return guideDetailInfo;
	}


}
