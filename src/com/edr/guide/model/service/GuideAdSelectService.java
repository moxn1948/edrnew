package com.edr.guide.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.guide.model.dao.AdGuideDao;
import com.edr.guide.model.dao.GuideAdSelectDao;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.dao.MemberAdSelectDao;
import com.edr.member.model.vo.Member;
import com.edr.review.model.vo.Review;

/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
public class GuideAdSelectService {

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Guide list select Service
	 * @param
	 * @return : list
	 */
	public ArrayList<GuideDetail> selectGuideList() {
		Connection con = getConnection();

		ArrayList<GuideDetail> list = new GuideAdSelectDao().selectGuideList(con);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Guide list count Service
	 * @param
	 * @return : listCount
	 */
	public int getListCount() {
		Connection con = getConnection();

		int listCount = new GuideAdSelectDao().getlistCount(con);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Guide list select Paging Service
	 * @param :
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectListWithPaging(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectDao().selectListWithPaging(con, currentPage, limit);

		close(con);

		return list;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : Ad Guide list select detail Service
	 * @param :
	 *            num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneGuide(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectDao().adSelectOneGuide(con, num);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search Id Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchIdGuideCount(String searchText) {
		Connection con = getConnection();

		int listCount = new GuideAdSelectDao().adSearchIdGuideCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search Id Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchIdGuide(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectDao().adSearchIdGuide(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search Name Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchNameMemberCount(String searchText) {
		Connection con = getConnection();

		int listCount = new GuideAdSelectDao().adSearchNameGuideCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search Name Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameMember(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectDao().adSearchNameGuide(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search City Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchCityGuideCount(String searchText) {
		Connection con = getConnection();

		int listCount = new GuideAdSelectDao().adSearchCityGuideCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Guide list Search City Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchCityGuide(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectDao().adSearchCityGuide(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}

	/**
	 * @param m
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Guide list Update Service
	 * @param :
	 *            gd
	 * @return : result
	 */
	public int adUpdateGuide(GuideDetail gd) {
		Connection con = getConnection();

		int result = new GuideAdSelectDao().adUpdateGuide(con, gd);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}
	/**
	 * @Author         : young il
	 * @CreateDate       : 2019. 12. 18.
	 * @Description      : Ad Guide Select One Attachment Download Service
	 * @param 			:num
	 * @return         : list2
	 */
	public ArrayList<Attachment> adSelectOneAttachment(int num) {
		Connection con = getConnection();

		ArrayList<Attachment> list2 = new GuideAdSelectDao().adSelectOneAttachment(con, num);
		close(con);

		return list2;
	}
	/**
	 * @Author         : young il
	 * @CreateDate       : 2019. 12. 18.
	 * @Description      : Ad Guide Select Attachment Service
	 * @param 			: num
	 * @return         : list2
	 */
	public ArrayList<Attachment> adSelectGuideAttachment(int num) {
		Connection con = getConnection();

		ArrayList<Attachment> list2 = new GuideAdSelectDao().adSelectGuideAttachment(con, num);

		close(con);

		return list2;
	}

	public ArrayList<HashMap<String, Object>> adSelectGuideAttachmentPro(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list3 = new GuideAdSelectDao().adSelectGuideAttachmentPro(con, num);

		close(con);

		return list3;
	}

	public ArrayList<HashMap<String, Object>> adSelectGuideGP(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list4 = new GuideAdSelectDao().adSelectGuideGP(con, num);

		close(con);

		return list4;
	}

	

}
