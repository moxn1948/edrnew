package com.edr.guide.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.guide.model.dao.GuideDaohj;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideLocal;
import com.edr.member.model.vo.Member;

import static com.edr.common.JDBCTemplate.*;
public class GuideServicehj {
	
	
	
	
	
	

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 15. 
	 * @Description  : 가이드 프로필 정보 출력 ,사진도 같이 들고 와야함,guidePwd.me에서 넘어옴
	 * @param   : mno
	 * @return    : HashMap guideInfo
	 */
	public HashMap<String, Object> guideSelectOne(int mno) {
		
		Connection con = getConnection();
		  
		
		HashMap<String, Object> guideInfo = new GuideDaohj().guideSelectOne(con,mno);
		
		
		close(con);
		
		return guideInfo;
	}

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 19. 
	 * @Description  :가이드 프로필 정보수정 전에 비밀번호입력받고 검사
	 * @param   :
	 * @return    :
	 */
	public int checkGdPwd(Member requestMember) {
		Connection con = getConnection();
		
		int result = new GuideDaohj().checkGdPwd(con,requestMember);
		
		close(con);
		
		
		   
		
		
		
		return result;
	}

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 22. 
	 * @Description  : 
	 * @param   : gpUpdate
	 * @return    :
	 */
	public int updateProfile(HashMap<String, Object> gdUpdate) {
		
		Connection con = getConnection();
		
		int result =0;
		
		GuideDetail gd = (GuideDetail) gdUpdate.get("gd");
		
		System.out.println("gdUpdate : " + gdUpdate);
		
		ArrayList<Local> localList = (ArrayList<Local>) gdUpdate.get("localList");
		
		for (int i = 0; i < localList.size(); i++) {
			System.out.println("ㅋㅋ : " + localList.get(i));
			
		}
		int result1 = new GuideDaohj().deleteguidelocal(con, gd.getMno());
		
		int result2 =0;
		for(int i = 0; i < localList.size();i++	) {
			result2 += new GuideDaohj().insertguidelocal(con, gd.getMno(),localList.get(i));
			System.out.println("result2 : " + result2);
		}
		System.out.println("Out result2 : " + result2);
		int result3 = new GuideDaohj().updateProfile(con, gd);
		if(result1 > 0 && result2 > localList.size() - 1 && result3>0) {
			commit(con);
			result = 1;
		}else {
			rollback(con);
		}
		
		return result;
	}



	
}
