package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.guide.model.service.GuideServicehj;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.service.MemberServicehj;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class GuideProfilePwdServlet
 */
@WebServlet("/guidePwd.me")
public class GuideProfilePwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuideProfilePwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//작성시작
		
		//값꺼내기 
		
		String pwd = request.getParameter("userPwd");
		String email = request.getParameter("userEmail");
		//멤버로 가져온 이유: guideDetail에는 pwd정보없음
		Member requestMember = new Member();
		
		requestMember.setEmail(email);
		requestMember.setMpwd(pwd);
		
//		int result= new MemberService().checkPwd(requestMember);
		
		int result = new GuideServicehj().checkGdPwd(requestMember);
		
		
		int mno =Integer.parseInt(request.getParameter("mno"));
		HashMap<String, Object> guideInfo = new GuideServicehj().guideSelectOne(mno);
		
		
		GuideDetail guideDetail = (GuideDetail) guideInfo.get("gd");
		ArrayList<Local> localList = (ArrayList<Local>) guideInfo.get("lcList");
		Attachment at =(Attachment) guideInfo.get("at");
		
		//랭귀지를 어떻게 넣어야하지?

		
		String msg="";
		if(result > 0) {
			    
			
			//guideProfileEdit만들기
			request.setAttribute("guideDetail", guideDetail);
			request.setAttribute("localList", localList);
			request.setAttribute("at", at);

			request.getRequestDispatcher("views/user/sub/guide_page/guideProfileEdit.jsp").forward(request, response);
	
		
		}else {
			request.setAttribute("msg", "비밀번호가 일치하지 않습니다.");
			request.getRequestDispatcher("/gdProfile.gd?mno="+mno ).forward(request, response);
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
