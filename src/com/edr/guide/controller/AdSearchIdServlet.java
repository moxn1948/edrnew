package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdSearchIdServlet
 */
@WebServlet("/searchIdAddG.gd")
public class AdSearchIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSearchIdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("들어오니??");
		String searchVal = request.getParameter("searchVal");
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 5;
		
		int listCount = 0;
		String searchText = request.getParameter("searchText");
		
		listCount = new AdGuideService().adSearchIdGuideHistoryCount(searchText);
		
		maxPage = (int) ((double) listCount / limit + 0.8);
		
		startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
		
		endPage = startPage + 5 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
		ArrayList<HashMap<String, Object>> list = null;
		
		list = new AdGuideService().AdSearchGuideHistoryId(searchText, currentPage, limit);
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/guide_history/searchGuideHistory.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("searchText", searchText);
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "게시판 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
