package com.edr.guide.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuideDetailHistoryDownServlet
 */
@WebServlet("/adGuideHistoryDown.ad")
public class AdGuideDetailHistoryDownServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdGuideDetailHistoryDownServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int num = Integer.parseInt(request.getParameter("num"));

		ArrayList<Attachment> list = new AdGuideService().selectAttachMent(num);

		

		String root = "";
		for(int i = 0; i < 1; i++) {
			//HashMap<String, Object> hmap = list.get(i);
			root = request.getSession().getServletContext().getRealPath("/");

		}
		

		int nrr = root.indexOf("edrnew");


		//String sub = root.substring(root.indexOf("edrnew"));
		String sub = root.substring(0, root.indexOf("git"));
		String file = "";
		String file2 = ""; ;
		String filePath = "";
		
		//sub는 내주소 git까지 짜른거
		
		

		for(int i = 0; i < list.size(); i++) {
		
			file = list.get(i).getChangeName();
			file2 = list.get(i).getOriginName();
			filePath = list.get(i).getFilePath();
					
			/*file3 = file + file2;
			hmap.put("file3", file3);*/
			
		}
		
		String fileS = filePath.substring(filePath.indexOf("git"));
		
		String fileSave = sub + fileS;
		
		//String userSub = file.substring(0, file.indexOf("git"));
		String userSub = file.substring(filePath.indexOf("git"));

		/*.println(sub);
		//.println("userSub : " + userSub);

		String savePath = sub + userSub;*/

		//.println("savePath : " + savePath);
		//String str = root.substring(0, nrr);
/*
		for(int i = 0; i < 1; i++) {
			HashMap<String, Object> hmap = list.get(i);
			hmap.put("filePath", savePath);
			file3 = savePath + hmap.get("changeName");
			hmap.put("file3", file3);
		}

		for(int i = 0; i < 1; i++) {
			HashMap<String, Object> hmap = list.get(i);
			.println("hmap : " + hmap.get("file3"));
			.println("hmap : " + hmap.get("gname"));
			.println("hmap : " + hmap.get("kakaoId"));

			.println("hmap : " + hmap.get("phone"));

			.println("hmap : " + hmap.get("phone2"));

			.println("hmap : " + hmap.get("address"));

			.println("hmap : " + hmap.get("localName"));

			.println("hmap : " + hmap.get("period"));

			.println("hmap : " + hmap.get("expYn"));
			.println("hmap : " + hmap.get("lang"));
			.println("hmap : " + hmap.get("introduce"));
			.println("hmap : " + hmap.get("interviewD"));
			.println("hmap : " + hmap.get("interviewT"));
			list.add(hmap);
			
			

		}*/
		File downFile = null;
		
		for(int i = 0; i < list.size(); i++) {

			downFile = new File(fileSave + list.get(i).getChangeName());
			


		}
		
		BufferedInputStream buf = null;

		ServletOutputStream downOut = null;

		downOut = response.getOutputStream();

		


		response.setContentType("text/plain; charset=UTF-8");
		
		

		



		

		for(int i =0; i < list.size(); i++) {
			response.setHeader("Content-Disposition", "attachment; filename=\"" 
					+ new String(list.get(i).getOriginName().getBytes("UTF-8"), "ISO-8859-1") + "\""); 
		}
		response.setContentLength((int) downFile.length());

		FileInputStream fin = new FileInputStream(downFile);
		buf = new BufferedInputStream(fin);

		int readBytes = 0;

		while((readBytes = buf.read()) != -1) {
			downOut.write(readBytes);
		}
		downOut.close();
		buf.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
