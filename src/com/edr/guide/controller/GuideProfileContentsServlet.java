package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Local;
import com.edr.guide.model.service.GuideServicehj;
import com.edr.guide.model.vo.GuideDetail;

/**
 * Servlet implementation class GuideProfileContentsServlet
 */
@WebServlet("/guideProfileContents.gd")
public class GuideProfileContentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuideProfileContentsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//값꺼내기
		
		
		//mno
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		
		//전화번호1
		String ctryCode=request.getParameter("country");
		String pho= request.getParameter("phonenum1");
		//연락처 합침
		String phone1 = ctryCode + ")" + pho;
		//비상연락처
		String ctrycode2 = request.getParameter("country2");
		String pho2 =request.getParameter("phonenum2");
		//비상연락처합침
		String phone2 = ctrycode2 + ")" + pho2;	
		/*//활동지역
		String[] loca = request.getParameterValues("activity");*/
		
		String tempLocal = (String) request.getParameter("localhidden");
		tempLocal = tempLocal.trim();
		String[] loca = tempLocal.split(", ");
		
		//이렇게 하는거 아님
		/*String location;
		for(int i=0;i<loca.length;i++) {
			
			if(i==loca.length-1) {
				location+=loca[i];
			}else {
				location+=loca[i]+",";
			}
		}*/
		//우편번호
		String post= request.getParameter("postcode");
		//도로명주소
		String address = request.getParameter("roadAddress");
		//상세주소
		String detailAddress = request.getParameter("detailAddress");
		//주소합침
		String ad= post+"/"+address+"/"+detailAddress;
		//카카오아이디
		String kakaoId = request.getParameter("kakaoId");
		String introduce = request.getParameter("intro");
		
		
		GuideDetail gd = new GuideDetail();
		
		gd.setPhone(phone1); //전화번호1
		gd.setPhone2(phone2);//비상연락처
		gd.setAddress(ad); //주소
		gd.setIntroduce(introduce);//소개
		gd.setKakaoId(kakaoId);//카카오아이디
		gd.setMno(mno);
		
		ArrayList<Local> localList = new ArrayList<>();
		
		for(int i = 0;i < loca.length; i++) {
			System.out.println(i + " : " + loca[i]);
			Local lc = new Local();
			
			switch (loca[i]) {
			case "서울": lc.setLocalNo(1); lc.setLocalName("서울"); break;
			case "경기": lc.setLocalNo(2); lc.setLocalName("경기"); break;
			case "경상": lc.setLocalNo(3); lc.setLocalName("경상"); break;
			case "강원": lc.setLocalNo(4); lc.setLocalName("강원");  break;
			case "전라": lc.setLocalNo(5); lc.setLocalName("전라");  break;
			case "충청": lc.setLocalNo(6); lc.setLocalName("충청");  break;
			case "제주": lc.setLocalNo(7); lc.setLocalName("제주");  break;
			case "부산": lc.setLocalNo(8); lc.setLocalName("부산");  break;
			case "대구": lc.setLocalNo(9); lc.setLocalName("대구");  break;
			case "대전": lc.setLocalNo(10); lc.setLocalName("대전");  break;
			case "광주": lc.setLocalNo(11); lc.setLocalName("광주");  break;
			case "인천": lc.setLocalNo(12); lc.setLocalName("인천");  break;
			case "울산": lc.setLocalNo(13); lc.setLocalName("울산");  break;
			}
			
			
			localList.add(lc);
			
		}
		
		
//		lc.setLocalNo(loca); //????
		
		
		HashMap<String, Object> gdUpdate =new HashMap<String, Object>();
		
		gdUpdate.put("gd", gd);
		gdUpdate.put("localList", localList);
		
		System.out.println("여기 : " + localList);
		
		int result = new GuideServicehj().updateProfile(gdUpdate);
		
		String page ="";
		
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/gdProfile.gd?mno=" + mno);
		}else {
			request.setAttribute("msg", "업데이트 실패");
			page ="views/user/sub/common/errorPage.jsp";
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
