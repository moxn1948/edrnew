package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.guide.model.service.GuideServicehj;
import com.edr.guide.model.vo.GuideDetail;

/**
 * Servlet implementation class GuideSelectOneServlet
 */
@WebServlet("/selectGd.gd")
public class GuideSelectOneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuideSelectOneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//guideDetail객체를 가져와야함
		
		
		
		int mno = Integer.parseInt(request.getParameter("mno"));
		System.out.println(mno);
		HashMap<String, Object> guideInfo = new GuideServicehj().guideSelectOne(mno);
		
		
		
		//lang배열 =guideInfo.get"langarr"가쟈옴
		String[] langArr = (String[]) guideInfo.get("langArr");
		ArrayList<Local> localList = (ArrayList) guideInfo.get("lcList");
		Attachment at =(Attachment) guideInfo.get("at");
		GuideDetail guideDetail = (GuideDetail) guideInfo.get("guideDetail");
		
		
		if(guideInfo !=null) {
			
			request.setAttribute("langArr", langArr);
			request.setAttribute("localList", localList);
			request.setAttribute("at", at);
			request.getSession().setAttribute("guideDetail", guideDetail);
			request.getRequestDispatcher("/views/user/sub/guide_page/guideProfile.jsp").forward(request, response);
			
			
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
