package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.GuideService;


/**
 * Servlet implementation class SelectOneGuideServlet
 */
@WebServlet("/selectOne.gd")
public class SelectOneGuideServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneGuideServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("mno"));
		ArrayList<HashMap<String, Object>> Attachlist = new GuideService().selectOneGuide(num);
		ArrayList<Attachment> Attachlist2 = new GuideService().selectAttachment(num);
		
		int result = new GuideService().countAttachment(num);

		
		
		String page = "";
		if(Attachlist != null) {
			page = "views/user/sub/member/guideSignUpCondition.jsp";
			request.setAttribute("Attachlist", Attachlist);
			request.setAttribute("num", num);
			request.setAttribute("result", result);
			request.setAttribute("Attachlist2", Attachlist2);
			
		}else {
			page = "views/user/sub/common/errorPage.jsp";
			request.setAttribute("msg", "현황을 불러오는중 오류 발생");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
