package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuideDetailServlet
 */
@WebServlet("/adGuideDetail.gd")
public class AdGuideDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//.println("넘어오니????");
		//num은 gh_no
		//num2는 mno
		int num = Integer.parseInt(request.getParameter("num"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		System.out.println("수정하는거 : num : " + num);
		System.out.println("수정하는거 num2 : " + num2);
		ArrayList<HashMap<String, Object>> list = new AdGuideService().selectOneGuideHistory(num);
		ArrayList<Attachment> list2 = new AdGuideService().adSelectAttachmentHistory(num);
		ArrayList<HashMap<String, Object>>list3 = new AdGuideService().adSelectOneGuideHistoryDetailLang(num);
		ArrayList<HashMap<String, Object>> listLocal = new AdGuideService().adSelectOneGuideLocal(num);
		//.println("list : " + list);
		System.out.println("list2 : " + list2);
		//
		
		String root = "";
		
			
			root = request.getSession().getServletContext().getRealPath("/");
			
			String localNa = "";
			HashMap<String, Object> hmap = new HashMap<String, Object>();
		for(int i = 0; i < listLocal.size(); i++) {
			if(i == 0) {
				localNa += listLocal.get(i).get("localName");
			}else {
				localNa += ", " + listLocal.get(i).get("localName");
			}
		}
		hmap.put("localNameFi", localNa);
		listLocal.add(hmap);
			
		//int nrr = root.indexOf("edrnew");
		
		
		//String sub = root.substring(root.indexOf("edrnew"));
		String sub = root.substring(0, root.indexOf("git"));
		String file = "";
		String file2 = "";
		String file3 = "";
		System.out.println("sub : " + sub);
		
		
		ArrayList<HashMap<String, Object>> list4 = new AdGuideService().adSelectAttachmentHistoryPro(num);
		/*for(int i = 0; i < list2.size(); i++) {
			HashMap<String, Object> hmap = new HashMap<String, Object>();
			System.out.println("list2 : " + list2);
			String path = list2.get(i).getFilePath();
			
			String filePath = path.substring(path.indexOf("git"));
			String finalPath = sub + filePath;
			
			hmap.put("fileNo", list2.get(i).getFileNo());
			hmap.put("filePath", finalPath);
			hmap.put("changeName", list2.get(i).getChangeName());
			hmap.put("originName", list2.get(i).getOriginName());
			
			
			String finalPath2 = finalPath + list2.get(i).getChangeName();
			//.println("finalPath : " + finalPath2);
			
			hmap.put("finalPath", finalPath2);
			
			//.println(hmap.get("fileNo"));
			//.println("들어가라제발 : " + list2.get(i).getFilePath());
			
			list4.add(hmap);
			
		}*/
		
		/*for(int i = 0; i < list3.size(); i++) {
		     HashMap<String, Object> hmap = list3.get(i);
		    .println("hmap : " + hmap.get("finalPath"));
		    .println("hmap : " + hmap.get("originName"));
		    .println("hmap : " + hmap.get("changeName"));

		    .println("hmap : " + hmap.get("fileNo"));

		     

		}*/
		System.out.println("list : " + list);
		System.out.println("list4 : " + list4);
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/guide_history/addGuideDetail.jsp";
			request.setAttribute("list", list);
			request.setAttribute("list2", list2);
			request.setAttribute("list3", list3);
			request.setAttribute("list4", list4);
			request.setAttribute("num", num);
			request.setAttribute("num2", num2);
			request.setAttribute("listLocal", listLocal);
			request.setAttribute("localNa", localNa);
			
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "상세보기 실패!");
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
