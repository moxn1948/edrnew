package com.edr.guide.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuideCalculateUpdateServlet
 */
@WebServlet("/adCalculateUpdate.gd")
public class AdGuideCalculateUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideCalculateUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	int num = Integer.parseInt(request.getParameter("num"));
	String str = request.getParameter("str");
	
	
	if(str.equals("지급 완료")) {
		str = "PAYMENT";
	}else if(str.equals("승인 대기중")) {
		str = "WAIT";
	}else {
		str = "HOLD";
	}
	System.out.println("num : " + num);
	System.out.println("str : " + str);
	int result = new AdGuideService().adUpdateCalculate(num, str);
	System.out.println("int : " + result);
	
	String page = "";
	
	if(result > 0) {
		
		request.setAttribute("num", num);
		request.getRequestDispatcher("adGuideCalculate.gd").forward(request, response);
		//response.sendRedirect("/edr/adGuideDetail.gd");
	}else {
		request.setAttribute("msg", "업데이트 실패");
		page = "views/admin/sub/common/errorPage.jsp";
		request.getRequestDispatcher(page).forward(request, response);
	}
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
