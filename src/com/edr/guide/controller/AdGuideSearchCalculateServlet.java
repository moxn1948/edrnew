package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.AdWaitProgramService;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuideSearchCalculateServlet
 */
@WebServlet("/adsearhCalculate.gd")
public class AdGuideSearchCalculateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideSearchCalculateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String category = request.getParameter("category");
		String searchTxt = "";
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		ArrayList<HashMap<String, Object>> list = null;
		PageInfo pi = null;
		
		if(category.equals("gname")) {
			if(request.getParameter("currentPAge") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			
			limit = 5;
			
			int listCount = 0;
			searchTxt = request.getParameter("searchTxt");
			
			
			listCount = new AdGuideService().adSearchNameCalculateCount(searchTxt);
			
			 maxPage = (int) ((double) listCount / limit + 0.8);
			
			 startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
		
			 endPage = startPage + 5 - 1;
			 
			 if(maxPage < endPage) {
				 endPage = maxPage;
			 }
			 
			 pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			 
			 
			 list = null;
			 list = new AdGuideService().adSearchNameCalculate(searchTxt, currentPage, limit);
		}else {
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			
			limit = 5;
			
			int listCount = 0;
			searchTxt = request.getParameter("searchTxt");
			
			
			listCount = new AdGuideService().adSearchIdCalCount(searchTxt);
			
			 maxPage = (int) ((double) listCount / limit + 0.8);
			
			 startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
		
			 endPage = startPage + 5 - 1;
			 
			 if(maxPage < endPage) {
				 endPage = maxPage;
			 }
			 
			 pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			 
			 
			 list = null;
			 list = new AdGuideService().adSearchIdCal(searchTxt, currentPage, limit);
			
			
			
			
		}
		String page = "";
		if(list != null) {
			page = "views/admin/sub/guide/calculateSearch.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("searchTxt", searchTxt);
			request.setAttribute("category", category);
			
			
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "정산검색 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
