package com.edr.guide.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.AdGuideService;
import com.edr.guide.model.vo.GuideHistory;

/**
 * Servlet implementation class AdGuideDetailUpdateInterviewServlet
 */
@WebServlet("/adGuiideDetailUpdateInterview.gd")
public class AdGuideDetailUpdateInterviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideDetailUpdateInterviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		
		String interYn = request.getParameter("interYn");
		
		
		GuideHistory gh = new GuideHistory();
		gh.setInterviewYn(interYn);
		//gh.setGhState(result);

		int r = new AdGuideService().updateGuideHistoryinterYn(gh, num);

		String page = "";

		if(r > 0) {

			request.setAttribute("num", num);
			request.getRequestDispatcher("adGuideDetail.gd").forward(request, response);
			//response.sendRedirect("/edr/adGuideDetail.gd");
		}else {
			request.setAttribute("msg", "업데이트 실패");
			page = "views/admin/sub/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
