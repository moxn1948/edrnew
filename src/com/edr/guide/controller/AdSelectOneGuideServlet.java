package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.guide.model.service.GuideAdSelectService;

/**
 * Servlet implementation class AdSelectOneGuideServlet
 */
@WebServlet("/selectOneGuide.ad")
public class AdSelectOneGuideServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSelectOneGuideServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		//int num2 = Integer.parseInt(request.getParameter("num2"));
				
		ArrayList<HashMap<String, Object>> list = new GuideAdSelectService().adSelectOneGuide(num);

		ArrayList<Attachment> list2 = new GuideAdSelectService().adSelectGuideAttachment(num);

		ArrayList<HashMap<String, Object>> list4 = new GuideAdSelectService().adSelectGuideGP(num);
		
		String root = "";

		root = request.getSession().getServletContext().getRealPath("/");

		String sub = root.substring(0, root.indexOf("git"));
		String file = "";
		String file2 = "";
		String file3 = "";

		ArrayList<HashMap<String, Object>> list3 = new GuideAdSelectService().adSelectGuideAttachmentPro(num);
	
		String page = "";

		if (list != null) {
			page = "views/admin/sub/guide/guideDetail.jsp";
			request.setAttribute("list", list);
			request.setAttribute("list2", list2);
			request.setAttribute("list3", list3);
			request.setAttribute("list4", list4);
			request.setAttribute("num", num);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "가이드 게시판 상세 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
