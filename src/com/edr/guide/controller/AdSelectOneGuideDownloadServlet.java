package com.edr.guide.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.GuideAdSelectService;

/**
 * Servlet implementation class AdSelectOneGuideDownloadServlet
 */
@WebServlet("/selectOneGuideDownload.ad")
public class AdSelectOneGuideDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSelectOneGuideDownloadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		
		System.out.println(num);

		ArrayList<Attachment> list = new GuideAdSelectService().adSelectOneAttachment(num);

		String root = "";

		for (int i = 0; i < 1; i++) {
			root = request.getSession().getServletContext().getRealPath("/");
		}

		int nrr = root.indexOf("edrnew");

		String sub = root.substring(0, root.indexOf("git"));
		String file = "";
		String file2 = "";

		String filePath = "";

		// sub는 내주소 git까지 짜른거
		System.out.println("sub : " + sub);

		for (int i = 0; i < list.size(); i++) {
			file = list.get(i).getChangeName();
			file2 = list.get(i).getOriginName();
			filePath = list.get(i).getFilePath();
		}

		String fileS = filePath.substring(filePath.indexOf("git"));

		String fileSave = sub + fileS;

		String userSub = file.substring(filePath.indexOf("git"));

		File downFile = null;

		for (int i = 0; i < list.size(); i++) {
			downFile = new File(fileSave + list.get(i).getChangeName());
		}

		BufferedInputStream buf = null;

		ServletOutputStream downOut = null;

		downOut = response.getOutputStream();

		response.setContentType("text/plain; charset=UTF-8");

		for (int i = 0; i < list.size(); i++) {
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ new String(list.get(i).getOriginName().getBytes("UTF-8"), "ISO-8859-1") + "\"");
		}
		
		response.setContentLength((int) downFile.length());

		FileInputStream fin = new FileInputStream(downFile);
		
		buf = new BufferedInputStream(fin);

		int readBytes = 0;

		while ((readBytes = buf.read()) != -1) {
			downOut.write(readBytes);
		}
		
		downOut.close();
		
		buf.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
