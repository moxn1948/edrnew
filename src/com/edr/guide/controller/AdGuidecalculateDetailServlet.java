package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuidecalculateDetailServlet
 */
@WebServlet("/adGuideCalculateDetail.gd")
public class AdGuidecalculateDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuidecalculateDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int num = Integer.parseInt(request.getParameter("num"));
		
		ArrayList<HashMap<String, Object>> list = new AdGuideService().adGuideCalSelectOne(num);
		
		
		
		
		
		
		
		
		
		
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/guide/calculateDetailList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("num", num);
			
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "상세보기 실패!");
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
