package com.edr.guide.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.AdGuideService;
import com.google.gson.Gson;

/**
 * Servlet implementation class AdGuideDetailTimeCheckServlet
 */
@WebServlet("/adTimeCheck.gd")
public class AdGuideDetailTimeCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdGuideDetailTimeCheckServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//String time = request.getParameter("time");
		String date = request.getParameter("date");
		//.println("asdasddate : " + date);
		//.println("time : " + time);
		int num = Integer.parseInt(request.getParameter("num"));
		System.out.println("num : " + num);

		java.sql.Date day = null;
		day = java.sql.Date.valueOf(date);
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
		String day2 = sdf.format(day);

		ArrayList<HashMap<String, Object>> list = new AdGuideService().adTimeCountCheck(day2);
		
		
		
		for(int i = 0; i < list.size(); i++) {
			
			if((int)list.get(i).get("count") >= 5) {
				//hmap.put("interviewTime", (int)list.get(i).get("count"));
				
			}else {
				list.remove(i);
			}
			//list2.add(hmap);
			
			
		}
		

		
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");

		new Gson().toJson(list, response.getWriter());

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
