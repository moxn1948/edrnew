package com.edr.guide.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.edr.common.MyFileRenamePolicy;
import com.edr.common.model.vo.Attachment;
import com.edr.guide.model.service.GuideService;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.guide.model.vo.GuideHistory;
import com.edr.guide.model.vo.GuideLocal;
import com.oreilly.servlet.MultipartRequest;

/**
 * Servlet implementation class GuideSignUpServlet
 */
/**
 * 
 *
 */

 
@WebServlet("/guideSignUp.gd")
public class GuideSignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GuideSignUpServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 10.
	 * @Description      : 파일 첨부 과정
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException         :
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 10;
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "uploadFiles/";
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			ArrayList<String> saveFiles = new ArrayList<String>();
			
			ArrayList<String> originFiles = new ArrayList<String>();
			
			multiRequest.getFileNames();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));

			}
			
			int mno = Integer.parseInt(multiRequest.getParameter("mno"));		
			String gName = multiRequest.getParameter("name");
			String nationNo = multiRequest.getParameter("nationNo");
			String phone1 = multiRequest.getParameter("phone1");
			String phone = nationNo + ")" + phone1;
			String nationNo2 = multiRequest.getParameter("nationNo2");
			String ephone1 = multiRequest.getParameter("ephone1");
			String ephone = nationNo2 + ")" + ephone1;
			String kakaoId = multiRequest.getParameter("kakaoId");
			String introduce = multiRequest.getParameter("introduce");
			String postCode = multiRequest.getParameter("postcode");
			String roadAddress = multiRequest.getParameter("roadAddress");
			String detailAddress = multiRequest.getParameter("detailAddress");
			String address = postCode + "/" + roadAddress + "/" + detailAddress;
			String[] local = multiRequest.getParameterValues("nation");	
			int residence = Integer.parseInt(multiRequest.getParameter("residence"));
			String exp = multiRequest.getParameter("exp");
			String[] lang = multiRequest.getParameterValues("language");
			String[] langEtc = multiRequest.getParameterValues("language5");
			String language = "";
			String necessary = multiRequest.getParameter("necessary");
			String add = multiRequest.getParameter("add");
			String dis[] = multiRequest.getParameterValues("disqual");
			String disqual = "";
			String clude = "";
			String clude1 = "";
			String profileYn = "";
			
			String date = multiRequest.getParameter("date");
			String dateTime = multiRequest.getParameter("dateTime");
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = null;
			java.sql.Date date3 = null;
				try {					
					date1 = dt.parse(date);
					
					
					date3 = new java.sql.Date(date1.getTime());	
									
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
							
			
			if(dis != null) {
				for(int i = 0; i < lang.length; i++) {
					if(i == 0) {
						disqual += dis[i];
					}
				}
			}
			
			
			 
			if(lang != null) {
				for(int i = 0; i < lang.length; i++) {
					if(i == 0) {
						language += lang[i];
					}else {
						language += ", " + lang[i];
					}
				}
			}
			
			if(langEtc != null) {
				for(int i = 0; i < langEtc.length; i++) {
					System.out.println("lang : " + langEtc[i]);
					language += ", " + langEtc[i];
				}
			}		

		/*private int mno;			// 회원번호
		private String gname;		// 가이드이름
		private String kakaoId;		// 카카오ID
		private String phone;		// 전화번호
		private String phone2;		// 비상연락망
		private String address;		// 주소
		private String lang;		// 가능언어
		private int period;			// 거주기간
		private String introduce;	// 소개
		private String accountNo;	// 계좌번호
		private String accountName;	// 예금주명
		private String bankName;	// 은행명
		private String expYn;		// 가이드경험유무
		private String tbBan;		// 해외결격사유
*/
			GuideDetail gd = new GuideDetail();
			gd.setMno(mno);
			gd.setGname(gName);
			gd.setKakaoId(kakaoId);
			gd.setPhone(phone);
			gd.setPhone2(ephone);
			gd.setAddress(address);
			gd.setLang(language);
			gd.setPeriod(residence);
			gd.setIntroduce(introduce);
			gd.setExpYn(exp);
			gd.setTbBan(disqual);
			int result = new GuideService().insertGuide(gd);
			/*if(result > 0) {
				 response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertGuideSignUp");
			 }else {
				 response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertGuideSignUp");
			 }*/
			
		 GuideLocal gl = new GuideLocal();
		// private int mno;			// 회원번호
		// private int localNo;		// 지역번호
		 gl.setMno(mno);
		// int ln[] = new int[3];
		 for(int i = 0; i < local.length; i++) {
			// ln[i] = Integer.parseInt(local[i]);
			 gl.setLocalNo(Integer.parseInt(local[i]));
			 int result2 = new GuideService().insertLocal(gl);
			 
			 /*if(result2 > 0) {
				 response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertGuideSignUp");
			 }else {
				 response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertGuideSignUp");
			 }*/
		 }
		 	
		 
		 GuideHistory gh = new GuideHistory();
		 gh.setMno(mno);
		 gh.setInterviewDate(date3);
		 gh.setInterviewTime(dateTime);
		 
		 
		 int result3 = new GuideService().insertHistory(gh);
		/* if(result3 > 0) {
			 response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertGuideSignUp");
		 }else {
			 response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertGuideSignUp");
		 }
		 System.out.println("servlet : " + result3);*/
		 // 시뭔스 번호를 받아오는 dao 를 실행 : 3
		 
		 
		 
		 ArrayList<Attachment> fileList = new ArrayList<Attachment>();
		 
		 
		 for(int i = originFiles.size() -1; i >= 0; i--) {
			 Attachment at = new Attachment();
			 if(originFiles.size() == 2) {
			 if(i == 0) {
				 at.setProfileYn("Y");
			 }else {
				 at.setProfileYn("N");
			 }
			 }else if(originFiles.size() >= 3) {
				 if(i == 1) {
					 at.setProfileYn("Y");
				 }else {
					 at.setProfileYn("N");
				 }
			 }
			 at.setFilePath(savePath);
			 at.setMno(mno);
			 at.setOriginName(originFiles.get(i));
			 at.setChangeName(saveFiles.get(i));
			 at.setGhNo(result3);
			
			 fileList.add(at);
			 
		 }
		 System.out.println("fileList : " + fileList);
		 int result4 = new GuideService().insertAttachment(fileList, result3);
		 
		 if(result >0  && result3 > 0 && result4 > 0) {
			 response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertGuideSignUp");
		 }else {
			 response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertGuideSignUp");
		 }
		 
		}
}

	



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
