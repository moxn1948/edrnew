package com.edr.guide.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.AdGuideService;
import com.edr.guide.model.vo.GuideHistory;

/**
 * Servlet implementation class AdGuideDetailUpdateServlet
 */
@WebServlet("/adGuideHistoryDetailUp.gd")
public class AdGuideDetailUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdGuideDetailUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		int num = Integer.parseInt(request.getParameter("num"));
		String date = request.getParameter("payDate");
		String time = request.getParameter("timeP");
		System.out.println("num : " + num);
		/*.println("date : " + date);
		.println("time : " + time);*/
		/*String yn = request.getParameter("interYn");
		String result = "";
		int r = 0;
		if(request.getParameter("interResult") != null) {
			if(request.getParameter("interResult").equals("승인")) {
				result = "OK";
			}else {
				result = "FAIL";
			}
		}*/
		
		
		/*if((date == null || time == null) && yn == null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			//gh.setGhDate(day);
			//gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
			
		}else if() {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}*/
		
		
		/*if(date == null && time != null && yn != null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time == null && yn != null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time != null && yn == null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time != null && yn != null && result == null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date == null && time == null && yn != null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			//gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date == null && time != null && yn == null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			//gh.setInterviewDate(day);
			gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date == null && time != null && yn != null && result == null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			//gh.setInterviewDate(day);
			gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time == null && yn == null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time == null && yn != null && result == null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			gh.setInterviewYn(yn);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time != null && yn == null && result == null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date == null && time == null && yn == null && result != null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			//gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}else if(date != null && time == null && yn == null && result == null) {
			java.sql.Date day = null;
			day = java.sql.Date.valueOf(date);


			GuideHistory gh = new GuideHistory();
			gh.setInterviewDate(day);
			//gh.setInterviewTime(time);
			//gh.setInterviewYn(yn);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistory(gh, num);
		}*/
		
		//전체 조회
		
		java.sql.Date day = null;
		day = java.sql.Date.valueOf(date);


		GuideHistory gh = new GuideHistory();
		gh.setInterviewDate(day);
		gh.setInterviewTime(time);
		//gh.setInterviewYn(yn);
		//gh.setGhState(result);







		int r = new AdGuideService().updateGuideHistory(gh, num);

		String page = "";
		
		

		if(r > 0) {

			request.setAttribute("num", num);
			request.getRequestDispatcher("adGuideDetail.gd").forward(request, response);
			//response.sendRedirect("/edr/adGuideDetail.gd");
		}else {
			request.setAttribute("msg", "업데이트 실패");
			page = "views/admin/sub/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
		}




	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
