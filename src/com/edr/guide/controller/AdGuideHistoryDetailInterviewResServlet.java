package com.edr.guide.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.AdGuideService;
import com.edr.guide.model.vo.GuideHistory;

/**
 * Servlet implementation class AdGuideHistoryDetailInterviewResServlet
 */
@WebServlet("/adGuideHistoryDetailInterviewRe.gd")
public class AdGuideHistoryDetailInterviewResServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdGuideHistoryDetailInterviewResServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//num2 = mno
		int num = Integer.parseInt(request.getParameter("num"));
		int num2 = Integer.parseInt(request.getParameter("num2"));

		System.out.println(" num 1  : " + num );
		System.out.println(" num 2  : " + num2 );
		
		String interResult = request.getParameter("interResult");
		String str = "";
		int r = 0;
		int r2 = 0;
		if(interResult.equals("승인")) {
			str = "OK";
			GuideHistory gh = new GuideHistory();
			gh.setGhState(str);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistoryinterResultOk(gh, num);
			

			if(r > 0) {
				r2 = new AdGuideService().updateGuideHistoryMtypeUpdate(num2);
			}else {
				r2 = 0;
			}

		}else {
			str = "FAIL";

			GuideHistory gh = new GuideHistory();
			gh.setGhState(str);
			//gh.setGhState(result);

			r = new AdGuideService().updateGuideHistoryinterResult(gh, num);
			if(r > 0) {
				r2 = 1;
			}else {
				r2 = 0;
			}
		}

		String page = "";

		if(r2 > 0) {

			request.setAttribute("num", num);
			request.getRequestDispatcher("adGuideHistory.gd").forward(request, response);
			//response.sendRedirect("/edr/adGuideDetail.gd");
		}else {
			request.setAttribute("msg", "업데이트 실패");
			page = "views/admin/sub/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
