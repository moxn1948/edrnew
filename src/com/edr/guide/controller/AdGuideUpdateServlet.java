package com.edr.guide.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.GuideAdSelectService;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.service.MemberAdSelectService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdGuideUpdateServlet
 */
@WebServlet("/updateGuide.ad")
public class AdGuideUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		System.out.println(mno);
		
		String gname = request.getParameter("gname");
		System.out.println(gname);
		
		GuideDetail gd = new GuideDetail();
		gd.setGname(gname);
		gd.setMno(mno);
		System.out.println("gd : "+gd);
		
		int result = new GuideAdSelectService().adUpdateGuide(gd);

		String page = "";

		if (result > 0) {
			response.sendRedirect("/edr/selectGuide.ad");
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "가이드 정보 수정 실패!!");

			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
