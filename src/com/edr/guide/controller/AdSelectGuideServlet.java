package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Local;
import com.edr.common.model.vo.PageInfo;
import com.edr.guide.model.service.GuideAdSelectService;
import com.edr.guide.model.vo.GuideDetail;

/**
 * Servlet implementation class AdSelectGuideServlet
 */
@WebServlet("/selectGuide.ad")
public class AdSelectGuideServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSelectGuideServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ArrayList<GuideDetail> list = new GuideAdSelectService().selectGuideList();

		// 페이저 처리 후
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 5;

		// 전체 목록 갯수 조회
		int listCount = new GuideAdSelectService().getListCount();

		maxPage = (int) ((double) listCount / limit + 0.8);

		startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

		// 목록 아래 쪽에 보여질 마지막 페이지 수
		endPage = startPage + 5 - 1;

		if (maxPage < endPage) {
			endPage = maxPage;
		}
 
		// 페이지 정보를 담을 vo 객체 생성
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

		ArrayList<HashMap<String, Object>> list = new GuideAdSelectService().selectListWithPaging(currentPage, limit);
		
		String page = "";

		if (list != null) {
			page = "views/admin/sub/guide/guideList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi); 
			
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "가이드 리스트 조회 실패!");
		}

		request.getRequestDispatcher(page).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
