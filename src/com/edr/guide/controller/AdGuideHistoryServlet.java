package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.guide.model.service.AdGuideService;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class AdGuideHistoryServlet
 */
@WebServlet("/adGuideHistory.gd")
public class AdGuideHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdGuideHistoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ArrayList<HashMap<String, Object>> list = new GuideService().adGuideHistory2();
		
		
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		
		currentPage = 1;
		
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
			//한 페이지에 보여질 목록 갯수
			limit = 5;
			
			//전체 목록 갯수 조회
			int listCount = new AdGuideService().adGuideHistoryCount();
			
			//총 페이지수 계싼
			maxPage = (int) ((double) listCount / limit + 0.8);
			
			//현재 페이지에 보여 줄 시작 페이지수
			startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
			
			//목록 아래 쪽에 보여질 마지막 페이지 수
			endPage = startPage + 5 - 1;
			
			if(maxPage < endPage) {
				endPage = maxPage;
			}
			
			
			PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
			ArrayList<HashMap<String, Object>> list = new AdGuideService().adGuideHistory(currentPage, limit);
			
			if(list == null) {
				
			}
					
			
			String page = "";
			if(list != null) {
				page = "views/admin/sub/guide_history/addGuideList.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
			}else {
				page = "views/admin/sub/common/errorPage.jsp";
				request.setAttribute("msg", "게시판 조회 실패!");
			}
			request.getRequestDispatcher(page).forward(request, response);
					
		}
	
		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
