package com.edr.guide.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.GuideServiceSh;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class UpdateGuideBankServlet
 */
@WebServlet("/updateGuideBank.gd")
public class UpdateGuideBankServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateGuideBankServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String accountName = request.getParameter("userName");
		String bankName = request.getParameter("bankName");
		String accountNumber = request.getParameter("accountNumber");
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		GuideDetail requestGuide = new GuideDetail();
		requestGuide.setMno(mno);
		requestGuide.setBankName(bankName);
		requestGuide.setAccountName(accountName);
		requestGuide.setAccountNo(accountNumber);
		
		int result = new GuideServiceSh().updateGuideBank(requestGuide);
		
		Member reLoginMember = new MemberService().reLogin(mno);
		
		if(result > 0) {
			request.getSession().invalidate();
			response.sendRedirect(request.getContextPath() + "/reGuideLogin?userEmail="+reLoginMember.getEmail());
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
