package com.edr.guide.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.common.model.vo.Local;
import com.edr.guide.model.service.GuideServicehj;
import com.edr.guide.model.vo.GuideDetail;
import com.sun.org.apache.regexp.internal.REUtil;

/**
 * Servlet implementation class GuideProfileSelectServlet
 */
@WebServlet("/gdProfile.gd")
public class GuideProfileSelectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
         
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuideProfileSelectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		
		//guideDetail객체 가져옴
		int mno = Integer.parseInt(request.getParameter("mno"));

		//해쉬맵 guideInfo
		HashMap<String,Object> guideInfo = new GuideServicehj().guideSelectOne(mno);
		
		
		
		
		String [] langArr= (String[]) guideInfo.get("langArr");
		ArrayList<Local> localList = (ArrayList) guideInfo.get("lcList");
		Attachment at = (Attachment) guideInfo.get("at");
		GuideDetail guideDetail = (GuideDetail) guideInfo.get("guideDetail");
		   
		
		if(guideInfo !=null) {
			
			request.setAttribute("langArr", langArr);
			request.setAttribute("localList", localList);
			request.setAttribute("at", at);
			
			
			request.getSession().setAttribute("guideDetail", guideDetail);
			request.getRequestDispatcher("views/user/sub/guide_page/guideProfile.jsp").forward(request, response);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
