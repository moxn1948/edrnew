package com.edr.guide.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.GuideServiceSh;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class GuideReLoginServlet
 */
@WebServlet("/reGuideLogin")
public class GuideReLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuideReLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userEmail = request.getParameter("userEmail");
		
		Member requestMember = new Member();
		requestMember.setEmail(userEmail);
		
		Member loginUser = new MemberService().reLoginMemberCheck(requestMember);
		
		GuideDetail guideDetailInfo = new GuideServiceSh().guideDetailInfo(loginUser);
		
		if(loginUser != null && guideDetailInfo != null) {
			request.getSession().setAttribute("loginUser", loginUser);
			request.getSession().setAttribute("guideDetailInfo", guideDetailInfo);
			response.sendRedirect("views/user/sub/guide_page/registerAccountPopup.jsp");
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
