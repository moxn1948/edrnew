package com.edr.guide.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.GuideService;

import com.edr.guide.model.vo.GuideHistory;

/**
 * Servlet implementation class UpdateGuideInterview
 */
@WebServlet("/updateInterview.gd")
public class UpdateGuideInterview extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateGuideInterview() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		String date = request.getParameter("date");
		String dateTime = request.getParameter("dateTime");
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = null;
		
		java.sql.Date date3 = null;
			try {
				date1 = dt.parse(date);
				System.out.println("servlet : " + date);
				date3 = new java.sql.Date(date1.getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		GuideHistory gh = new GuideHistory();
		gh.setMno(mno);
		gh.setInterviewDate(date3);
		gh.setInterviewTime(dateTime);
		
		System.out.println("servlet : " + mno);
		System.out.println(date3);
		System.out.println(dateTime);
		
		int result = new GuideService().updateHistory(gh);
		System.out.println("servelt2 : " + result );
		if(result > 0) {
			 response.sendRedirect("views/user/sub/common/successPage.jsp?page=updateHistory");
		 }else {
			 response.sendRedirect("views/user/sub/common/errorPage.jsp?page=updateHistory");
		 }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
