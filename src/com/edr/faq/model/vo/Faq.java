package com.edr.faq.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Faq implements java.io.Serializable{
	private int faqNo;			// FAQ번호
	private String faqTitle;	// FAQ제목
	private String faqCnt;		// FAQ내용
}
