package com.edr.message.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message implements java.io.Serializable{
	private int msgNo;			// 메세지번호
	private String msgName;		// 메세지제목
	private String msgCnt;		// 메시제내용
	private int sender;			// 보내는사람
	private int receiver;		// 받는사람
}
