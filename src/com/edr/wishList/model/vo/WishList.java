package com.edr.wishList.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WishList implements java.io.Serializable{
	private int pno;	// 상품번호
	private int mno;	// 회원번호
}
