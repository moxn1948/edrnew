package com.edr.qna.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Qna implements java.io.Serializable{
	private int qno;		// 문의번호
	private String qtitle;	// 문의제목
	private String qcnt;	// 문의내용
	private Date qnaDate;	// 문의일자
	private String replyYn;	// 답변여부
	private int mno;		// 회원번호
}
