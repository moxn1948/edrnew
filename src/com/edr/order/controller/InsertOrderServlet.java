package com.edr.order.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.order.model.service.OrderService;
import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;

/**
 * Servlet implementation class InsertOrderServlet
 */
@WebServlet("/insertOrder.od")
public class InsertOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int programPrice = Integer.parseInt(request.getParameter("programPrice"));			// 프로그램 가격
		int programPno = Integer.parseInt(request.getParameter("programPno"));				// PRODUCT 번호
		int userMno = Integer.parseInt(request.getParameter("userMno"));					// 결제진행하는 USER NO
		String programName = request.getParameter("programName");							// 프로그램 명
		String orderName = request.getParameter("orderName");								// 주문자 명
		String orderPhoneNumber = request.getParameter("orderPhoneNumber");					// 주문자 핸드폰 번호
		String orderEmail = request.getParameter("orderEmail");								// 주문자 이메일
		String receiverName = request.getParameter("receiverName");							// 수령인 이름
		String userGender = request.getParameter("userGender");								// 수령인 성별
		String receiverAge = request.getParameter("receiverAge");							// 수령인 연령대
		String receiverPhoneNumber = request.getParameter("receiverPhoneNumber");			// 수령인 연락처
		String receiverSubPhoneNumber = request.getParameter("receiverSubPhoneNumber");		// 수령인 비상연락망
		String receiverEmail = request.getParameter("receiverEmail");						// 수령인 이메일
		String requestContent = request.getParameter("requestContent");						// 수령인 요청사항
		Long orderCode = Long.parseLong(request.getParameter("orderCode"));								// 생성된 주문 번혼
		
		int recPerson = Integer.parseInt(request.getParameter("recPerson"));
		String payDate = request.getParameter("payDate");
		String payType = request.getParameter("payType");
		
		
		

		OrderList orderList = new OrderList();
		orderList.setOrderCode(orderCode);
		orderList.setOrderEmail(orderEmail);
		orderList.setOrderName(orderName);
		orderList.setOrderPhone(orderPhoneNumber);
		orderList.setPno(programPno);
		orderList.setMno(userMno);

		int result = new OrderService().insertOrderList(orderList);
		System.out.println("result : " + result);

		int orderNo = new OrderService().selectOrderNo(orderList);

		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setOrderNo(orderNo);
		orderDetail.setPno(programPno);
		orderDetail.setReceAge(receiverAge);
		orderDetail.setReceEmail(receiverEmail);
		orderDetail.setReceGender(userGender);
		orderDetail.setReceName(receiverName);
		orderDetail.setRecePhone(receiverPhoneNumber);
		orderDetail.setRecePhone2(receiverSubPhoneNumber);
		orderDetail.setReceRequest(requestContent);
		orderDetail.setRecPerson(recPerson);

		int result2 = new OrderService().insertOrderDetail(orderDetail);
		System.out.println("result2 : " + result2);


		Payment payment = new Payment();
		payment.setOrderNo(orderNo);
		payment.setPno(programPno);
		payment.setPayCost(programPrice);
		payment.setPayKind(payType);

		int result3 = new OrderService().insertPaymnet(payment);
		System.out.println("result3 : " + result3);

		if(result > 0 && result2 > 0 && result3 > 0) {
			
			request.setAttribute("orderCode", orderCode);
			request.getRequestDispatcher("views/user/sub/common/orderComplete.jsp").forward(request, response);;

		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
