package com.edr.order.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.order.model.service.AdOrderListService;

/**
 * Servlet implementation class AdOrderResetServlet
 */
@WebServlet("/adOrderReset.od")
public class AdOrderResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdOrderResetServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long num = Long.parseLong(request.getParameter("num"));
		int ono = Integer.parseInt(request.getParameter("ono"));
		System.out.println("num : " + num);
		System.out.println("ono : " + ono);
		
		int result = new AdOrderListService().adOrderReset(ono);
		
		String page = "";
		if(result > 0) {
			request.setAttribute("num", num);
			request.getRequestDispatcher("adOrderList.od").forward(request, response);
		}else {
			request.setAttribute("msg", "업데이트 실패!");
			page = "views/admin/sub/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
