package com.edr.order.controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.order.model.service.OrderService;
import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

/**
 * Servlet implementation class InsertCustomOrderServlet
 */
@WebServlet("/insertCustomOrder.od")
public class InsertCustomOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertCustomOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int programPrice = Integer.parseInt(request.getParameter("programPrice"));			// 프로그램 가격
		int userMno = Integer.parseInt(request.getParameter("userMno"));					// 결제진행하는 USER NO
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));							// cpno
		int cpEstNo = Integer.parseInt(request.getParameter("cpEstNo"));					// cpestno
		
		
		String pDate = request.getParameter("pDate");										// 프로그램 시작날짜
		String programName = request.getParameter("programName");							// 프로그램 명
		String orderName = request.getParameter("orderName");								// 주문자 명
		String orderPhoneNumber = request.getParameter("orderPhoneNumber");					// 주문자 핸드폰 번호
		String orderEmail = request.getParameter("orderEmail");								// 주문자 이메일
		String receiverName = request.getParameter("receiverName");							// 수령인 이름
		String userGender = request.getParameter("userGender");								// 수령인 성별
		String receiverAge = request.getParameter("receiverAge");							// 수령인 연령대
		String receiverPhoneNumber = request.getParameter("receiverPhoneNumber");			// 수령인 연락처
		String receiverSubPhoneNumber = request.getParameter("receiverSubPhoneNumber");		// 수령인 비상연락망
		String receiverEmail = request.getParameter("receiverEmail");						// 수령인 이메일
		String requestContent = request.getParameter("requestContent");						// 수령인 요청사항
		Long orderCode = Long.parseLong(request.getParameter("orderCode"));					// 생성된 주문 번혼
		
		int recPerson = Integer.parseInt(request.getParameter("recPerson"));
		String payType = request.getParameter("payType");
		
		java.sql.Date pdate = Date.valueOf(pDate);
		
		Product product = new Product();
		product.setCpNo(cpNo);
		product.setPdate(pdate);
		
		
		int result = new OrderService().insertCustomProgramProduct(product);
		
		int pno = new OrderService().selectPno(product);
		
		OrderList orderList = new OrderList();
		orderList.setMno(userMno);
		orderList.setPno(pno);
		orderList.setOrderCode(orderCode);
		orderList.setOrderName(orderName);
		orderList.setOrderPhone(orderPhoneNumber);
		orderList.setOrderEmail(orderEmail);
		
		int result2 = new OrderService().insertCustomProgramOrderList(orderList);
		
		int orderNo = new OrderService().selectCustomOrderNo(orderList);
		
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setOrderNo(orderNo);
		orderDetail.setPno(pno);
		orderDetail.setReceName(receiverName);
		orderDetail.setReceGender(userGender);
		orderDetail.setRecePhone(receiverPhoneNumber);
		orderDetail.setRecePhone2(receiverSubPhoneNumber);
		orderDetail.setReceEmail(receiverEmail);
		orderDetail.setReceAge(receiverAge);
		orderDetail.setReceRequest(requestContent);
		orderDetail.setRecPerson(recPerson);
		
		int result3 = new OrderService().insertCustomProgramOrderDetail(orderDetail);
		
		Payment payment = new Payment();
		payment.setOrderNo(orderNo);
		payment.setPno(pno);
		payment.setPayCost(programPrice);
		payment.setPayKind(payType);
		
		int result4 = new OrderService().insertCustomProgramPayment(payment);
		
		int result5 = new OrderService().updateCustomState(cpNo);
		
		int result6 = new OrderService().updateCustomEstStatus(cpEstNo);
		
		if( result > 0 && result2 > 0 && result3 > 0 && result4 > 0 && result5 > 0 && result6 > 0) {
			
			request.setAttribute("orderCode", orderCode);
			request.getRequestDispatcher("views/user/sub/common/orderCustomComplete.jsp").forward(request, response);
			
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
