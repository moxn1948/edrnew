package com.edr.order.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.order.model.service.AdOrderListService;

/**
 * Servlet implementation class AdOrderDetailServlet
 */
@WebServlet("/adOrderDetail.od")
public class AdOrderDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdOrderDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Long num = Long.parseLong(request.getParameter("num"));
		String str = request.getParameter("str");
		int ono = Integer.parseInt(request.getParameter("ono"));
		System.out.println("들어가기 전 str : " + str);
		String str2 = "";
		if(str.equals("결제완료")) {
			str2 = "ENDPAY";
		}else if(str.equals("환불취소")) {
			str2 = "CANCLEREFUND";
		}else if(str.equals("환불완료")) {
			str2 = "ENDREFUND";
		}else if(str.equals("결제취소")){
			str2 = "CANCLEPAY";
		}else {
			str2 = "REFUNDIN";
		}
		
	
		System.out.println("num : " + num);
		System.out.println("str : " + str2);
		System.out.println("ono : " + ono);
		
		ArrayList<HashMap<String, Object>>list = new AdOrderListService().adOrderListDetail(num,str2);
		System.out.println("orderList : " + list);
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/order/orderDetail.jsp";
			request.setAttribute("list", list);
			request.setAttribute("num", num);
			request.setAttribute("ono", ono);
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "orderListDetail 조회실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}
	
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
