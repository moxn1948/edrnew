package com.edr.order.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.order.model.service.AdOrderListService;

/**
 * Servlet implementation class AdOrderSearchServlet
 */
@WebServlet("/adSearchOrder.od")
public class AdOrderSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdOrderSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String category = request.getParameter("category");
		
		System.out.println("category : " + category);
		
		String searchArea = request.getParameter("searchArea");
		System.out.println("searchArea : " + searchArea);
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		ArrayList<HashMap<String, Object>> list = null;
		PageInfo pi = null;
		
		if(category.equals("ORDER_NAME")) {
			

			currentPage = 1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			//한 페이지에 보여질 목록 갯수
			limit = 5;

			System.out.println("ml???");
			//전체 목록 갯수 조회
			int listCount = new AdOrderListService().adOrderSearchNameListCount(searchArea);

			//총 페이지수 계싼
			maxPage = (int) ((double) listCount / limit + 0.8);

			//현재 페이지에 보여 줄 시작 페이지수
			startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			//목록 아래 쪽에 보여질 마지막 페이지 수
			endPage = startPage + 5 - 1;

			if(maxPage < endPage) {
				endPage = maxPage;
			}


			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
			list = new AdOrderListService().adOrderSearchName(searchArea,currentPage, limit);
			
			
		}else {
			int searchTxt = Integer.parseInt(searchArea);
			currentPage = 1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			//한 페이지에 보여질 목록 갯수
			limit = 5;

			System.out.println("ml???");
			//전체 목록 갯수 조회
			int listCount = new AdOrderListService().adOrderSearchNoListCount(searchTxt);

			//총 페이지수 계싼
			maxPage = (int) ((double) listCount / limit + 0.8);

			//현재 페이지에 보여 줄 시작 페이지수
			startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			//목록 아래 쪽에 보여질 마지막 페이지 수
			endPage = startPage + 5 - 1;

			if(maxPage < endPage) {
				endPage = maxPage;
			}


			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
			list = new AdOrderListService().adOrderSearchNo(searchTxt,currentPage, limit);
		}
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/order/orderSearchList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("category", category);
			request.setAttribute("searchArea", searchArea);
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "주문내역 서치 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
