package com.edr.order.model.service;

import com.edr.order.model.dao.OrderDao;
import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;

public class OrderService {

	public int insertOrderList(OrderList orderList) {
		
		Connection con = getConnection();
		
		int result = new OrderDao().insertOrderList(con, orderList);
		
		if( result > 0 ) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int selectOrderNo(OrderList orderList) {

		Connection con = getConnection();
		
		int orderNo = new OrderDao().selectOrderNo(con, orderList);
		
		close(con);
		
		return orderNo;
	}

	public int insertOrderDetail(OrderDetail orderDetail) {

		Connection con = getConnection();
		
		int result2 = new OrderDao().insertOrderDetail(con, orderDetail);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result2;
	}

	public int insertPaymnet(Payment payment) {
		
		Connection con = getConnection();
		
		int result3 = new OrderDao().insertPayment(con, payment);
		
		if(result3 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		return result3;
	}

	public int insertCustomProgramProduct(Product product) {

		Connection con = getConnection();
		
		int result = new OrderDao().insertProduct(con, product);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int selectPno(Product product) {
		
		Connection con = getConnection();
		
		int pno = new OrderDao().selectPno(con, product);
		
		close(con);
		
		return pno;
	}

	public int insertCustomProgramOrderList(OrderList orderList) {
		
		Connection con = getConnection();

		int result2 = new OrderDao().insertCustomOrderList(con, orderList);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result2;
	}

	public int selectCustomOrderNo(OrderList orderList) {
		
		Connection con = getConnection();

		int orderNo = new OrderDao().selectCustomOrderNo(con, orderList);
		
		close(con);
		
		return orderNo;
	}

	public int insertCustomProgramOrderDetail(OrderDetail orderDetail) {

		Connection con = getConnection();
		
		int result3 = new OrderDao().insertCustomProgramOrderDetail(con, orderDetail);
		
		if(result3 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result3;
	}

	public int insertCustomProgramPayment(Payment payment) {
		
		Connection con = getConnection();

		int result4 = new OrderDao().insertCustomProgramPayment(con, payment);
		
		if(result4 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result4;
	}

	public int updateCustomState(int cpNo) {
		
		Connection con = getConnection();
		
		int result5 = new OrderDao().updateCustomState(con, cpNo);
		
		if(result5 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result5;
	}

	public int updateCustomEstStatus(int cpEstNo) {
		
		Connection con = getConnection();

		int result6 = new OrderDao().updateCustomEstStatus(con, cpEstNo);
		
		if(result6 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result6;
	}

}
