package com.edr.order.model.service;
import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.order.model.dao.AdOrderListDao;

public class AdOrderListService {

	public int adOrderListCount() {
		Connection con = getConnection();

		int listCount = new AdOrderListDao().adOrderListCount(con);

		return listCount;
	}


	public ArrayList<HashMap<String, Object>> adOrderList(int currentPage, int limit) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdOrderListDao().adOrderListdao(con, currentPage, limit);
		close(con);

		return list;
	}


	public ArrayList<HashMap<String, Object>> adOrderListGp(int currentPage, int limit) {

		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list2 = new AdOrderListDao().adOrderListdaoGp(con, currentPage, limit);
		close(con);

		return list2;
	}


	public ArrayList<HashMap<String, Object>> adOrderListCp(int currentPage, int limit) {

		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = new AdOrderListDao().adOrderListdaoCp(con, currentPage, limit);
		close(con);

		return list;

	}


	public ArrayList<HashMap<String, Object>> adOrderSearchName(String searchArea, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdOrderListDao().adSearchOrderName(con, searchArea, currentPage, limit);
		close(con);

		return list;
	}


	public ArrayList<HashMap<String, Object>> adOrderSearchNo(int searchTxt, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdOrderListDao().adSearchOrderNo(con, searchTxt, currentPage, limit);
		close(con);

		return list;
	}


	public int adOrderSearchNameListCount(String searchArea) {
		Connection con = getConnection();

		int listCount = new AdOrderListDao().adOrderSearchListCount(con, searchArea);

		return listCount;
	}


	public int adOrderSearchNoListCount(int searchTxt) {
		Connection con = getConnection();

		int listCount = new AdOrderListDao().adOrderSearchIdListCount(con,searchTxt);

		return listCount;
	}


	public int adOrderFilterCount(String filter) {
		Connection con = getConnection();
		
		int listCount = new AdOrderListDao().adOrderListFilterCount(con, filter);
		
		
		
		
		return listCount;
	}


	public ArrayList<HashMap<String, Object>> adOrderListFileter(int currentPage, int limit, String filter) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>>list = new AdOrderListDao().adOrderListFilter(con, currentPage, limit,filter);
		
		close(con);
		
		return list;
	}


	public ArrayList<HashMap<String, Object>> adOrderListDetail(Long num, String str2) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdOrderListDao().adOrderListDetail(con, num, str2);
		
		close(con);
		
		return list;
	}


	public int adOrderReset(int ono) {
		Connection con = getConnection();
		int result = new AdOrderListDao().adOrderReset(con, ono);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result;
	}

}
