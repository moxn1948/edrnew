package com.edr.order.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
public class AdOrderListDao {
	private Properties prop = new Properties();

	public AdOrderListDao() {
		String fileName = AdOrderListDao.class.getResource("/sql/order/order-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int adOrderListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adOrderListCount");
		try {

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				listCount = rset.getInt(1);
			}




		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adOrderListdao(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adOrderList");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("ono", rset.getInt("ONO"));
				hmap.put("payKind", rset.getString("PK"));
				hmap.put("orderCode", rset.getLong("OCODE"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("gpNo", rset.getInt("GNO"));
				hmap.put("cpNo", rset.getInt("CNO"));
				hmap.put("gpName", rset.getString("GNAME"));
				hmap.put("cpTitle", rset.getString("CTITLE"));
				hmap.put("orderName", rset.getString("ONAME"));
				hmap.put("gpCost", rset.getInt("GCOST"));
				hmap.put("totalCost", rset.getInt("TCOST"));
				hmap.put("payChange", rset.getDate("PCHANGE"));
				hmap.put("payk", rset.getString("PAYK"));
				hmap.put("pcost", rset.getInt("PCOST"));
				hmap.put("cpDate", rset.getDate("CPDATE"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				hmap.put("epi", rset.getInt("EPI"));


				list.add(hmap);

			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public ArrayList<HashMap<String, Object>> adOrderListdaoGp(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adOrderListGp");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("orderCode", rset.getLong("ORDER_CODE"));
				hmap.put("pname", rset.getString("GP_NAME"));
				hmap.put("ono", rset.getInt("ORDER_NO"));
				hmap.put("orderName", rset.getString("ORDER_NAME"));
				hmap.put("payCost", rset.getInt("PAY_COST"));
				hmap.put("payChange", rset.getDate("PAY_CHANGE"));
				hmap.put("pNumber", rset.getInt("GP_NO"));
				hmap.put("payKind", rset.getString("PK"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				list.add(hmap);

			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;

	}

	public ArrayList<HashMap<String, Object>> adOrderListdaoCp(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adOrderListCp");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("orderCode", rset.getLong("ORDER_CODE"));
				hmap.put("pNuber", rset.getInt("CP_NO"));
				hmap.put("ono", rset.getInt("ONO"));
				hmap.put("pname", rset.getString("CP_TITLE"));
				hmap.put("orderName", rset.getString("ORDER_NAME"));
				hmap.put("payCost", rset.getInt("TOTAL_COST"));
				hmap.put("payChange", rset.getDate("PAY_CHANGE"));
				hmap.put("payKind", rset.getString("PK"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				list.add(hmap);

			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}

	public ArrayList<HashMap<String, Object>> adSearchOrderName(Connection con, String searchArea, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchOrderName");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchArea);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			list = new ArrayList<HashMap<String, Object>>();


			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("payKind", rset.getString("PK"));
				hmap.put("orderCode", rset.getLong("OCODE"));
				hmap.put("ono", rset.getInt("ONO"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("gpNo", rset.getInt("GNO"));
				hmap.put("cpNo", rset.getInt("CNO"));
				hmap.put("gpName", rset.getString("GNAME"));
				hmap.put("cpTitle", rset.getString("CTITLE"));
				hmap.put("orderName", rset.getString("ONAME"));
				hmap.put("gpCost", rset.getInt("GCOST"));
				hmap.put("totalCost", rset.getInt("TCOST"));
				hmap.put("payChange", rset.getDate("PCHANGE"));
				hmap.put("payk", rset.getString("PAYK"));
				hmap.put("pcost", rset.getInt("PCOST"));
				hmap.put("cpDate", rset.getDate("CPDATE"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				hmap.put("epi", rset.getInt("EPI"));


				list.add(hmap);
			}




		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		System.out.println("daolist : " + list);
		return list;
	}

	public ArrayList<HashMap<String, Object>> adSearchOrderNo(Connection con, int searchTxt, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchOrderNo");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, searchTxt);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			list = new ArrayList<HashMap<String, Object>>();


			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("payKind", rset.getString("PK"));
				hmap.put("orderCode", rset.getLong("OCODE"));
				hmap.put("ono", rset.getInt("ONO"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("gpNo", rset.getInt("GNO"));
				hmap.put("cpNo", rset.getInt("CNO"));
				hmap.put("gpName", rset.getString("GNAME"));
				hmap.put("cpTitle", rset.getString("CTITLE"));
				hmap.put("orderName", rset.getString("ONAME"));
				hmap.put("gpCost", rset.getInt("GCOST"));
				hmap.put("totalCost", rset.getInt("TCOST"));
				hmap.put("payChange", rset.getDate("PCHANGE"));
				hmap.put("payk", rset.getString("PAYK"));
				hmap.put("pcost", rset.getInt("PCOST"));
				hmap.put("cpDate", rset.getDate("CPDATE"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				hmap.put("epi", rset.getInt("EPI"));


				list.add(hmap);
			}




		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}



		return list;
	}

	public int adOrderSearchListCount(Connection con, String searchArea) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adOrderSearchNameListCount");
		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchArea);
			rset = pstmt.executeQuery();

			if(rset.next()) {
				listCount = rset.getInt(1);
			}




		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return listCount;
	}

	public int adOrderSearchIdListCount(Connection con, int searchTxt) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adOrderSearchNoListCount");
		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, searchTxt);
			rset = pstmt.executeQuery();

			if(rset.next()) {
				listCount = rset.getInt(1);
			}




		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return listCount;
	}

	public int adOrderListFilterCount(Connection con, String filter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adOrderListFilterCount");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, filter);
			rset = pstmt.executeQuery();
			if(rset.next()) {
				listCount = rset.getInt(1);
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adOrderListFilter(Connection con, int currentPage, int limit,
			String filter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adOrderListFilter");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, filter);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("payKind", rset.getString("PK"));
				hmap.put("orderCode", rset.getLong("OCODE"));
				hmap.put("ono", rset.getInt("ONO"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("gpNo", rset.getInt("GNO"));
				hmap.put("cpNo", rset.getInt("CNO"));
				hmap.put("gpName", rset.getString("GNAME"));
				hmap.put("cpTitle", rset.getString("CTITLE"));
				hmap.put("orderName", rset.getString("ONAME"));
				hmap.put("gpCost", rset.getInt("GCOST"));
				hmap.put("totalCost", rset.getInt("TCOST"));
				hmap.put("payChange", rset.getDate("PCHANGE"));
				hmap.put("payk", rset.getString("PAYK"));
				hmap.put("pcost", rset.getInt("PCOST"));
				hmap.put("cpDate", rset.getDate("CPDATE"));
				if(rset.getString("PSTATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PSTATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PSTATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PSTATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				hmap.put("epi", rset.getInt("EPI"));


				list.add(hmap);

			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public ArrayList<HashMap<String, Object>> adOrderListDetail(Connection con, Long num, String str2) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adOrderListDetail");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setLong(1, num);
			pstmt.setString(2, str2);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>> ();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("orderNo", rset.getInt("ORDER_NO"));
				hmap.put("gpName", rset.getString("GP_NAME"));
				hmap.put("cpTitle", rset.getString("CP_TITLE"));
				hmap.put("orderName", rset.getString("ORDER_NAME"));
				hmap.put("orderPhone", rset.getString("ORDER_PHONE"));
				hmap.put("orderEmail", rset.getString("ORDER_EMAIL"));
				hmap.put("payChange", rset.getDate("PAY_CHANGE"));
				hmap.put("payKind", rset.getString("PAY_KIND"));
				hmap.put("payCost", rset.getInt("PAY_COST"));
				/*hmap.put("payState", rset.getString("PAY_STATE"));*/
				if(rset.getString("PAY_STATE").equals("ENDPAY")) {
					hmap.put("payState", "결제완료");
				}else if(rset.getString("PAY_STATE").equals("ENDREFUND")) {
					hmap.put("payState", "환불완료");
				}else if(rset.getString("PAY_STATE").equals("CANCLEPAY")) {
					hmap.put("payState", "결제취소");
				}else if(rset.getString("PAY_STATE").equals("CANCLEREFUND")){
					hmap.put("payState", "환불취소");
				}else {
					hmap.put("payState", "환불요청");
				}
				
				
				hmap.put("receRe", rset.getString("RECE_REQUEST"));
				hmap.put("reName", rset.getString("RECE_NAME"));
				hmap.put("rePhone", rset.getString("RECE_PHONE"));
				hmap.put("rePhone2", rset.getString("RECE_PHONE2"));
				hmap.put("reEmail", rset.getString("RECE_EMAIL"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("pDate", rset.getDate("PDATE"));
				list.add(hmap);
			}
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		
		
		
		return list;
	}

	public int adOrderReset(Connection con, int ono) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("adOrderReset");
		try {
			 
			pstmt = con.prepareStatement(query);
			 pstmt.setLong(1, ono);
			
			 result = pstmt.executeUpdate();
			 
			
			
			
		} catch (SQLException e) {
			
			
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		
		
		return result;
	}

}
