package com.edr.order.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import static com.edr.common.JDBCTemplate.*;

import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

public class OrderDao {

	Properties prop = new Properties();
	
	public OrderDao() {
		
		String fileName = OrderDao.class.getResource("/sql/order/order-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public int insertOrderList(Connection con, OrderList orderList) {
		
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertOrderList");
		
		System.out.println("OrderList DAO PNO : " + orderList.getPno());
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderList.getPno());
			pstmt.setInt(2, orderList.getMno());
			pstmt.setString(3, orderList.getOrderName());
			pstmt.setString(4, orderList.getOrderPhone());
			pstmt.setString(5, orderList.getOrderEmail());
			pstmt.setLong(6, orderList.getOrderCode());
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int selectOrderNo(Connection con, OrderList orderList) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int orderNo = 0;
		
		String query = prop.getProperty("selectOrderNo");
		
		System.out.println("OrderNo Dao : " + orderList.getPno());
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderList.getPno());
			pstmt.setInt(2, orderList.getMno());
			pstmt.setLong(3, orderList.getOrderCode());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				orderNo = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return orderNo;
	}

	public int insertOrderDetail(Connection con, OrderDetail orderDetail) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertOrderDetail");
		
		System.out.println("OrderDetail Dao pno : " + orderDetail.getPno());
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderDetail.getOrderNo());
			pstmt.setInt(2, orderDetail.getPno());
			pstmt.setString(3, orderDetail.getReceName());
			pstmt.setString(4, orderDetail.getReceGender());
			pstmt.setString(5, orderDetail.getRecePhone());
			pstmt.setString(6, orderDetail.getRecePhone2());
			pstmt.setString(7, orderDetail.getReceEmail());
			pstmt.setString(8, orderDetail.getReceAge());
			pstmt.setString(9, orderDetail.getReceRequest());
			pstmt.setInt(10, orderDetail.getRecPerson());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		
		return result;
	}

	public int insertPayment(Connection con, Payment payment) {
		
		PreparedStatement pstmt = null;
		int result3 = 0;
		
		String query = prop.getProperty("insertPayment");
		
		System.out.println("OrderDao payment pno : " + payment.getPno());
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, payment.getPno());
			pstmt.setInt(2, payment.getOrderNo());
			pstmt.setInt(3, payment.getPayCost());
			pstmt.setString(4, payment.getPayKind());
			
			result3 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result3;
	}

	public int insertProduct(Connection con, Product product) {

		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCustomProduct");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, product.getCpNo());
			pstmt.setDate(2, product.getPdate());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int selectPno(Connection con, Product product) {

		PreparedStatement pstmt = null;
		int pno = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCustomPno");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, product.getCpNo());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				pno = rset.getInt("PNO");
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return pno;
	}

	public int insertCustomOrderList(Connection con, OrderList orderList) {

		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCustomOrderList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderList.getPno());
			pstmt.setInt(2, orderList.getMno());
			pstmt.setString(3, orderList.getOrderName());
			pstmt.setString(4, orderList.getOrderPhone());
			pstmt.setString(5, orderList.getOrderEmail());
			pstmt.setLong(6, orderList.getOrderCode());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int selectCustomOrderNo(Connection con, OrderList orderList) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("selectCustomOrderNo");
		int orderNo = 0;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setLong(1, orderList.getOrderCode());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				orderNo = rset.getInt("ORDER_NO");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return orderNo;
	}

	public int insertCustomProgramOrderDetail(Connection con, OrderDetail orderDetail) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCustomProgramOrderDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderDetail.getOrderNo());
			pstmt.setInt(2, orderDetail.getPno());
			pstmt.setString(3, orderDetail.getReceName());
			pstmt.setString(4, orderDetail.getReceGender());
			pstmt.setString(5, orderDetail.getRecePhone());
			pstmt.setString(6, orderDetail.getRecePhone2());
			pstmt.setString(7, orderDetail.getReceEmail());
			pstmt.setString(8, orderDetail.getReceAge());
			pstmt.setString(9, orderDetail.getReceRequest());
			pstmt.setInt(10, orderDetail.getRecPerson());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertCustomProgramPayment(Connection con, Payment payment) {
		
		PreparedStatement pstmt = null;
		int result4 = 0;
		String query = prop.getProperty("insertCustomProgramPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, payment.getPno());
			pstmt.setInt(2, payment.getOrderNo());
			pstmt.setInt(3, payment.getPayCost());
			pstmt.setString(4, payment.getPayKind());
			
			result4 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result4;
	}

	public int updateCustomState(Connection con, int cpNo) {
		
		PreparedStatement pstmt = null;
		int result5 = 0;
		String query = prop.getProperty("updateCustomState");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			result5 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result5;
	}

	public int updateCustomEstStatus(Connection con, int cpEstNo) {

		PreparedStatement pstmt = null;
		int result6 = 0;
		String query = prop.getProperty("updateCustomEstStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpEstNo);
			
			result6 = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result6;
	}


}
