package com.edr.order.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements java.io.Serializable{
	private int orderNo;			// 주문번호
	private int pno;				// 상품번호
	private int orderDetailNo;		// 주문상세번호
	private String receName;		// 수령자명
	private String receGender;		// 수령자성별
	private String recePhone;		// 수령자연락처
	private String recePhone2;		// 수령자연락처2
	private String receEmail;		// 수령자 이메일
	private String receAge;			// 수령자 연령대
	private String receRequest;		// 수령자 요청사항
	private int recPerson;   		// 신청인원
	private String tourState;		// 투어상태
	
}
