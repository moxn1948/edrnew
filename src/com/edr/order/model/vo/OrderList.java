package com.edr.order.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderList {
	private int orderNo;		// 주문번호
	private long orderCode;
	private int pno;			// 상품번호
	private int mno;			// 회원번호
	private String orderName;	// 주문자명
	private String orderPhone;	// 주문자연락처
	private String orderEmail;	// 주문자이메일
}
