package com.edr.generalProgram.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.AllowDelService;

  
/**
 * Servlet implementation class AllowListDelServlet
 */
@WebServlet("/allowDel.gp")
public class AllowListDelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllowListDelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		int gpno = Integer.parseInt(request.getParameter("gpno"));
		
		System.out.println("servlet : " + mno);
		System.out.println("servlet : " + gpno);
		
		int result = new AllowDelService().allowDel(mno, gpno);
		
		String page = "";
		
		if(result > 0) {
		
			
			response.sendRedirect(request.getContextPath()+"/allowList.gp?mno=" +  mno);
		}else {
			page = "views/user/sub/common/errorPage.jsp";
			request.setAttribute("msg", "승인 대기중인 프로그램 삭제 실패");
		}		
		// request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
