package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.service.MyProgramDetailService;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

/**
 * Servlet implementation class SelectOneWaitingProgramServlet
 */
@WebServlet("/selectOneWaitingProgram.gp")
public class SelectOneWaitingProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneWaitingProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int mno = Integer.parseInt(request.getParameter("mno"));
		int gpNo = Integer.parseInt(request.getParameter("gpNo"));
		
		HashMap<String, Object> gpDetail = new MyProgramDetailService().selectMyProgramDetail(gpNo);
		
		Gp gpObj = (Gp) gpDetail.get("gpObj");
		ArrayList<GpPriceDetail> gpPriceList  = (ArrayList<GpPriceDetail>) gpDetail.get("gpPriceList");
		ArrayList<GpDetail> gpDetailList  = (ArrayList<GpDetail>) gpDetail.get("gpDetailList");
		ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) gpDetail.get("gpAttachmentList");
		ArrayList<GpLang> gpLangList  = (ArrayList<GpLang>) gpDetail.get("gpLangList");
		String guideName = (String) gpDetail.get("guideName");
		
		if(gpDetail != null) {
			request.setAttribute("gpNo", gpNo);
			request.setAttribute("gpObj", gpObj);
			request.setAttribute("gpPriceList", gpPriceList);
			request.setAttribute("gpDetailList", gpDetailList);
			request.setAttribute("gpAttachmentList", gpAttachmentList);
			request.setAttribute("gpLangList", gpLangList);
			request.setAttribute("guideName", guideName);
			
			request.getRequestDispatcher("views/user/sub/guide_page/waitingProgramDetail.jsp").forward(request, response);
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
