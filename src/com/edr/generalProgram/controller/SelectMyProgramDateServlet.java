package com.edr.generalProgram.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.MyProgramService;
import com.google.gson.Gson;

/**
 * Servlet implementation class SelectMyProgramDateServlet
 */
@WebServlet("/selectMyProgramDate.gp")
public class SelectMyProgramDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectMyProgramDateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int gpNo = Integer.parseInt(request.getParameter("gpNo"));
		
		System.out.println(gpNo);
		
		ArrayList<Date> myProgramDateList = new MyProgramService().selectMyProgramDate(gpNo);
		int tday =  new MyProgramService().selectMyProgramTday(gpNo);
		
		
		HashMap<String, Object> myProgramDetail = new HashMap<>();
		
		myProgramDetail.put("myProgramDateList", myProgramDateList);
		myProgramDetail.put("tday", tday);
		
		
		
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(myProgramDetail, response.getWriter());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
