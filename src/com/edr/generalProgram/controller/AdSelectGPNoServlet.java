package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.AdGPService;

/**
 * Servlet implementation class AdSelectGPNoServlet
 */
@WebServlet("/selectGPNo.ad")
public class AdSelectGPNoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSelectGPNoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ArrayList<Gp> list = new AdGPService().selectGPNoList();
		
		int num = Integer.parseInt(request.getParameter("gpno"));
		System.out.println(num);
		
		// 페이저 처리 후
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage = 1;

		// 전달 받든 페이지 추출
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 5;		
		
		// 전체 목록 갯수 조회
		int listCount = new AdGPService().getNoListCount(num);
		
		
		maxPage = (int) ((double) listCount / limit + 0.8);

		startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

		// 목록 아래 쪽에 보여질 마지막 페이지 수
		endPage = startPage + 5 - 1;

		if (maxPage < endPage) {
			endPage = maxPage;
		}
		
		// 페이지 정보를 담을 vo 객체 생성
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
		ArrayList<HashMap<String, Object>> list = new AdGPService().selectNoListWithPaging(num, currentPage, limit);
		
		String page = "";

		if (list != null) {
			page = "views/admin/sub/general_program/allProgramNo.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "일반프로그램 게시판 조회 실패!");
		}

		request.getRequestDispatcher(page).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
