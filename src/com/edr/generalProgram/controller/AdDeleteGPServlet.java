package com.edr.generalProgram.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.AdGPService;
import com.edr.generalProgram.model.vo.Gp;


/**
 * Servlet implementation class AdDeleteGPServlet
 */
@WebServlet("/deleteGP.ad")
public class AdDeleteGPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdDeleteGPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		System.out.println(num);
		Gp g = new Gp();
		g.setGpNo(num);
	
		int result = new AdGPService().adDeleteGP(num);

		String page = "";
		
		if (result > 0) {
			response.sendRedirect("/edr/selectGP.ad");
			
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "프로그램 정보 삭제 실패!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
