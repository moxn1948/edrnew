package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.edr.common.MyFileRenamePolicy;
import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.service.InsertNewGpService;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.oreilly.servlet.MultipartRequest;

/**
 * moxn
 * 191209
 * 프로그램 신청 페이지 서블릿
 */
@WebServlet(name = "InsertNewGpServlet", urlPatterns = { "/insertNew.gp" })
public class InsertNewGPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertNewGPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(ServletFileUpload.isMultipartContent(request)) {

			int maxSize = 1024 * 1024 * 10; 

			String root = request.getSession().getServletContext().getRealPath("/");
			String savePath = root + "uploadFiles/";
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			

			ArrayList<String> saveFiles = new ArrayList<String>();				 // 변경이름 저장
			ArrayList<String> originFiles = new ArrayList<String>(); 
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				
				saveFiles.add(multiRequest.getFilesystemName(name)); 			// 변경된 이름
				originFiles.add(multiRequest.getOriginalFileName(name)); 		// 원 이름
			}
						
			int mno = Integer.parseInt(multiRequest.getParameter("mno"));		// 회원번호
			String gpName = multiRequest.getParameter("gpName");			 	// 프로그램 명
			int localNo = Integer.parseInt(multiRequest.getParameter("localNo")); 			// 프로그램 지역
			String gpDescr = multiRequest.getParameter("gpDescr");				// 프로그램 설명
			int gpTday = Integer.parseInt(multiRequest.getParameter("gpTday"));
			int gpMin = Integer.parseInt(multiRequest.getParameter("gpMin"));					// 최소 참가 가능 인원
			int gpMax = Integer.parseInt(multiRequest.getParameter("gpMax"));					// 최대 참가 가능 인원 
			String gpLang[] = multiRequest.getParameterValues("gpLang");		// 프로그램 진행 가능 언어 - 디폴트
			String gpLangEtc[] = multiRequest.getParameterValues("gpLangEtc");	// 프로그램 진행 가능 언어 - 그 외
			String gpMeetTime = multiRequest.getParameter("gpMeetTime");		// 만나는 시간
			
			String postcode = multiRequest.getParameter("postcode");			// 만나는 장소 - 우편번호 
			String roadAddress = multiRequest.getParameter("roadAddress");		// 만나는 장소 - 도로명 
			String detailAddress = multiRequest.getParameter("detailAddress");	// 만나는 장소 - 상세주소 
			double gpMeetX = Double.parseDouble(multiRequest.getParameter("addressX"));				// 만나는 장소 - x 좌표
			double gpMeetY = Double.parseDouble(multiRequest.getParameter("addressY"));				// 만나는 장소 - y 좌표
			
			String gpMeetArea = postcode + "/" + roadAddress + "/" + detailAddress;
			
			int gpTotalDay = Integer.parseInt(multiRequest.getParameter("gpTday"));		// 총 프로그램 기간 

			ArrayList day = new ArrayList();
			
			for (int i = 0; i < gpTotalDay; i++) {
				String placeDayDetail[] = multiRequest.getParameterValues("placeDayDetail" + (i+1));
				ArrayList dayDetail = new ArrayList();
				
				for (int j = 0; j < placeDayDetail.length; j++) {
					// ArrayList dayDetailCnt = new ArrayList();
					HashMap map = new HashMap();
					
					map.put("place", multiRequest.getParameterValues("placeDayDetail"+(i+1))[j]);
					map.put("time", multiRequest.getParameterValues("timeDayDetail"+(i+1))[j]);
					map.put("desc", multiRequest.getParameterValues("descDayDetail"+(i+1))[j]);
					
					// dayDetailCnt.add(map);
					dayDetail.add(map);
				}
				
				day.add(dayDetail);
					
			}
			
			String gpNotice = multiRequest.getParameter("gpNotice");								// 공지사항
			
			// String includeYn[] = multiRequest.getParameterValues("includeYn");						// 포함, 불포함 사항 
		
			String gpCategory[] = multiRequest.getParameterValues("gpCategory");					// 항목명
			String gpPrice[] = multiRequest.getParameterValues("gpPrice");							// 금액
			
			// 여기 부분 문제~
			String[] includeYn = new String[gpCategory.length];
			
			for (int i = 0; i < includeYn.length; i++) {
				includeYn[i] = multiRequest.getParameter("includeYn"+i);
				System.out.println( i + " : " + includeYn[i]);
			}
			
			int gpCost = Integer.parseInt(multiRequest.getParameter("gpCost"));
			
			// 객체 저정
			
			
			
			// 프로그램 기본정보 객체
			Gp reqGp = new Gp(); 
			
			reqGp.setGpName(gpName);
			reqGp.setLocalNo(localNo);
			reqGp.setGpDescr(gpDescr);
			reqGp.setGpTday(gpTday);
			reqGp.setGpMin(gpMin);
			reqGp.setGpMax(gpMax);
			reqGp.setGpCost(gpCost);
			reqGp.setGpNotice(gpNotice);
			reqGp.setGpMeetTime(gpMeetTime);
			reqGp.setGpMeetArea(gpMeetArea);
			reqGp.setGpMeetX(gpMeetX);
			reqGp.setGpMeetY(gpMeetY);
			reqGp.setMno(mno);
			reqGp.setLocalNo(localNo);
			
			System.out.println("1. Gp : " + reqGp);
			
			// 프로그램 가능 언어 객체
			ArrayList<GpLang> langList = new ArrayList<>();
			GpLang reqGpLang = null;
			if(gpLang != null) {
				for (int j = 0; j < gpLang.length; j++) {
					reqGpLang = new GpLang();
					reqGpLang.setLang(gpLang[j]);
					langList.add(reqGpLang);
				}
			}

			if(gpLangEtc != null) {
				for (int j = 0; j < gpLangEtc.length; j++) {
					reqGpLang = new GpLang();
					reqGpLang.setLang(gpLangEtc[j]);
					langList.add(reqGpLang);
				}	
			}
			
			System.out.println("2. 가능 언어 " + langList);
			
			// 포함, 불포함 사항 객체
			ArrayList<GpPriceDetail> priceDetailList = new ArrayList<>();
			GpPriceDetail reqGpPriceDetail = null;
			// String includeYn = (String) request.getParameter("includeYn");
			
			for (int i = 0; i < gpPrice.length; i++) {
				reqGpPriceDetail = new GpPriceDetail();
				reqGpPriceDetail.setGpCategory(gpCategory[i]);
				
				if(includeYn[i].equals("include")) {
					
					reqGpPriceDetail.setGpPrice(Integer.parseInt(gpPrice[i]));
					reqGpPriceDetail.setGpInc("INCLUDE");
				}else {
					reqGpPriceDetail.setGpInc("NOTCLUDE");
				}
				
				priceDetailList.add(reqGpPriceDetail);
			}
			
			System.out.println("3. 포함/불포함 사항 : " + priceDetailList);
			
			// 상세 일정
			ArrayList<GpDetail> detailList = new ArrayList<>();
			
			GpDetail gpDetail = null;
			for (int i = 0; i < day.size(); i++) { // 일자만큼 돌아야함
				for (int j = 0; j < ((ArrayList) day.get(i)).size(); j++) { // 일정
					HashMap map = (HashMap) ((ArrayList) day.get(i)).get(j);
					
					gpDetail = new GpDetail(); 

					gpDetail.setGpLocation((String) map.get("place"));
					gpDetail.setGpTime(Integer.parseInt((String) map.get("time")));
					gpDetail.setGpCnt((String) map.get("desc"));
					gpDetail.setGpDay(i + 1);
					gpDetail.setGpDaySeq(j + 1);
					
					detailList.add(gpDetail);
					
				}
			}
			
			System.out.println("4. gpDetailList : " + detailList);
			
			
			
			// 첨부파일 객체
			ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			
			for (int i = originFiles.size() - 1; i >= 0; i--) {
				Attachment at = new Attachment();
				
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				at.setFilePath(savePath);
				
				fileList.add(at);
			}
			
			System.out.println("5. fileList : " + fileList);
			
			HashMap<String, Object> reqMap = new HashMap<>();
			
			reqMap.put("reqGp", reqGp);
			reqMap.put("langList", langList);
			reqMap.put("priceDetailList", priceDetailList);
			reqMap.put("detailList", detailList);
			reqMap.put("fileList", fileList);
			
			int result = new InsertNewGpService().insertNewGp(reqMap);
			
			if(result > 0) {
				response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertNewGp");
				
			}else {
				response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertNewGp");
			}
			
			
			

			/* 출력 시작 */
			/*
			 * 
			System.out.println("save : " + saveFiles);							// 저장 파일명
			System.out.println("originFiles : " + originFiles);					// 원 파일명

			System.out.println("mno : " + mno);									// 회원번호
			System.out.println("gpName : " + gpName);							// 프로그램 명
			System.out.println("localNo : " + localNo);						// 프로그램 지역
			System.out.println("gpDescr : " + gpDescr);							// 프로그램 설명
			System.out.println("gpMin : " + gpMin);								// 최소 참가 가능 인원
			System.out.println("gpMax : " + gpMax);								// 최대 참가 가능 인원
			for (int i = 0; i < gpLang.length; i++) {							// 프로그램 진행 가능 언어 - 디폴트
				// System.out.println("gpLang - " + i + " : "  + gpLang[i]);
			}						
			for (int i = 0; i < gpLangEtc.length; i++) {						// 프로그램 진행 가능 언어 - 그 외
				// System.out.println("gpLangEtc - " + i + " : "  + gpLangEtc[i]);
			}
			System.out.println("gpMeetTime : " + gpMeetTime);					// 만나는 시간 
			System.out.println("postcode : " + postcode);						// 만나는 장소 - 우편번호 
			System.out.println("roadAddress : " + roadAddress);					// 만나는 장소 - 도로명주소 
			System.out.println("detailAddress : " + detailAddress);				// 만나는 장소 - 상세주소 	
			System.out.println("gtTotalDay : " + gpTotalDay);					// 총 프로그램 기간	
			System.out.println("gpNotice : " + gpNotice);						// 공지사항
			
			for (int i = 0; i < includeYn.length; i++) { 						// 포함, 불포함 사항 
				System.out.println("includeYn - " + i + " : "  + includeYn[i]);
			}				
			for (int i = 0; i < gpCategory.length; i++) { 						// 항목명
				System.out.println("gpNotice - " + i + " : "  + gpCategory[i]);
			}				
			for (int i = 0; i < gpPrice.length; i++) { 							// 금액
				System.out.println("gpPrice - " + i + " : "  + gpPrice[i]);
			}
			System.out.println("gpCose : " + gpCost);
			
			
			System.out.println("day : " + day);
			
			*/
			/* 출력 끝 */
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
