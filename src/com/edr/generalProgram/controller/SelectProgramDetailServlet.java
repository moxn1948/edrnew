package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.service.MyProgramDetailService;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.product.model.vo.Product;
import com.edr.review.model.vo.Review;

/**
 * Servlet implementation class SelectProgramDetailServlet
 */
@WebServlet("/selectProgramDetail.gp")
public class SelectProgramDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectProgramDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int gpNo = Integer.parseInt(request.getParameter("no"));
		
		System.out.println(gpNo);
		
		HashMap<String, Object> gpDetail = new MyProgramDetailService().selectMyProgramDetail(gpNo);

		Gp gpObj = (Gp) gpDetail.get("gpObj");
		ArrayList<GpPriceDetail> gpPriceList  = (ArrayList<GpPriceDetail>) gpDetail.get("gpPriceList");
		ArrayList<GpDetail> gpDetailList  = (ArrayList<GpDetail>) gpDetail.get("gpDetailList");
		ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) gpDetail.get("gpAttachmentList");
		ArrayList<GpLang> gpLangList  = (ArrayList<GpLang>) gpDetail.get("gpLangList");
		HashMap<String, Object> gpReviewCnt = (HashMap<String, Object>) gpDetail.get("gpReviewCnt");
		ArrayList<Product> gpProductList = (ArrayList<Product>) gpDetail.get("gpProductList");
		String guideName = (String) gpDetail.get("guideName");
		ArrayList<HashMap<String, Object>> mainReviewCnt = ((ArrayList<HashMap<String, Object>>) gpDetail.get("mainReviewCnt"));
		
		
		if(gpDetail != null) {
			request.setAttribute("gpNo", gpNo);
			request.setAttribute("gpObj", gpObj);
			request.setAttribute("gpPriceList", gpPriceList);
			request.setAttribute("gpDetailList", gpDetailList);
			request.setAttribute("gpAttachmentList", gpAttachmentList);
			request.setAttribute("gpLangList", gpLangList);
			request.setAttribute("gpReviewCnt", gpReviewCnt);
			request.setAttribute("gpProductList", gpProductList);
			request.setAttribute("guideName", guideName);
			request.setAttribute("mainReviewCnt", mainReviewCnt);
			
			request.getRequestDispatcher("views/user/sub/general_program/programDetail.jsp").forward(request, response);
			
		}else {
			
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=selectProgramDetail");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
