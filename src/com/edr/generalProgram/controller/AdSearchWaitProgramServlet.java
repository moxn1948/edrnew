package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.AdWaitProgramService;

/**
 * Servlet implementation class AdSearchWaitProgramServlet
 */
@WebServlet("/adSearchWait.gp")
public class AdSearchWaitProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSearchWaitProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String waitCategory = request.getParameter("waitCategory");
		String waitSearch = request.getParameter("waitSearch");
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		ArrayList<HashMap<String, Object>> list = null;
		PageInfo pi = null;
		if(waitCategory.equals("name")) {
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			limit = 5;
			
			int listCount = 0;
			
			
			
			listCount = new AdWaitProgramService().adSearchNameWaitProgramCount(waitSearch);
			
			 maxPage = (int) ((double) listCount / limit + 0.8);
			
			 startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
		
			 endPage = startPage + 5 - 1;
			 
			 if(maxPage < endPage) {
				 endPage = maxPage;
			 }
			 
			 pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			 
			 
			 list = null;
			 list = new AdWaitProgramService().adSearchWaitName(waitSearch, currentPage, limit);
		}else {
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			
			limit = 5;
			
			int listCount = 0;
			
			
			listCount = new AdWaitProgramService().adSearchIdProgramCount(waitSearch);
			
			 maxPage = (int) ((double) listCount / limit + 0.8);
			
			 startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
		
			 endPage = startPage + 5 - 1;
			 
			 if(maxPage < endPage) {
				 endPage = maxPage;
			 }
			 
			 pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			 
			 
			 list = null;
			 list = new AdWaitProgramService().adSearchWaitId(waitSearch, currentPage, limit);
			
			
			
		}
		String page = "";
		if(list != null) {
			page = "views/admin/sub/general_program/waitSearchPrigramList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("waitCategory", waitCategory);
			request.setAttribute("waitSearch", waitSearch);
		}else {
			page = "views/admin/sub/general_program/waitProgramList.jsp";
			request.setAttribute("msg", "게시판 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
