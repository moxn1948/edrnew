package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.MyProgramService;

/**
 * Servlet implementation class SelectMyProgramListServlet
 */
@WebServlet("/selectMyProgramList.gp")
public class SelectMyProgramListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectMyProgramListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));

		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 5;

		int listCount = new MyProgramService().myProgramCount(mno);
		
		System.out.println(listCount  + " ddd");
		maxPage = (int) ((double)listCount / limit + 0.8);
		
		startPage = (((int)((double) currentPage / 5 + 0.8)) - 1) * 5 + 1;
		
		endPage = startPage + 5 - 1;

		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		System.out.println(" pi : " + pi);
		
		HashMap<String, Object> programMap = new MyProgramService().selectMyProgram(currentPage, limit, mno, listCount);
		
		if(programMap != null) {
			request.setAttribute("programMap", programMap);
			request.setAttribute("pi", pi);

			request.getRequestDispatcher("views/user/sub/guide_page/myProgram.jsp").forward(request, response);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=selectMyProgramList");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
