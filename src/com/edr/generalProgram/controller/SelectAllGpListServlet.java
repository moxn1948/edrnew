package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.SelectAllGpListService;
import com.edr.generalProgram.model.vo.Gp;
 
/**
 * Servlet implementation class SelectAllGpListServlet
 */
@WebServlet("/selectAllListWithPaging.gp")
public class SelectAllGpListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectAllGpListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int localNo = Integer.parseInt(request.getParameter("localNo"));
		
		
		// 추천 프로그램 
		HashMap<String, Object> recomGp = new SelectAllGpListService().selectRecomGp(localNo);
		int no = ((Gp) recomGp.get("recomGpObj")).getGpNo();
		HashMap<String, Object> recomReviewCnt = new SelectAllGpListService().selectReviewCnt(no);
		
		// 프로그램 목록

		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		//전달받은 페이지 추출
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한 페이지에 보여질 목록 갯수
		limit = 9;

		
		//전체 목록 갯수 조회
		int listCount = new SelectAllGpListService().gpListCount(localNo);
		System.out.println(listCount  + " ddd");
		maxPage = (int) ((double)listCount / limit + 0.88888888888888889);
		
		startPage = (((int)((double) currentPage / 5 + 0.8)) - 1) * 5 + 1;
		
		endPage = startPage + 5 - 1;

		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		HashMap<String, Object> list = new SelectAllGpListService().selectGpListWithPaging(currentPage, limit, localNo);
		
		
		if(recomGp != null && recomReviewCnt != null) {
			request.setAttribute("localNo", localNo);
			request.setAttribute("recomGp", recomGp);
			request.setAttribute("recomReviewCnt", recomReviewCnt);

			request.setAttribute("pi", pi);
			request.setAttribute("list", list);
			request.getRequestDispatcher("views/user/sub/general_program/areaList.jsp").forward(request, response);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=SelectAllGPListWithPaging");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
