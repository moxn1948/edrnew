package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.AdGPService;
import com.edr.generalProgram.model.service.AdWaitProgramService;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

/**
 * Servlet implementation class AdSelectOneGPNoServlet
 */
@WebServlet("/selectOneGPNo.ad")
public class AdSelectOneGPNoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSelectOneGPNoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("str"));
		int pno = Integer.parseInt(request.getParameter("pno"));
		
		HashMap<String, Object> epiDetail = new AdGPService().adSelectOneGpNo(num, pno);
		ArrayList<HashMap<String, Object>> list = new AdGPService().adSelectOneGPNo(num);
		ArrayList<GpPriceDetail> listPrice = new AdGPService().adSelectOneGPNoPriceDetail(num);
		//ArrayList<HashMap<String, Object>> gpDetailList  =  new AdWaitProgramService().adSelectOneGpDetailMoon(num);
		//ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) gpDetail.get("gpAttachmentList");
		
		
		System.out.println("list : " + list);
		System.out.println("listPrice : " + listPrice);
			
		for(int i = 0; i < listPrice.size(); i++) {
			if(listPrice.get(i).getGpInc().equals("INCLUDE")) {
				listPrice.get(i).setGpInc("포함사항");
			}else {
				listPrice.get(i).setGpInc("불포함사항");
			}
		}
		int sum = 0;
		
		String include = "";
		String notClude = "";
		for(int i = 0; i < listPrice.size(); i++) {
			
			sum += listPrice.get(i).getGpPrice();
			
			if(listPrice.get(i).getGpInc().equals("포함사항")) {
				if(i != listPrice.size() - 1) {
					include += listPrice.get(i).getGpCategory() + ", ";
				}else {
					include += listPrice.get(i).getGpCategory();
				}
				
			}else {
				if(i != list.size() - 1) {
					notClude += listPrice.get(i).getGpCategory() + ", ";
				}else {
					notClude += listPrice.get(i).getGpCategory();
				}
			}
		}
		//ArrayList<GpDetail> listGpDetail2 = null;
		//HashMap<String, Object> hamp = null;
		
		String page = "";

		if (list != null) {
			page = "views/admin/sub/general_program/allProgramNoDetail.jsp";
			request.setAttribute("epiDetail", epiDetail);
			request.setAttribute("list", list);
			request.setAttribute("listPrice", listPrice);
			request.setAttribute("num", num);
			request.setAttribute("sum", sum);
			request.setAttribute("include", include);
			request.setAttribute("notClude", notClude);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "게시판 상세 조회 실패!!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
