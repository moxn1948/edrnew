package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Local;
import com.edr.generalProgram.model.service.GuideLocalService;
import com.google.gson.Gson;

/**
 * moxn
 * 191209
 * 프로그램 신청 페이지에서 가이드에 대한 정보 보내는 서블릿 : ajax 이용
 */
@WebServlet("/selectLocal.gp")
public class SelectGPLocalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectGPLocalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		ArrayList<Local> localList = new GuideLocalService().selectGuideLocal(mno);
		
		
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(localList, response.getWriter());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
