package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.service.AllowListService;
import com.edr.generalProgram.model.service.ProgressGpListService;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class allowList
 */
@WebServlet("/allowList.gp")
public class AllowListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AllowListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage = 1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		//한 페이지에 보여질 목록 갯수
		limit = 5;

		//전체 목록 갯수 조회
		int listCount = new AllowListService().allowCount(mno);

		//총 페이지수 계싼
		maxPage = (int) ((double) listCount / limit + 0.8);

		//현재 페이지에 보여 줄 시작 페이지수
		startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

		//목록 아래 쪽에 보여질 마지막 페이지 수
		endPage = startPage + 5 - 1;

		if(maxPage < endPage) {
			endPage = maxPage;
		}


		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

		ArrayList<HashMap<String, Object>> list = new AllowListService().allowListWithPaging(currentPage, limit, mno);


		String page = "";		
		if(list != null) {
			page = "views/user/sub/guide_page/waitingProgram.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
		}else {
			page = "views/user/sub/common/errorPage.jsp";
			request.setAttribute("msg", "승인 대기중인 프로그램 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
