package com.edr.generalProgram.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.AdWaitProgramService;

/**
 * Servlet implementation class AdWaitProgramApprovalServlet
 */
@WebServlet("/adWaitProgramApproval.gp")
public class AdWaitProgramApprovalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdWaitProgramApprovalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		String appro = request.getParameter("str");
		int num = Integer.parseInt(request.getParameter("num"));
		int result = 0;
		
		if(appro.equals("Y")) {
			result = new AdWaitProgramService().adWaitProgramApproval(appro, num);
		}else {
			result = new AdWaitProgramService().adWaitProgramNotApprovalN(appro, num);
		}
		
		
		
		String page = "";
		if(result > 0) {
			response.sendRedirect("/edr/adWaitProgramList.gp");
		}else {
			request.setAttribute("msg", "승인실패");
			page = "views/admin/sub/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
