package com.edr.generalProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.service.AdWaitProgramService;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

/**
 * Servlet implementation class AdSelectOneWaitProgramListServlet
 */
@WebServlet("/adSelectOneWaitProgramList.gp")
public class AdSelectOneWaitProgramListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSelectOneWaitProgramListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		
		ArrayList<HashMap<String, Object>> list = new AdWaitProgramService().adSelectOneWait(num);
		ArrayList<GpLang> listLang = new AdWaitProgramService().adSelectOneLang(num);
		ArrayList<GpDetail> listGpDetail = new AdWaitProgramService().adSelectOneGpDetail(num);
		ArrayList<GpPriceDetail> listPrice = new AdWaitProgramService().adSelectPrice(num);
		//ArrayList<HashMap<String, Object>> gpDetailList  =  new AdWaitProgramService().adSelectOneGpDetailMoon(num);
		//ArrayList<Attachment> gpAttachmentList  = (ArrayList<Attachment>) gpDetail.get("gpAttachmentList");
		
			
		for(int i = 0; i < listPrice.size(); i++) {
			if(listPrice.get(i).getGpInc().equals("INCLUDE")) {
				listPrice.get(i).setGpInc("포함사항");
			}else {
				listPrice.get(i).setGpInc("불포함사항");
			}
		}
		int sum = 0;
		String include = "";
		String notClude = "";
		for(int i = 0; i < listPrice.size(); i++) {
			
			sum += listPrice.get(i).getGpPrice();
			
			if(listPrice.get(i).getGpInc().equals("포함사항")) {
				if(i != listPrice.size() - 1) {
					include += listPrice.get(i).getGpCategory() + ", ";
				}else {
					include += listPrice.get(i).getGpCategory();
				}
				
			}else {
				if(i != list.size() - 1) {
					notClude += listPrice.get(i).getGpCategory() + ", ";
				}else {
					notClude += listPrice.get(i).getGpCategory();
				}
			}
		}
		ArrayList<GpDetail> listGpDetail2 = null;
		HashMap<String, Object> hamp = null;
		
		/*for(int i = 0; i < listGpDetail.size(); i++) {
			hmap = new HashMap<String, Object>();
			if(i != 0) {
				if(listGpDetail.get(i).getGpDay() == listGpDetail.get(i-1).getGpDay()) {
					Integer.parseInt((listGpDetail.get(i-1).getGpDaySeq() + ", " + listGpDetail.get(i).getGpDaySeq())));
					
					
					listGpDetail.get(i-1).setGpDaySeq()
					listGpDetail.get(i-1).setGpCnt(listGpDetail.get(i).getGpCnt() + ", " + listGpDetail.get(i-1).getGpCnt()); 
					listGpDetail.remove(i);
				}else {
					
				}
			}else {
				
				
			}
		}*/
		//.println("gpDetailListasdasdasd : " + gpDetailList);
			
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/general_program/waitProgramDetail.jsp";
			request.setAttribute("list", list);
			request.setAttribute("listLange", listLang);
			request.setAttribute("listGpDetail", listGpDetail);
			request.setAttribute("listPrice", listPrice);
			request.setAttribute("num", num);
			request.setAttribute("sum", sum);
			request.setAttribute("include", include);
			request.setAttribute("notClude", notClude);
			//request.setAttribute("gpDetailList", gpDetailList);
			
			
			
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "승인대기중인프로그램 상세보기 실패!");
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
