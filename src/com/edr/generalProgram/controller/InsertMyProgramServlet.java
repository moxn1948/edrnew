package com.edr.generalProgram.controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.service.MyProgramService;

/**
 * Servlet implementation class InsertMyProgramServlet
 */
@WebServlet("/insertMyProgram.gp")
public class InsertMyProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertMyProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int gpNo = Integer.parseInt(request.getParameter("myProgramSel"));
		Date startDate = Date.valueOf((String) request.getParameter("startDate"));
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		int result = new MyProgramService().insertMyProgram(gpNo, startDate);
		
		if(result > 0) {
			response.sendRedirect(request.getContextPath() + "/selectMyProgramList.gp?mno=" + mno);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=insertMyProgram");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
