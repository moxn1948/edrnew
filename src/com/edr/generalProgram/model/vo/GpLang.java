package com.edr.generalProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GpLang implements java.io.Serializable{
	private String lang; 	// 언어
	private int gpNo; 		// 프로그램번호
	
}
