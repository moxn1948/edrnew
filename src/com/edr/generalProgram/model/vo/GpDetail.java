package com.edr.generalProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GpDetail implements java.io.Serializable{
	private int gpNo;			// 프로그램 번호
	private int gpDay;			// 프로그램 일차
	private int gpDaySeq;		// 프로그램 일정순서
	private String gpLocation;	// 장소
	private int gpTime;			// 소요시간
	private String gpCnt;		// 프로그램 내용
}
