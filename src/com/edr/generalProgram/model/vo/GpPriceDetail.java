package com.edr.generalProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GpPriceDetail implements java.io.Serializable{
	private String gpCategory;		// 항목
	private int gpNo;				// 프로그램번호
	private int gpPrice;			// 프로그램 가격
	private String gpInc;			// 구분
}
