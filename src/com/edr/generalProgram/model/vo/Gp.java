package com.edr.generalProgram.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gp implements java.io.Serializable{
	private int gpNo;			// 일반프로그램번호
	private String gpName;		// 일반프로그램명
	private String gpDescr;		// 일반프로그램 설명
	private int gpTday;			// 총 기간
	private int gpMin;			// 최소인원
	private int gpMax;			// 최대인원
	private int gpCost;			// 프로그램 비용
	private String gpNotice;	// 일반 프로그램 공지
	private String gpMeetTime;	// 만나는 시간
	private String gpMeetArea;	// 만나는 장소
	private double gpMeetX;		// 만나는 장소 X 좌표
	private double gpMeetY;		// 만나는 장소 Y 좌표
	private String allowYn;		// 승인여부
	private int mno;			// 회원번호
	private int localNo;        // 지역번호
	private Date gpRday;			// 등록일
	private String gpStatus;		// 삭제여부
}
