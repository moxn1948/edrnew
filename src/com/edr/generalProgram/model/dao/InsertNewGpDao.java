package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.member.model.dao.MemberDao;

public class InsertNewGpDao {
	Properties prop = new Properties();
	
	public InsertNewGpDao() {
		
		String fileName = MemberDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Author           : moun
	 * @CreateDate       : 2019. 12. 10.
	 * @Description      : 내 프로그램 신청 - 기본정보 insert : Gp 
	 * @param con
	 * @param reqGp		 : 사용자가 입력한 Gp 정보
	 * @return           :
	 */
	public int insertNewGp(Connection con, Gp reqGp) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertNewGp");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, reqGp.getGpName());			// 일반프로그램 명 1
			pstmt.setString(2, reqGp.getGpDescr());			// 일반프로그램 설명 2
			pstmt.setInt(3, reqGp.getGpMin());				// 최소인원 3
			pstmt.setInt(4, reqGp.getGpMax());				// 최대인원 4
			pstmt.setInt(5, reqGp.getGpCost());				// 프로그램 비용 5
			pstmt.setString(6, reqGp.getGpNotice());		// 일반 프로그램 공지 6
			pstmt.setString(7, reqGp.getGpMeetTime());		// 만나는 시간 7
			pstmt.setString(8, reqGp.getGpMeetArea());		// 만나는 장소 8
			pstmt.setInt(9, reqGp.getMno());				// 회원번호 9
			pstmt.setInt(10, reqGp.getLocalNo());
			pstmt.setDouble(11, reqGp.getGpMeetX());		// 만나는 장소 X 10
			pstmt.setDouble(12, reqGp.getGpMeetY());		// 만나는 장소 Y 11
			pstmt.setInt(13, reqGp.getGpTday());			// 총기간 12
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
				
		
		return result;
	}

	/**
	 * @Author           : moun
	 * @CreateDate       : 2019. 12. 11.
	 * @Description      : 현재 게시글 시퀀스 객체 번호 
	 * @param con
	 * @return           : 현재 GP 시퀀스 번호
	 */
	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int gpNo = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				gpNo = rset.getInt("CURRVAL");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return gpNo;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 11.
	 * @Description      : 새 프로그램 신청 - 포함/불포함 사항 insert
	 * @param con
	 * @param priceDetailList
	 * @return         : 성공 시 1 반환
	 */
	public int insertGpPriceDetail(Connection con, GpPriceDetail priceDetail) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertGpPriceDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, priceDetail.getGpCategory());
			pstmt.setInt(2, priceDetail.getGpNo());
			pstmt.setInt(3, priceDetail.getGpPrice());
			pstmt.setString(4, priceDetail.getGpInc());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 11.
	 * @Description      : 프로그램 등록 - 프로그램 상세 내용 insert
	 * @param con
	 * @param gpDetail
	 * @return         :
	 */
	public int insertGpDetail(Connection con, GpDetail gpDetail) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertGpDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpDetail.getGpNo());
			pstmt.setInt(2, gpDetail.getGpDay());
			pstmt.setInt(3, gpDetail.getGpDaySeq());
			pstmt.setString(4, gpDetail.getGpLocation());
			pstmt.setInt(5, gpDetail.getGpTime());
			pstmt.setString(6, gpDetail.getGpCnt());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * @Author           : moun
	 * @CreateDate       : 2019. 12. 11.
	 * @Description      : 프로그램 등록 - 첨부파일
	 * @param con
	 * @param attachment
	 * @return         :
	 */
	public int insertGpFile(Connection con, Attachment attachment) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertGpFile");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, attachment.getOriginName());
			pstmt.setString(2, attachment.getChangeName());
			pstmt.setString(3, attachment.getFilePath());
			pstmt.setString(4, "GP_DETAIL");
			pstmt.setInt(5, attachment.getGpNo());
			pstmt.setInt(6, attachment.getGpDay());
			pstmt.setInt(7, attachment.getGpDateSeq());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int insertGpLangList(Connection con, GpLang gpLang) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertGpLangList");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, gpLang.getLang());
			pstmt.setInt(2, gpLang.getGpNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

}
