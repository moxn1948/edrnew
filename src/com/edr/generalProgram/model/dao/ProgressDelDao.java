package com.edr.generalProgram.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import static com.edr.common.JDBCTemplate.*;

public class ProgressDelDao {
	Properties prop = new Properties();

	public ProgressDelDao() {
		String fileName = ProgressGpListDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int progressDel(Connection con, int pno) {
		PreparedStatement pstmt = null;
		int result = 0;
 		String query = prop.getProperty("progressDel");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pno);
			
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

}
