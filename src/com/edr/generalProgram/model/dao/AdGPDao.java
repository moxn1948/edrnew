package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.member.model.dao.MemberAdSelectDao;
import com.edr.member.model.vo.Member;
import com.edr.order.model.vo.OrderDetail;

public class AdGPDao {
	Properties prop = new Properties();

	public AdGPDao() {

		String fileName = AdGPDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list select Dao
	 * @param con
	 * @return : list
	 */
	public ArrayList<Gp> selectGPList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Gp> list = null;

		String query = prop.getProperty("selectGPList");

		try {
			stmt = con.createStatement();

			rset = stmt.executeQuery(query);

			list = new ArrayList<>();

			while (rset.next()) {
				Gp g = new Gp();
				g.setGpNo(rset.getInt("GP_NO"));
				g.setGpName(rset.getString("GP_NAME"));
				g.setGpDescr(rset.getString("GP_DESCR"));
				g.setGpMin(rset.getInt("GP_MIN"));
				g.setGpMax(rset.getInt("GP_MAX"));
				g.setGpCost(rset.getInt("GP_COST"));
				g.setGpNotice(rset.getString("GP_NOTICE"));
				g.setGpMeetTime(rset.getString("GP_MEET_TIME"));
				g.setGpMeetArea(rset.getString("GP_MEET_AREA"));
				// g.setOpenYn(rset.getString("OPEN_YN"));
				g.setAllowYn(rset.getString("ALLOW_YN"));
				g.setMno(rset.getInt("MNO"));
				g.setLocalNo(rset.getInt("LOCAL_NO"));
				g.setGpMeetX(rset.getInt("GP_MEET_X"));
				g.setGpMeetY(rset.getInt("GP_MEET_Y"));

				list.add(g);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list count Dao
	 * @param con
	 * @return : listCount
	 */
	public int getlistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("listCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			close(stmt);
			close(rset);

		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list select paging Dao
	 * @param con,
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectListWithPaging(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("selectListWithPaging");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				// hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("gname", rset.getString("GNAME"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list select Dao
	 * @param con
	 * @return : list
	 */
	public ArrayList<Gp> selectGPNoList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Gp> list = null;

		String query = prop.getProperty("selectGPNoList");

		try {
			stmt = con.createStatement();

			rset = stmt.executeQuery(query);

			list = new ArrayList<>();

			while (rset.next()) {
				Gp g = new Gp();
				g.setGpNo(rset.getInt("GP_NO"));
				g.setGpName(rset.getString("GP_NAME"));
				g.setGpDescr(rset.getString("GP_DESCR"));
				g.setGpMin(rset.getInt("GP_MIN"));
				g.setGpMax(rset.getInt("GP_MAX"));
				g.setGpCost(rset.getInt("GP_COST"));
				g.setGpNotice(rset.getString("GP_NOTICE"));
				g.setGpMeetTime(rset.getString("GP_MEET_TIME"));
				g.setGpMeetArea(rset.getString("GP_MEET_AREA"));
				// g.setOpenYn(rset.getString("OPEN_YN"));
				g.setAllowYn(rset.getString("ALLOW_YN"));
				g.setMno(rset.getInt("MNO"));
				g.setLocalNo(rset.getInt("LOCAL_NO"));
				g.setGpMeetX(rset.getInt("GP_MEET_X"));
				g.setGpMeetY(rset.getInt("GP_MEET_Y"));

				list.add(g);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list count Dao
	 * @param con
	 * @param num
	 * @return : listCount
	 */
	public int getNolistCount(Connection con, int num) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("listNoCount");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			close(pstmt);
			close(rset);

		}

		return listCount;
	}

	/**
	 * @param num
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list paging Dao
	 * @param con,
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectNoListWithPaging(int num, Connection con, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

//		String query = prop.getProperty("selectNoListWithPaging");
		String query = prop.getProperty("selectNoListWithPagingMoxn");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				// hmap.put("mno", rset.getInt("MNO"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("gname", rset.getString("GNAME"));
				// hmap.put("openyn", rset.getString("OPEN_YN"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("rnum", rset.getInt("RNUM"));
				
				if(rset.getString("OSTATE") == null) {
					hmap.put("tourstate", "결제되지 않은 투어");
				}else {
					if (rset.getString("OSTATE").equals("WAITTOUR")) {
						hmap.put("tourstate", "대기중인 투어");
					} else if (rset.getString("OSTATE").equals("ENDTOUR")) {
						hmap.put("tourstate", "종료된 투어");
					}	
				}
			

				list.add(hmap);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Pname Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchPnameGPCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchPnameCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Pname Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameGP(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchPnameGP");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));				
				//hmap.put("mno", rset.getInt("MNO"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("gname", rset.getString("GNAME"));
				//hmap.put("openyn", rset.getString("OPEN_YN"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Name Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchNameGPCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameGPCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Name Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameGP(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameGP");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				//hmap.put("mno", rset.getInt("MNO"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("gname", rset.getString("GNAME"));
				//hmap.put("openyn", rset.getString("OPEN_YN"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 14.
	 * @Description : Ad GP list Detail Select Dao
	 * @param :
	 *            con, num
	 * @return : hmap
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneGP(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		String query = prop.getProperty("adSelectOneGP");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("lang", rset.getString("LANG"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("gpdescr", rset.getString("GP_DESCR"));
				hmap.put("gpmin", rset.getInt("GP_MIN"));
				hmap.put("gpmax", rset.getInt("GP_MAX"));
				hmap.put("gpday", rset.getInt("GP_DAY"));
				hmap.put("gpcnt", rset.getString("GP_CNT"));
				hmap.put("gptday", rset.getInt("GP_TDAY"));
				hmap.put("gpcost", rset.getInt("GP_COST"));
				hmap.put("gpnotice", rset.getString("GP_NOTICE"));
				hmap.put("gpmeettime", rset.getString("GP_MEET_TIME"));
				hmap.put("gpmeetarea", rset.getString("GP_MEET_AREA"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list Delete Dao
	 * @param :
	 *            con, num
	 * @return : result
	 */
	public int adDeleteGP(Connection con, int num) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("adDeleteGP");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list Select Detail Dao
	 * @param :
	 *            con, num
	 * @return : list
	 */

	public ArrayList<HashMap<String, Object>> adSelectOneGPNo(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		String query = prop.getProperty("adSelectOneGPNo");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("receperson", rset.getInt("RECE_PERSON"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("ordername", rset.getString("ORDER_NAME"));
				hmap.put("orderphone", rset.getString("ORDER_PHONE"));
				hmap.put("orderemail", rset.getString("ORDER_EMAIL"));
				hmap.put("recename", rset.getString("RECE_NAME"));

				if (rset.getString("RECE_GENDER").equals("M")) {
					hmap.put("recegender", "남성");
				} else {
					hmap.put("recegender", "여성");
				}

				hmap.put("recephone", rset.getString("RECE_PHONE"));
				hmap.put("recephone2", rset.getString("RECE_PHONE2"));
				hmap.put("receemail", rset.getString("RECE_EMAIL"));
				hmap.put("receage", rset.getInt("RECE_AGE"));
				hmap.put("recerequest", rset.getString("RECE_REQUEST"));
				hmap.put("gptday", rset.getInt("GP_TDAY"));
				hmap.put("gpcost", rset.getInt("GP_COST"));
				hmap.put("paycost", rset.getInt("PAY_COST"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP No list Search Pname Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchPnameGPNoCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchPnameGPNoCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP No list Search Pname Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameGPNo(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchPnameGPNo");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("tourstate", rset.getString("TOUR_STATE"));
				// hmap.put("gpno", rset.getInt("GP_NO"));
				// hmap.put("mno", rset.getInt("MNO"));
				// hmap.put("pno", rset.getInt("PNO"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP No list Search Name Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchNameGPNoCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameGPNoCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP No list Search Pname Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameGPNo(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameGPNo");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("tourstate", rset.getString("TOUR_STATE"));
				// hmap.put("gpno", rset.getInt("GP_NO"));
				// hmap.put("mno", rset.getInt("MNO"));
				// hmap.put("pno", rset.getInt("PNO"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	public ArrayList<GpPriceDetail> adSelectOneGPNoPriceDetail(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpPriceDetail> list = null;
		GpPriceDetail gpd = null;

		String query = prop.getProperty("adSelectOneGPNoPriceDetail");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<GpPriceDetail>();

			while (rset.next()) {
				gpd = new GpPriceDetail();
				gpd.setGpCategory(rset.getString("GP_CATEGORY"));
				gpd.setGpPrice(rset.getInt("GP_PRICE"));
				gpd.setGpInc(rset.getString("GP_INC"));

				list.add(gpd);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public ArrayList<GpPriceDetail> adSelectOneGPPriceDetail(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpPriceDetail> list = null;
		GpPriceDetail gpd = null;

		String query = prop.getProperty("adSelectOneGPPriceDetail");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<GpPriceDetail>();

			while (rset.next()) {
				gpd = new GpPriceDetail();
				gpd.setGpCategory(rset.getString("GP_CATEGORY"));
				gpd.setGpPrice(rset.getInt("GP_PRICE"));
				gpd.setGpInc(rset.getString("GP_INC"));

				list.add(gpd);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public HashMap<String, Object> adSelectOneGpNo(Connection con, int num, int pno) {
		HashMap<String, Object> epiDetail = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("adSelectOneGpNoMoxn");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, pno);
			
			rset = pstmt.executeQuery();
			
			epiDetail = new HashMap<>();
			 /*RECE_NAME, RECE_GENDER, RECE_PHONE, RECE_PHONE2, RECE_EMAIL, 
			  * RECE_AGE, RECE_REQUEST, RECE_PERSON, 
		       OL.ORDER_NO ONO, G.GP_TDAY TDAY, G.GP_NAME , EPI, GD.GNAME*/
			if(rset.next()) {
				OrderDetail od = new OrderDetail();
				
				od.setReceName(rset.getString("RECE_NAME"));
				od.setReceGender(rset.getString("RECE_GENDER"));
				od.setRecePhone(rset.getString("RECE_PHONE"));
				od.setRecePhone2(rset.getString("RECE_PHONE2"));
				od.setReceEmail(rset.getString("RECE_EMAIL"));
				od.setReceAge(rset.getString("RECE_AGE"));
				if(rset.getString("RECE_REQUEST") != null){
					od.setReceRequest(rset.getString("RECE_REQUEST"));
				}else {
					od.setReceRequest("요청사항 없음");
				}
				od.setRecPerson(rset.getInt("RECE_PERSON"));
				
				epiDetail.put("od", od);
				epiDetail.put("ono", rset.getInt("ONO"));
				epiDetail.put("tday", rset.getInt("TDAY"));
				epiDetail.put("gpname", rset.getString("GPNAME"));
				epiDetail.put("epi", rset.getInt("EPI"));
				epiDetail.put("gdname", rset.getString("GDNAME"));
				epiDetail.put("oname", rset.getString("ORDER_NAME"));
				epiDetail.put("ophone", rset.getString("ORDER_PHONE"));
				epiDetail.put("oemail", rset.getString("ORDER_EMAIL"));
				epiDetail.put("ocode", rset.getString("ORDER_CODE"));
				epiDetail.put("cost", rset.getString("PAY_COST"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		
		return epiDetail;
	}

}
