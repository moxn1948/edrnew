package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.dao.MemberDao;
/*
 * moxn
 * 프로그램 보기 Dao
*/
public class SelectAllGpListDao {

	Properties prop = new Properties();
	
	public SelectAllGpListDao() {
		
		String fileName = MemberDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 프로그램 보기에서 신규 추천 프로그램에서 기본적인 프로그램 내용 가져오는 메소드
	 * @param con
	 * @param localNo
	 * @param recomGpIdx
	 * @return         :
	 */
	public HashMap<String, Object> selectRecomGp(Connection con, int localNo) {
		
		HashMap<String, Object> recomGp = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectRecomGp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, localNo);
			
			rset = pstmt.executeQuery();
			
			recomGp = new HashMap<>();
			
			if(rset.next()) {
				Gp recomGpObj = new Gp();
				recomGpObj.setGpNo(rset.getInt("GPNNO"));
				recomGpObj.setGpName(rset.getString("GPNNAME"));
				recomGpObj.setGpDescr(rset.getString("GPNDESCR"));
				recomGpObj.setGpMin(rset.getInt("GPNMIN"));
				recomGpObj.setGpMax(rset.getInt("GPNMAX"));
				recomGpObj.setGpCost(rset.getInt("GPNCOST"));
				recomGpObj.setGpTday(rset.getInt("GPNTDAY"));
				recomGpObj.setMno(rset.getInt("GPNMNO"));
				
				GuideDetail recomGuideObj = new GuideDetail();
				recomGuideObj.setGname(rset.getString("NNAME"));
				
				Attachment recomAttaObj = new Attachment();
				recomAttaObj.setChangeName(rset.getString("CHANGE_NAME"));
				
				recomGp.put("recomGpObj", recomGpObj);
				recomGp.put("recomGuideObj", recomGuideObj);
				recomGp.put("recomAttaObj", recomAttaObj);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return recomGp;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 프로그램 보기에서 신규 프로그램 추천 - 리뷰 개수
	 * @param con
	 * @param localNo
	 * @param recomGpIdx
	 * @return         :
	 */
	public HashMap<String, Object> selectReviewCnt(Connection con, int no) {
		HashMap<String, Object> recomReviewCnt = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectReviewCnt");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, no);
			
			rset = pstmt.executeQuery();
			
			recomReviewCnt = new HashMap<>();
			
			if(rset.next()) {
				recomReviewCnt.put("recomReviewCount", rset.getInt(1));

				recomReviewCnt.put("recomReviewAvg", rset.getDouble(2));
			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return recomReviewCnt;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 해당 지역의 총 프로그램 개수 
	 * @param con
	 * @param localNo
	 * @return         :
	 */
	public int gpListCount(Connection con, int localNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;
		

		String query = prop.getProperty("gpListCount");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, localNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return listCount;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 해당 지역의 프로그램 목록 - 프로그램 내용 
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @param localNo
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> selectAllGp(Connection con, int currentPage, int limit, int localNo) {
		ArrayList<HashMap<String, Object>> gpList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectAllGp");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit -1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, localNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			gpList = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				Gp GpObj = new Gp();
				GpObj.setGpNo(rset.getInt("GPNNO"));
				GpObj.setGpName(rset.getString("GPNNAME"));
				GpObj.setGpMin(rset.getInt("GPNMIN"));
				GpObj.setGpMax(rset.getInt("GPNMAX"));
				GpObj.setGpCost(rset.getInt("GPNCOST"));
				GpObj.setGpTday(rset.getInt("GPNTDAY"));
				GpObj.setMno(rset.getInt("GPNMNO"));
				
				GuideDetail GuideObj = new GuideDetail();
				GuideObj.setGname(rset.getString("NNAME"));
				
				Attachment AttaObj = new Attachment();
				AttaObj.setChangeName(rset.getString("CHANGE_NAME"));
				

				map.put("GpObj", GpObj);
				map.put("GuideObj", GuideObj);
				map.put("AttaObj", AttaObj);
				
				gpList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 해당 지역의 프로그램 목록 - 리뷰 
	 * @param con
	 * @param selGpNo
	 * @return         :
	 */
	public HashMap<String, Object> selectAllReviewCnt(Connection con, int selGpNo) {
		HashMap<String, Object> allReviewCnt = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectAllReviewCnt");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, selGpNo);
			
			rset = pstmt.executeQuery();
			
			allReviewCnt = new HashMap<>();
			
			if(rset.next()) {
				
				allReviewCnt.put("reviewCount", rset.getInt(1));
				allReviewCnt.put("reviewAvg", rset.getDouble(2));
			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return allReviewCnt;
	}

}
