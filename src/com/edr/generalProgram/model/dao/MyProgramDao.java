package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.generalProgram.model.vo.Gp;
import com.edr.member.model.dao.MemberDao;

public class MyProgramDao {

	Properties prop = new Properties();
	
	public MyProgramDao() {
		
		String fileName = MemberDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 14.
	 * @Description      :  테이블 쪽 프로그램 목록 : 프로그램 명과 프로그램 번호 조회
	 * @param con
	 * @param mno
	 * @param mno2 
	 * @param limit 
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> selectMyProgramList(Connection con, int currentPage, int limit, int mno) {
		ArrayList<HashMap<String, Object>> programList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		

		String query = prop.getProperty("selectMyProgramList");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit -1;
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			programList = new ArrayList<>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				Gp myGp = new Gp();
				
				myGp.setGpNo(rset.getInt("GP_NO"));
				myGp.setGpName(rset.getString("GP_NAME"));
				
				map.put("myGp", myGp);
				
				map.put("idx", rset.getInt("IDXNUM"));
				
				programList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return programList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 15.
	 * @Description      : 내가 가진 프로그램 개수
	 * @param con
	 * @param mno
	 * @return         :
	 */
	public int myProgramCount(Connection con, int mno) {
		int listCount = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("myProgramCount");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> selectMyProgramAllList(Connection con, int mno, int listCount) {
		ArrayList<HashMap<String, Object>> programAllList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		

		String query = prop.getProperty("selectMyProgramList");
		
		int startRow = 1;
		int endRow = listCount;
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			programAllList = new ArrayList<>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				Gp myGp = new Gp();
				
				myGp.setGpNo(rset.getInt("GP_NO"));
				myGp.setGpName(rset.getString("GP_NAME"));
				
				map.put("myGp", myGp);
				
				programAllList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return programAllList;
	}

	public int insertMyProgram(Connection con, int gpNo, Date startDate) {
		int result = 0;
		PreparedStatement pstmt = null;

		String query = prop.getProperty("insertMyProgram");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			pstmt.setInt(2, gpNo);
			pstmt.setDate(3, startDate);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<Date> selectMyProgramDate(Connection con, int gpNo) {
		ArrayList<Date> myProgramDateList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectMyProgramDate");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			myProgramDateList = new ArrayList<>();
			
			while(rset.next()) {
				myProgramDateList.add(rset.getDate("PDATE"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return myProgramDateList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 선택한 프로그램의 총 날짜 
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public int selectMyProgramTday(Connection con, int gpNo) {
		int tday = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectMyProgramTday");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();

			if(rset.next()) {
				tday = rset.getInt("GP_TDAY");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return tday;
	}

	
}
