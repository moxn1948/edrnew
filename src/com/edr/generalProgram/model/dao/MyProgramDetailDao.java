package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.member.model.dao.MemberDao;
import com.edr.product.model.vo.Product;
import com.edr.review.model.vo.Review;

public class MyProgramDetailDao {
	Properties prop = new Properties();
	
	public MyProgramDetailDao() {
		
		String fileName = MemberDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 16.
	 * @Description      : 내프로그램 상세보기 - gp
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public Gp selectGpObj(Connection con, int gpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Gp gpObj = null;
		
		String query = prop.getProperty("selectGpObj");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpObj = new Gp();
			
			if(rset.next()) {
				
				gpObj.setGpNo(rset.getInt("GP_NO"));
				gpObj.setGpName(rset.getString("GP_NAME"));			// 일반프로그램 명 1
				gpObj.setGpDescr(rset.getString("GP_DESCR"));			// 일반프로그램 설명 2
				gpObj.setGpMin(rset.getInt("GP_MIN"));				// 최소인원 3
				gpObj.setGpMax(rset.getInt("GP_MAX"));				// 최대인원 4
				gpObj.setGpCost(rset.getInt("GP_COST"));				// 프로그램 비용 5
				gpObj.setGpNotice(rset.getString("GP_NOTICE"));		// 일반 프로그램 공지 6
				gpObj.setGpMeetTime(rset.getString("GP_MEET_TIME"));		// 만나는 시간 7
				gpObj.setGpMeetArea(rset.getString("GP_MEET_AREA"));		// 만나는 장소 8
				gpObj.setMno(rset.getInt("MNO"));				// 회원번호 9
				gpObj.setLocalNo(rset.getInt("LOCAL_NO"));
				gpObj.setGpMeetX(rset.getDouble("GP_MEET_X"));		// 만나는 장소 X 10
				gpObj.setGpMeetY(rset.getDouble("GP_MEET_Y"));		// 만나는 장소 Y 11
				gpObj.setGpTday(rset.getInt("GP_TDAY"));			// 총기간 12
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpObj;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 상세 조회 - 가격 
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public ArrayList<GpPriceDetail> selectGpPriceList(Connection con, int gpNo) {
		ArrayList<GpPriceDetail> gpPriceList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectGpPriceList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpPriceList = new ArrayList<>();
			
			while(rset.next()) {

				GpPriceDetail gpPrice = new GpPriceDetail();
				
				gpPrice.setGpCategory(rset.getString("GP_CATEGORY"));
				gpPrice.setGpNo(gpNo);
				gpPrice.setGpPrice(rset.getInt("GP_PRICE"));
				gpPrice.setGpInc(rset.getString("GP_INC"));

				gpPriceList.add(gpPrice);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpPriceList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 상세조회 - 일정
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public ArrayList<GpDetail> selectGpDetailList(Connection con, int gpNo) {
		ArrayList<GpDetail> gpDetailList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectGpDetailList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpDetailList = new ArrayList<>();
			
			while(rset.next()) {

				GpDetail gpDetail = new GpDetail();
				
				
				gpDetail.setGpNo(gpNo);
				gpDetail.setGpDay(rset.getInt("GP_DAY"));
				gpDetail.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
				gpDetail.setGpLocation(rset.getString("GP_LOCATION"));
				gpDetail.setGpTime(rset.getInt("GP_TIME"));
				gpDetail.setGpCnt(rset.getString("GP_CNT"));

				gpDetailList.add(gpDetail);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpDetailList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 상세조회 - 첨뷰파일
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public ArrayList<Attachment> selectAttachmentlList(Connection con, int gpNo) {
		ArrayList<Attachment> gpAttachmentList  =  null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectAttachmentList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpAttachmentList = new ArrayList<>();
			
			while(rset.next()) {

				Attachment gpAttachment = new Attachment();
				
				gpAttachment.setFileNo(rset.getInt("FILE_NO"));
				gpAttachment.setOriginName(rset.getString("ORIGIN_NAME"));
				gpAttachment.setChangeName(rset.getString("CHANGE_NAME"));
				gpAttachment.setFilePath(rset.getString("FILE_PATH"));
				gpAttachment.setTtype(rset.getString("TTYPE"));
				gpAttachment.setGpNo(gpNo);
				
				gpAttachmentList.add(gpAttachment);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpAttachmentList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 상세조회 - 언어
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public ArrayList<GpLang> selectGpLangList(Connection con, int gpNo) {
		ArrayList<GpLang> gpLangList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectLangList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpLangList = new ArrayList<>();
			
			while(rset.next()) {

				GpLang gpLang = new GpLang();
				
				gpLang.setLang(rset.getString("LANG"));
				gpLang.setGpNo(rset.getInt("GP_NO"));
				
				gpLangList.add(gpLang);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpLangList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 - 공지사항 수정
	 * @param con
	 * @param gpNo
	 * @param gpNotice
	 * @return         :
	 */
	public int updateMyProgramNotice(Connection con, int gpNo, String gpNotice) {
		int result = 0;
		PreparedStatement pstmt = null;

		String query = prop.getProperty("updateMyProgramNotice");
		

		try {
			pstmt = con.prepareStatement(query);
			
			System.out.println(query);
			
			pstmt.setString(1, gpNotice);
			pstmt.setInt(2, gpNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 18.
	 * @Description      : 프로그램 상세보기 - 리뷰 총 카운트
	 * @param con
	 * @param gpNo
	 * @return         :
	 */
	public HashMap<String, Object> selectGpReviewCnt(Connection con, int gpNo) {
		HashMap<String, Object> gpReviewCnt = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectReviewCnt");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpReviewCnt = new HashMap<>();
			
			if(rset.next()) {
				gpReviewCnt.put("reviewCount", rset.getInt(1));
				gpReviewCnt.put("reviewAvg", rset.getDouble(2));
			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpReviewCnt;
	}

	public ArrayList<Product> selectGpProductList(Connection con, int gpNo) {
		ArrayList<Product> gpProductList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectGpProductList");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			gpProductList = new ArrayList<>();
			
			while(rset.next()) {
				Product pd = new Product();
				
				pd.setPno(rset.getInt(1));
				pd.setPdate(rset.getDate("PDATE"));
				
				gpProductList.add(pd);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return gpProductList;
	}

	public String selectGuideName(Connection con, int mno) {
		String guideName = "";
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectGuideName");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				guideName = rset.getString("GNAME");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return guideName;
	}

	public ArrayList<HashMap<String, Object>> selectMainReviewCnt(Connection con, int gpNo) {
		ArrayList<HashMap<String, Object>> mainReviewCnt = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMainReviewCnt");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gpNo);
			
			rset = pstmt.executeQuery();
			
			mainReviewCnt = new ArrayList<>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				Review rv = new Review();
				
				rv.setReviewNo(rset.getInt("REVIEW_NO"));
				rv.setGuideRating(rset.getDouble("GUIDE_RATING"));
				rv.setProgramRating(rset.getDouble("PROGRAM_RATING"));
				rv.setReviewCnt(rset.getString("REVIEW_CNT"));
				rv.setReplyYn(rset.getString("REPLY_YN"));
				rv.setOrderDetailNo(rset.getInt("ORDER_DETAIL_NO"));
				rv.setMno(rset.getInt("MNO"));
				rv.setGpNo(rset.getInt("GP_NO"));
				rv.setReviewYn(rset.getString("REVIEW_YN"));
				
				
				map.put("reviewCnt", rv);
				map.put("reviewName", rset.getString("MNAME"));
				
				mainReviewCnt.add(map);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return mainReviewCnt;
	}

	public String selectMainReviewGuideName(Connection con, int mno) {
		String guideName = "";

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMainReviewGuideName");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				guideName = rset.getString("GNAME");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return guideName;
	}

	public String selectMainReviewMemberName(Connection con, int mno) {
		String memberName = "";
	
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMainReviewMemberName");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				memberName = rset.getString("NICKNAME");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return memberName;
	}
	


}
