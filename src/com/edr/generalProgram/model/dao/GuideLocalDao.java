package com.edr.generalProgram.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.edr.common.model.vo.Local;
import com.edr.member.model.dao.MemberDao;
import static com.edr.common.JDBCTemplate.close;

public class GuideLocalDao {

	Properties prop = new Properties();
	
	public GuideLocalDao() {
		
		String fileName = MemberDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 가이드의 활동 지역 가져오는 것 
	 * @param con		 : Connection
	 * @param mno		 : 회원 번호
	 * @return           : 활동 지역 리스트
	 */
	public ArrayList<Local> selectGuideLocal(Connection con, int mno) {
		ArrayList<Local> localList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectGuideLocal");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			localList = new ArrayList<>();
			
			while(rset.next()) {
				Local l = new Local();
				l.setLocalNo(rset.getInt(1));
				l.setLocalName(rset.getString(2));
				
				localList.add(l);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return localList;
	}
}
