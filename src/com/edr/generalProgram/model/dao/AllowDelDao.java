package com.edr.generalProgram.model.dao;

import java.io.FileReader;

import static com.edr.common.JDBCTemplate.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class AllowDelDao {
	Properties prop = new Properties();

	public AllowDelDao() {
		String fileName = ProgressGpListDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
	public int allowDel(Connection con, int mno, int gpno) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("allowDel");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, gpno);
			
			result = pstmt.executeUpdate();
			System.out.println("pstmt : " + pstmt);
			System.out.println("result :" + result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}


		return result;
	}

}
