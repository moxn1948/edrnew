package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class AllowListDao {
Properties prop = new Properties();
	
	public AllowListDao() {
		String fileName = ProgressGpListDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @param mno 
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> allowListWithPaging(Connection con, int currentPage, int limit, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("allowList");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
		
			while (rset.next()) {
				hmap = new HashMap<>();
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpName", rset.getString("GP_NAME"));				
				hmap.put("gpRday", rset.getDate("GP_RDAY"));				
				hmap.put("allow", rset.getString("ALLOW_YN"));
				
				list.add(hmap);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	public int allowCount(Connection con, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("allowCount");
		//System.out.println("query : " + query);

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			
			if (rset.next()) {
			listCount = rset.getInt(1);

			}
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}
		return listCount;
	}

}
