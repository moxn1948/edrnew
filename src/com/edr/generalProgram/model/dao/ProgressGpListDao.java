package com.edr.generalProgram.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import static com.edr.common.JDBCTemplate.*;



public class ProgressGpListDao {
	Properties prop = new Properties();
	
	public ProgressGpListDao() {
		String fileName = ProgressGpListDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}

	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 13.
	 * @Description      : 
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @param mno 
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> ProgressWithPaging(Connection con, int currentPage, int limit, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("processList");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("gpno", rset.getInt("GP_NO"));				
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("epi", rset.getInt("EPI"));
				hmap.put("pno", rset.getInt("PNO"));
				hmap.put("gpname", rset.getString("GP_NAME"));				
				hmap.put("pdate", rset.getDate("PDATE"));
				hmap.put("paystate", rset.getString("PAY_STATE"));
				
				list.add(hmap);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	public int progressListCount(Connection con, int mno) {
		int progressListCount = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("progressListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				progressListCount = rset.getInt(1);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
			close(pstmt);
			close(rset);
		}
		
		
		return progressListCount;
	}
	

}
