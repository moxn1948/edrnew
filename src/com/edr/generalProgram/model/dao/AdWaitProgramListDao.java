package com.edr.generalProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

public class AdWaitProgramListDao {

	private Properties prop = new Properties();

	public AdWaitProgramListDao() {
		String fileName = AdWaitProgramListDao.class.getResource("/sql/generalProgram/generalProgram-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 신청대기프로그램의 갯수
	 * @param con
	 * @return         : 신청대기프로그램의 갯수
	 */
	public int adwaitProgramListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adwaitProgramListCount");
		//.println("query : " + query);

		try {

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}
		return listCount;
	}

	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 신청 대기프로그램 목록 가져오기
	 * @param con
	 * @param currentPage
	 * @param limit
	 * @return         :신청 대기프로그램 목록 리턴
	 */
	public ArrayList<HashMap<String, Object>> adSelectWaitProgram(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adWaitProgram");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;


		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();


			list = new ArrayList<HashMap<String, Object>>();
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("name", rset.getString("GNAME"));
				hmap.put("gpName", rset.getString("GP_NAME"));
				hmap.put("cost", rset.getInt("GP_COST"));

				list.add(hmap);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}


		return list;
	}

	public int adsearchNameWait(Connection con, String waitSearch) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameWaitCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, waitSearch);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchNameList(Connection con, String waitSearch, int currentPage,
			int limit) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameWaitProgramList");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;


		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, waitSearch);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("name", rset.getString("GNAME"));
				hmap.put("gpName", rset.getString("GP_NAME"));
				hmap.put("cost", rset.getInt("GP_COST"));

				list.add(hmap);

			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}

	public int adSearchIdWait(Connection con, String waitSearch) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchIdWaitCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, waitSearch);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return listCount;


	}

	public ArrayList<HashMap<String, Object>> adSearcIdList(Connection con, String waitSearch, int currentPage,
			int limit) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		ResultSet rset = null; 

		String query = prop.getProperty("adSearchIdWaitProgramList");
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;


		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, waitSearch);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("gpno", rset.getInt("GP_NO"));
				hmap.put("id", rset.getString("EMAIL"));
				hmap.put("name", rset.getString("GNAME"));
				hmap.put("gpName", rset.getString("GP_NAME"));
				hmap.put("cost", rset.getInt("GP_COST"));

				list.add(hmap);
			}





		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}

	public ArrayList<HashMap<String, Object>> adSelectOneWait(Connection con, int num) {
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		ArrayList<GpDetail> listGp = null;
		GpDetail gd = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;


		String query = prop.getProperty("adSelectOneWait");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,num);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				listGp = new ArrayList<GpDetail>();

				hmap.put("gname", rset.getString("GNAME"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("proName", rset.getString("GP_NAME"));
				hmap.put("gpDescr", rset.getString("GP_DESCR"));
				hmap.put("gpmin", rset.getInt("GP_MIN"));
				hmap.put("gpmax", rset.getInt("GP_MAX"));
				hmap.put("gpTday", rset.getInt("GP_TDAY"));
				hmap.put("gpNotice", rset.getString("GP_NOTICE"));
				hmap.put("meetTime", rset.getString("GP_MEET_TIME"));
				hmap.put("meetArea", rset.getString("GP_MEET_AREA"));
				list.add(hmap);

			}
			



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}

	public ArrayList<GpLang> adSelectOneWaitSer(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpLang> listLang = null;
		GpLang gl = null;

		String query = prop.getProperty("adSelectLang");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			listLang = new ArrayList<GpLang>();

			while(rset.next()) {
				gl = new GpLang();	
				gl.setLang(rset.getString("LANG"));
				listLang.add(gl);
			}



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		
		return listLang;
	}

	public ArrayList<GpDetail> adSelectOneGpDetail(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpDetail> list = null;
		GpDetail gd = null;
		
		String query = prop.getProperty("adSelectGpDetail");
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();
			list = new ArrayList<GpDetail>();
			
			while(rset.next()) {
				gd = new GpDetail();
				gd.setGpNo(rset.getInt("GP_NO"));
				gd.setGpDay(rset.getInt("GP_DAY"));
				gd.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
				gd.setGpCnt(rset.getString("GP_CNT"));
				
				list.add(gd);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return list;
	}

	public ArrayList<GpPriceDetail> adSelectPrice(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpPriceDetail> list = null;
		GpPriceDetail gpd = null;
		
		String query = prop.getProperty("adSelectOnePrice");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<GpPriceDetail>();
			
			while(rset.next()) {
				gpd = new GpPriceDetail();
				gpd.setGpCategory(rset.getString("GP_CATEGORY"));
				gpd.setGpPrice(rset.getInt("GP_PRICE"));
				gpd.setGpInc(rset.getString("GP_INC"));
				
				list.add(gpd);
			
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return list;
	}

	public int adWaitProgramApproval(Connection con, int num, String appro) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("adWaitProgramAppro"); 
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, appro);
			pstmt.setInt(2, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}

	/*public int adWaitProgramNotApproval(Connection con, int num, String appro) {
		
	}*/

	public ArrayList<HashMap<String, Object>> adSelectOneGpDetailMoon(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("adSelectGpDetail");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("gpNo", rset.getInt("GP_NO"));
				hmap.put("gpDay", rset.getInt("GP_DAY"));
				hmap.put("gpDaySeq", rset.getInt("GP_DAY_SEQ"));
				hmap.put("gpCnt", rset.getString("GP_CNT"));
				hmap.put("gpTime", rset.getInt("GP_TIME"));
				hmap.put("gpDescr", rset.getString("GP_DESCR"));
				hmap.put("gpLocation", rset.getString("GP_LOCATION"));
				
				list.add(hmap);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return list;
	}

	public int adWaitProgramNotApprovalN(Connection con, int num, String appro) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("adWaitProgramNotApproN"); 
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}

}
