package com.edr.generalProgram.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.generalProgram.model.dao.SelectAllGpListDao;
import com.edr.generalProgram.model.vo.Gp;

import static com.edr.common.JDBCTemplate.*;

public class SelectAllGpListService {

	public HashMap<String, Object> selectRecomGp(int localNo) {
		Connection con = getConnection();
		
		HashMap<String, Object> recomGp = new SelectAllGpListDao().selectRecomGp(con, localNo);
		close(con);
		
		return recomGp;
	}
 
	public HashMap<String, Object> selectReviewCnt(int no) {
		Connection con = getConnection();
		
		HashMap<String, Object> recomReviewCnt = new SelectAllGpListDao().selectReviewCnt(con, no);
		
		close(con);
		
		return recomReviewCnt;
	}

	public int gpListCount(int localNo) {
		Connection con = getConnection();

		int listCount = new SelectAllGpListDao().gpListCount(con, localNo);

		close(con);
	
		return listCount;
	}

	public HashMap<String, Object> selectGpListWithPaging(int currentPage, int limit, int localNo) {
		Connection con = getConnection();
		
		HashMap<String, Object> list = new HashMap<>();
		
		ArrayList<HashMap<String, Object>> gpList = new SelectAllGpListDao().selectAllGp(con, currentPage, limit, localNo);
		
		int selGpNo[] = new int[9];
		
		for (int i = 0; i < gpList.size(); i++) {
			selGpNo[i] = ((Gp) gpList.get(i).get("GpObj")).getGpNo();
		}
		
		list.put("gpList", gpList);
		
		ArrayList<HashMap<String, Object>> selectAllReviewCnt = new ArrayList<>();
		
		
		for (int i = 0; i < gpList.size(); i++) {
			HashMap<String, Object> reviewCnt = new SelectAllGpListDao().selectAllReviewCnt(con, selGpNo[i]);
			
			selectAllReviewCnt.add(reviewCnt);
		}

		list.put("selectAllReviewCnt", selectAllReviewCnt);
		
		
		close(con);
		
		return list;
		
	}
		

}
