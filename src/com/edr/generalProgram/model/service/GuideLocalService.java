package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;

import com.edr.common.model.vo.Local;
import com.edr.generalProgram.model.dao.GuideLocalDao;

public class GuideLocalService {

	/**
	 * @Author           : moun
	 * @CreateDate       : 2019. 12. 9.
	 * @Description      : 가이드의 활동 지역 가져오는 것 
	 * @param mno		 : 회원 번호
	 * @return           : 활동 지역 리스트
	 */
	public ArrayList<Local> selectGuideLocal(int mno) {
		Connection con = getConnection();
		
		ArrayList<Local> localList = new GuideLocalDao().selectGuideLocal(con, mno);
		
		close(con);
		
		return localList;
	}

}
