package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.dao.MyProgramDetailDao;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.product.model.vo.Product;
import com.edr.review.model.vo.Review;

public class MyProgramDetailService {

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 한 개 조회
	 * @param gpNo
	 * @return         :
	 */
	public HashMap<String, Object> selectMyProgramDetail(int gpNo) {
		Connection con = getConnection();

		HashMap<String, Object> gpDetail = new HashMap<>();
		
		Gp gpObj = new MyProgramDetailDao().selectGpObj(con, gpNo);
		ArrayList<GpPriceDetail> gpPriceList  =  new MyProgramDetailDao().selectGpPriceList(con, gpNo);
		ArrayList<GpDetail> gpDetailList  =  new MyProgramDetailDao().selectGpDetailList(con, gpNo);
		ArrayList<Attachment> gpAttachmentList  =  new MyProgramDetailDao().selectAttachmentlList(con, gpNo);
		ArrayList<GpLang> gpLangList  =  new MyProgramDetailDao().selectGpLangList(con, gpNo);
		HashMap<String, Object> gpReviewCnt =  new MyProgramDetailDao().selectGpReviewCnt(con, gpNo);
		ArrayList<Product> gpProductList =  new MyProgramDetailDao().selectGpProductList(con, gpNo);
		String guideName =  new MyProgramDetailDao().selectGuideName(con, gpObj.getMno());
		
		ArrayList<HashMap<String, Object>> mainReviewCnt = new MyProgramDetailDao().selectMainReviewCnt(con, gpNo); 
		// ArrayList<String> mainReviewName = new ArrayList<>();; 
		
		
	
		
		gpDetail.put("gpObj", gpObj);
		gpDetail.put("gpPriceList", gpPriceList);
		gpDetail.put("gpDetailList", gpDetailList);
		gpDetail.put("gpAttachmentList", gpAttachmentList);
		gpDetail.put("gpLangList", gpLangList);
		gpDetail.put("gpReviewCnt", gpReviewCnt);
		gpDetail.put("gpProductList", gpProductList);
		gpDetail.put("guideName", guideName);
		gpDetail.put("mainReviewCnt", mainReviewCnt);
		//gpDetail.put("mainReviewCnt", mainReviewCnt);
		//gpDetail.put("mainReviewName", mainReviewName);
//		
//		System.out.println(mainReviewCnt);
//		System.out.println(mainReviewName);
		
		return gpDetail;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 내 프로그램 공지사항 업데이트
	 * @param gpNo
	 * @param gpNotice
	 * @return         :
	 */
	public int updateMyProgramNotice(int gpNo, String gpNotice) {
		Connection con = getConnection();
		
		int result = new MyProgramDetailDao().updateMyProgramNotice(con, gpNo, gpNotice);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}
	
}
