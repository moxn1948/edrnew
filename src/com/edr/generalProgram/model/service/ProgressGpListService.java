package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.generalProgram.model.dao.AdWaitProgramListDao;
import com.edr.generalProgram.model.dao.ProgressGpListDao;

public class ProgressGpListService {


	public int progressListCount(int mno) {
		Connection con = getConnection();
		
		int progressListCount = new ProgressGpListDao().progressListCount(con, mno);
		
		close(con);
		return progressListCount;
	}
 
	public ArrayList<HashMap<String, Object>> ProgressWithPaging(int currentPage, int limit, int mno) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new ProgressGpListDao().ProgressWithPaging(con, currentPage, limit, mno);


		close(con);

		return list;
	}

}
