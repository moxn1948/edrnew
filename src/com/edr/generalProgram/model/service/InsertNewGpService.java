package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.dao.InsertNewGpDao;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

public class InsertNewGpService {

	/**
	 * @Author         : moun
	 * @CreateDate     : 2019. 12. 10.
	 * @Description    : 새 프로그램 신청하는 메소드 
	 * @param reqMap   : 클라이언트가 작성한 모든 값
	 * @return         : 성공여부
	 */
	public int insertNewGp(HashMap<String, Object> reqMap) {
		Connection con = getConnection();
		
		int totalResult = 0;
		
		Gp reqGp = (Gp) reqMap.get("reqGp");
		
		ArrayList<GpLang> langList = (ArrayList<GpLang>) reqMap.get("langList");
		
		ArrayList<GpPriceDetail> priceDetailList = (ArrayList<GpPriceDetail>) reqMap.get("priceDetailList");
		
		ArrayList<GpDetail> detailList =(ArrayList<GpDetail>) reqMap.get("detailList");
		
		ArrayList<Attachment> fileList = (ArrayList<Attachment>) reqMap.get("fileList");
		
		
		int result1 = new InsertNewGpDao().insertNewGp(con, reqGp);
		
		if(result1 > 0) {
			int gpNo = new InsertNewGpDao().selectCurrval(con);
			
			for (int i = 0; i < langList.size(); i++) {
				langList.get(i).setGpNo(gpNo);
			}
			for (int i = 0; i < priceDetailList.size(); i++) {
				priceDetailList.get(i).setGpNo(gpNo);
			}
			for (int i = 0; i < detailList.size(); i++) {
				detailList.get(i).setGpNo(gpNo);
			}
			for (int i = 0; i < fileList.size(); i++) {
				fileList.get(i).setGpNo(gpNo);
			}
		}

		int result2 = 0;
		
		for (int i = 0; i < langList.size(); i++) {
			
			result2 += new InsertNewGpDao().insertGpLangList(con, langList.get(i));
		}
		
		int result3 = 0;
		
		for (int i = 0; i < priceDetailList.size(); i++) {
			
			result3 += new InsertNewGpDao().insertGpPriceDetail(con, priceDetailList.get(i));
		}
		
		int result4 = 0;
		
		for (int i = 0; i < detailList.size(); i++) {
			result4 += new InsertNewGpDao().insertGpDetail(con, detailList.get(i));
		}
		
		int result5 = 0;
		
		for (int i = 0; i < fileList.size(); i++) {
			result5 += new InsertNewGpDao().insertGpFile(con, fileList.get(i));
		}
		
		if(result1 > 0 && result2 > 0 && result3 > 0 && result4 > 0 && result5 > 0) {
			commit(con);
			totalResult = 1;
		}else {
			rollback(con);
		}
		
		close(con);
		
		return totalResult;
	}

}
