package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.generalProgram.model.dao.AllowListDao;


public class AllowListService {

	public ArrayList<HashMap<String, Object>> allowListWithPaging(int currentPage, int limit, int mno) {
Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AllowListDao().allowListWithPaging(con, currentPage, limit, mno);
		
		close(con);
		
		return list;
	}
 
	public int allowCount(int mno) {
		Connection con = getConnection();
		
		int allowCount = new AllowListDao().allowCount(con, mno);
		
		close(con);
		
		return allowCount;
	}


	

}
