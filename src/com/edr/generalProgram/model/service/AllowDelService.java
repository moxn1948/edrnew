package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.rollback;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;

import com.edr.generalProgram.model.dao.AllowDelDao;

public class AllowDelService {

	public int allowDel(int mno, int gpno) {
		Connection con = getConnection();
		int result = new AllowDelDao().allowDel(con, mno, gpno);
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
	 	
		return result;
	}

}
