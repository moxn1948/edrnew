package com.edr.generalProgram.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import static com.edr.common.JDBCTemplate.*;

import com.edr.generalProgram.model.dao.AdGPDao;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;


public class AdGPService {
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list select Service
	 * @param
	 * @return : list
	 */
	public ArrayList<Gp> selectGPList() {
		Connection con = getConnection();

		ArrayList<Gp> list = new AdGPDao().selectGPList(con);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list Count Service
	 * @param
	 * @return : result
	 */
	public int getListCount() {

		Connection con = getConnection();

		int result = new AdGPDao().getlistCount(con);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP list Paging Service
	 * @param :
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectListWithPaging(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().selectListWithPaging(con, currentPage, limit);

		close(con);

		return list;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list select Service
	 * @param
	 * @return : list
	 */
	public ArrayList<Gp> selectGPNoList() {
		Connection con = getConnection();

		ArrayList<Gp> list = new AdGPDao().selectGPNoList(con);

		close(con);

		return list;
	}

	/**
	 * @param num
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list count Service
	 * @param :
	 *            con
	 * @return : listCount
	 */
	public int getNoListCount(int num) {
		Connection con = getConnection();

		int listCount = new AdGPDao().getNolistCount(con, num);

		if (listCount > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return listCount;
	}

	/**
	 * @param num
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : GP No list paging Service
	 * @param :
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> selectNoListWithPaging(int num, int currentPage, int limit) {

		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().selectNoListWithPaging(num, con, currentPage, limit);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Pname Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchPnameGPCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdGPDao().adSearchPnameGPCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Pname Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameGP(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSearchPnameGP(con, currentPage, limit, searchText);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Name Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchNameGPCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdGPDao().adSearchNameGPCount(con, searchText);

		close(con);

		return listCount;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad GP list Search Name Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameGP(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSearchNameGP(con, currentPage, limit, searchText);

		close(con);

		return list;
	}

	/**
	 * @param num
	 * @Author : young il
	 * @CreateDate : 2019. 12. 14.
	 * @Description : Ad GP list Select Detail Service
	 * @param :
	 *            num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneGP(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSelectOneGP(con, num);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 14.
	 * @Description : Ad GP list Delete Service
	 * @param :
	 *            g
	 * @return : result
	 */
	public int adDeleteGP(int num) {
		Connection con = getConnection();

		int result = new AdGPDao().adDeleteGP(con, num);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 14.
	 * @Description : Ad GP list No Select Detail Service
	 * @param :
	 *            num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneGPNo(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSelectOneGPNo(con, num);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list No Search Pname Count Service
	 * @param :
	 *            num
	 * @return : listCount
	 */

	public int adSearchPnameGPNoCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdGPDao().adSearchPnameGPNoCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list No Search Pname Service
	 * @param :
	 *            num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameGPNo(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSearchPnameGPNo(con, currentPage, limit, searchText);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list No Search Name Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchNameGPNoCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdGPDao().adSearchNameGPNoCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad GP list No Search Name Service
	 * @param :
	 *            num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameNoGP(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdGPDao().adSearchNameGPNo(con, currentPage, limit, searchText);

		close(con);

		return list;
	}

	

	public ArrayList<GpPriceDetail> adSelectOneGPNoPriceDetail(int num) {
		Connection con = getConnection();
		ArrayList<GpPriceDetail> list = new AdGPDao().adSelectOneGPNoPriceDetail(con, num);
		close(con);

		return list;
	}

	public ArrayList<GpPriceDetail> adSelectOneGPPriceDetail(int num) {
		Connection con = getConnection();
		ArrayList<GpPriceDetail> list = new AdGPDao().adSelectOneGPPriceDetail(con, num);
		close(con);

		return list;
	}

	public HashMap<String, Object> adSelectOneGpNo(int num, int pno) {
		Connection con = getConnection();
		
		HashMap<String, Object> epiDetail = new AdGPDao().adSelectOneGpNo(con, num, pno);
		
		close(con);
		
		return epiDetail;
	}

}
