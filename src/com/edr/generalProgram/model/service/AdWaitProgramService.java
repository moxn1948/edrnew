package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.*;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.generalProgram.model.dao.AdWaitProgramListDao;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;
import com.edr.generalProgram.model.vo.GpPriceDetail;

public class AdWaitProgramService {


	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 승인대기프로그램 갯수
	 * @return         :승인대기프로그램 갯수
	 */
	public int adWaitProgramCount() {
		Connection con = getConnection();

		int listCount = new AdWaitProgramListDao().adwaitProgramListCount(con);

		close(con);

		return listCount;
	}



	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 12.
	 * @Description      : 승인대기프로그램 목록 가져오기
	 * @param currentPage
	 * @param limit
	 * @return         : 승인대기프로그램 목록 리턴
	 */
	public ArrayList<HashMap<String, Object>> adSelectWaitProgram(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdWaitProgramListDao().adSelectWaitProgram(con, currentPage, limit);


		close(con);

		return list;
	}



	public int adSearchNameWaitProgramCount(String waitSearch) {
		Connection con = getConnection();

		int listCount = new AdWaitProgramListDao().adsearchNameWait(con, waitSearch);

		close(con);

		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adSearchWaitName(String waitSearch, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdWaitProgramListDao().adSearchNameList(con, waitSearch, currentPage, limit);

		close(con);

		return list;
	}


	public int adSearchIdProgramCount(String waitSearch) {
		Connection con = getConnection();

		int listCount = new AdWaitProgramListDao().adSearchIdWait(con, waitSearch);

		return listCount;
	}



	public ArrayList<HashMap<String, Object>> adSearchWaitId(String waitSearch, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdWaitProgramListDao().adSearcIdList(con, waitSearch, currentPage, limit);

		close(con);


		return list;
	}



	public ArrayList<HashMap<String, Object>> adSelectOneWait(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdWaitProgramListDao().adSelectOneWait(con, num);

		close(con);

		return list;
	}



	public ArrayList<GpLang> adSelectOneLang(int num) {
		Connection con = getConnection();

		ArrayList<GpLang> listLang = new AdWaitProgramListDao().adSelectOneWaitSer(con, num);
		close(con);

		return listLang;
	}



	public ArrayList<GpDetail> adSelectOneGpDetail(int num) {
		Connection con = getConnection();

		ArrayList<GpDetail> list = new AdWaitProgramListDao().adSelectOneGpDetail(con, num);
		close(con);

		return list;
	}



	public ArrayList<com.edr.generalProgram.model.vo.GpPriceDetail> adSelectPrice(int num) {
		Connection con = getConnection();
		ArrayList<GpPriceDetail> list = new AdWaitProgramListDao().adSelectPrice(con, num);
		close(con);

		return list;
	}



	public int adWaitProgramApproval(String appro, int num) {
		Connection con = getConnection();

		int result = new AdWaitProgramListDao().adWaitProgramApproval(con, num, appro);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}



	/*public int adWaitProgramNotApproval(String appro, int num) {
		Connection con = getConnection();

		int result = new AdWaitProgramListDao().adWaitProgramNotApproval(con, num, appro);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}
*/


	public ArrayList<HashMap<String, Object>> adSelectOneGpDetailMoon(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdWaitProgramListDao().adSelectOneGpDetailMoon(con, num);
		close(con);

		return list;
	}



	public int adWaitProgramNotApprovalN(String appro, int num) {
		Connection con = getConnection();

		int result = new AdWaitProgramListDao().adWaitProgramNotApprovalN(con, num, appro);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);

		return result;
	}














}
