package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;

import com.edr.generalProgram.model.dao.ProgressDelDao;

public class ProgressDelService {



	public int progressDel(int pno) {
		Connection con = getConnection();
		
		int result = new ProgressDelDao().progressDel(con, pno);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
	 	
		return result;
	}

}
