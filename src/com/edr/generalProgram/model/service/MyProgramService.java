package com.edr.generalProgram.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.generalProgram.model.dao.MyProgramDao;
import com.edr.generalProgram.model.vo.Gp;

public class MyProgramService {

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 14.
	 * @Description      : 내 프로그램 목록 및 프로그램 개설 회차 
	 * @param mno
	 * @param mno2 
	 * @param limit 
	 * @return         :
	 */
	public HashMap<String, Object> selectMyProgram(int currentPage, int limit, int mno, int listCount) {
		Connection con = getConnection();
		
		HashMap<String, Object> programMap = new HashMap<>();
		
//		ArrayList<HashMap<String, Object>> programName = new MyProgramDao().selectProgramCnt(mno);
		
		// 테이블 쪽 프로그램 목록 : 프로그램 명과 프로그램 번호
		ArrayList<HashMap<String, Object>> programList = new MyProgramDao().selectMyProgramList(con, currentPage, limit, mno);
		
		ArrayList<HashMap<String, Object>> programAllList = new MyProgramDao().selectMyProgramAllList(con, mno, listCount);
		
		
		programMap.put("programList", programList);
		programMap.put("programAllList", programAllList);
		
		close(con);
		
		return programMap;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 14.
	 * @Description      : 내 프로그램 목록 개수
	 * @param mno
	 * @return         :
	 */
	public int myProgramCount(int mno) {
		Connection con = getConnection();
		
		int listCount = new MyProgramDao().myProgramCount(con, mno);
		
		close(con);
	
		return listCount;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 15.
	 * @Description      : 내 프로그램 insert - 회차 등록
	 * @param gpNo
	 * @param startDate
	 * @return         :
	 */
	public int insertMyProgram(int gpNo, Date startDate) {

		Connection con = getConnection();
		
		int result = new MyProgramDao().insertMyProgram(con, gpNo, startDate);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<Date> selectMyProgramDate(int gpNo) {
		Connection con = getConnection();
		
		ArrayList<Date> myProgramDateList = new MyProgramDao().selectMyProgramDate(con, gpNo);

		close(con);
		
		return myProgramDateList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 선택한 프로그램 총 날짜 가져오기
	 * @param gpNo
	 * @return         :
	 */
	public int selectMyProgramTday(int gpNo) {
		Connection con = getConnection();
		
		int tday = new MyProgramDao().selectMyProgramTday(con, gpNo);

		close(con);
		
		return tday;
	}

}
