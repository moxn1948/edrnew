package com.edr.blackList.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlackList implements java.io.Serializable{
	private int mno;					// 회원번호
	private String blacklistReason;		// 블랙리스트 사유
	private Date blacklistDate;			// 등록일자
	private Date releaseDate;			// 해제일자
}
