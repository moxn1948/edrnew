package com.edr.payment.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements java.io.Serializable{
	private int payNo;		// 결제번호
	private int pno;		// 상품번호
	private int orderNo;	// 주문번호
	private int payCost;	// 결제비용
	private String payKind;	// 결제수단
	private Date payDate;	// 결제일자
	private Date payChange;	// 변경일자
	private String payState;// 결제상태
}
