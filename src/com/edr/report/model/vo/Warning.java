package com.edr.report.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Warning implements java.io.Serializable{
	private int rno;		// 신고번호
	private int mno;		// 회원번호
	private Date regDate;	// 신고등록일자
}
