package com.edr.report.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Report implements java.io.Serializable{	
	private int rno;			// 신고번호
	private String rtitle;		// 신고제목
	private String rcnt;		// 신고내용
	private String reportYn;	// 접수유무
	private int orderNo;		// 주문번호
	private int pno;			// 상품번호
	private String rname;		// 신고자
	private String rtarget;		// 신고대상
	
}
