package com.edr.calculate.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.HashMap;

import com.edr.calculate.model.dao.CalculateDao;
import com.edr.calculate.model.vo.Calc;
import com.edr.member.model.dao.MemberDao;

public class CalculateService {

	public HashMap<String, Object> selectListCalculate(int mno, int currentPage, int limit) {
		
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = new CalculateDao().selectListCalculate(con, currentPage, limit, mno);
		
		close(con);
		
		return hmap;
	}

	public int getCalculateListCount(int mno) {
		
		Connection con = getConnection();
		
		int listCount = new CalculateDao().getCalculateListCount(con, mno);
		
		close(con);
		
		return listCount;
		
	}

	public int getWaitCalculateListCount(int mno) {
		
		Connection con = getConnection();
		
		int listCount = new CalculateDao().getWaitCalculateListCount(con, mno);
		
		close(con);
		
		return listCount;
		
	}

	public HashMap<String, Object> selectListWaitCalculate(int mno, int currentPage, int limit) {
		
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = new CalculateDao().selectListWaitCalculate(con, currentPage, limit, mno);
		
		close(con);
		
		return hmap;
	}

	public int updateCalculateState(Calc requestCalc) {

		Connection con = getConnection();
		
		int result = new CalculateDao().updateCalculateState(con, requestCalc);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int getCompleteCalculateListCount(int mno) {
		
		Connection con = getConnection();
		
		int listCount = new CalculateDao().getCompleteCalculateListCount(con, mno);
		
		close(con);
		
		return listCount;

		
	}

	public HashMap<String, Object> selectListCompleteCalculate(int mno, int currentPage, int limit) {
	
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = new CalculateDao().selectListCompleteCalculate(con, currentPage, limit, mno);
		
		close(con);
		
		return hmap;
	}
	
	

}
