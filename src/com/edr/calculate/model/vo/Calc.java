package com.edr.calculate.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Calc implements java.io.Serializable{
	private int calcNo;			// 정산번호
	private int mno;			// 회원번호
	private int orderDetailNo;	// 주문상세번호
	private int calcCost;		// 정산비용
	private Date calcDate;		// 정산신청일
	private Date calcPayment;	// 정산결제일
	private Date calcChance;	// 변경일
	private String calcState;	// 정산상태
}
