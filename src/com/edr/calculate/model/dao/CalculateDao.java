package com.edr.calculate.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.calculate.model.vo.Calc;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.order.model.vo.OrderDetail;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

public class CalculateDao {
	
	Properties prop = new Properties();
	
	public CalculateDao() {
		
		String fileName = CalculateDao.class.getResource("/sql/calculate/calculate-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public HashMap<String, Object> selectListCalculate(Connection con, int currentPage, int limit, int mno) {
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		
		ArrayList<Integer> calNoList = null;
		ArrayList<Product> productEpiList = null;
		ArrayList<Product> productDateList = null;
		ArrayList<Gp> gpNameList = null;
		ArrayList<Payment> payCostList = null;
		ArrayList<OrderDetail> orderDetailNoList = null;
		
		
		Product productEpi = null;
		Product productDate = null;
		Gp gpName = null;
		Payment payCost = null;
		OrderDetail orderDetailNo = null;
		
		int startRow = (currentPage - 1) * limit + 1; // 21
		int endRow = startRow + limit - 1; // 30
		
		String query = prop.getProperty("selectListCalculate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, mno);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			calNoList = new ArrayList<>();
			productEpiList = new ArrayList<>();
			productDateList = new ArrayList<>();
			gpNameList = new ArrayList<>();
			payCostList = new ArrayList<>();
			orderDetailNoList = new ArrayList<>();
			
			while(rset.next()) {
				
				productEpi = new Product();
				productDate = new Product();
				gpName = new Gp();
				payCost = new Payment();
				orderDetailNo = new OrderDetail();
				
				int calNo = rset.getInt("RNUM");
				productEpi.setEpi(rset.getInt("EPI"));
				gpName.setGpName(rset.getString("GP_NAME"));
				productDate.setPdate(rset.getDate("PDATE"));
				payCost.setPayCost(rset.getInt("PAY_COST"));
				orderDetailNo.setOrderDetailNo(rset.getInt("ORDER_DETAIL_NO"));
				
				calNoList.add(calNo);
				productEpiList.add(productEpi);
				productDateList.add(productDate);
				gpNameList.add(gpName);
				payCostList.add(payCost);
				orderDetailNoList.add(orderDetailNo);
				
			}
			
			hmap = new HashMap<>();
			
			hmap.put("calNoList", calNoList);
			hmap.put("productEpiList", productEpiList);
			hmap.put("productDateList", productDateList);
			hmap.put("gpNameList", gpNameList);
			hmap.put("payCostList", payCostList);
			hmap.put("orderDetailNoList", orderDetailNoList);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return hmap;
	}

	public int getCalculateListCount(Connection con, int mno) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int result = 0;
		
		String query = prop.getProperty("calculateListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int getWaitCalculateListCount(Connection con, int mno) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int result = 0;
		
		String query = prop.getProperty("WaitCalculateListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
		
	}

	public HashMap<String, Object> selectListWaitCalculate(Connection con, int currentPage, int limit, int mno) {
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		
		ArrayList<Integer> calNoList = null;
		ArrayList<Product> productEpiList = null;
		ArrayList<Gp> gpNameList = null;
		ArrayList<Payment> costList = null;
		ArrayList<Product> productDateList = null;
		
		Product productEpi = null;
		Gp gpName = null;
		Payment cost = null;
		Product productDate = null;
		

		int startRow = (currentPage - 1) * limit + 1; // 21
		int endRow = startRow + limit - 1; // 30
		
		String query = prop.getProperty("selectListWaitCalculate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			calNoList = new ArrayList<>();
			productEpiList = new ArrayList<>();
			gpNameList = new ArrayList<>();
			costList = new ArrayList<>();
			productDateList = new ArrayList<>();
			
			while(rset.next()) {
				
				productEpi = new Product();
				gpName = new Gp();
				cost = new Payment();
				productDate = new Product();
				
				int calNo = rset.getInt("RNUM");
				productEpi.setEpi(rset.getInt("EPI"));
				gpName.setGpName(rset.getString("GP_NAME"));
				cost.setPayCost(rset.getInt("PAY_COST"));
				productDate.setPdate(rset.getDate("PDATE"));
				
				calNoList.add(calNo);
				productEpiList.add(productEpi);
				gpNameList.add(gpName);
				costList.add(cost);
				productDateList.add(productDate);
				
			}
			
			hmap = new HashMap<>();
			
			hmap.put("calNoList", calNoList);
			hmap.put("productEpiList", productEpiList);
			hmap.put("gpNameList", gpNameList);
			hmap.put("costList", costList);
			hmap.put("productDateList", productDateList);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return hmap;
	}

	public int updateCalculateState(Connection con, Calc requestCalc) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCalculateState");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestCalc.getMno());
			pstmt.setInt(2, requestCalc.getOrderDetailNo());
			pstmt.setInt(3, requestCalc.getCalcCost());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int getCompleteCalculateListCount(Connection con, int mno) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int result = 0;
		
		String query = prop.getProperty("CompleteCalculateListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
		
		
	}

	public HashMap<String, Object> selectListCompleteCalculate(Connection con, int currentPage, int limit, int mno) {
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		
		ArrayList<Integer> calNoList = null;
		ArrayList<Product> productEpiList = null;
		ArrayList<Gp> gpNameList = null;
		ArrayList<Payment> costList = null;
		ArrayList<Product> productDateList = null;
		ArrayList<Calc> calculatePaymentDateList = null;
		
		
		Product productEpi = null;
		Gp gpName = null;
		Payment cost = null;
		Product productDate = null;
		Calc calculatePaymentDate = null;
		

		int startRow = (currentPage - 1) * limit + 1; // 21
		int endRow = startRow + limit - 1; // 30
		
		String query = prop.getProperty("selectListCompleteCalculate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			calNoList = new ArrayList<>();
			productEpiList = new ArrayList<>();
			gpNameList = new ArrayList<>();
			costList = new ArrayList<>();
			productDateList = new ArrayList<>();
			calculatePaymentDateList = new ArrayList<>();
			
			while(rset.next()) {
				
				productEpi = new Product();
				gpName = new Gp();
				cost = new Payment();
				productDate = new Product();
				calculatePaymentDate = new Calc();
				
				int calNo = rset.getInt("RNUM");
				productEpi.setEpi(rset.getInt("EPI"));
				gpName.setGpName(rset.getString("GP_NAME"));
				cost.setPayCost(rset.getInt("PAY_COST"));
				productDate.setPdate(rset.getDate("PDATE"));
				calculatePaymentDate.setCalcPayment(rset.getDate("CALC_PAYMENT"));
				
				calNoList.add(calNo);
				productEpiList.add(productEpi);
				gpNameList.add(gpName);
				costList.add(cost);
				productDateList.add(productDate);
				calculatePaymentDateList.add(calculatePaymentDate);
				
			}
			
			hmap = new HashMap<>();
			
			hmap.put("calNoList", calNoList);
			hmap.put("productEpiList", productEpiList);
			hmap.put("gpNameList", gpNameList);
			hmap.put("costList", costList);
			hmap.put("productDateList", productDateList);
			hmap.put("calculatePaymentDateList", calculatePaymentDateList);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return hmap;
	}

}
