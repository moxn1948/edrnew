package com.edr.calculate.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.calculate.model.service.CalculateService;
import com.edr.calculate.model.vo.Calc;

/**
 * Servlet implementation class UpdateCalculateStateServlet
 */
@WebServlet("/updateCalculateState.calc")
public class UpdateCalculateStateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCalculateStateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int orderDetailNo = Integer.parseInt(request.getParameter("orderDetailNo"));
		int mno = Integer.parseInt(request.getParameter("mno"));
		int calcCost = Integer.parseInt(request.getParameter("calcCost"));
		
		Calc requestCalc = new Calc();
		requestCalc.setOrderDetailNo(orderDetailNo);
		requestCalc.setMno(mno);
		requestCalc.setCalcCost(calcCost);
		
		int result = new CalculateService().updateCalculateState(requestCalc);
		
		if(result > 0) {
			response.sendRedirect(request.getContextPath() + "/selectList.calc?mno="+mno);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
