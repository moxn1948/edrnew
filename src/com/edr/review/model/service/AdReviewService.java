package com.edr.review.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.guide.model.dao.GuideAdSelectDao;
import com.edr.member.model.dao.MemberAdSelectDao;
import com.edr.review.model.dao.AdReviewDao;
import com.edr.review.model.vo.Review;

public class AdReviewService {
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list count Service
	 * @param
	 * @return : listCount
	 */
	public int adGetListCount() {
		Connection con = getConnection();

		int listCount = new AdReviewDao().adGetlistCount(con);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list select Paging Service
	 * @param :
	 *            currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectListWithPaging(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdReviewDao().adSelectListWithPaging(con, currentPage, limit);

		close(con);

		return list;

	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list Select One Service
	 * @param : num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneReveiw(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdReviewDao().adSelectOneReview(con, num);

		close(con);

		return list;
	}
	/**
	 * @param m
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Delete Service
	 * @param :   r
	 * @return : result
	 */
	public int adDeleteReview(Review r) {
		Connection con = getConnection();

		int result = new AdReviewDao().adDeleteReview(con, r);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Pname Count Service
	 * @param :  searchText
	 * @return : listCount
	 */
	public int adSearchPnameReviewCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdReviewDao().adSearchPnameReviewCount(con, searchText);

		close(con);

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Pname Service
	 * @param : con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameReview(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdReviewDao().adSearchPnameReview(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Name Count Service
	 * @param :  searchText
	 * @return : listCount
	 */
	public int adSearchNameReviewCount(String searchText) {
		Connection con = getConnection();

		int listCount = new AdReviewDao().adSearchNameReviewCount(con, searchText);

		close(con);

		return listCount;
		
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Name Service
	 * @param : con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameReview(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdReviewDao().adSearchNameReview(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}

}
