package com.edr.review.model.service;

import com.edr.order.model.vo.OrderList;
import com.edr.review.model.dao.ReviewDaohj;
import com.edr.review.model.vo.Review;
import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
public class ReviewServicehj {

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 22. 
	 * @Description  :리뷰 등록service
	 * @param   :re
	 * @return    :
	 */
	public int insertReview(Review re) {
		Connection con = getConnection();
		
		int result = new ReviewDaohj().insertReview(con,re);		
		
		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}
	
	
	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 22. 
	 * @Description  :ordercode보내서 ordernumber가져오기
	 * @param   : ordercode
	 * @return    : result
	 */
	public int getOrderNo(long orderCode) {
		Connection con = getConnection();
		
		int orderDetailNo = new ReviewDaohj().getOrderNo(con,orderCode);		
		
		close(con);
		
		return orderDetailNo;
	}


	public ArrayList<Review> selectReviewList(int mno) {
		Connection con = getConnection();
		
		ArrayList<Review> reviewList = new ReviewDaohj().selectReviewList(con,mno);
		close(con);
		return reviewList;
	}


	public ArrayList<Integer> selectReviewCount(ArrayList<OrderList> orderCodeList) {
		Connection con = getConnection();
		
		ArrayList<Integer> reviewCount = new ArrayList<>();
		
		for (int i = 0; i < orderCodeList.size(); i++) {
			reviewCount.add(new ReviewDaohj().selectReviewCount(con, ((OrderList)orderCodeList.get(i)).getOrderCode()));
		}
		
		close(con);
		
		return reviewCount;
	}


	
}
