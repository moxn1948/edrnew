package com.edr.review.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import static com.edr.common.JDBCTemplate.*;
import com.edr.review.model.vo.Review;

public class ReviewDaohj {
	
	Properties prop = new Properties();
	public ReviewDaohj() {
		
		String fileName = ReviewDaohj.class.getResource("/sql/review/review-query.properties").getPath();
		
		
		try {
			prop.load(new FileReader(fileName));
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 22. 
	 * @Description  : 리뷰 등록 dao
	 * @param   :con,re(Review)
	 * @return    : int result
	 */
	public int insertReview(Connection con, Review re) {
	
		PreparedStatement pstmt = null;
		int result =0;
		
		
		String query = prop.getProperty("insertReview");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setDouble(1, re.getGuideRating()); //가이드평점
			pstmt.setDouble(2, re.getProgramRating());//프로그램평점
			pstmt.setString(3, re.getReviewCnt()); //리뷰내용
			pstmt.setInt(4, re.getOrderDetailNo()); //주문상세번호
			pstmt.setInt(5, re.getMno());//회원번호
			pstmt.setInt(6, re.getGpNo()); //프로그램번호
			
			
			result = pstmt.executeUpdate();
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		
		return result;
	}

	
	
	public int getOrderNo(Connection con, long orderCode) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int orderDetailNo = 0;
		
		
		String query = prop.getProperty("getOrderNo");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setLong(1, orderCode);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				orderDetailNo = rset.getInt("ORDER_DETAIL_NO");
			}
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return orderDetailNo;
	}


	public ArrayList<Review> selectReviewList(Connection con, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		ArrayList<Review> reviewList = null;
		Review review = null;
		
		String query = prop.getProperty("selectReviewList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset =pstmt.executeQuery();
			
			reviewList = new ArrayList<>();
			
			while(rset.next()) {
				review=new Review();
				
				review.setReviewNo(rset.getInt("REVIEW_NO"));
				review.setGuideRating(rset.getDouble("GUIDE_RATING"));
				review.setProgramRating(rset.getDouble("PROGRAM_RATING"));
				review.setReviewCnt(rset.getString("REVIEW_CNT"));
				review.setReplyYn(rset.getString("REPLY_YN"));
				review.setOrderDetailNo(rset.getInt("ORDER_DETAIL_NO"));
				review.setMno(rset.getInt("MNO"));
				review.setReplyYn(rset.getString("REVIEW_YN"));
				review.setGpNo(rset.getInt("GP_NO"));
				
				
				reviewList.add(review);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		return reviewList;
	}


	public int selectReviewCount(Connection con, long orderCode) {
		int reviewCount = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectReviewBtnCountMoxn");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setLong(1, orderCode);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				reviewCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return reviewCount;
	}

}
