package com.edr.review.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.review.model.vo.Review;

public class AdReviewDao {
	Properties prop = new Properties();

	public AdReviewDao() {

		String fileName = AdReviewDao.class.getResource("/sql/review/review-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list Count Dao
	 * @param con
	 * @return : listCount
	 */
	public int adGetlistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adGetlistCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			close(stmt);
			close(rset);

		}

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list select Paging Dao
	 * @param con, currentPage, limit
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectListWithPaging(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSelectListWithPaging");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("reviewno", rset.getInt("REVIEW_NO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("ordername", rset.getString("ORDER_NAME"));
				hmap.put("guiderating", rset.getDouble("GUIDE_RATING"));
				hmap.put("programrating", rset.getDouble("PROGRAM_RATING"));		
				
				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;

	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 16.
	 * @Description : Ad Review list select One Dao
	 * @param con, num
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSelectOneReview(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSelectOneReview");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			
			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("reviewno", rset.getInt("REVIEW_NO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				
				//hmap.put("gname", rset.getString("GNAME"));
				hmap.put("ordername", rset.getString("ORDER_NAME"));
				hmap.put("guiderating", rset.getDouble("GUIDE_RATING"));
				hmap.put("programrating", rset.getDouble("PROGRAM_RATING"));
				hmap.put("reviewcnt", rset.getString("REVIEW_CNT"));			
				
				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}
		return list;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Delete Dao
	 * @param :con, r
	 * @return : result
	 */
	public int adDeleteReview(Connection con, Review r) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("adDeleteReview");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, r.getReviewNo());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Pname Count Dao
	 * @param :  con, searchText
	 * @return : listCount
	 */
	public int adSearchPnameReviewCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchPnameReviewCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Pname Dao
	 * @param : con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchPnameReview(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchPnameReview");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("reviewno", rset.getInt("REVIEW_NO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("ordername", rset.getString("ORDER_NAME"));
				hmap.put("guiderating", rset.getDouble("GUIDE_RATING"));
				hmap.put("programrating", rset.getDouble("PROGRAM_RATING"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
		
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Member list Search Name Count Dao
	 * @param :  con, searchText
	 * @return : listCount
	 */
	public int adSearchNameReviewCount(Connection con, String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameReviewCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 17.
	 * @Description : Ad Review list Search Name Dao
	 * @param : con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameReview(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameReview");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("reviewno", rset.getInt("REVIEW_NO"));
				hmap.put("gpname", rset.getString("GP_NAME"));
				hmap.put("ordername", rset.getString("ORDER_NAME"));
				hmap.put("guiderating", rset.getDouble("GUIDE_RATING"));
				hmap.put("programrating", rset.getDouble("PROGRAM_RATING"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}
}
