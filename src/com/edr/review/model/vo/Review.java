package com.edr.review.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Review implements java.io.Serializable{
	private int reviewNo;		// 리뷰번호
	private double guideRating;	// 가이드평점
	private double programRating;	// 프로그램평점
	private String reviewCnt;	// 리뷰내용
	private String replyYn;		// 답변여부
	private int orderDetailNo;	// 주문상세번호
	private int mno;			// 회원번호
	private int gpNo;			// 일반프로그램번호
	private String reviewYn;   // 삭제여부
	
}
