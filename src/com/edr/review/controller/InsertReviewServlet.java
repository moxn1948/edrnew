package com.edr.review.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.review.model.service.ReviewServicehj;
import com.edr.review.model.vo.Review;

/**
 * Servlet implementation class InsertReviewServlet
 */
@WebServlet("/insertReview.re")
public class InsertReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//값 꺼내기
		
				//가이드평점
				double gdRate = Double.parseDouble(request.getParameter("gdRating"));
				//프로그램 평점
				double pRate = Double.parseDouble(request.getParameter("pRating"));
				System.out.println("gdRate: "+gdRate);
				//리뷰 내용
				String rContents = request.getParameter("rContents");
				//회원번호
				int mno = Integer.parseInt(request.getParameter("mno"));
				//프로그램번호
				int gpNo = Integer.parseInt(request.getParameter("gpNo"));
				//주문 상세번호
				//int orderDetailNo = Integer.parseInt(request.getParameter("orderDetailNo"));
				
				//뷰에서받아와야함
				long orderCode = Long.parseLong(request.getParameter("orderCode"));
				
				int orderDetailNo = new ReviewServicehj().getOrderNo(orderCode);				 
				
				Review re = new Review();
				
				re.setGuideRating(gdRate);
				re.setProgramRating(pRate);
				
				re.setGpNo(gpNo);
				re.setMno(mno);
				//리뷰내용
				re.setReviewCnt(rContents);
				
				//이것이 꼭필요한 것
				re.setOrderDetailNo(orderDetailNo);
				
				
				
				
				
				 int result = new ReviewServicehj().insertReview(re);
				 
				 
				 
				 String page = "";
				 
				 
				 if(result>0) {
					 //여기 뭐라고 수정하지
					 response.sendRedirect(request.getContextPath() +"/index");
				 }else {
					 
					 page = "views/admin/sub/common/errorPage.jsp";
					request.setAttribute("msg", "리뷰등록 실패");
				 
				 }
				
				
				
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
