package com.edr.review.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.guide.model.service.GuideAdSelectService;
import com.edr.review.model.service.AdReviewService;

/**
 * Servlet implementation class AdSelectOneReivewServlet
 */
@WebServlet("/selectOneReview.ad")
public class AdSelectOneReivewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSelectOneReivewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("str"));		
		
		ArrayList<HashMap<String, Object>> list = new AdReviewService().adSelectOneReveiw(num);
		
		String page = "";

		if (list != null) {
			page = "views/admin/sub/popup/reviewDetailPop.jsp";
			request.setAttribute("list", list);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "리뷰 상세 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
