package com.edr.review.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.review.model.service.AdReviewService;
import com.edr.review.model.vo.Review;

/**
 * Servlet implementation class AdDeleteReviewServlet
 */
@WebServlet("/deleteReview.ad")
public class AdDeleteReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdDeleteReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("reviewno"));
		System.out.println("num 22 : "+ num);
			
		Review r = new Review();
		r.setReviewNo(num);
	
		int result = new AdReviewService().adDeleteReview(r);

		String page = "";
		
		if (result > 0) {
			response.sendRedirect("/edr/selectReview.ad");
			
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "리뷰 정보 삭제 실패!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
