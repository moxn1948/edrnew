package com.edr.product.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product implements java.io.Serializable{
	private int pno;		// 상품 번호
	private String pkind;	// 상품 종류
	private int gpNo;		// 일반 프로그램 번호
	private int cpNo;		// 맞춤 프로그램 번호
	private Date pdate;		// 시작날짜
	private int epi;        // 회차
	private String status;	// 삭제상태
	
}
