package com.edr.common.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProduct implements java.io.Serializable{
	
	private int pno;
	private String gpName;
	private String localName;
	private Date pdate;
	private int gpTday;
	private int gpCost;

}
