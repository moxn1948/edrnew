package com.edr.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo implements java.io.Serializable{
	private int currentPage;	// 현재 페이지
	private int listCount;		// 전체 목록 개수
	private int limit;			// 한 페이지에 나올 게시글 개수
	private int maxPage;		// 최대 페이징 수
	private int startPage;		// 페이징 시작 번호
	private int endPage;		// 페이징 끝 번호
	
}
