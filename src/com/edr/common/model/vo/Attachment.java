package com.edr.common.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Attachment implements java.io.Serializable{
	private int fileNo;				// 파일번호
	private String originName;		// 기존이름
	private String changeName;		// 바뀐이름
	private String filePath;		// 파일경로
	private Date uploadDate;		// 업로드일
	private int fileLevel;			// 우선순위
	private String ttype;			// 테이블타입
	private String daySeq;			// 맞춤일정순서
	private int day;				// 맞춤일차
	private int cpNo;				// 맞춤신청번호
	private int rno;				// 신고번호
	private int msgNo;				// 메세지번호
	private int qno;				// 문의번호
	private int gpNo;				// 일반프로그램번호
	private int gpDay;				// 일반일차
	private int gpDateSeq;			// 일반일정순서
	private int ghNo;				// 가이드 신청번호
	private String cludeYn;			// 서류종류
	private int mno;				// 회원번호
	private String profileYn;		// 프로필 유무
	
}
