package com.edr.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Local implements java.io.Serializable{
	private int localNo;		// 지역번호
	private String localName;	// 지역명
}
