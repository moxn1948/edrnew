package com.edr.common.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.dao.PaymentDao;
import com.edr.common.model.vo.PaymentProduct;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.product.model.vo.Product;
import static com.edr.common.JDBCTemplate.*;

public class PaymentService {

	public PaymentProduct selectPaymentProduct(Product requestProduct) {
		
		Connection con = getConnection();
		
		PaymentProduct paymentProduct = new PaymentDao().selectPaymentProduct(con, requestProduct);
		
		close(con);
		
		return paymentProduct;
	}

	public ArrayList<GpDetail> selectPaymentProductGpDay(Product requestProduct) {
		
		Connection con = getConnection();
		
		ArrayList<GpDetail> gpDayList = new PaymentDao().selectPaymentProductGpDay(con, requestProduct);
		
		close(con);
		
		return gpDayList;
	}

	public ArrayList<GpDetail> selectPaymentProductGpDayDetail(Product requestProduct) {
		
		Connection con = getConnection();
		
		ArrayList<GpDetail> gpDayDetailList = new PaymentDao().selectPaymentProductGpDayDetail(con, requestProduct);
		
		close(con);
		
		return gpDayDetailList;
	}

	public ArrayList<GpPriceDetail> selectPaymentProductGpPriceDetailList(Product requestProduct) {
		
		Connection con = getConnection();
		
		ArrayList<GpPriceDetail> gpPriceDetailList = new PaymentDao().selectPaymentProductGpPriceDetailList(con, requestProduct);
		
		close(con);
		
		return gpPriceDetailList;
	}

	public HashMap<String, Object> selectCustomProductPaymentInfo(CpGuideEst requestCge) {
		
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = new PaymentDao().selectCustomProductPaymentInfo(con, requestCge);
		
		close(con);
		
		return hmap;
	}

/*	public HashMap<String, Object> selectPaymentProduct(Product requestProduct) {
		
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = new PaymentDao().selectPaymentProduct(con, requestProduct);
		
		close(con);
		
		return hmap;
	}
*/


}
