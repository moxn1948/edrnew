package com.edr.common.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.dao.MainSearchDao;
import com.edr.generalProgram.model.vo.Gp;

public class MainSearchService {

	public HashMap<String, Object> selectSearchList(String keyWord) {
		Connection con = getConnection();

		HashMap<String, Object> list = new HashMap<>();

		ArrayList<HashMap<String, Object>> gpList = new MainSearchDao().selectSrchGp(con, keyWord);

		int selGpNo[] = new int[gpList.size()];
		
		for (int i = 0; i < gpList.size(); i++) {
			selGpNo[i] = ((Gp) gpList.get(i).get("GpObj")).getGpNo();
		}
		
		list.put("gpList", gpList);
		
		ArrayList<HashMap<String, Object>> selectAllReviewCnt = new ArrayList<>();
		
		
		for (int i = 0; i < gpList.size(); i++) {
			HashMap<String, Object> reviewCnt = new MainSearchDao().selectSrchReviewCnt(con, selGpNo[i]);
			
			selectAllReviewCnt.add(reviewCnt);
		}

		list.put("selectAllReviewCnt", selectAllReviewCnt);
		
		
		close(con);
		
		return list;
	}

}
