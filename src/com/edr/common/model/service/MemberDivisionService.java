package com.edr.common.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;

import com.edr.common.model.dao.MemberDivisionDao;

public class MemberDivisionService {

	public String selectMemberDivision(int mno) {
		Connection con = getConnection();
		
		String status = new MemberDivisionDao().selectMemberDivision(con, mno);
		
		if(status != null) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return status;
	}

}
