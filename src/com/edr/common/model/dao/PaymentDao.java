package com.edr.common.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Local;
import com.edr.common.model.vo.PaymentProduct;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpRequest;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.product.model.vo.Product;

import static com.edr.common.JDBCTemplate.*;

public class PaymentDao {
	
	Properties prop = new Properties();
	
	public PaymentDao() {
		
		String fileName = PaymentDao.class.getResource("/sql/payment/payment-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public PaymentProduct selectPaymentProduct(Connection con, Product requestProduct) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("selectPaymentProduct");
		PaymentProduct paymentProduct = null;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestProduct.getPno());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				paymentProduct = new PaymentProduct();
			
				paymentProduct.setPno(rset.getInt("PNO"));
				paymentProduct.setGpName(rset.getString("GP_NAME"));
				paymentProduct.setLocalName(rset.getString("LOCAL_NAME"));
				paymentProduct.setPdate(rset.getDate("PDATE"));
				paymentProduct.setGpTday(rset.getInt("GP_TDAY"));
				paymentProduct.setGpCost(rset.getInt("GP_COST"));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return paymentProduct;
	}

	public ArrayList<GpDetail> selectPaymentProductGpDay(Connection con, Product requestProduct) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpDetail> gpDayList = null;
		
		String query = prop.getProperty("selectPaymentProductGpDay");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestProduct.getPno());
			
			rset = pstmt.executeQuery();
			
			gpDayList = new ArrayList<>();
			
			while(rset.next()) {
				GpDetail gpDetail = new GpDetail();
				
				gpDetail.setGpDay(rset.getInt("GP_DAY"));

				gpDayList.add(gpDetail);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return gpDayList;
	}

	public ArrayList<GpDetail> selectPaymentProductGpDayDetail(Connection con, Product requestProduct) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpDetail> gpDayDetailList = null;
		
		String query = prop.getProperty("selectPaymentProductGpDayDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestProduct.getPno());
			
			rset = pstmt.executeQuery();
			
			gpDayDetailList = new ArrayList<>();
			
			while(rset.next()) {
				GpDetail gpDayDetail = new GpDetail();
				
				gpDayDetail.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
				gpDayDetail.setGpLocation(rset.getString("GP_LOCATION"));
				gpDayDetail.setGpTime(rset.getInt("GP_TIME"));
				gpDayDetail.setGpDay(rset.getInt("GP_DAY"));
				
				gpDayDetailList.add(gpDayDetail);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return gpDayDetailList;
	}

	public ArrayList<GpPriceDetail> selectPaymentProductGpPriceDetailList(Connection con, Product requestProduct) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpPriceDetail> gpPriceDetailList = null;
		
		String query = prop.getProperty("selectPaymentProductGpPriceDetailList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestProduct.getPno());
			
			rset = pstmt.executeQuery();
			
			gpPriceDetailList = new ArrayList<>();
			
			while(rset.next()) {
				GpPriceDetail gpPriceDetail = new GpPriceDetail();
				
				gpPriceDetail.setGpCategory(rset.getString("GP_CATEGORY"));
				gpPriceDetail.setGpPrice(rset.getInt("GP_PRICE"));
				
				gpPriceDetailList.add(gpPriceDetail);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return gpPriceDetailList;
	}

	public HashMap<String, Object> selectCustomProductPaymentInfo(Connection con, CpGuideEst requestCge) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		CpRequest cpRequest = null;
		CpGuideEst cpGuideEst = null;
		CpGuideDetail cpGuideDetail = null;
		ArrayList<CpGuideDetail> cpGuideDetailList = null;
		
		String query = prop.getProperty("selectCustomProductInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestCge.getCpNo());
			pstmt.setInt(2, requestCge.getEstNo());
			
			rset = pstmt.executeQuery();
			
			cpGuideDetailList = new ArrayList<>();
			
			while(rset.next()) {
				cpRequest = new CpRequest();
				cpGuideEst = new CpGuideEst();
				cpGuideDetail = new CpGuideDetail();
				
				cpRequest.setCpNo(rset.getInt("CP_NO"));
				cpRequest.setCpTitle(rset.getString("CP_TITLE"));
				cpRequest.setCpSdate(rset.getDate("CP_SDATE"));
				cpRequest.setCpEdate(rset.getDate("CP_EDATE"));
				cpRequest.setCpPerson(rset.getInt("CP_PERSON"));
				
				
				cpGuideEst.setEstNo(rset.getInt("CP_EST_NO"));
				cpGuideEst.setTotalCost(rset.getInt("TOTAL_COST"));
				
				cpGuideDetail.setCpDay(rset.getInt("CP_DAY"));
				cpGuideDetail.setCpCnt(rset.getString("CP_CNT"));
				
				cpGuideDetailList.add(cpGuideDetail);
			}
			
			hmap = new HashMap<>();
			hmap.put("cpRequest", cpRequest);
			hmap.put("cpGuideEst", cpGuideEst);
			hmap.put("cpGuideDetailList", cpGuideDetailList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return hmap;
	}

	/*public HashMap<String, Object> selectPaymentProduct(Connection con, Product requestProduct) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<GpPriceDetail> gppList = null;
		ArrayList<GpDetail> gpdList = null;
		ArrayList<GpDetail> gpdSubList = null;
		HashMap<String, Object> hmap = null;
		PaymentProduct pp = null;
		GpPriceDetail gpd = null;
		GpDetail gd = null;
		GpDetail gdSub = null;
		
		
		String query = prop.getProperty("selectOneProduct");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestProduct.getPno());
			
			rset = pstmt.executeQuery();
			
			gppList = new ArrayList<>();
			gpdList = new ArrayList<>();
			gpdSubList = new ArrayList<>();
			
			while(rset.next()) {
				
				pp = new PaymentProduct();
				
				pp.setPno(rset.getInt("PNO"));
				pp.setGpName(rset.getString("GP_NAME"));
				pp.setLocalName(rset.getString("LOCAL_NAME"));
				pp.setPdate(rset.getDate("PDATE"));
				pp.setGpTday(rset.getInt("GP_TDAY"));
				pp.setGpCost(rset.getInt("GP_COST"));
				
				gd = new GpDetail();
				
				gd.setGpDay(rset.getInt("GP_DAY"));
				
				gpd = new GpPriceDetail();
				
				gpd.setGpCategory(rset.getString("GP_CATEGORY"));
				gpd.setGpPrice(rset.getInt("GP_PRICE"));
				
				gdSub = new GpDetail();
				
				gdSub.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
				gdSub.setGpLocation(rset.getString("GP_LOCATION"));
				gdSub.setGpTime(rset.getInt("GP_TIME"));
				gdSub.setGpDay(rset.getInt("GP_DAY"));
				
				gppList.add(gpd);
				gpdList.add(gd);
				gpdSubList.add(gdSub);
				
				
				
			}
			
			hmap = new HashMap<>();
			hmap.put("paymentProduct", pp);
			hmap.put("gpDay", gpdList);
			hmap.put("gpPrice", gppList);
			hmap.put("gpDetail", gpdSubList);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		return hmap;
	}
*/
}
