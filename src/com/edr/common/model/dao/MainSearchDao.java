package com.edr.common.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.dao.MemberDao;

public class MainSearchDao {

	Properties prop = new Properties();
	
	public MainSearchDao() {
		
		String fileName = MemberDao.class.getResource("/sql/common/common-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public ArrayList<HashMap<String, Object>> selectSrchGp(Connection con, String keyWord) {
		ArrayList<HashMap<String, Object>> gpList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectSrchGp");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyWord);
			
			rset = pstmt.executeQuery();
			
			gpList = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				Gp GpObj = new Gp();
				GpObj.setGpNo(rset.getInt("GPNNO"));
				GpObj.setGpName(rset.getString("GPNNAME"));
				GpObj.setGpDescr(rset.getString("GPNDESCR"));
				GpObj.setGpMin(rset.getInt("GPNMIN"));
				GpObj.setGpMax(rset.getInt("GPNMAX"));
				GpObj.setGpCost(rset.getInt("GPNCOST"));
				GpObj.setGpTday(rset.getInt("GPNTDAY"));
				GpObj.setMno(rset.getInt("GPNMNO"));
				GpObj.setLocalNo(rset.getInt("GPNLNO"));
				
				GuideDetail GuideObj = new GuideDetail();
				GuideObj.setGname(rset.getString("NNAME"));
				
				Attachment AttaObj = new Attachment();
				AttaObj.setChangeName(rset.getString("CHANGE_NAME"));
				

				map.put("GpObj", GpObj);
				map.put("GuideObj", GuideObj);
				map.put("AttaObj", AttaObj);
				
				gpList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return gpList;
	}

	public HashMap<String, Object> selectSrchReviewCnt(Connection con, int selGpNo) {
		HashMap<String, Object> reviewCnt = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectSrchReviewCnt");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, selGpNo);
			
			rset = pstmt.executeQuery();
			
			reviewCnt = new HashMap<>();
			
			if(rset.next()) {
				
				reviewCnt.put("reviewCount", rset.getInt(1));
				reviewCnt.put("reviewAvg", rset.getDouble(2));
			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return reviewCnt;
	}
}
