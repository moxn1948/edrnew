package com.edr.common.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.edr.member.model.dao.MemberDao;

public class MemberDivisionDao {

	Properties prop = new Properties();
	
	public MemberDivisionDao() {
		
		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public String selectMemberDivision(Connection con, int mno) {
		// int result = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String status = "";
		
		String query = prop.getProperty("selectMemberDivision");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				status = rset.getString("GH_STATE");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return status;
	}

}
