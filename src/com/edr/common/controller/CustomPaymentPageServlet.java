package com.edr.common.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.service.PaymentService;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpRequest;

/**
 * Servlet implementation class CustomPaymentPageServlet
 */
@WebServlet("/customPayment.common")
public class CustomPaymentPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomPaymentPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));
		int cpEstNo = Integer.parseInt(request.getParameter("cpEstNo"));
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		System.out.println("cpNo : " + cpNo);
		System.out.println("cpEstNo : " + cpEstNo);
		
		CpGuideEst requestCge = new CpGuideEst();
		requestCge.setEstNo(cpEstNo);
		requestCge.setCpNo(cpNo);;
		
		
		HashMap<String, Object> hmap = new PaymentService().selectCustomProductPaymentInfo(requestCge);
		
		CpRequest cpRequest = (CpRequest)hmap.get("cpRequest");
		CpGuideEst cpGuideEst = (CpGuideEst)hmap.get("cpGuideEst");
		ArrayList<CpGuideDetail> cpGuideDetailList = (ArrayList)hmap.get("cpGuideDetailList");
		
		System.out.println("cpRequest  : "+ cpRequest);
		System.out.println("cpGuideEst  : "+ cpGuideEst);
		System.out.println("cpGuideDetailList  : "+ cpGuideDetailList);
		
		if( hmap != null ) {
			
			request.setAttribute("cpRequest", cpRequest);
			request.setAttribute("cpGuideEst", cpGuideEst);
			request.setAttribute("cpGuideDetailList", cpGuideDetailList);
			
			request.getRequestDispatcher("views/user/sub/common/customPayment.jsp").forward(request, response);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
