package com.edr.common.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.collections.SynchronizedStack;

import com.edr.common.model.service.PaymentService;
import com.edr.common.model.vo.PaymentProduct;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpPriceDetail;
import com.edr.product.model.vo.Product;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

/**
 * Servlet implementation class PaymentPageServlet
 */
@WebServlet("/payment.common")
public class PaymentPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int pno = Integer.parseInt(request.getParameter("productNo"));
		int recPerson = Integer.parseInt(request.getParameter("recPerson"));
		String payDate = request.getParameter("payDate");
		
		
		
		Product requestProduct = new Product();
		requestProduct.setPno(pno);
		
		PaymentProduct paymentProduct = new PaymentService().selectPaymentProduct(requestProduct);
		ArrayList<GpDetail> gpDayList = new PaymentService().selectPaymentProductGpDay(requestProduct);
		ArrayList<GpDetail> gpDayDetailList = new PaymentService().selectPaymentProductGpDayDetail(requestProduct);
		ArrayList<GpPriceDetail> gpPriceDetailList = new PaymentService().selectPaymentProductGpPriceDetailList(requestProduct);
		
		System.out.println(" paymentProduct : " + paymentProduct );
		System.out.println(" gpDayList : " + gpDayList );
		System.out.println(" gpDayDetailList : " + gpDayDetailList );
		System.out.println(" gpPriceDetailList : " + gpPriceDetailList );
		
		if( paymentProduct != null && gpDayList != null && gpDayDetailList != null && gpPriceDetailList != null ) {
			request.setAttribute("paymentProduct", paymentProduct);
			request.setAttribute("gpDayList", gpDayList);
			request.setAttribute("gpDayDetailList", gpDayDetailList);
			request.setAttribute("gpPriceDetailList", gpPriceDetailList);
			request.setAttribute("recPerson", recPerson);
			request.setAttribute("payDate", payDate);
			request.getRequestDispatcher("views/user/sub/common/payment.jsp").forward(request, response);
		}else {
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
