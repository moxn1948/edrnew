package com.edr.common.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.service.MemberDivisionService;
import com.edr.common.model.vo.Local;
import com.google.gson.Gson;

/**
 * Servlet implementation class MemberDivisionServlet
 */
@WebServlet("/memberDivision.common")
public class MemberDivisionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberDivisionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		String mtype = request.getParameter("mtype");
		int result = 0;
	
		if(mtype.equals("GUIDE")) {
			result = 1; // 가이드인 경우
		}else {
			String status = new MemberDivisionService().selectMemberDivision(mno); // 2면 일반 투어객, 3이면 가이드 신청한 투어객
			
			if(status.equals("WAIT")) {
				result = 2; // 가이드 신청한 경우
			}else {
				result = 3;  // 일반 투어객인 경우
			}
		}
	
		
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(result, response.getWriter());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
