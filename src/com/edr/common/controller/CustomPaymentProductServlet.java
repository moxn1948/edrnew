package com.edr.common.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CustomPaymentProductServlet
 */
@WebServlet("/customPaymentProduct.common")
public class CustomPaymentProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomPaymentProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int programPrice = Integer.parseInt(request.getParameter("programPrice")); // 넘길값
		int userMno = Integer.parseInt(request.getParameter("userMno"));		   // 넘길값
		String programName = request.getParameter("programName");				   // 넘길값
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));            	   // 넘길값
		int cpEstNo = Integer.parseInt(request.getParameter("cpEstNo"));
		String pDate = request.getParameter("pDate");
		
		
		
		String orderName = request.getParameter("orderName"); 					   // 주문자명 
		String orderNationPhone = request.getParameter("orderTelList");            
		String orderPhone = request.getParameter("orderPhone");				
		String orderPhoneNumber = orderNationPhone + ")" + orderPhone;			   // 주문자 전화번호
		String orderEmail = request.getParameter("orderEmail");   				   // 주문자 이메일

		String receiverName = request.getParameter("receiverName");				   // 수령자 이름
		String userGender = request.getParameter("userGender");					   // 수령자 성별
		String receiverAge = request.getParameter("receiverAge");				   // 수령자 연령대
		
		String receiverTelList = request.getParameter("receiverTelList");		    
		String receiverPhone = request.getParameter("receiverPhone");
		String receiverPhoneNumber = receiverTelList + ")" + receiverPhone;		   // 수령자 연락처
		
		String receiverSubTelList = request.getParameter("receiverSubTelList");
		String receiverSubPhone = request.getParameter("receiverSubPhone");
		String receiverSubPhoneNumber = receiverSubTelList + ")" + receiverSubPhone;// 수령자 비상연락망
		
		String receiverEmail = request.getParameter("receiverEmail");				// 수령자 이메일
		String requestContent = request.getParameter("requestContent");				// 수령자 요청사항
		
		int recPerson = Integer.parseInt(request.getParameter("recPerson"));
		
		request.setAttribute("programPrice", programPrice);
		request.setAttribute("userMno", userMno);
		request.setAttribute("programName", programName);
		request.setAttribute("orderName", orderName);
		request.setAttribute("orderPhoneNumber", orderPhoneNumber);
		request.setAttribute("orderEmail", orderEmail);
		request.setAttribute("receiverName", receiverName);
		request.setAttribute("userGender", userGender);
		request.setAttribute("receiverAge", receiverAge);
		request.setAttribute("receiverPhoneNumber", receiverPhoneNumber);
		request.setAttribute("receiverSubPhoneNumber", receiverSubPhoneNumber);
		request.setAttribute("receiverEmail", receiverEmail);
		request.setAttribute("requestContent", requestContent);
		request.setAttribute("recPerson", recPerson);
		request.setAttribute("cpNo", cpNo);
		request.setAttribute("pDate", pDate);
		request.setAttribute("cpEstNo", cpEstNo);
		
		request.getRequestDispatcher("views/user/sub/common/customPaymentBootpay.jsp").forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
