package com.edr.wrapper;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


public class LoginWrapper extends HttpServletRequestWrapper{

	public LoginWrapper(HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String getParameter(String key) {
		
		String value = "";
		
		if( key != null && key.equals("userPwd")) {
			value = getSha512(super.getParameter("userPwd"));
		}else {
			value = super.getParameter(key);
		}
		
		return value;
	}
	
	public static String getSha512(String userPwd) {
		
		String encUserPwd = null;

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = userPwd.getBytes(Charset.forName("UTF-8"));
			md.update(bytes);
			
			encUserPwd = Base64.getEncoder().encodeToString(md.digest());
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encUserPwd;
	}
}
