package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class AdCustomProgramDao {

   private Properties prop = new Properties();

   public AdCustomProgramDao() {
      String fileName = AdCustomProgramDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();

      try {
         prop.load(new FileReader(fileName));
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }


   public int adCustomProgramCount(Connection con) {
      Statement stmt = null;
      ResultSet rset = null;
      int listCount = 0;

      String query = prop.getProperty("adCustomProgramCount");
      try {
         stmt = con.createStatement();
         rset = stmt.executeQuery(query);

         if(rset.next()) {
            listCount = rset.getInt(1);
         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(stmt);
         close(rset);
      }

      System.out.println("컬큘러listCount : " + listCount);
      
      return listCount;
   }


   public ArrayList<HashMap<String, Object>> adCustomProgramList(Connection con, int currentPage, int limit) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list = null;
      HashMap<String, Object> hmap = null;

      String query = prop.getProperty("adCustomProgramList");

      int startRow = (currentPage - 1) * limit + 1;

      int endRow = startRow + limit - 1;

      try {

         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, startRow);
         pstmt.setInt(2, endRow);


         rset = pstmt.executeQuery();

         list = new ArrayList<HashMap<String, Object>> ();


         while(rset.next()) {

            hmap = new HashMap<String, Object>();

            hmap.put("rnum", rset.getInt("RNUM"));
            hmap.put("idxnum", rset.getInt("IDXNUM"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("cpEstNo", rset.getInt("CP_EST_NO"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("nickname", rset.getString("NICKNAME"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));

            if(rset.getString("CP_STATE").equals("WAIT")) {
               hmap.put("cpState", "배정중");
            }else if(rset.getString("CP_STATE").equals("ENDPAY")) {
               hmap.put("cpState", "결제완료");
            }else {
               hmap.put("cpState", "배정완료"); 
            }

            list.add(hmap);

         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(rset);
         close(pstmt);
      }

      return list;
   }


   public int adCustomProgramCount(Connection con, String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int listCount = 0;

      String query = prop.getProperty("adCustomSearchNameCount");

      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         rset = pstmt.executeQuery();

         if(rset.next()) {
            listCount = rset.getInt(1);
         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }


      return listCount;
   }


   public ArrayList<HashMap<String, Object>> adSearchCustomList(Connection con, int currentPage, int limit,
         String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list = null;

      String query = prop.getProperty("adSearchOrderCustomList");
      int startRow = (currentPage - 1) * limit + 1;

      int endRow = startRow + limit - 1;
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         pstmt.setInt(2, startRow);
         pstmt.setInt(3, endRow);
         rset = pstmt.executeQuery();

         list = new ArrayList<HashMap<String, Object>>();

         while(rset.next()) {
            HashMap<String, Object> hmap = new HashMap<String, Object>();
            hmap.put("rnum", rset.getInt("RNUM"));
            hmap.put("idxnum", rset.getInt("IDXNUM"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("cpEstNo", rset.getInt("CP_EST_NO"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("orderName", rset.getString("NICKNAME"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));

            if(rset.getString("CP_STATE").equals("WAIT")) {
               hmap.put("cpState", "배정중");
            }else if(rset.getString("CP_STATE").equals("GWAIT")) {
               hmap.put("cpState", "배정완료");
            }else  {
               hmap.put("cpState", "결제완료"); 
            }
            
            System.out.println("list : " + list);
            list.add(hmap);

         }

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      return list;
   }


   public int adSearchGuideCustomProgramCount(Connection con, String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int listCount = 0;

      String query = prop.getProperty("adSearchGuideCustomListCount");

      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         rset = pstmt.executeQuery();

         if(rset.next()) {
            listCount = rset.getInt(1);
         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }


      return listCount;
   }


   public ArrayList<HashMap<String, Object>> adSearchGuideCustomProgramList(Connection con, int currentPage, int limit,
         String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list = null;

      String query = prop.getProperty("adSearchGuideCustomProgramList");
      int startRow = (currentPage - 1) * limit + 1;

      int endRow = startRow + limit - 1;
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         pstmt.setInt(2, startRow);
         pstmt.setInt(3, endRow);
         rset = pstmt.executeQuery();

         list = new ArrayList<HashMap<String, Object>>();

         while(rset.next()) {
            HashMap<String, Object> hmap = new HashMap<String, Object>();
            hmap.put("rnum", rset.getInt("RNUM"));
            hmap.put("idxnum", rset.getInt("IDXNUM"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("orderName", rset.getString("NICKNAME"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));

            if(rset.getString("CP_STATE").equals("ENDPAY")) {
               hmap.put("payState", "결제완료");
            }else if(rset.getString("CP_STATE").equals("REFUNDIN")) {
               hmap.put("payState", "환불요청");
            }else if(rset.getString("CP_STATE").equals("ENDREFUND")) {
               hmap.put("payState", "환불완료"); 
            }else if(rset.getString("CP_STATE").equals("CANCLEPAY")) {
               hmap.put("payState", "결제취소");
            }else {
               hmap.put("payState", "환불취소");
            }

            list.add(hmap);

         }

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      return list;
   }


   public int adSearchTitleCustomProgramCount(Connection con, String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int listCount = 0;

      String query = prop.getProperty("adSearchTitleCustomListCount");

      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         rset = pstmt.executeQuery();

         if(rset.next()) {
            listCount = rset.getInt(1);
         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }


      return listCount;
   }


   public ArrayList<HashMap<String, Object>> adSearchTitleCustomProgramList(Connection con, int currentPage, int limit,
         String searchTxt) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list = null;

      String query = prop.getProperty("adSearchTitleCustomProgramList");
      int startRow = (currentPage - 1) * limit + 1;

      int endRow = startRow + limit - 1;
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, searchTxt);
         pstmt.setInt(2, startRow);
         pstmt.setInt(3, endRow);
         rset = pstmt.executeQuery();

         list = new ArrayList<HashMap<String, Object>>();

         while(rset.next()) {
            HashMap<String, Object> hmap = new HashMap<String, Object>();
            hmap.put("rnum", rset.getInt("RNUM"));
            hmap.put("idxnum", rset.getInt("IDXNUM"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("orderName", rset.getString("NICKNAME"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));

            if(rset.getString("CP_STATE").equals("WAIT")) {
               hmap.put("cpState", "배정중");
            }else if(rset.getString("CP_STATE").equals("GWAIT")) {
               hmap.put("cpState", "배정완료");
            }else  {
               hmap.put("cpState", "결제완료"); 
            }

            list.add(hmap);

         }

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      return list;
   }


   public int adCustomProgramFilterCount(Connection con, String str) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int listCount = 0;

      String query = prop.getProperty("adCustomProgramFilterCount");

      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, str);
         rset = pstmt.executeQuery();

         if(rset.next()) {
            listCount = rset.getInt(1);
         }


      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }


      return listCount;
   }


   public ArrayList<HashMap<String, Object>> adCustomProgramFilterList(Connection con, int currentPage, int limit,
         String str) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list = null;

      String query = prop.getProperty("adCustomProgramFilterList");
      int startRow = (currentPage - 1) * limit + 1;

      int endRow = startRow + limit - 1;
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, str);
         pstmt.setInt(2, startRow);
         pstmt.setInt(3, endRow);
         rset = pstmt.executeQuery();

         list = new ArrayList<HashMap<String, Object>>();

         while(rset.next()) {
            HashMap<String, Object> hmap = new HashMap<String, Object>();
            hmap.put("rnum", rset.getInt("RNUM"));
            hmap.put("idxnum", rset.getInt("IDXNUM"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("orderName", rset.getString("NICKNAME"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));

            if(rset.getString("CP_STATE").equals("ENDPAY")) {
               hmap.put("payState", "결제완료");
            }else if(rset.getString("CP_STATE").equals("REFUNDIN")) {
               hmap.put("payState", "환불요청");
            }else if(rset.getString("CP_STATE").equals("ENDREFUND")) {
               hmap.put("payState", "환불완료"); 
            }else if(rset.getString("CP_STATE").equals("CANCLEPAY")) {
               hmap.put("payState", "결제취소");
            }else {
               hmap.put("payState", "환불취소");
            }

            list.add(hmap);

         }

      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }

      return list;
   }


   public ArrayList<HashMap<String, Object>> adCustomProgramDetail(Connection con, int num) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      ArrayList<HashMap<String, Object>> list =  null;
      
      String query = prop.getProperty("adCustomProgramDetail");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, num);
         rset = pstmt.executeQuery();
         
         list = new ArrayList<HashMap<String, Object>>();
         while(rset.next()) {
            HashMap<String, Object> hmap = new HashMap<String, Object>();
            hmap.put("nickname", rset.getString("NICKNAME"));
            hmap.put("cpNo", rset.getInt("CP_NO"));
            hmap.put("email", rset.getString("EMAIL"));
            hmap.put("cpDate", rset.getDate("CP_DATE"));
            hmap.put("cpState", rset.getString("CP_STATE"));
            hmap.put("localName", rset.getString("LOCAL_NAME"));
            hmap.put("cpPerson", rset.getInt("CP_PERSON"));
            hmap.put("cpSdate", rset.getDate("CP_SDATE"));
            hmap.put("totalDay", rset.getInt("TOTAL_DAY"));
            hmap.put("cpCost", rset.getInt("CP_COST"));
            hmap.put("cpGender", rset.getString("CP_GENDER"));
            hmap.put("cpAge", rset.getInt("CP_AGE"));
            hmap.put("cpTitle", rset.getString("CP_TITLE"));
            hmap.put("cpCnt", rset.getString("CP_CNT"));
            hmap.put("lang", rset.getString("LANG"));
            
            list.add(hmap);
            
            
            
            
            
            
            
            
         }
         
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }
      
      
      
      return list;
   }

}