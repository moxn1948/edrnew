package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpPriceDetatil;
import com.edr.guide.model.dao.GuideDao;

public class InsertMyCustomProgramDao {

	Properties prop = new Properties();
	
	public InsertMyCustomProgramDao() {
		String fileName = GuideDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} 
	public int insertMCP(Connection con, CpGuideEst ce) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertMCP");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, ce.getCpNo());
			pstmt.setInt(2, ce.getMno());
			pstmt.setInt(3,	ce.getTotalCost());
			pstmt.setString(4, ce.getMeetTime());
			pstmt.setString(5, ce.getMeetArea());
			pstmt.setString(6, ce.getUniqueness());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		
		return result;
	}
	public int insertCgdMCP(Connection con, CpGuideDetail cpGuideDetail, int estNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCgdMCP");
		 
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, estNo);
			pstmt.setInt(2, cpGuideDetail.getCpDay());
			pstmt.setString(3, cpGuideDetail.getCpCnt());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		
		return result;
	}
	public int insertAttachment(Connection con, ArrayList<Attachment> fileList, int cgdResult, int estNo, int cpDay) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertAttachment");
		
			try {
				for(int i = 0; i < fileList.size(); i++) {
				pstmt =con.prepareStatement(query);
				pstmt.setString(1, fileList.get(i).getOriginName());
				pstmt.setString(2, fileList.get(i).getChangeName());
				pstmt.setString(3, fileList.get(i).getFilePath());
				pstmt.setInt(4, cpDay);
				pstmt.setInt(5, estNo);
				
				result += pstmt.executeUpdate();
				
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
		
		return result;
	}
	public int updateMCP(Connection con, CpGuideEst ce) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("updateMCP");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, ce.getCpNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}
	public int selectEstNo(Connection con, CpGuideEst ce) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int estNo = 0;
		
		String query = prop.getProperty("selectEstNo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, ce.getMno());
			pstmt.setInt(2, ce.getCpNo());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				estNo = rset.getInt(1);
				System.out.println("estNo : " + estNo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return estNo;
	}
	public int insertCgpdMCP(Connection con, int estNo, CpPriceDetatil cpPriceDetail) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCgpdMCP");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, estNo);
			pstmt.setString(2, cpPriceDetail.getCpCategory());
			pstmt.setInt(3, cpPriceDetail.getCpPrice());
			pstmt.setString(4, cpPriceDetail.getCpInc());
			
			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
	
		return result;
	}
	public int updateStatus(Connection con, int estNo, int mno, int cpNo) {
		
	      PreparedStatement pstmt = null;
	      
	      int result = 0;
	      
	      String query = prop.getProperty("updateStatus");
	      
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setInt(1, cpNo);
	         pstmt.setInt(2, mno);
	         
	         result = pstmt.executeUpdate();
	         
	      } catch (SQLException e) {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      } finally {
	    	  close(pstmt);
	      }
	      
	      
	      return result;
	   }
	public int selectUserMno(Connection con, int cpNo) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("selectUserMno");
		int userMno = 0;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				userMno = rset.getInt("MNO");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return userMno;
	}

}
