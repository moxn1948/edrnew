package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;

import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpRequest;

public class InsertCustomProgramDao {
	Properties prop = new Properties();
	public InsertCustomProgramDao() {
		String fileName = InsertCustomProgramDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
 
	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 17.
	 * @Description      : 맞춤프로그램 신청
	 * @param con
	 * @param cr
	 * @return         :
	 */
	public int insertCp(Connection con, CpRequest cr) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cr.getMno());
			pstmt.setString(2, cr.getCpTitle());
			pstmt.setString(3, cr.getCpCnt());
			pstmt.setInt(4,	cr.getCpPerson());
			pstmt.setDate(5, cr.getCpSdate());
			pstmt.setDate(6, cr.getCpEdate());
			pstmt.setInt(7, cr.getCpCost());
			pstmt.setString(8, cr.getCpGender());
			pstmt.setInt(9, cr.getCpAge());
			pstmt.setInt(10, cr.getLocalNo());
			
			result = pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int insertLang(Connection con, CpLang cpLang, int cpNo) {
		PreparedStatement pstmt = null;
		int result3 = 0;
		String query = prop.getProperty("insertLang");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, cpLang.getLang());
			pstmt.setInt(2, cpNo);
			
			result3 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result3;
	}

	public int selectCpCurrval(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int cpNo = 0;
		
		String query = prop.getProperty("selectCpCurrval");
		
		try {
			pstmt = con.prepareStatement(query);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				cpNo = rset.getInt(1);	
				System.out.println( rset.getInt(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);

		}
		return cpNo;
	}

}
