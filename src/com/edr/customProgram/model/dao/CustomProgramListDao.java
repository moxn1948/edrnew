package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class CustomProgramListDao {
	Properties prop = new Properties();
	
	public CustomProgramListDao() {
		String fileName = CustomProgramListDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int cpListCount(Connection con, int mno) {
		int cpListCount = 0;
		PreparedStatement pstmt =null;
		ResultSet rset = null;
		
		String query = prop.getProperty("cpListCount");
			
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, mno);
			pstmt.setInt(3, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				cpListCount = rset.getInt(1);
				System.out.println("cpList : " + cpListCount);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		 
		 
		
		return cpListCount;
	}

	public ArrayList<HashMap<String, Object>> cpWithPaging(Connection con, int currentPage, int limit, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("cpWithPaging");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, mno);
			pstmt.setInt(3, mno);
			pstmt.setInt(4, startRow);
			pstmt.setInt(5, endRow);
			
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rNum", rset.getInt("RNUM"));
				hmap.put("cpNo", rset.getInt("CP_NO"));
				hmap.put("cpName", rset.getString("CP_TITLE"));
				hmap.put("gender", rset.getString("CP_GENDER"));
				hmap.put("age", rset.getInt("CP_AGE"));
				hmap.put("cost", rset.getInt("CP_COST"));
				hmap.put("cpDate", rset.getDate("CP_DATE"));
				hmap.put("state", rset.getString("CP_STATE"));

				list.add(hmap);
				System.out.println("list : " + list);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
	
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> cpFilter(Connection con, int currentPage, int limit, String str, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("cpFilter");
		

		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setString(2, str);
			pstmt.setInt(3, mno);
			pstmt.setInt(4, startRow);
			pstmt.setInt(5, endRow);
			
			rset = pstmt.executeQuery();
			
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("cpNo", rset.getInt("CP_NO"));
				hmap.put("cpName", rset.getString("CP_TITLE"));
				hmap.put("gender", rset.getString("CP_GENDER"));
				hmap.put("age", rset.getInt("CP_AGE"));
				hmap.put("cost", rset.getInt("CP_COST"));
				hmap.put("cpDate", rset.getDate("CP_DATE"));
				hmap.put("state", rset.getString("CP_STATE"));
				
				list.add(hmap);
			}
			
			System.out.println("dao : " + list);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int cpListFilterCount(Connection con, String str, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;
		
		String query = prop.getProperty("cpListFilterCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setString(2, str);
			pstmt.setInt(3, mno);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
			
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return listCount;
	}

	public int cpListCountFilterYN(Connection con, String str, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;
		
		String query = prop.getProperty("cpListCountFilterYN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, mno);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return listCount;
	}

}
