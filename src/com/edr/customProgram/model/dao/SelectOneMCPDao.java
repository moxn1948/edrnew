package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpPriceDetatil;
import com.edr.customProgram.model.vo.CpRequest;

public class SelectOneMCPDao {
Properties prop = new Properties();
	
	public SelectOneMCPDao() {
		String fileName = selectOneCpDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public ArrayList<HashMap<String, Object>> selectOneMCP(Connection con, int mno, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ArrayList<CpRequest> cpList = null;
		
		String query = prop.getProperty("selectOneMCP");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				cpList = new ArrayList<CpRequest>();
						
				hmap.put("state", rset.getString("CP_STATE"));
				hmap.put("cpDate", rset.getDate("CP_DATE"));
				hmap.put("person", rset.getInt("CP_PERSON"));
				hmap.put("sDate", rset.getDate("CP_SDATE"));
				hmap.put("eDate", rset.getDate("CP_EDATE"));
				hmap.put("cost", rset.getInt("CP_COST"));
				hmap.put("gender", rset.getString("CP_GENDER"));
				hmap.put("age", rset.getInt("CP_AGE"));
				hmap.put("title", rset.getString("CP_TITLE"));
				hmap.put("cnt", rset.getString("CP_CNT"));
				hmap.put("local", rset.getString("LOCAL_NAME"));
				
				list.add(hmap);
						
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<CpLang> selectLangCp(Connection con, int mno, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<CpLang> listLang = null;
		CpLang cl = null;

		String query = prop.getProperty("selectLangMCP");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			rset = pstmt.executeQuery();

			listLang = new ArrayList<CpLang>();

			while(rset.next()) {
				cl = new CpLang();
				cl.setCpNo(cpNo);
				cl.setLang(rset.getString("LANG"));
				listLang.add(cl);
				
				System.out.println("listLang" + listLang);
			}
			System.out.println("listLang2" + listLang);



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return listLang;
	}

	public int selectOneDate(Connection con, int mno, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectOneDate");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
				System.out.println("result3 : " + result);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		System.out.println("result2 : " + result);
		
		return result;
	}

	public int selectEstNo(Connection con, int mno, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		String query = prop.getProperty("selectEstNo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, cpNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		System.out.println("mno : " + mno);
		System.out.println("result : " + result);
		
		
		
		return result;
	}

	public ArrayList<CpGuideEst> selectOneMCP2(Connection con, int mno, int cpNo, int estNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		ArrayList<CpGuideEst> cpEstList = null;
		CpGuideEst ce = null;
		
		String query = prop.getProperty("selectOneMCP2");
		
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setInt(1, estNo);
			
			rset = pstmt.executeQuery();
			
			cpEstList = new ArrayList<CpGuideEst>();
			while(rset.next()) {
				cpEstList = new ArrayList<CpGuideEst>();
				ce =new CpGuideEst();
				
				ce.setTotalCost(rset.getInt("TOTAL_COST"));
				ce.setMeetTime(rset.getString("MEET_TIME"));
				ce.setMeetArea(rset.getString("MEET_AREA"));
				ce.setUniqueness(rset.getString("UNIQUENESS"));
				
				
				
				cpEstList.add(ce);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return cpEstList;
	}

	public ArrayList<CpPriceDetatil> selectOnePrice(Connection con, int mno, int cpNo, int estNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		ArrayList<CpPriceDetatil> cpPriceList = null;
		CpPriceDetatil cpl = null;
		String query = prop.getProperty("selectOnePrice");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, estNo);
			
			rset = pstmt.executeQuery();
			
			cpPriceList = new ArrayList<CpPriceDetatil>();
			while(rset.next()) {
				cpl = new CpPriceDetatil();
				cpPriceList = new ArrayList<CpPriceDetatil>();
				
				cpl.setCpCategory(rset.getString("CP_CATEGORY"));
				cpl.setCpPrice(rset.getInt("CP_PRICE"));
				cpl.setCpInc(rset.getString("CP_INC"));
				
				cpPriceList.add(cpl);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return cpPriceList;
	}

}
