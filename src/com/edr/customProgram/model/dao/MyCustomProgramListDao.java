package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class MyCustomProgramListDao {
	Properties prop = new Properties();
	
	public MyCustomProgramListDao() {
		String fileName = CustomProgramListDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int mcpListCount(Connection con, int mno) {
		int mcpListCount = 0;
		PreparedStatement pstmt =null;
		ResultSet rset = null;
		
		String query = prop.getProperty("mcpListCount");
			
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			 
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				mcpListCount = rset.getInt(1);
				System.out.println("mcpList : " + mcpListCount);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return mcpListCount;
		
	}

	public ArrayList<HashMap<String, Object>> mcpWithPaging(Connection con, int currentPage, int limit, int mno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		
		String query = prop.getProperty("mcpWithPaging");
		
		int startRow = (currentPage - 1) * limit +1;
		int endRow = startRow + limit - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, mno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rNum", rset.getInt("RNUM"));
				hmap.put("cpNo", rset.getInt("CP_NO"));
				hmap.put("cpTitle", rset.getString("CP_TITLE"));
				hmap.put("nickName", rset.getString("NICKNAME"));
				hmap.put("cDate", rset.getDate("CP_DATE"));
				hmap.put("status", rset.getString("STATUS"));
				
				list.add(hmap);
				System.out.println("list : " + list);
			}
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return list;
	}

}
