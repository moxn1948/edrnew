package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpPriceDetatil;
import com.edr.customProgram.model.vo.CpRequest;
import com.edr.payment.model.vo.Payment;

public class MyCpListDao {
	Properties prop = new Properties();
	
	public MyCpListDao() {
		String fileName = MyCpListDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 21.
	 * @Description      : 내 맞춤 프로그램 목록
	 * @param con
	 * @param mno
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> selectMyCpList(Connection con, int currentPage, int limit, int mno) {
		ArrayList<HashMap<String, Object>> myCpList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMyCpList");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit -1;
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno); 
			pstmt.setInt(2, startRow); 
			pstmt.setInt(3, endRow); 
			
			rset = pstmt.executeQuery();

			myCpList = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				int idxnum = rset.getInt("IDXNUM");
				
				CpRequest cpList = new CpRequest();
				cpList.setCpNo(rset.getInt("CP_NO"));
				cpList.setCpTitle(rset.getString("CP_TITLE"));
				cpList.setCpDate(rset.getDate("CP_DATE"));
				cpList.setCpState(rset.getString("CP_STATE"));
				
				map.put("idxnum", idxnum);
				map.put("cpList", cpList);
				
				myCpList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return myCpList;
	}

	public int selectMyCpCount(Connection con, int mno) {
		int myCpCount = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMyCpCount");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				myCpCount = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return myCpCount;
	}

	public CpRequest selectMyCpRequest(Connection con, int cpNo) {
		CpRequest cpRe = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectMyCpRequest");

		
		try {
			pstmt = con.prepareStatement(query);
			

			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			cpRe = new CpRequest();
			
			if(rset.next()) {
				cpRe.setCpDate(rset.getDate("CP_DATE"));
				cpRe.setCpTitle(rset.getString("CP_TITLE"));
				cpRe.setCpCnt(rset.getString("CP_CNT"));
				cpRe.setCpPerson(rset.getInt("CP_PERSON"));
				cpRe.setCpSdate(rset.getDate("CP_SDATE"));
				cpRe.setCpCost(rset.getInt("CP_COST"));
				cpRe.setCpGender(rset.getString("CP_GENDER"));
				cpRe.setCpAge(rset.getInt("CP_AGE"));
				cpRe.setCpState(rset.getString("CP_STATE"));
				cpRe.setLocalNo(rset.getInt("LOCAL_NO"));
				cpRe.setCpEdate(rset.getDate("CP_EDATE"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return cpRe;
	}

	public ArrayList<String> selectMyCpLang(Connection con, int cpNo) {
		ArrayList<String> cpLa = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectMyCpLang");
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			cpLa = new ArrayList<>();			
			
			while(rset.next()) {
				cpLa.add(rset.getString("LANG"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return cpLa;
	}

	public int selectMyCpTday(Connection con, int cpNo) {
		int tday = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectMyCpRequest");

		
		try {
			pstmt = con.prepareStatement(query);
			

			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			
			if(rset.next()) {
				tday = rset.getInt("TDAY");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return tday;
	}

	public ArrayList<CpGuideDetail> selectCpGdetail(Connection con, int cpNo) {
		ArrayList<CpGuideDetail> cpGdetail = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCpGdetail");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			cpGdetail = new ArrayList<>();			
			
			while(rset.next()) {
				CpGuideDetail obj = new CpGuideDetail();
				
				obj.setCpDay(rset.getInt("CP_DAY"));
				obj.setCpCnt(rset.getString("CP_CNT"));
				
				cpGdetail.add(obj);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return cpGdetail;
	}

	public CpGuideEst selectCpGest(Connection con, int cpNo) {
		CpGuideEst cpGest = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCpGest");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			cpGest = new CpGuideEst();
			
			if(rset.next()) {
				cpGest.setMno(rset.getInt("EM"));
				cpGest.setTotalCost(rset.getInt("TOTAL_COST"));
				cpGest.setMeetTime(rset.getString("MEET_TIME"));
				cpGest.setMeetArea(rset.getString("MEET_AREA"));
				cpGest.setUniqueness(rset.getString("UNIQUENESS"));
				cpGest.setStatus(rset.getString("STATUS"));
				cpGest.setEstNo(rset.getInt("CP_EST_NO"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return cpGest;
	}

	public String selectGname(Connection con, int cpNo) {
		String gname = "";
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectGname");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				gname = rset.getString("GG");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return gname;
	}

	public ArrayList<CpPriceDetatil> selectCpPdetail(Connection con, int cpNo) {
		ArrayList<CpPriceDetatil> cpPdetail = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCpPdetail");
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			cpPdetail = new ArrayList<>();			
			
			while(rset.next()) {
				CpPriceDetatil obj = new CpPriceDetatil();
				
				obj.setCpCategory(rset.getString("CP_CATEGORY"));
				obj.setCpPrice(rset.getInt("CP_PRICE"));
				obj.setCpInc(rset.getString("CP_INC"));
				
				cpPdetail.add(obj);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return cpPdetail;
	}

	public int updateGuideEstState(Connection con, int cpNo, int estNo) {
		int result = 0;
		PreparedStatement pstmt = null;

		String query = prop.getProperty("updateGuideEstState");
		
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, estNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateMyGpReqState(Connection con, int cpNo, int estNo) {
		int result = 0;
		PreparedStatement pstmt = null;

		String query = prop.getProperty("updateMyGpReqState");
		
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, cpNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

	public Attachment selectCpAt(Connection con, int estNo) {
		// TODO Auto-generated method stub
		return null;
	}

	public Attachment selectAttachment(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Attachment cpAtta = null;


		String query = prop.getProperty("selectMyCpAttachment");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			cpAtta = new Attachment();
			
			if(rset.next()) {
				cpAtta.setChangeName(rset.getString("CHANGE_NAME"));
				cpAtta.setOriginName(rset.getString("ORIGIN_NAME"));
				cpAtta.setFilePath(rset.getString("FILE_PATH"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return cpAtta;
	}

	public int selectCompMyCpCount(Connection con, int mno) {
		int myCpCount = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCompMyCpCount");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				myCpCount = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return myCpCount;
	}

	public ArrayList<HashMap<String, Object>> selectCompMyCpList(Connection con, int currentPage, int limit, int mno) {
		ArrayList<HashMap<String, Object>> myCpList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCompMyCpList");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit -1;
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, mno); 
			pstmt.setInt(2, startRow); 
			pstmt.setInt(3, endRow); 
			
			rset = pstmt.executeQuery();

			myCpList = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> map = new HashMap<>();
				
				int idxnum = rset.getInt("IDXNUM");
				
				CpRequest cpList = new CpRequest();
				cpList.setCpNo(rset.getInt("CNO"));
				cpList.setCpTitle(rset.getString("CTIT"));
				cpList.setCpSdate(rset.getDate("SDATE"));
				cpList.setCpEdate(rset.getDate("EDATE"));
				
				Payment pay = new Payment();
				pay.setPayDate(rset.getDate("PDATE"));
				pay.setPayCost(rset.getInt("PAY_COST"));
				
				map.put("idxnum", idxnum);
				map.put("cpList", cpList);
				map.put("pay", pay);
				
				myCpList.add(map);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return myCpList;
	} 
}
