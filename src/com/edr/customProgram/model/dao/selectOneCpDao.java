package com.edr.customProgram.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpRequest;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.generalProgram.model.vo.GpLang;

public class selectOneCpDao {
	Properties prop = new Properties();
	
	public selectOneCpDao() {
		String fileName = selectOneCpDao.class.getResource("/sql/customProgram/customProgram-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
 
	public ArrayList<HashMap<String, Object>> selectOneCp(Connection con, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;
		HashMap<String, Object> hmap = null;
		ArrayList<CpRequest> cpList = null;
		
		String query = prop.getProperty("selectOneCp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				cpList = new ArrayList<CpRequest>();
				
				
				hmap.put("state", rset.getString("CP_STATE"));
				hmap.put("cpDate", rset.getDate("CP_DATE"));
				hmap.put("person", rset.getInt("CP_PERSON"));
				hmap.put("sDate", rset.getDate("CP_SDATE"));
				hmap.put("eDate", rset.getDate("CP_EDATE"));
				hmap.put("cost", rset.getInt("CP_COST"));
				hmap.put("gender", rset.getString("CP_GENDER"));
				hmap.put("age", rset.getInt("CP_AGE"));
				hmap.put("title", rset.getString("CP_TITLE"));
				hmap.put("cnt", rset.getString("CP_CNT"));
				hmap.put("local", rset.getString("LOCAL_NAME"));
				
				list.add(hmap);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<CpLang> selectLangCp(Connection con,int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<CpLang> listLang = null;
		CpLang cl = null;

		String query = prop.getProperty("selectLangCp");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			rset = pstmt.executeQuery();

			listLang = new ArrayList<CpLang>();

			while(rset.next()) {
				cl = new CpLang();
				cl.setCpNo(cpNo);
				cl.setLang(rset.getString("LANG"));
				listLang.add(cl);
				
				System.out.println("listLang" + listLang);
			}
			System.out.println("listLang2" + listLang);



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}


		return listLang;
	}

	public int selectDate(Connection con, int cpNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectDate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cpNo);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			System.out.println("result : " + result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}



}
