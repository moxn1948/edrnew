package com.edr.customProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpGuideDetail implements java.io.Serializable{
	private int estNo;
	private int cpDay;			// 일차
	private String cpCnt;		// 프로그램 내용
}
