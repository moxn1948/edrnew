package com.edr.customProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpLang implements java.io.Serializable{
	private String lang;	// 언어
	private int cpNo;		// 맞춤 프로그램 번호
}
