package com.edr.customProgram.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpPriceDetatil implements java.io.Serializable{
	private int estNo; 
	private String cpCategory;	// 항목    
	private int cpPrice;		// 비용    
	private String cpInc;		// 구분
}
