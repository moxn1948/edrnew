package com.edr.customProgram.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpRequest implements java.io.Serializable{
	private int cpNo;				// 맞춤프로그램 번호
	private int mno;				// 회원번호
	private Date cpDate;			// 신청일
	private String cpTitle;			// 신청제목
	private String cpCnt;			// 원하는 내용
	private String cpArea;			// 원하는 장소
	private int cpPerson;			// 참여인원수
	private Date cpSdate;			// 프로그램 시작일
	private Date cpEdate;			// 프로그램 종료일
	private int cpCost;				// 가능 비용
	private String cpLang;			// 원하는 언어
	private String cpGender;		// 가이드 성별
	private int cpAge;			// 가이드 연령대
	private int localNo;			//지역번호
	private String cpState;			// 상태

}
