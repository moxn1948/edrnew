package com.edr.customProgram.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpGuideEst implements java.io.Serializable{
	private int estNo;
    private int cpNo;			// 신청번호
	private int mno;			// 회원번호
	private int totalCost;	// 총비용
	private String meetTime;		// 만나는시간	
	private String meetArea;	// 만나는장소
	private String uniqueness;	// 특이사항
	private String status; 		// 상태
}
