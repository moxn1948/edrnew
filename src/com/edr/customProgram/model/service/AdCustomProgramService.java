package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.AdCustomProgramDao;

public class AdCustomProgramService {

	public int adCustomProgramCount() {
		Connection con = getConnection();
		int listCount = new AdCustomProgramDao().adCustomProgramCount(con);

		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adCustomProgramList(int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adCustomProgramList(con, currentPage, limit);
		close(con);

		return list;
	}

	public int adSearchOrderCustomCount(String searchTxt) {
		Connection con = getConnection();
		int listCount = new AdCustomProgramDao().adCustomProgramCount(con, searchTxt);
		close(con);

		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchOrderList(int currentPage, int limit, String searchTxt) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adSearchCustomList(con, currentPage, limit, searchTxt);

		close(con);
		return list;
	}

	public int adSearchGuideCustomProgramCount(String searchTxt) {
		Connection con = getConnection();
		int listCount = new AdCustomProgramDao().adSearchGuideCustomProgramCount(con, searchTxt);
		close(con);

		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchGuideCustomProgramList(int currentPage, int limit,
			String searchTxt) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adSearchGuideCustomProgramList(con, currentPage, limit, searchTxt);

		close(con);
		return list;
	}

	public int adSearchTitleCustomCount(String searchTxt) {
		Connection con = getConnection();
		int listCount = new AdCustomProgramDao().adSearchTitleCustomProgramCount(con, searchTxt);
		close(con);

		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adSearchTitleCustomProgram(int currentPage, int limit, String searchTxt) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adSearchTitleCustomProgramList(con, currentPage, limit, searchTxt);
		close(con);
		
		return list;
	}

	public int adCustomProgramFilterCount(String str) {
		Connection con = getConnection();
		
		int listCount = new AdCustomProgramDao().adCustomProgramFilterCount(con, str);
		close(con);
		
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> adCustomProgramFileter(int currentPage, int limit, String str) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adCustomProgramFilterList(con, currentPage, limit, str);
		close(con);
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> adCustomProgramDetail(int num) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdCustomProgramDao().adCustomProgramDetail(con, num);
		
		close(con);
		
		return list;
	}

}
