package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.selectOneCpDao;
import com.edr.customProgram.model.vo.CpLang;

public class SelectOneCpService {

	public ArrayList<HashMap<String, Object>> selectOneCp(int cpNo) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new selectOneCpDao().selectOneCp(con,cpNo);
		
		close(con);
		
		return list;
	}
 
	public ArrayList<CpLang> adSelectOneLang(int cpNo) {
		Connection con = getConnection();
		
		ArrayList<CpLang> listLang = new selectOneCpDao().selectLangCp(con,cpNo);
		
		close(con);
		return listLang;
	}

	public int selectDate(int cpNo) {
		Connection con = getConnection();
		
		int result = new selectOneCpDao().selectDate(con, cpNo);
		
		
		close(con);
		return result;
	}



}
