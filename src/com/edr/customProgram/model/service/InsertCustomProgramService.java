package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.InsertCustomProgramDao;
import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpRequest;

public class InsertCustomProgramService {

	public int inserCp(CpRequest cr) {
		Connection con = getConnection();
		
		
		int result = new InsertCustomProgramDao().insertCp(con, cr);
		
		
		if(result > 0) {
			commit(con);
		}else {
	 		rollback(con);
		}
		close(con);
		
		return result;
	}

	public int insertLang(HashMap<String, Object> reqMap, int cpNo) {
		Connection con = getConnection();
		
		ArrayList<CpLang> langList = (ArrayList<CpLang>) reqMap.get("langList");
		int result3 = 0;
		for(int i =0; i < langList.size(); i++) {
		result3 = new InsertCustomProgramDao().insertLang(con, langList.get(i) ,cpNo);
		
		}
		if(result3 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result3;
	}

	public int selectCurrval() {
		Connection con = getConnection();
		
		int cpNo = new InsertCustomProgramDao().selectCpCurrval(con);
		
		close(con);
		return cpNo;
	}



}
