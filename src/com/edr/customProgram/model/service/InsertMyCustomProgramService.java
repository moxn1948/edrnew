package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.*;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.dao.InsertMyCustomProgramDao;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpPriceDetatil;

public class InsertMyCustomProgramService {

	public int insertMCP(CpGuideEst ce) {
		Connection con = getConnection();
		int result = new InsertMyCustomProgramDao().insertMCP(con, ce);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}

	public int insertCgdMCP(ArrayList<CpGuideDetail> cpDetailList, int estNo) {
		Connection con = getConnection();
		int result = 0;
		for (int i = 0; i < cpDetailList.size(); i++) {
			
			 result = new InsertMyCustomProgramDao().insertCgdMCP(con, cpDetailList.get(i), estNo);
		}
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result;
	} 

	public int insertAttachment(ArrayList<Attachment> fileList, int cgdResult, int estNo, int cpDay) {
		Connection con = getConnection();
		int result = new InsertMyCustomProgramDao().insertAttachment(con, fileList, cgdResult, estNo, cpDay);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		return result;
	}

	public int updateMCP(CpGuideEst ce) {
		Connection con = getConnection();
		int result = new InsertMyCustomProgramDao().updateMCP(con, ce);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		return result;
	}

	public int selectEstNo(CpGuideEst ce) {
		Connection con = getConnection();
		int estNo = new InsertMyCustomProgramDao().selectEstNo(con, ce);
		
		close(con);
		return estNo;
	}

	public int insertCgpdMCP(ArrayList<CpPriceDetatil> cpPriceDetailList, int estNo) {
		Connection con = getConnection();
		int result = 0;
		for (int i = 0; i < cpPriceDetailList.size(); i++) {
			
			 result = new InsertMyCustomProgramDao().insertCgpdMCP(con, estNo, cpPriceDetailList.get(i));
		}

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int updateStatus(int estNo, int mno, int cpNo) {
		
	      Connection con = getConnection();
	      
	      int result = new InsertMyCustomProgramDao().updateStatus(con, estNo, mno, cpNo);
	      
	      if(result > 0) {
	         commit(con);
	      }else {
	         rollback(con);
	      }
	      
	      close(con);
	      
	      return result;
	   }

	public int selectUserMno(int cpNo) {
		
		Connection con = getConnection();
		
		int userMno = new InsertMyCustomProgramDao().selectUserMno(con, cpNo);
		
		close(con);
		
		return userMno;
	}



}
