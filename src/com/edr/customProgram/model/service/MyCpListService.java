package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.dao.MyCpListDao;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpPriceDetatil;
import com.edr.customProgram.model.vo.CpRequest;
import com.edr.guide.model.dao.AdGuideDao;

public class MyCpListService {

	public ArrayList<HashMap<String, Object>> selectMyCpList(int currentPage, int limit, int mno) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> myCpList = new MyCpListDao().selectMyCpList(con, currentPage, limit, mno);
		
		return myCpList;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 22.
	 * @Description      : 페이징 위한 리스트
	 * @param mno
	 * @return         :
	 */
	public int selectMyCpCount(int mno) {
		Connection con = getConnection();
		
		int myCpCount = new MyCpListDao().selectMyCpCount(con, mno);

		close(con);
		
		return myCpCount;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 22.
	 * @Description      : 조회하기
	 * @param cpNo
	 * @param state
	 * @return         :
	 */
	public HashMap<String, Object> SelectMyCpDetail(int cpNo, String state) {
		Connection con = getConnection();
		
		HashMap<String, Object> cpDetail = new HashMap<>();
		
		// 기본적인 요청사항에 대한 것
		CpRequest cpRe = new MyCpListDao().selectMyCpRequest(con, cpNo);
		ArrayList<String> cpLa = new MyCpListDao().selectMyCpLang(con, cpNo);
		int tday = new MyCpListDao().selectMyCpTday(con, cpNo);
		
		ArrayList<CpGuideDetail> cpGdetail = null;
		CpGuideEst cpGest = null;
		String gname = "";
		ArrayList<CpPriceDetatil> cpPdetail = null;
		Attachment cpAttr = null;
		
		int estNo = 0;
		
		if(!state.equals("WAIT")) {
			
			// CP_GUIDE_DETAIL
			cpGdetail = new MyCpListDao().selectCpGdetail(con, cpNo);
			
			// CP_GUIDE_EST
			cpGest = new MyCpListDao().selectCpGest(con, cpNo);
			estNo = cpGest.getEstNo();
			
			// 가이드 이름
			gname = new MyCpListDao().selectGname(con, cpNo);
			
			// CP_PRICEDETAIL
			cpPdetail = new MyCpListDao().selectCpPdetail(con, cpNo);
			
			// 첨부파일
			cpAttr = new MyCpListDao().selectAttachment(con, estNo);
			

		}
		
		cpDetail.put("cpRe", cpRe);
		cpDetail.put("cpLa", cpLa);
		cpDetail.put("tday", tday);
		cpDetail.put("cpGdetail", cpGdetail);
		cpDetail.put("cpGest", cpGest);
		cpDetail.put("gname", gname);
		cpDetail.put("cpPdetail", cpPdetail);
		cpDetail.put("cpAttr", cpAttr);

		close(con);
		
		return cpDetail;
	}

	/**
	 * @Author         : moun
	 * @CreateDate       : 2019. 12. 22.
	 * @Description      : 거절하기
	 * @param cpNo
	 * @param estNo
	 * @return         :
	 */
	public int updateGuideEstState(int cpNo, int estNo) {
		Connection con = getConnection();
		
		int result = new MyCpListDao().updateGuideEstState(con, cpNo, estNo);
		int result2  =  new MyCpListDao().updateMyGpReqState(con, cpNo, estNo);
		
		if(result > 0 && result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public Attachment selectAttachMent(int num) {	
		Connection con = getConnection();
	
		Attachment cpAttr = new MyCpListDao().selectAttachment(con, num);
	
		close(con);
	
		return cpAttr;
	}

	public int selectCompMyCpCount(int mno) {
		Connection con = getConnection();
		
		int myCpCount = new MyCpListDao().selectCompMyCpCount(con, mno);

		close(con);
		
		return myCpCount;
	}

	public ArrayList<HashMap<String, Object>> selectCompMyCpList(int currentPage, int limit, int mno) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> myCpList = new MyCpListDao().selectCompMyCpList(con, currentPage, limit, mno);
		
		return myCpList;
	}

	public HashMap<String, Object> SelectMyCpDetail(int cpNo) {
		Connection con = getConnection();
		
		HashMap<String, Object> cpDetail = new HashMap<>();

		CpRequest cpRe = new MyCpListDao().selectMyCpRequest(con, cpNo);
		ArrayList<CpGuideDetail> cpGdetail = new MyCpListDao().selectCpGdetail(con, cpNo);;
		CpGuideEst cpGest = new MyCpListDao().selectCpGest(con, cpNo);
		int estNo = cpGest.getEstNo();
		String gname = new MyCpListDao().selectGname(con, cpNo);
		ArrayList<CpPriceDetatil> cpPdetail = new MyCpListDao().selectCpPdetail(con, cpNo);
		Attachment cpAttr = new MyCpListDao().selectAttachment(con, estNo);
		
		cpDetail.put("cpRe", cpRe);
		cpDetail.put("cpGdetail", cpGdetail);
		cpDetail.put("cpGest", cpGest);
		cpDetail.put("gname", gname);
		cpDetail.put("cpPdetail", cpPdetail);
		cpDetail.put("cpAttr", cpAttr);

		close(con);
		
		return cpDetail;
	}

}
