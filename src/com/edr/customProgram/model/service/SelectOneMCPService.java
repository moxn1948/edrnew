package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.SelectOneMCPDao;
import com.edr.customProgram.model.dao.selectOneCpDao;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpPriceDetatil;

public class SelectOneMCPService {

	public ArrayList<HashMap<String, Object>> selectOneMCP(int mno, int cpNo) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new SelectOneMCPDao().selectOneMCP(con, mno, cpNo);
		
		close(con);
		
		return list;
	}

	public ArrayList<CpLang> selectOneLang(int mno, int cpNo) {
		Connection con = getConnection();
		
		ArrayList<CpLang> listLang = new SelectOneMCPDao().selectLangCp(con, mno, cpNo);
		
		close(con);
		return listLang;
	}

	public int selectOneDate(int mno, int cpNo) {
		Connection con = getConnection();
		
		int result = new SelectOneMCPDao().selectOneDate(con, mno, cpNo);
		
		close(con);
		
		return result;
	}

	public int selectEstNo(int mno, int cpNo) {
		Connection con = getConnection();
		
		int result = new SelectOneMCPDao().selectEstNo(con, mno, cpNo);
		
		close(con);
		
		return result;
	}

	public ArrayList<CpGuideEst> selectOneMCP2(int mno, int cpNo, int estNo) {
		Connection con = getConnection();
		
		ArrayList<CpGuideEst> list = new SelectOneMCPDao().selectOneMCP2(con, mno, cpNo, estNo);
		
		close(con);
		
		return list;
	}

	public ArrayList<CpPriceDetatil> selectOnePrice(int mno, int cpNo, int estNo) {
		Connection con = getConnection();
		
		ArrayList<CpPriceDetatil> list = new SelectOneMCPDao().selectOnePrice(con, mno, cpNo, estNo);
		
		close(con);
		
		return list;
		
	}

}
