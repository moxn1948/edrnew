package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.CustomProgramListDao;

public class CustomProgramListService {

	public int cpListCount(int mno) {
		Connection con = getConnection();

		int progressListCount = new CustomProgramListDao().cpListCount(con, mno);

		close(con);
		return progressListCount;
		
	}
 
	public ArrayList<HashMap<String, Object>> cpWithPaging(int currentPage, int limit, int mno) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new CustomProgramListDao().cpWithPaging(con, currentPage, limit, mno);


		close(con);
		
		return list;
	}

	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 18.
	 * @Description      :  filter
	 * @param currentPage
	 * @param limit
	 * @param str
	 * @param mno 
	 * @return         :
	 */
	public ArrayList<HashMap<String, Object>> cpFilter(int currentPage, int limit, String str, int mno) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new CustomProgramListDao().cpFilter(con, currentPage, limit, str, mno);
		
		return list;
	}

	/**
	 * @Author         : wonky
	 * @CreateDate       : 2019. 12. 18.
	 * @Description      :  filter count
	 * @param str
	 * @param mno 
	 * @return         :
	 */
	public int cpListFilterCount(String str, int mno) {
		Connection con = getConnection();
		
		int listCount = new CustomProgramListDao().cpListFilterCount(con, str, mno);
		
		close(con);
		return listCount;
	}

	public int cpListCountFilterYN(String str, int mno) {
		Connection con = getConnection();
		
		int listCount = new CustomProgramListDao().cpListCountFilterYN(con, str, mno);
		
		close(con);
		return listCount;
	}

	public ArrayList<HashMap<String, Object>> cpFilterYN(int currentPage, int limit, String str, int mno) {
		// TODO Auto-generated method stub
		return null;
	}

}
