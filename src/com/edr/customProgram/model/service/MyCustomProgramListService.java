package com.edr.customProgram.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.customProgram.model.dao.MyCustomProgramListDao;

public class MyCustomProgramListService {

	public int mcpListCount(int mno) {
		Connection con = getConnection();

		int mcpListCount = new MyCustomProgramListDao().mcpListCount(con, mno);

		close(con);
		return mcpListCount;
	}

	public ArrayList<HashMap<String, Object>> mcpWithPaging(int currentPage, int limit, int mno) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MyCustomProgramListDao().mcpWithPaging(con, currentPage, limit, mno);
 

		close(con);
		
		return list;
	}

	public int selectEstNo(int mno) {
		
		return 0;
	}

}
