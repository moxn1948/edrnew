package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.AdCustomProgramService;
import com.edr.customProgram.model.service.MyCpListService;

/**
 * Servlet implementation class AdCustomProgramDetailServlet
 */
@WebServlet("/adCustomProgramDetail.cp")
public class AdCustomProgramDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdCustomProgramDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int cpNo = Integer.parseInt(request.getParameter("cpNo"));
		int cpEstNo = Integer.parseInt(request.getParameter("cpEstNo"));
		String cpState = request.getParameter("cpState");

		System.out.println("cpNo : " + cpNo);
		System.out.println("cpEstNo : " + cpEstNo);
		System.out.println("cpState : " + cpState);

		// 서비스 측으로 연결
		HashMap<String, Object> cpDetail = new MyCpListService().SelectMyCpDetail(cpNo, cpState);

		if(true) {
			request.setAttribute("cpNo", cpNo);
			request.setAttribute("cpDetail", cpDetail);
			request.setAttribute("cpState", cpState);
			

			request.getRequestDispatcher("views/admin/sub/custom_program/customProgramDetail.jsp").forward(request, response);
		}else {

		}

		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
