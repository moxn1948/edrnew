package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.MyCpListService;
import com.edr.customProgram.model.service.SelectOneMCPService;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpPriceDetatil;

/**
 * Servlet implementation class SelectOneMCP
 */
@WebServlet("/selectOneMCP.cp")
public class SelectOneMCP extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneMCP() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));
		String state = request.getParameter("state");
		
		HashMap<String, Object> cpDetail = new MyCpListService().SelectMyCpDetail(cpNo, state);
		 
		if(true) {
			request.setAttribute("cpNo", cpNo);
			request.setAttribute("cpDetail", cpDetail);
			
			request.getRequestDispatcher("views/user/sub/guide_page/guide_MyCustomProgramDetail.jsp").forward(request, response);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=selectMyCpDetail");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
