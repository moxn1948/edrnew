package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.edr.common.MyFileRenamePolicy;
import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.service.InsertMyCustomProgramService;
import com.edr.customProgram.model.vo.CpGuideDetail;
import com.edr.customProgram.model.vo.CpGuideEst;
import com.edr.customProgram.model.vo.CpPriceDetatil;
import com.oreilly.servlet.MultipartRequest;

/**
 * Servlet implementation class InsertMyCustomProgramServlet
 */
@WebServlet("/insertMCP.cp")
public class InsertMyCustomProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertMyCustomProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 10;
			
			String root = request.getSession().getServletContext().getRealPath("/");
			String savePath = root + "uploadFiles/";
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();
			
			multiRequest.getFileNames();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files. nextElement();
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			int cpNo = Integer.parseInt(multiRequest.getParameter("cpNo"));
			int mno = Integer.parseInt(multiRequest.getParameter("mno"));
			int cpDay = Integer.parseInt(multiRequest.getParameter("result"));
			int pay =  Integer.parseInt(multiRequest.getParameter("pay"));
			
			String[] include = multiRequest.getParameterValues("includeYn0");
			String[] notClude = multiRequest.getParameterValues("includeYn1");
			String dateTime = multiRequest.getParameter("dateTime");
			String postcode = multiRequest.getParameter("postcode");
			String roadAddress = multiRequest.getParameter("roadAddress");
			String detailAddress = multiRequest.getParameter("detailAddress");
			String address = postcode + "/" + roadAddress + "/" + detailAddress;
			String cnt = multiRequest.getParameter("dayCnt");
			String notice = multiRequest.getParameter("notice");
			ArrayList day = new ArrayList(); 
			String allCnt = "";
			String cpCategory[] = multiRequest.getParameterValues("cpCategory");
			String cpPrice[] = multiRequest.getParameterValues("cpPrice");
			
			String[] includeYn = new String[cpCategory.length];			
			String[] dayCnt = multiRequest.getParameterValues("dayCnt");
			
			
			
			
			for(int i = 0; i < includeYn.length; i++) {
				includeYn[i] = multiRequest.getParameter("includeYn" + i);

				
				
			}
			 
			
			CpGuideEst ce = new CpGuideEst();
			ce.setCpNo(cpNo);
			ce.setMno(mno);
			ce.setTotalCost(pay);
			ce.setMeetTime(dateTime);
			ce.setMeetArea(address);
			ce.setUniqueness(notice);
			
			
			int ceResult = new InsertMyCustomProgramService().insertMCP(ce);
			
			int estNo = new InsertMyCustomProgramService().selectEstNo(ce);
			
			
		
			ArrayList<CpGuideDetail> cpDetailList = new ArrayList<>();
			CpGuideDetail cgd = null;
			
			for(int i = 0; i < dayCnt.length; i++) { 
				cgd = new CpGuideDetail();
				cgd.setCpDay(i +1);
				cgd.setCpCnt(dayCnt[i]);
				
				cpDetailList.add(cgd);
				
				
			}
					 
			int cgdResult = new InsertMyCustomProgramService().insertCgdMCP(cpDetailList, estNo);
			
			ArrayList<CpPriceDetatil> cpPriceDetailList = new ArrayList<>();
			CpPriceDetatil cgpd = null;
			
			for(int i = 0; i < cpCategory.length; i++) {
				cgpd = new CpPriceDetatil();
				cgpd.setCpCategory(cpCategory[i]);
				
				if(includeYn[i].equals("include")) {
					cgpd.setCpPrice(Integer.parseInt(cpPrice[i]));
					cgpd.setCpInc("INCLUDE");
				}else {
					cgpd.setCpInc("NOTCLUDE");
				}
				cpPriceDetailList.add(cgpd);
			}
			
			int cgpdResult = new InsertMyCustomProgramService().insertCgpdMCP(cpPriceDetailList, estNo);
			
			
			
			
			ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			Attachment at = null;
			for(int i = originFiles.size() -1 ; i>=0; i--) {
			at = new Attachment();
			at.setFilePath(savePath);
			at.setMno(mno);
			at.setDay(cpDay);
			at.setOriginName(originFiles.get(i));
			at.setChangeName(saveFiles.get(i));
			at.setGhNo(cgdResult);
			
			fileList.add(at);
			
			System.out.println("at : " + at);
			}
			int atResult = new InsertMyCustomProgramService().insertAttachment(fileList, cgdResult, estNo, cpDay);

			int userMno = new InsertMyCustomProgramService().selectUserMno(cpNo);
			
			int upResult = new InsertMyCustomProgramService().updateStatus(estNo, userMno, cpNo);
			
			System.out.println("upResult : "+upResult);
			
			String page = "";
			if(ceResult > 0 && cgdResult > 0 && atResult > 0) {
				response.sendRedirect(request.getContextPath()+"/selectCp.cp?mno=" + mno);
				
				
			}else {
				
			}
		
			
		}	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
