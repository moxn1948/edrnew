package com.edr.customProgram.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.MyCpListService;

/**
 * Servlet implementation class UpdateMyCpBackServlet
 */
@WebServlet("/updateMyCpBack.cp")
public class UpdateMyCpBackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateMyCpBackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));
		int estNo = Integer.parseInt(request.getParameter("estNo"));
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		System.out.println(cpNo);
		System.out.println(estNo);
		
		int result = new MyCpListService().updateGuideEstState(cpNo, estNo);
		
		if(result > 0) {
			response.sendRedirect(request.getContextPath() + "/selectMyCpList.cp?mno=" + mno);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=updateMyCpBack");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
