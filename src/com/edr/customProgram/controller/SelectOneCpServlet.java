package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.SelectOneCpService;
import com.edr.customProgram.model.vo.CpLang;

/**
 * Servlet implementation class SelectOneCpServlet
 */
@WebServlet("/selectOne.cp")
public class SelectOneCpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneCpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));

		
		ArrayList<HashMap<String, Object>> list = new SelectOneCpService().selectOneCp(cpNo);
		ArrayList<CpLang> listLang = new SelectOneCpService().adSelectOneLang(cpNo);
		int result = new SelectOneCpService().selectDate(cpNo);
		
		
		
		
		String page ="";
		if(list != null) {
			page= "views/user/sub/guide_page/guide_customProgramDetail.jsp";
			request.setAttribute("list", list);
			request.setAttribute("listLang", listLang);
			request.setAttribute("result", result);
			request.setAttribute("cpNo", cpNo);
			
		}else {
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
