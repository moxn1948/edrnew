package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.MyCpListService;

/**
 * Servlet implementation class SelectCompMyCpDetailServlet
 */
@WebServlet("/selectCompMyCpDetail.cp")
public class SelectCompMyCpDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectCompMyCpDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cpNo = Integer.parseInt(request.getParameter("cpNo"));
		
		System.out.println(cpNo);
		// 서비스 측으로 연결
		HashMap<String, Object> cpDetail = new MyCpListService().SelectMyCpDetail(cpNo);
		 
		if(true) {
			request.setAttribute("cpNo", cpNo);
			request.setAttribute("cpDetail", cpDetail);
			
			request.getRequestDispatcher("views/user/sub/my_page/processedCustomProgramDetail.jsp").forward(request, response);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=selectMyCpDetail");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
