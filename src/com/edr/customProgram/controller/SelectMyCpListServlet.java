package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.customProgram.model.service.MyCpListService;
import com.edr.customProgram.model.vo.CpRequest;
import com.edr.guide.model.service.CompGpService;

/**
 * Servlet implementation class SelectMyCpListServlet
 * moxn
 * 내 맞춤 프로그램 목록
 */
@WebServlet("/selectMyCpList.cp")
public class SelectMyCpListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectMyCpListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));

		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		//전달받은 페이지 추출
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한 페이지에 보여질 목록 갯수
		limit = 5;

		
		//전체 목록 갯수 조회
		int listCount = new MyCpListService().selectMyCpCount(mno);
		
		maxPage = (int) ((double)listCount / limit + 0.8);
		
		startPage = (((int)((double) currentPage / 5 + 0.8)) - 1) * 5 + 1;
		
		endPage = startPage + 5 - 1;

		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
		ArrayList<HashMap<String, Object>> myCpList = new MyCpListService().selectMyCpList(currentPage, limit, mno);
		
		if(myCpList != null) {
			request.setAttribute("pi", pi);
			request.setAttribute("myCpList", myCpList);
			
			request.getRequestDispatcher("views/user/sub/my_page/unprocessedCustomProgramList.jsp").forward(request, response);
		}else {
			response.sendRedirect("views/user/sub/common/errorPage.jsp?page=selectMyCpList");
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
