package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.customProgram.model.service.AdCustomProgramService;

/**
 * Servlet implementation class AdSearchCustomServlet
 */
@WebServlet("/adSearchCustom.cp")
public class AdSearchCustomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSearchCustomServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String category = request.getParameter("category");
		String searchTxt = request.getParameter("searchTxt");
		
		
		ArrayList<HashMap<String, Object>> list = null;
		PageInfo pi = null;
		
		if(category.equals("order")) {
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;
			
			currentPage = 1;
			
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
				//한 페이지에 보여질 목록 갯수
				limit = 5;
				
				//전체 목록 갯수 조회
				int listCount = new AdCustomProgramService().adSearchOrderCustomCount(searchTxt);
				//총 페이지수 계싼
				maxPage = (int) ((double) listCount / limit + 0.8);
				
				//현재 페이지에 보여 줄 시작 페이지수
				startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
				
				//목록 아래 쪽에 보여질 마지막 페이지 수
				endPage = startPage + 5 - 1;
				
				if(maxPage < endPage) {
					endPage = maxPage;
				}
				
				
				pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
				list = new AdCustomProgramService().adSearchOrderList(currentPage, limit, searchTxt);
		}else if(category.equals("guide")) {
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;
			
			currentPage = 1;
			
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
				//한 페이지에 보여질 목록 갯수
				limit = 5;
				
				//전체 목록 갯수 조회
				int listCount = new AdCustomProgramService().adSearchGuideCustomProgramCount(searchTxt);
				//총 페이지수 계싼
				maxPage = (int) ((double) listCount / limit + 0.8);
				
				//현재 페이지에 보여 줄 시작 페이지수
				startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
				
				//목록 아래 쪽에 보여질 마지막 페이지 수
				endPage = startPage + 5 - 1;
				
				if(maxPage < endPage) {
					endPage = maxPage;
				}
				
				
				pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
				list = new AdCustomProgramService().adSearchGuideCustomProgramList(currentPage, limit,searchTxt);
		}else {
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;
			
			currentPage = 1;
			
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
				//한 페이지에 보여질 목록 갯수
				limit = 5;
				
				//전체 목록 갯수 조회
				int listCount = new AdCustomProgramService().adSearchTitleCustomCount(searchTxt);
				//총 페이지수 계싼
				maxPage = (int) ((double) listCount / limit + 0.8);
				
				//현재 페이지에 보여 줄 시작 페이지수
				startPage = (((int)((double) currentPage / limit + 0.8)) - 1) * 5 + 1;
				
				//목록 아래 쪽에 보여질 마지막 페이지 수
				endPage = startPage + 5 - 1;
				
				if(maxPage < endPage) {
					endPage = maxPage;
				}
				
				
				pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
				list = new AdCustomProgramService().adSearchTitleCustomProgram(currentPage, limit,searchTxt);
		}
		
		String page = "";
		if(list != null) {
			page = "views/admin/sub/custom_program/SearchCustomProgram.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("category", category);
			request.setAttribute("searchTxt", searchTxt);
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "안돼");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
