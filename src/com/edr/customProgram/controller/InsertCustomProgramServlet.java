package com.edr.customProgram.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.customProgram.model.service.InsertCustomProgramService;
import com.edr.customProgram.model.vo.CpLang;
import com.edr.customProgram.model.vo.CpRequest;

/**
 * Servlet implementation class InsertCustomProgramServlet
 */
@WebServlet("/insertCp.cp")
public class InsertCustomProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertCustomProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		int local = Integer.parseInt(request.getParameter("local"));
		int person = Integer.parseInt(request.getParameter("person"));
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		int money = Integer.parseInt(request.getParameter("money"));
		String[] lang = request.getParameterValues("language");
		String[] langEtc = request.getParameterValues("language5");
		String language = "";
		String gender = request.getParameter("gender");
		int age = Integer.parseInt(request.getParameter("age"));
		String title = request.getParameter("subject");
		String cnt = request.getParameter("cnt");
		Date date1 = null;
		Date date2 = null;
		java.sql.Date date3 = null;
		java.sql.Date date4 = null;
				
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date1 = dt.parse(startDate);
			date2 = dt.parse(endDate);
			
			date3 = new java.sql.Date(date1.getTime());
			date4 = new java.sql.Date(date2.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ArrayList<CpLang> langList = new ArrayList<>();
		CpLang reqCpLang = null;
		if(lang != null) {
			for(int i = 0; i < lang.length; i++) {
				reqCpLang = new CpLang();
				reqCpLang.setLang(lang[i]);
				langList.add(reqCpLang);
			}
		}
		if(langEtc != null) {
			for(int i = 0; i < langEtc.length; i++) {
				
				language += ", "  + langEtc[i];
				
			}
		}
		System.out.println("langList2 : " + langList);
		CpRequest cr = new CpRequest();
		cr.setMno(mno);
		cr.setCpTitle(title);
		cr.setCpCnt(cnt);
		cr.setCpPerson(person);
		cr.setCpSdate(date3);
		cr.setCpEdate(date4);
		cr.setCpCost(money);
		cr.setCpGender(gender);
		cr.setCpAge(age);
		cr.setLocalNo(local);
		int result = new InsertCustomProgramService().inserCp(cr);
		
		
		
		int cpNo = new InsertCustomProgramService().selectCurrval();
		
		HashMap<String, Object> reqMap = new HashMap<>();
		reqMap.put("langList", langList);
		System.out.println("langList : " + langList);
		
		int result3 = new InsertCustomProgramService().insertLang(reqMap, cpNo);
		
		if(result > 0 && result3 > 0) {
			response.sendRedirect("views/user/sub/common/successPage.jsp?page=insertCp");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
