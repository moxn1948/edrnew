package com.edr.customProgram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.customProgram.model.service.CustomProgramListService;

/**
 * Servlet implementation class SelectCustomProgram
 */
@WebServlet("/selectCpFilter.cp")
public class SelectCustomProgramFilter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectCustomProgramFilter() {
        super();
        // TODO Auto-generated constructor stub
    }
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		String str = request.getParameter("str");
		System.out.println("str:" + str);

		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		int listCount = 0;
		
		ArrayList<HashMap<String, Object>> list = null;
		
		PageInfo pi = null;
		
		
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 5;

		listCount = new CustomProgramListService().cpListFilterCount(str, mno);
		
		maxPage = (int) ((double)listCount / limit + 0.8);
		
		startPage = (((int)((double) currentPage / 5 + 0.8)) - 1) * 5 + 1;
		
		endPage = startPage + 5 - 1;

		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
		list = new CustomProgramListService().cpFilter(currentPage, limit, str, mno);
		
		
		String page = "";
		if(list != null) {
			page = "views/user/sub/guide_page/guide_customProgramListFilter.jsp";
			request.setAttribute("list", list);
			request.setAttribute("str", str);
			request.setAttribute("pi", pi);
		}else {
			page = "views/user/sub/common/errorPage.jsp";
			request.setAttribute("msg", "맞춤 프로그램 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
