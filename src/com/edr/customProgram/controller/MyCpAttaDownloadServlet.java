package com.edr.customProgram.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.Attachment;
import com.edr.customProgram.model.service.MyCpListService;
import com.edr.guide.model.service.AdGuideService;

/**
 * Servlet implementation class MyCpAttaDownloadServlet
 */
@WebServlet("/myCpAttaDownload.cp")
public class MyCpAttaDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyCpAttaDownloadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int num = Integer.parseInt(request.getParameter("estNo"));

		Attachment cpAttr = new MyCpListService().selectAttachMent(num);

		String root = "";
		for(int i = 0; i < 1; i++) {
			root = request.getSession().getServletContext().getRealPath("/");

		}
		
		int nrr = root.indexOf("edrnew");

		String sub = root.substring(0, root.indexOf("git"));
		String file = "";
		String file2 = ""; ;
		String filePath = "";
		//sub는 내주소 git까지 짜른거
	
		file = cpAttr.getChangeName();
		file2 = cpAttr.getOriginName();
		filePath = cpAttr.getFilePath();
					
		
		String fileS = filePath.substring(filePath.indexOf("git"));
		
		String fileSave = sub + fileS;
		
		String userSub = file.substring(filePath.indexOf("git"));

		File downFile = null;
		
		downFile = new File(fileSave + cpAttr.getChangeName());
		
		BufferedInputStream buf = null;

		ServletOutputStream downOut = null;

		downOut = response.getOutputStream();



		response.setContentType("text/plain; charset=UTF-8");
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(cpAttr.getOriginName().getBytes("UTF-8"), "ISO-8859-1") + "\""); 
		response.setContentLength((int) downFile.length());

		FileInputStream fin = new FileInputStream(downFile);
		buf = new BufferedInputStream(fin);

		int readBytes = 0;

		while((readBytes = buf.read()) != -1) {
			downOut.write(readBytes);
		}
		
		downOut.close();
		buf.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
