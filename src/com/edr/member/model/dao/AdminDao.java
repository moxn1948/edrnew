package com.edr.member.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.edr.member.model.vo.Member;

public class AdminDao {
	
Properties prop = new Properties();
	
		public AdminDao() {
		
		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : admin loginCheck
	 * @param con
	 * @param requestMember
	 * @return         : loginAdmin
	 */
	public Member adminLoginCheck(Connection con, Member requestMember) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member loginAdmin = null;
		
		String query = prop.getProperty("adminLoginCheck");
		System.out.println("query : " + query);
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getNickName());
			pstmt.setString(2, requestMember.getMpwd());
			
			rset = pstmt.executeQuery();
			System.out.println("rset : " + rset);
			
			
			if(rset.next()) {
				loginAdmin = new Member();
				
					
					loginAdmin.setMno(rset.getInt("MNO"));
					loginAdmin.setNickName(rset.getString("NICKNAME"));
					loginAdmin.setEmail(rset.getString("EMAIL"));
					loginAdmin.setMpwd(rset.getString("MPWD"));
					loginAdmin.setMgender(rset.getString("MGENDER"));
					loginAdmin.setMbirth(rset.getString("MBIRTH"));
					loginAdmin.setWcount(rset.getInt("WCOUNT"));
					loginAdmin.setBankName(rset.getString("BANK_NAME"));
					loginAdmin.setAccountName(rset.getString("ACCOUNT_NAME"));
					loginAdmin.setAccountNo(rset.getString("ACCOUNT_NO"));
					loginAdmin.setEnrollDate(rset.getDate("ENROLL_DATE"));
					loginAdmin.setEmailYn(rset.getString("EMAIL_YN"));
					loginAdmin.setModifyYn(rset.getString("MODIFY_YN"));
					loginAdmin.setModifyDate(rset.getDate("MODIFY_DATE"));
					loginAdmin.setMtype(rset.getString("MTYPE"));
					
					System.out.println("loginAdmin : " + loginAdmin);
				
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return loginAdmin;
	}


	public int idPwdCheck(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("idPwdCheckAdmin");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getNickName());
			pstmt.setString(2, requestMember.getMpwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

}
