package com.edr.member.model.dao;

import static com.edr.common.JDBCTemplate.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.member.model.vo.Member;

public class MemberAdSelectDao {

	Properties prop = new Properties();

	public MemberAdSelectDao() {

		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Member list select Dao
	 * @param con
	 * @return : list
	 */
	public ArrayList<Member> selectMemberList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Member> list = null;

		String query = prop.getProperty("selectMemberList");

		try {
			stmt = con.createStatement();

			rset = stmt.executeQuery(query);

			list = new ArrayList<>();

			while (rset.next()) {
				Member m = new Member();
				m.setMno(rset.getInt("MNO"));
				m.setNickName(rset.getString("NICKNAME"));
				m.setEmail(rset.getString("EMAIL"));
				m.setMpwd(rset.getString("MPWD"));
				m.setMgender(rset.getString("MGENDER"));
				m.setMbirth(rset.getString("MBIRTH"));
				m.setWcount(rset.getInt("WCOUNT"));
				m.setBankName(rset.getString("BANK_NAME"));
				m.setAccountName(rset.getString("ACCOUNT_NAME"));
				m.setAccountNo(rset.getString("ACCOUNT_NO"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setEmailYn(rset.getString("EMAIL_YN"));
				m.setModifyYn(rset.getString("MODIFY_YN"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setMtype(rset.getString("MTYPE"));

				list.add(m);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Member list Count Dao
	 * @param :
	 *            con
	 * @return : list
	 */
	public int getlistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("listCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			close(stmt);
			close(rset);

		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Ad Member list selectWithPaging Dao
	 * @param :
	 *            con, currentPage, limit
	 * @return : list
	 */

	public ArrayList<HashMap<String, Object>> selectListWithPaging(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("selectListWithPaging");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				
				if(rset.getString("MTYPE").equals("TOURIST")) {
					hmap.put("mtype", "투어객");
				}else {
					hmap.put("mtype", "가이드");
				}				

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : Ad Member list detail select Dao
	 * @param :
	 *            con, num
	 * @return : m
	 */
	public HashMap<String, Object> selectOneMember(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;

		String query = prop.getProperty("selectOneMember");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				hmap =new HashMap<String, Object>();
				
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				if(rset.getString("MGENDER").equals("M")) {
					hmap.put("mgender", "남성");
				}else {
					hmap.put("mgender", "여성");
				}
				
				if(rset.getString("MTYPE").equals("TOURIST")) {
					hmap.put("mtype", "투어객");
				}else {
					hmap.put("mtype", "가이드");
				}
				
				hmap.put("mbirth", rset.getString("MBIRTH"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				hmap.put("enrolldate", rset.getDate("ENROLL_DATE"));
				hmap.put("accountname", rset.getString("ACCOUNT_NAME"));
				hmap.put("accountno", rset.getString("ACCOUNT_NO"));
				
				if(rset.getString("ACCOUNT_NO") == null) {
					hmap.put("accountno", "");
				}else {
					hmap.put("accountno", rset.getString("ACCOUNT_NO"));
				}
				
				if(rset.getString("ACCOUNT_NAME") == null) {
					hmap.put("accountname", "미등록");
				}else {
					hmap.put("accountname", rset.getString("ACCOUNT_NAME"));
				}
				
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return hmap;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Member list Search Name listCount Dao
	 * @param :
	 *            con, searchText
	 * @return : m
	 */
	public int adSearchNameMemberCount(Connection con, String searchText) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchNameMemberCount");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);

		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Member list Search Name Dao
	 * @param :
	 *            con, currentPage, limit,searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchNameMember(Connection con, int currentPage, int limit,
			String searchText) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchNameMember");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				hmap.put("mtype", rset.getString("MTYPE"));

				list.add(hmap);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Member list Search Id Count Dao
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchIdMemberCount(Connection con, String searchText) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adSearchIdMemberCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Ad Member list Search Id Dao
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchIdMember(Connection con, int currentPage, int limit,
			String searchText) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adSearchIdMember");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchText);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				hmap.put("mtype", rset.getString("MTYPE"));

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Member list Update Dao
	 * @param :con,
	 *            m
	 * @return : result
	 */
	public int adUpdateMember(Connection con, Member m) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("adUpdateMember");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, m.getNickName());
			pstmt.setInt(2, m.getMno());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Ad Member list Delete Dao
	 * @param :con,
	 *            m
	 * @return : result
	 */
	public int adDeleteMember(Connection con, Member m) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("adDeleteMember");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, m.getMno());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad Member list Filter TG listCount Dao
	 * @param :con,
	 *            str
	 * @return : listCount
	 */
	public int adMemberFilterTGlistCount(Connection con, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adMemberFilterTGlistCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad Member list Filter TG Dao
	 * @param :con,
	 *            currentPage, limit, str
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adMemberTGFilter(Connection con, int currentPage, int limit, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adMemberTGFilter");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				
				if(rset.getString("MTYPE").equals("TOURIST")) {
					hmap.put("mtype", "투어객");
				}else {
					hmap.put("mtype", "가이드");
				}			

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}
		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad Member list Filter YN listCount Dao
	 * @param :con,
	 *            str
	 * @return : listCount
	 */
	public int adMemberFilterYNlistCount(Connection con, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int listCount = 0;

		String query = prop.getProperty("adMemberFilterYNlistCount");

		try {

			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				listCount = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Ad Member list Filter YN Dao
	 * @param :con,
	 *            currentPage, limit, str
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adMemberYNFilter(Connection con, int currentPage, int limit, String str) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		String query = prop.getProperty("adMemberYNFilter");

		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, str);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while (rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				hmap.put("rnum", rset.getInt("RNUM"));
				hmap.put("idxnum", rset.getInt("IDXNUM"));
				hmap.put("mno", rset.getInt("MNO"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nickname", rset.getString("NICKNAME"));
				
				if(rset.getString("MTYPE").equals("TOURIST")) {
					hmap.put("mtype", "투어객");
				}else {
					hmap.put("mtype", "가이드");
				}			

				list.add(hmap);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(pstmt);

		}

		return list;
	}

}
