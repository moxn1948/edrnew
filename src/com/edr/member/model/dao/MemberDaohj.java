package com.edr.member.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.common.model.vo.Local;
import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.vo.Member;
import static com.edr.common.JDBCTemplate.*;

public class MemberDaohj {
	
	Properties prop = new Properties();
	
	
			
	
	
	public MemberDaohj() {
		
		String fileName = MemberDaohj.class.getResource("/sql/member/member-query.properties").getPath();
		
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 13. 
	 * @Description  :
	 * @param   :
	 * @return    :
	 */
	public int sendPwdMail(Connection con, Member checkMember) {
		PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		
		int result =0;
		
		String query = prop.getProperty("sendPwdMail");
		
		try {
			pstmt = con.prepareStatement(query);
		
			pstmt.setString(1, checkMember.getEmail());
		
			
			rset =pstmt.executeQuery();
			
			if(rset.next()) {
				
				
				//resultSet의 첫번째결과값을 가져온다.
				result = rset.getInt(1);
				
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		
		
		
		
		return result;
	}





	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 14. 
	 * @Description  : 비밀번호 잊었을시 새롭게 설정하는 기능 
	 * @param   :con,requestMember
	 * @return    : result
	 */
	public int forgotChange(Connection con, Member requestMember) {
		
		PreparedStatement pstmt = null;
		
		int result=  0;
		
		String query = prop.getProperty("changePwd");
		
		try {
			pstmt=con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getMpwd());
			pstmt.setString(2, requestMember.getEmail());
			
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		return result;
	}












	
}
