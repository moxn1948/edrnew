package com.edr.member.model.dao;

import static com.edr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.member.model.vo.Member;
import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;


public class MemberDao {

	Properties prop = new Properties();
	
	public MemberDao() {
		
		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 활동명 중복확인
	 * @param con
	 * @param checkMember
	 * @return         : result
	 */
	public int idCheck(Connection con, Member checkMember) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("idCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, checkMember.getNickName());

			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 이메일 중복확인
	 * @param con
	 * @param checkMember
	 * @return         : result
	 */
	public int emailCheck(Connection con, Member checkMember) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		String query = prop.getProperty("emailCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, checkMember.getEmail());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 회원가입
	 * @param con
	 * @param m
	 * @return         : result
	 */
	public int insertMember(Connection con, Member m) {

		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertMember");
		System.out.println(m);
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, m.getNickName());
			pstmt.setString(2, m.getEmail());
			pstmt.setString(3, m.getMpwd());
			pstmt.setString(4, m.getMgender());
			pstmt.setString(5, m.getMbirth());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 이메일 인증
	 * @param con
	 * @param email
	 * @return         : result
	 */
	public int accessEmail(Connection con, String email) {
		
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("accessEmail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, email);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 로그인할때 멤버가 있는지 확인
	 * @param con
	 * @param requestMember
	 * @return         : loginUser
	 */
	public Member memberCheck(Connection con, Member requestMember) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member loginUser = null;
		
		String query = prop.getProperty("checkMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getEmail());
			pstmt.setString(2, requestMember.getMpwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				
				loginUser = new Member();
				loginUser.setMno(rset.getInt("MNO"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setMpwd(rset.getString("MPWD"));
				loginUser.setNickName(rset.getString("NICKNAME"));
				loginUser.setMgender(rset.getString("MGENDER"));
				loginUser.setMbirth(rset.getString("MBIRTH"));
				loginUser.setWcount(rset.getInt("WCOUNT"));
				loginUser.setBankName(rset.getString("BANK_NAME"));
				loginUser.setAccountName(rset.getString("ACCOUNT_NAME"));
				loginUser.setAccountNo(rset.getString("ACCOUNT_NO"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setEmailYn(rset.getString("EMAIL_YN"));
				loginUser.setModifyYn(rset.getString("MODIFY_YN"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setMtype(rset.getString("MTYPE"));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		
		return loginUser;
	}



	public int emailAccessCheck(Connection con, Member requestMember) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		String emailYn = null;
		
		String query = prop.getProperty("emailAccessCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getEmail());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				emailYn = rset.getString("EMAIL_YN");
				if(emailYn.equals("N")) {
					result = 2;
				}else if(emailYn.equals("Y")) {
					result = 3;
				}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		
		return result;
	}

	public int idPwdCheck(Connection con, Member requestMember) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("idPwdCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getEmail());
			pstmt.setString(2, requestMember.getMpwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				
				result = rset.getInt(1);
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		
		return result;
	}
       /**
    * @Author  : hjheo
    * @CreateDate  : 2019. 12. 10. 
    * @Description
    * @param
    * @return 
    */
   public int checkPwd(Connection con, Member requestMember) {
         
         PreparedStatement pstmt = null;
         int result =0;
         ResultSet rset = null;
         String query = prop.getProperty("checkPwd");
         
         try {
         pstmt =con.prepareStatement(query);
         
         pstmt.setString(1, requestMember.getEmail());
         pstmt.setString(2, requestMember.getMpwd());
         
         rset =pstmt.executeQuery();
         
         if(rset.next()) {
        	 result=rset.getInt(1);
         }
             
         
         
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         close(pstmt);
         close(rset);
      }
         
         
         return result;
      }
   
   /** dao
    * @Author  : hjheo
    * @CreateDate  : 2019. 12. 11. 
    * @Description  : changePwd
    * @param   : con, requsetMember
    * @return    : result
    */
   public int changePwd(Connection con, Member requestMember) {
      PreparedStatement pstmt = null;
      
      int result  = 0;
      
      String query = prop.getProperty("changePwd");
      
      try {
         pstmt = con.prepareStatement(query);
         
         pstmt.setString(1, requestMember.getMpwd());
         pstmt.setString(2, requestMember.getEmail());
         
         result =pstmt.executeUpdate();
         System.out.println("resultdao: "+result);
         
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }finally {
         
         close(pstmt);
      }
            
      
      return result;
   }

public HashMap<String, Object> selectUnProcessedProgramList(Connection con, int currentPage, int limit, int mno) {
	
	PreparedStatement pstmt = null;
	HashMap<String, Object> hmap = null;
	String query = prop.getProperty("selectListUnProcessedProgram");
	ResultSet rset = null;
	ArrayList<Gp> gpNameList = null;
	ArrayList<Payment> paymentDateList = null;
	ArrayList<Product> productDateList = null;
	ArrayList<Payment> paymentStateList = null;
	ArrayList<Integer> unProNo = null;
	ArrayList<Product> productNumber = null;
	ArrayList<OrderList> orderCodeList = null;
	Gp gp = null;
	Payment paymentDate = null;
	Product productDate = null;
	Payment paymentState = null;
	Product productNum = null;
	OrderList orderCode = null;
	
	int startRow = (currentPage - 1) * limit + 1; // 21
	int endRow = startRow + limit - 1; // 30
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		pstmt.setInt(2, startRow);
		pstmt.setInt(3, endRow);
		
		rset = pstmt.executeQuery();
		
		unProNo = new ArrayList<>();
		gpNameList = new ArrayList<>();
		paymentDateList = new ArrayList<>();
		productDateList = new ArrayList<>();
		paymentStateList = new ArrayList<>();
		productNumber = new ArrayList<>();
		orderCodeList = new ArrayList<>();
		
		while(rset.next()) {
			gp = new Gp();
			paymentDate = new Payment();
			productDate = new Product();
			paymentState = new Payment();
			productNum = new Product();
			orderCode = new OrderList();
			
			int unProNumber = rset.getInt("RNUM");
			gp.setGpName(rset.getString("GP_NAME"));
			paymentDate.setPayChange(rset.getDate("PAY_CHANGE"));
			productDate.setPdate(rset.getDate("PDATE"));
			paymentState.setPayState(rset.getString("PAY_STATE"));
			productNum.setPno(rset.getInt("PNO"));
			orderCode.setOrderCode(rset.getLong("ORDER_CODE"));
			
			unProNo.add(unProNumber);
			gpNameList.add(gp);
			paymentDateList.add(paymentDate);
			productDateList.add(productDate);
			paymentStateList.add(paymentState);
			productNumber.add(productNum);
			orderCodeList.add(orderCode);
		}
		hmap = new HashMap<>();
		
		hmap.put("unProNo", unProNo);
		hmap.put("gpNameList", gpNameList);
		hmap.put("paymentDateList", paymentDateList);
		hmap.put("productDateList", productDateList);
		hmap.put("paymentStateList", paymentStateList);
		hmap.put("productNumber", productNumber);
		hmap.put("orderCodeList", orderCodeList);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	
	return hmap;
}

public int getUnProcessedProgramListCount(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	
	int result = 0;
	
	String query = prop.getProperty("unpListCount");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			result = rset.getInt(1);
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return result;
}

public HashMap<String, Object> selectOneUnProcessedProgramDetail(Connection con, OrderList orderList) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	HashMap<String, Object> hmap = null;
	ArrayList<GpDetail> gpDetailList = null;
	GpDetail gpDetail = null;
	Gp gp = null;
	OrderDetail recPerson = null;
	
	String query = prop.getProperty("selectOneUnProcessedProgramDetail");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setLong(1, orderList.getOrderCode());
		
		rset = pstmt.executeQuery();
		
		gpDetailList = new ArrayList<>();
		
		while(rset.next()) {
			gp = new Gp();
			recPerson = new OrderDetail();
			gpDetail = new GpDetail();
			
			gp.setGpTday(rset.getInt("GP_TDAY"));
			gp.setGpCost(rset.getInt("GP_COST"));
			
			recPerson.setRecPerson(rset.getInt("RECE_PERSON"));
			
			gpDetail.setGpDay(rset.getInt("GP_DAY"));
			gpDetail.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
			gpDetail.setGpLocation(rset.getString("GP_LOCATION"));
			gpDetail.setGpTime(rset.getInt("GP_TIME"));
			
			gpDetailList.add(gpDetail);
		}
		
		hmap = new HashMap<>();
		hmap.put("gp", gp);
		hmap.put("recPerson", recPerson);
		hmap.put("gpDetailList", gpDetailList);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return hmap;
}

public HashMap<String, Integer> searchOrderNo(Connection con, OrderList orderList) {

	PreparedStatement pstmt = null;
	ResultSet rset = null;
	int orderNo = 0;
	int pno = 0;
	HashMap<String, Integer> orderNoandPno = null;
	
	String query = prop.getProperty("searchOrderNo");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setLong(1, orderList.getOrderCode());
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			orderNo = rset.getInt("ORDER_NO");
			pno = rset.getInt("PNO");
		}
		orderNoandPno = new HashMap<>();
		orderNoandPno.put("orderNo", orderNo);
		orderNoandPno.put("pno", pno);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return orderNoandPno;
}

public Payment cancelGeneralProgramSelect(Connection con, HashMap<String, Integer> orderNoandPno) {

	PreparedStatement pstmt = null;
	ResultSet rset = null;
			
	String query = prop.getProperty("cancelGeneralProgramSelect");
	int orderNo = orderNoandPno.get("orderNo");
	int pno = orderNoandPno.get("pno");
	Payment payment = null;
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, orderNo);
		pstmt.setInt(2, pno);
		
		rset = pstmt.executeQuery();
		if(rset.next()) {
			payment = new Payment();
			payment.setPayNo(rset.getInt("PAY_NO"));
			payment.setPno(rset.getInt("PNO"));
			payment.setOrderNo(rset.getInt("ORDER_NO"));
			payment.setPayCost(rset.getInt("PAY_COST"));
			payment.setPayKind(rset.getString("PAY_KIND"));
			payment.setPayChange(rset.getDate("PAY_CHANGE"));
			payment.setPayState(rset.getString("PAY_STATE"));
			
		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return payment;
}

public int getCompleteProcessedProgramListCount(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	
	int result = 0;
	
	String query = prop.getProperty("cnpListCount");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			result = rset.getInt(1);
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return result;
	
}

public HashMap<String, Object> selectCompleteProcessedProgramList(Connection con, int currentPage, int limit, int mno) {
	
	PreparedStatement pstmt = null;
	HashMap<String, Object> hmap = null;
	String query = prop.getProperty("selectListCompleteProcessedProgram");
	ResultSet rset = null;
	ArrayList<Gp> gpNameList = null;
	ArrayList<Payment> paymentDateList = null;
	ArrayList<Product> productDateList = null;
	ArrayList<Integer> unProNo = null;
	ArrayList<Product> productNumber = null;
	ArrayList<OrderList> orderCodeList = null;
	ArrayList<Gp> gpNoList = null;
	Gp gp = null; //일반프로그램
	Payment paymentDate = null;
	Product productDate = null;
	Product productNum = null; //상품
	OrderList orderCode = null; //주문
	Gp gpNo = null;
	
	int startRow = (currentPage - 1) * limit + 1; // 21
	int endRow = startRow + limit - 1; // 30
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		pstmt.setInt(2, startRow);
		pstmt.setInt(3, endRow);
		
		rset = pstmt.executeQuery();
		
		unProNo = new ArrayList<>();
		gpNameList = new ArrayList<>();
		paymentDateList = new ArrayList<>();
		productDateList = new ArrayList<>();
		productNumber = new ArrayList<>();
		orderCodeList = new ArrayList<>();
		gpNoList = new ArrayList<>();
		
		while(rset.next()) {
			gp = new Gp();
			paymentDate = new Payment();
			productDate = new Product();
			productNum = new Product();
			orderCode = new OrderList();
			gpNo = new Gp();
			
			int unProNumber = rset.getInt("RNUM");
			gp.setGpName(rset.getString("GP_NAME"));
			paymentDate.setPayChange(rset.getDate("PAY_CHANGE"));
			productDate.setPdate(rset.getDate("PDATE"));
			productNum.setPno(rset.getInt("PNO"));
			orderCode.setOrderCode(rset.getLong("ORDER_CODE"));
			gpNo.setGpNo(rset.getInt("GP_NO"));
			
			unProNo.add(unProNumber);
			gpNameList.add(gp);
			paymentDateList.add(paymentDate);
			productDateList.add(productDate);
			productNumber.add(productNum);
			orderCodeList.add(orderCode);
			gpNoList.add(gpNo);
		}
		hmap = new HashMap<>();
		
		hmap.put("unProNo", unProNo);
		hmap.put("gpNameList", gpNameList);
		hmap.put("paymentDateList", paymentDateList);
		hmap.put("productDateList", productDateList);
		hmap.put("productNumber", productNumber);
		hmap.put("orderCodeList", orderCodeList);
		hmap.put("gpNoList", gpNoList);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	
	return hmap;
}

public HashMap<String, Object> selectOneProcessedProgramDetail(Connection con, OrderList orderList) {
	
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	HashMap<String, Object> hmap = null;
	ArrayList<GpDetail> gpDetailList = null;
	GpDetail gpDetail = null;
	Gp gp = null;
	OrderDetail recPerson = null;
	
	String query = prop.getProperty("selectOneProcessedProgramDetail");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setLong(1, orderList.getOrderCode());
		
		rset = pstmt.executeQuery();
		
		gpDetailList = new ArrayList<>();
		
		while(rset.next()) {
			gp = new Gp();
			recPerson = new OrderDetail();
			gpDetail = new GpDetail();
			
			gp.setGpTday(rset.getInt("GP_TDAY"));
			gp.setGpCost(rset.getInt("GP_COST"));
			
			recPerson.setRecPerson(rset.getInt("RECE_PERSON"));
			
			gpDetail.setGpDay(rset.getInt("GP_DAY"));
			gpDetail.setGpDaySeq(rset.getInt("GP_DAY_SEQ"));
			gpDetail.setGpLocation(rset.getString("GP_LOCATION"));
			gpDetail.setGpTime(rset.getInt("GP_TIME"));
			
			gpDetailList.add(gpDetail);
		}
		
		hmap = new HashMap<>();
		hmap.put("gp", gp);
		hmap.put("recPerson", recPerson);
		hmap.put("gpDetailList", gpDetailList);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return hmap;
	
}

public int updateMemberBank(Connection con, Member requestMember) {
	
	PreparedStatement pstmt = null;
	int result = 0;
	String query = prop.getProperty("updateMemberBank");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setString(1, requestMember.getBankName());
		pstmt.setString(2, requestMember.getAccountName());
		pstmt.setString(3, requestMember.getAccountNo());
		pstmt.setInt(4, requestMember.getMno());
		
		result = pstmt.executeUpdate();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
	}
	
	return result;
}

public int checkBank(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	String bankName = null;
	int result = 0;
	String query = prop.getProperty("checkBank");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			bankName = rset.getString("BANK_NAME");
			if(bankName == null) {
				result = 1;
			}else {
				result = 2;
			}
		}
		System.out.println("checkBank memberDao result = " + result);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(rset);
		close(pstmt);
	}
	
	return result;
}

public Member reLogin(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	Member reLoginMember = null;
	ResultSet rset = null;
	
	String query = prop.getProperty("reLogin");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			reLoginMember = new Member();
			reLoginMember.setMno(rset.getInt("MNO"));
			reLoginMember.setEmail(rset.getString("EMAIL"));
			reLoginMember.setMpwd(rset.getString("MPWD"));
		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	
	return reLoginMember;
}

public Member reLoginMemberCheck(Connection con, Member requestMember) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	Member loginUser = null;
	
	String query = prop.getProperty("checkReLoginMember");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setString(1, requestMember.getEmail());
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			
			loginUser = new Member();
			loginUser.setMno(rset.getInt("MNO"));
			loginUser.setEmail(rset.getString("EMAIL"));
			loginUser.setMpwd(rset.getString("MPWD"));
			loginUser.setNickName(rset.getString("NICKNAME"));
			loginUser.setMgender(rset.getString("MGENDER"));
			loginUser.setMbirth(rset.getString("MBIRTH"));
			loginUser.setWcount(rset.getInt("WCOUNT"));
			loginUser.setBankName(rset.getString("BANK_NAME"));
			loginUser.setAccountName(rset.getString("ACCOUNT_NAME"));
			loginUser.setAccountNo(rset.getString("ACCOUNT_NO"));
			loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
			loginUser.setEmailYn(rset.getString("EMAIL_YN"));
			loginUser.setModifyYn(rset.getString("MODIFY_YN"));
			loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
			loginUser.setMtype(rset.getString("MTYPE"));
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(rset);
		close(pstmt);
	}
	
	
	return loginUser;
	
	
}

public int getCanceledProcessedProgramListCount(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	
	int result = 0;
	
	String query = prop.getProperty("canceledGpListCount");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			result = rset.getInt(1);
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	return result;
}

public HashMap<String, Object> selectCanceledProgramList(Connection con, int currentPage, int limit, int mno) {

	
	PreparedStatement pstmt = null;
	HashMap<String, Object> hmap = null;
	ResultSet rset = null;
	ArrayList<Gp> gpNameList = null;
	ArrayList<Payment> endPaymentDateList = null;
	ArrayList<Integer> unProNo = null;
	ArrayList<Payment> paymentCostList = null;
	ArrayList<Member> bankNameAndAccountNoList = null;
	Gp gp = null;
	Payment endPaymentDate = null;
	Payment paymentCost = null;
	Member bankNameAndAccountNo = null;
	
	
	
	String query = prop.getProperty("selectListCanceledProgram");
	
	int startRow = (currentPage - 1) * limit + 1; // 21
	int endRow = startRow + limit - 1; // 30
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		pstmt.setInt(2, startRow);
		pstmt.setInt(3, endRow);
		
		rset = pstmt.executeQuery();
		
		unProNo = new ArrayList<>();
		gpNameList = new ArrayList<>();
		endPaymentDateList = new ArrayList<>();
		paymentCostList = new ArrayList<>();
		bankNameAndAccountNoList = new ArrayList<>();
		
		while(rset.next()) {
			gp = new Gp();
			endPaymentDate = new Payment();
			paymentCost = new Payment();
			bankNameAndAccountNo = new Member();
			
			int unProNumber = rset.getInt("RNUM");
			gp.setGpName(rset.getString("GP_NAME"));
			endPaymentDate.setPayChange(rset.getDate("PAY_CHANGE"));
			paymentCost.setPayCost(rset.getInt("PAY_COST"));
			bankNameAndAccountNo.setBankName(rset.getString("BANK_NAME"));
			bankNameAndAccountNo.setAccountNo(rset.getString("ACCOUNT_NO"));
			
			unProNo.add(unProNumber);
			gpNameList.add(gp);
			endPaymentDateList.add(endPaymentDate);
			paymentCostList.add(paymentCost);
			bankNameAndAccountNoList.add(bankNameAndAccountNo);
		}
		hmap = new HashMap<>();
		
		hmap.put("unProNo", unProNo);
		hmap.put("gpNameList", gpNameList);
		hmap.put("endPaymentDateList", endPaymentDateList);
		hmap.put("paymentCostList", paymentCostList);
		hmap.put("bankNameAndAccountNoList", bankNameAndAccountNoList);
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
	
	return hmap;
}

public int cancelGenralProgram(Connection con, Payment payment) {
	
	PreparedStatement pstmt = null;
	int result = 0;
	String query = prop.getProperty("updateCancelGeneralProgram");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, payment.getPno());
		pstmt.setInt(2, payment.getOrderNo());

		result = pstmt.executeUpdate();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
	}
	
	return result;
}

public int updateOrderDetailTourState(Connection con, HashMap<String, Integer> orderNoandPno) {
	
	PreparedStatement pstmt = null;
	int result = 0;
	String query = prop.getProperty("updateOrderDetailTourState");
	int orderNo = orderNoandPno.get("orderNo");
	int pno = orderNoandPno.get("pno");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, pno);
		pstmt.setInt(2, orderNo);
		
		result = pstmt.executeUpdate();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
	}
	
	return result;
}

public int modifyCheck(Connection con, Member requestMember) {
	
	PreparedStatement pstmt = null;
	ResultSet rset = null;
	int result = 0;
	String modifyCheck = null;
	
	String query = prop.getProperty("modifyCheck");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setString(1, requestMember.getEmail());
		pstmt.setString(2, requestMember.getMpwd());
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
			modifyCheck = rset.getString("MODIFY_YN");
			
			if(modifyCheck.equals("Y")) {
				result = 4;
			}else {
				result = 5;
			}
		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return result;
}

public int delectMember(Connection con, int mno) {
	
	PreparedStatement pstmt = null;
	int result = 0;
	String query = prop.getProperty("deleteMember");
	
	try {
		pstmt = con.prepareStatement(query);
		pstmt.setInt(1, mno);
		
		result = pstmt.executeUpdate();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
		close(pstmt);
	}
	
	
	return result;
}



}	
