package com.edr.member.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member implements java.io.Serializable{
	
	private int mno;			// 회원번호
	private String nickName;    // 활동명
	private String email;		// 이메일
	private String mpwd;		// 비밀번호
	private String mgender;		// 성별
	private String mbirth;		// 생년
	private int wcount;			// 경고횟수
	private String bankName;	// 은행명
	private String accountName;	// 예금주명
	private String accountNo;	// 계좌번호
	private Date enrollDate;	// 가입일
	private String emailYn;		// 이메일인증여부
	private String modifyYn;	// 탈퇴여부
	private Date modifyDate;	// 탈퇴일
	private String mtype;		// 이용자 상태

}
