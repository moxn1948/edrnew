package com.edr.member.model.service;

import static com.edr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.guide.model.dao.AdGuideDao;
import com.edr.member.model.dao.MemberAdSelectDao;
import com.edr.member.model.vo.Member;

/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
public class MemberAdSelectService {

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Member list select Service
	 * @param
	 * @return : list
	 */
	public ArrayList<Member> selectMemberList() {
		Connection con = getConnection();

		ArrayList<Member> list = new MemberAdSelectDao().selectMemberList(con);

		close(con);

		return list;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Member list Count Service
	 * @param
	 * @return : listCount
	 */
	public int getListCount() {
		Connection con = getConnection();
		int listCount = new MemberAdSelectDao().getlistCount(con);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 08.
	 * @Description : Member list selectListWithPaging Service
	 * @param :
	 *            currentPage, limit
	 * @return : list
	 */

	public ArrayList<HashMap<String, Object>> selectListWithPaging(int currentPage, int limit) {

		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MemberAdSelectDao().selectListWithPaging(con, currentPage, limit);

		close(con);

		return list;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 10.
	 * @Description : Member list Detail select Service
	 * @param :
	 *            num, con
	 * @return : m
	 */
	public HashMap<String, Object> selectOneMember(int num) {
		Connection con = getConnection();

		HashMap<String, Object> hmap = new MemberAdSelectDao().selectOneMember(con, num);

		if (hmap != null) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return hmap;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Member list Search Name listCount Service
	 * @param :
	 *            con, searchText
	 * @return : listCount
	 */
	public int adSearchNameMemberCount(String searchText) {
		Connection con = getConnection();

		int listCount = new MemberAdSelectDao().adSearchNameMemberCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Member list Search Name Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */

	public ArrayList<HashMap<String, Object>> adSearchNameMember(String searchText, int currentPage, int limit) {

		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MemberAdSelectDao().adSearchNameMember(con, currentPage, limit,
				searchText);

		close(con);

		return list;

	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Member list Search Id Count Service
	 * @param :
	 *            searchText
	 * @return : listCount
	 */
	public int adSearchIdMemberCount(String searchText) {
		Connection con = getConnection();

		int listCount = new MemberAdSelectDao().adSearchIdMemberCount(con, searchText);

		close(con);

		return listCount;
	}

	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 12.
	 * @Description : Member list Search Id Service
	 * @param :
	 *            con, currentPage, limit, searchText
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adSearchIdMember(String searchText, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MemberAdSelectDao().adSearchIdMember(con, currentPage, limit,
				searchText);

		close(con);

		return list;
	}

	/**
	 * @param m
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Member list Update Service
	 * @param : m
	 * @return : result
	 */
	public int adUpdateMember(Member m) {
		Connection con = getConnection();

		int result = new MemberAdSelectDao().adUpdateMember(con, m);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}
	/**
	 * @param m
	 * @Author : young il
	 * @CreateDate : 2019. 12. 13.
	 * @Description : Member list Delete Service
	 * @param :   m
	 * @return : result
	 */
	public int adDeleteMember(Member m) {
		Connection con = getConnection();

		int result = new MemberAdSelectDao().adDeleteMember(con, m);

		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Member list Filter TG listCount Service
	 * @param : str
	 * @return : listCount
	 */
	public int adMemberFilterTGlistCount(String str) {
		Connection con = getConnection();

		int listCount = new MemberAdSelectDao().adMemberFilterTGlistCount(con, str);

		close(con);

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Member list Filter Service
	 * @param : con, currentPage, limit, str
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adMemberTGFilter(int currentPage, int limit, String str) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MemberAdSelectDao().adMemberTGFilter(con, currentPage, limit,
				str);

		close(con);

		return list;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Member list YN Filter listCount Service
	 * @param : str
	 * @return : listCount
	 */
	public int adMemberFilterYNlistCount(String str) {
		Connection con = getConnection();

		int listCount = new MemberAdSelectDao().adMemberFilterYNlistCount(con, str);

		close(con);

		return listCount;
	}
	/**
	 * @Author : young il
	 * @CreateDate : 2019. 12. 15.
	 * @Description : Member list YN Filter Service
	 * @param : con, currentPage, limit, str
	 * @return : list
	 */
	public ArrayList<HashMap<String, Object>> adMemberYNFilter(String str, int currentPage, int limit) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new MemberAdSelectDao().adMemberYNFilter(con, currentPage, limit,
				str);

		close(con);

		return list;
	}

}
