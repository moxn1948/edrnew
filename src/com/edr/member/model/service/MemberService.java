package com.edr.member.model.service;

import static com.edr.common.JDBCTemplate.close;
import static com.edr.common.JDBCTemplate.commit;
import static com.edr.common.JDBCTemplate.getConnection;
import static com.edr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.edr.member.model.dao.AdminDao;
import com.edr.member.model.dao.MemberDao;
import com.edr.member.model.vo.Member;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

public class MemberService {

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 활동명 중복확인
	 * @param checkMember
	 * @return         : result
	 */
	public int idCheck(Member checkMember) {

		Connection con = getConnection();
		
		int result = new MemberDao().idCheck(con, checkMember);

		close(con);
		
		return result;
	}

	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 이메일 중복확인
	 * @param checkMember
	 * @return         : result
	 */
	public int checkEmail(Member checkMember) {
		
		Connection con = getConnection();
		
		int result = new MemberDao().emailCheck(con, checkMember);
		
		close(con);
		
		return result;
	}
	
	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 회원가입
	 * @param m
	 * @return         : result
	 */
	public int insertMember(Member m) {
		
		Connection con = getConnection();
		
		int result = new MemberDao().insertMember(con,m);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}


	
	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 로그인시 멤버있는지 여부 확인
	 * @param requestMember
	 * @return         :
	 */
	public Member memberCheck(Member requestMember) {
		
		Connection con = getConnection();
		
		Member loginUser = new MemberDao().memberCheck(con, requestMember);
		
		close(con);
		
		return loginUser;
	}
	
	/**
	 * @Author         : shjeon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : 이메일 인증
	 * @param email
	 * @return         :
	 */
	public int accessEmail(String email) {

		Connection con = getConnection();
		
		int result = new MemberDao().accessEmail(con, email);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}
	

	public int emailAccessCheck(Member requestMember) {
		
		Connection con = getConnection();
		
		int result = new MemberDao().emailAccessCheck(con,requestMember);
		
		close(con);
		
		return result;
	}

	public int idPwdCheck(Member requestMember) {
		
		Connection con = getConnection();

		int result = new MemberDao().idPwdCheck(con,requestMember);
		
		if(result > 0) {
			result = new MemberDao().emailAccessCheck(con, requestMember);
			System.out.println("memberService email :: " + result);
			if(result > 2) {
				result = new MemberDao().modifyCheck(con, requestMember);
				System.out.println("memberService modify :: " + result);
			}
		}
		
		close(con);
		
		return result;
	}
    /**   
    * @Author :hjheo
    * @CreateDate  : 2019.12.10
    * @Description : 비밀번호 확인
    * @param  : 이메일, 비밀번호
    * @return 
    */
   public int checkPwd(Member requestMember) {
      
      Connection con = getConnection();
      
      int result = new MemberDao().checkPwd(con,requestMember);
      
      
      
      
      
      close(con);
      
      return result;
   }
   /** service
    * @Author  : hjheo
    * @CreateDate  : 2019. 12. 11. 
    * @Description  : 비밀번호 변경
    * @param   :requestMember
    * @return   result :
    */
   public int changePwd(Member requestMember) {
      
      Connection con = getConnection();
      
      int result = new MemberDao().changePwd(con,requestMember);
      
      
      if(result>0) {
         
         commit(con);
      }else {
         rollback(con);
      }
      
      close(con);
      
      return result;
   }

public HashMap<String, Object> selectUnProcessedProgramList(int mno, int currentPage, int limit) {

	Connection con = getConnection();
	
	HashMap<String, Object> hmap = new MemberDao().selectUnProcessedProgramList(con, currentPage, limit, mno);
	
	close(con);
	
	return hmap;
}

public int getUnProcessedProgramListCount(int mno) {
	
	Connection con = getConnection();
	
	int listCount = new MemberDao().getUnProcessedProgramListCount(con, mno);
	
	close(con);
	
	return listCount;
}

public HashMap<String, Object> selectOneUnProcessedProgramDetail(OrderList orderList) {

	Connection con = getConnection();
	
	HashMap<String, Object> hmap = new MemberDao().selectOneUnProcessedProgramDetail(con, orderList);
	
	close(con);
	
	return hmap;
}

public int cancelGeneralProgram(OrderList orderList) {

	Connection con = getConnection();
	
	HashMap<String, Integer> orderNoandPno = new MemberDao().searchOrderNo(con, orderList);
	
	int result = 0;
	int result2 = 0;
	
	if(orderNoandPno != null) {
		Payment payment = new MemberDao().cancelGeneralProgramSelect(con, orderNoandPno);	
		if(payment != null) {
			result2 = new MemberDao().cancelGenralProgram(con, payment);
			if(result2 > 0) {
				result = new MemberDao().updateOrderDetailTourState(con, orderNoandPno);
			}
		}
	}
	
	if(result > 0) {
		commit(con);
	}else {
		rollback(con);
	}
	close(con);
	
	return result;
}

public int getCompleteProcessedProgramListCount(int mno) {
	
	Connection con = getConnection();
	
	int listCount = new MemberDao().getCompleteProcessedProgramListCount(con, mno);
	
	close(con);
	
	return listCount;
}

public HashMap<String, Object> selectCompleteProcessedProgramList(int mno, int currentPage, int limit) {

	Connection con = getConnection();
	
	HashMap<String, Object> hmap = new MemberDao().selectCompleteProcessedProgramList(con, currentPage, limit, mno);
	
	close(con);
	
	return hmap;
	
}

public HashMap<String, Object> selectOneProcessedProgramDetail(OrderList orderList) {
	
	Connection con = getConnection();
	
	HashMap<String, Object> hmap = new MemberDao().selectOneProcessedProgramDetail(con, orderList);
	
	close(con);
	
	return hmap;
}

public int updateMemberBank(Member requestMember) {

	Connection con = getConnection();
	
	int result = new MemberDao().updateMemberBank(con, requestMember);
	
	if(result > 0) {
		commit(con);
	}else {
		rollback(con);
	}
	close(con);
	
	return result;
}

public int checkBank(int mno) {
	
	Connection con = getConnection();
	
	int result = new MemberDao().checkBank(con, mno);
	
	close(con);
	
	return result;
}

public Member reLogin(int mno) {
	
	Connection con = getConnection();
	
	Member reLoginMember = new MemberDao().reLogin(con, mno);
	
	close(con);
	
	return reLoginMember;
}

public Member reLoginMemberCheck(Member requestMember) {
	
	Connection con = getConnection();
	
	Member loginUser = new MemberDao().reLoginMemberCheck(con, requestMember);
	
	close(con);
	
	return loginUser;
}

public int getCanceledProcessedProgramListCount(int mno) {

	Connection con = getConnection();
	
	int listCount = new MemberDao().getCanceledProcessedProgramListCount(con, mno);
	
	close(con);
	
	return listCount;
	
}

public HashMap<String, Object> selectCanceledProgramList(int mno, int currentPage, int limit) {

	Connection con = getConnection();
	
	HashMap<String, Object> hmap = new MemberDao().selectCanceledProgramList(con, currentPage, limit, mno);
	
	close(con);
	
	return hmap;
	
}

public int deleteMember(int mno) {
	
	Connection con = getConnection();
	
	int result = new MemberDao().delectMember(con, mno);
	
	if(result > 0) {
		commit(con);
	}else {
		rollback(con);
	}
	close(con);
	
	return result;
}

}
