package com.edr.member.model.service;

import com.edr.guide.model.vo.GuideDetail;
import com.edr.member.model.dao.MemberDaohj;
import com.edr.member.model.vo.Member;

import java.sql.Connection;
import java.util.HashMap;

import static com.edr.common.JDBCTemplate.*;
public class MemberServicehj {

	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 13. 
	 * @Description  : 이메일 전송
	 * @param   : checkMember
	 * @return    : result
	 */
	public int sendPwdMail(Member checkMember) {
		Connection con = getConnection();
		
		int result = new MemberDaohj().sendPwdMail(con,checkMember);
		
		
		close(con);
		
		
		
		
		return result;
	}



	/**
	 * @Author  : hjheo
	 * @CreateDate  : 2019. 12. 14. 
	 * @Description  : 비밀번호  잊었을때 진짜 변경하는 기능을 담당
	 * @param   : requestMember
	 * @return    : result
	 */
	public int forgotChange(Member requestMember) {
		Connection con = getConnection();
		
		int result = new MemberDaohj().forgotChange(con,requestMember);
		
		
		if(result>0) {
			
			commit(con);
		}else {
			rollback(con);
			
		}
		
		
		close(con);
		
		
		return result;
	}



	






}
