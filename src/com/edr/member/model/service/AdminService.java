package com.edr.member.model.service;

import java.sql.Connection;

import com.edr.member.model.dao.AdminDao;
import com.edr.member.model.dao.MemberDao;
import com.edr.member.model.vo.Member;
import static com.edr.common.JDBCTemplate.*;

public class AdminService {
	
	/**
	 * @Author         : kijoon
	 * @CreateDate       : 2019. 12. 7.
	 * @Description      : admin loginService
	 * @param requestMember
	 * @return         : loginAdmin
	 */
	public Member adminLoginCheck(Member requestMember) {

		Connection con = getConnection();

		Member loginAdmin = new AdminDao().adminLoginCheck(con, requestMember);

		close(con);

		return loginAdmin;

	}

	public int idPwdCheck(Member requestMember) {
		
		Connection con = getConnection();
		int result = new AdminDao().idPwdCheck(con,requestMember);
		
		return result;
	}


}
