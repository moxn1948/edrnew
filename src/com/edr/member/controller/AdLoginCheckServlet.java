package com.edr.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.AdminService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdLoginCheckServlet
 */
@WebServlet("/adLoginCheck.me")
public class AdLoginCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdLoginCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adminId = request.getParameter("adminId");
		String userPwd = request.getParameter("userPwd");
		
		Member requestMember = new Member();
		requestMember.setNickName(adminId);
		requestMember.setMpwd(userPwd);
		
		int result = new AdminService().idPwdCheck(requestMember);
		
		PrintWriter out = response.getWriter();
		if(result == 0) {
			out.append("fail");
		}
		out.flush();
		out.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
