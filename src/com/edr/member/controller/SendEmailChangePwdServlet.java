package com.edr.member.controller;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.Gmail;

/**
 * Servlet implementation class SendEmailChangePwdServlet
 */
@WebServlet("/sendChangePwd.me")
public class SendEmailChangePwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendEmailChangePwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		//useremail을 받아오고
		String userEmail = request.getParameter("userEmail");
		
		Properties prop = new Properties();
		prop.put("mail.smtp.user", "gmail");
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "465");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.socketFactory.port", "465"); //gailport
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		prop.put("mail.smtp.socketFactory.fallback", "false");

		Authenticator auth = new Gmail(); //구글 계정인증

		Session sess = Session.getInstance(prop, auth); //session생성 

		sess.setDebug(true);
		String host = "http://192.168.30.135/edr/";
		MimeMessage msg = new MimeMessage(sess); // 메일의 내용을 담을 객체
		String sendTitle = "요청하신[EDR] 비밀번호 찾기 서비스입니다.";
		String sendContent = "<form action='http://192.168.30.135:8001/edr/newPwd.me' method='post'><input type='hidden' value='"+userEmail+"' name='userEmail'><button type='submit'>확인</button></form>";

		try {
			Address userEmailAddr = new InternetAddress(userEmail);

			msg.setSubject(sendTitle);
			msg.setContent(sendContent, "text/html;charset=UTF-8");
			msg.addRecipient(Message.RecipientType.TO, userEmailAddr);

			Transport.send(msg);

		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.sendRedirect(request.getContextPath()+"/index");
		  
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
