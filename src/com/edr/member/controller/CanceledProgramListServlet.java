package com.edr.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.generalProgram.model.vo.Gp;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;
import com.edr.order.model.vo.OrderList;
import com.edr.payment.model.vo.Payment;
import com.edr.product.model.vo.Product;

/**
 * Servlet implementation class CanceledProgramListServlet
 */
@WebServlet("/selectList.cangp")
public class CanceledProgramListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CanceledProgramListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		// 페이징 처리 후
		int currentPage; // 현재 페이지를 표시할 변수
		int limit;		 // 한 페이지에 게시글이 몇개 보여질 것인지
		int maxPage;	 // 전체 페이지에서 가장 마지막 페이지
		int startPage;	 // 한번에 표시될 페이지의 시작할 페이지
		int endPage;	 // 한번에 표시될 페이지의 마지막 페이지
		
		// 게시판은 1 페이지 부터 시작함
		currentPage = 1;
		
		// 전달받은 페이지 추출
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
			
		// 한 페이지에 보여질 목록 갯수
		limit = 5;
			
		// 전체 목록 갯수 조회
		int listCount = new MemberService().getCanceledProcessedProgramListCount(mno);
				
				
		// 총 페이지 수 계산
		// 예를 들면 목록수가 124 이면 페이지는 13페이지가 필요하다
		// 짜투리 목록이 최소 1개일 때 , 1page 가 추가되는 로직 작성
		maxPage = (int)((double)listCount / limit + 0.8);
			
		// 현재 페이지에 보여 줄 시작 페이지 (10 개 씩 보여지게 할 경우)
		// 아래쪽 페이지 수가 10 개 씩 보여지게 한다면
		// 1 , 11 ,21 , 31
		startPage = (((int)((double)currentPage/5+0.8))-1) * 5 +1;
				
		// 목록 아래 쪽에 보여질 마지막 페이지 수
		endPage = startPage + 5 - 1;
			
		if(maxPage < endPage) {
			endPage = maxPage;
		}
				
		// 페이지 정보를 담을 vo 객체 생성
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
		//ArrayList<Board> list = new BoardService().selectListWithPaging(currentPage, limit);
				
		// 페이징 처리 전
		/*String page="";
				
		if(list != null) {
			page ="views/board/boardList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "게시판조회실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		*/
		
		HashMap<String, Object> hmap = new MemberService().selectCanceledProgramList(mno, currentPage, limit);
		
		ArrayList<Integer> unProNo = (ArrayList)hmap.get("unProNo");
		ArrayList<Gp> gpNameList = (ArrayList)hmap.get("gpNameList");
		ArrayList<Payment> endPaymentDateList = (ArrayList)hmap.get("endPaymentDateList");
		ArrayList<Payment> paymentCostList = (ArrayList)hmap.get("paymentCostList");
		ArrayList<Member> bankNameAndAccountNoList = (ArrayList)hmap.get("bankNameAndAccountNoList");
		
		
		if(hmap != null) {
			request.setAttribute("unProNo", unProNo);
			request.setAttribute("gpNameList", gpNameList);
			request.setAttribute("endPaymentDateList", endPaymentDateList);
			request.setAttribute("paymentCostList", paymentCostList);
			request.setAttribute("bankNameAndAccountNoList", bankNameAndAccountNoList);
			request.setAttribute("pi", pi);
			request.getRequestDispatcher("views/user/sub/my_page/canceledProgramList.jsp").forward(request, response);
		}
		
		

		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
