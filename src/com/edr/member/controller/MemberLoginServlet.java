package com.edr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class MemberLoginServlet
 */
@WebServlet("/login.me")
public class MemberLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("userEmail");
		String userPwd = request.getParameter("userPwd");
		
		Member requestMember = new Member();
		requestMember.setEmail(email);
		requestMember.setMpwd(userPwd);
		
		Member loginUser = new MemberService().memberCheck(requestMember);
		
		if(loginUser != null) {
			request.getSession().setAttribute("loginUser", loginUser);
			request.getSession().setMaxInactiveInterval(120*60);

			response.sendRedirect(request.getContextPath() + "/index");
		}else {
			request.setAttribute("msg", "아이디와 비밀번호를 확인해 주세요.");
			request.getRequestDispatcher("views/user/sub/member/login.jsp").forward(request, response);;
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
