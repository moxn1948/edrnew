package com.edr.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.AdminService;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdLoginServlet
 */
@WebServlet("/adLogin.me")
public class AdLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adminId = request.getParameter("adminId");
		String userPwd = request.getParameter("userPwd");
		
		System.out.println("userPwd : " + userPwd);
		
		
		Member requestMember = new Member();
		
		requestMember.setNickName(adminId);
		requestMember.setMpwd(userPwd);
		
		Member loginAdmin = new AdminService().adminLoginCheck(requestMember);
		System.out.println("loginAdmin : " + loginAdmin);
		
		if(loginAdmin != null) {
			request.getSession().setAttribute("loginAdmin", loginAdmin);
			response.sendRedirect("views/admin/index.jsp");
			System.out.println("로그인 성공");
			
		}else {
			
			request.setAttribute("msg", "입력하신 정보가 일치하지 않습니다");
			request.getRequestDispatcher("views/admin/index.jsp").forward(request, response);
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
