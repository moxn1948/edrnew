package com.edr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberService;
import com.edr.order.model.vo.OrderList;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

/**
 * Servlet implementation class CancelGeneralProgramServlet
 */
@WebServlet("/cancelProgram.ugp")
public class CancelGeneralProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelGeneralProgramServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("서블릿진입");
		
		Long orderCode = Long.parseLong(request.getParameter("orderCode"));
		
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		OrderList orderList = new OrderList();
		orderList.setOrderCode(orderCode);
		
		int result = new MemberService().cancelGeneralProgram(orderList);
		
		if(result > 0) {
			response.sendRedirect( request.getContextPath()+"/selectList.ugp?mno="+ mno);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
