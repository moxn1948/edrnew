package com.edr.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.guide.model.service.AdGuideService;
import com.edr.member.model.service.MemberAdSelectService;

/**
 * Servlet implementation class AdSearchMemberServlet
 */
@WebServlet("/searchMember.ad")
public class AdSearchMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdSearchMemberServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String searchVal = request.getParameter("searchVal");

		int currentPage;
		int limit;
		int startPage;
		

		currentPage = 1;

		ArrayList<HashMap<String, Object>> list = null;

		String searchText = "";

		PageInfo pi = null;

		if (searchVal.equals("id")) {

			searchText = request.getParameter("searchText");
			if (request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			// 한 페이지에 보여질 목록 갯수
			limit = 5;

			// 전체 목록 갯수 조회

			int listCount = 0;
			
			searchText = request.getParameter("searchText");
			
			
			
			listCount = new MemberAdSelectService().adSearchIdMemberCount(searchText);
			
			int maxPage = (int) ((double) listCount / limit + 0.8);

			// 현재 페이지에 보여 줄 시작 페이지수
			startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			// 목록 아래 쪽에 보여질 마지막 페이지 수
			int endPage = startPage + 5 - 1;

			if (maxPage < endPage) {
				endPage = maxPage;
			}

			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
			list = null;

			list = new MemberAdSelectService().adSearchIdMember(searchText, currentPage, limit);
		
		} else {

			if (request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}

			limit = 5;

			int listCount = 0;

			searchText = request.getParameter("searchText");

			listCount = new MemberAdSelectService().adSearchNameMemberCount(searchText);
						
			int maxPage = (int) ((double) listCount / limit + 0.8);

			startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			int endPage = startPage + 5 - 1;

			if (maxPage < endPage) {
				endPage = maxPage;
			}

			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
			list = null;

			list = new MemberAdSelectService().adSearchNameMember(searchText, currentPage, limit);
		}
		String page = "";

		if (list != null) {
			page = "views/admin/sub/member/touristSearch.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("searchText", searchText);
			request.setAttribute("searchVal", searchVal);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "게시판 조회 실패");
		}

		request.getRequestDispatcher(page).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
