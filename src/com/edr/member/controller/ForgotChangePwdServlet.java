package com.edr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberServicehj;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class ForgotChangePwdServlet
 */
@WebServlet("/forgotChange.me")
public class ForgotChangePwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgotChangePwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("userEmail");
//		System.out.println(email); 만들었는데 안쓰면 노란줄
		
		
		String pwd = request.getParameter("userPwd");
		
		
		Member requestMember = new Member();
		
		requestMember.setEmail(email);
		requestMember.setMpwd(pwd);
		
		int result =new MemberServicehj().forgotChange(requestMember);
		
		
		
		if(result>0) {
			
			response.sendRedirect(request.getContextPath()+"/index");
		}
		
		
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
