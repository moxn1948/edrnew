package com.edr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberServicehj;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class SetNewPwdServlet
 */
@WebServlet("/newPwd.me")
public class SetNewPwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetNewPwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 이메일에서 form 태그로 보낸 eamil 정보를 받는 서블릿
		
	
		String email = request.getParameter("userEmail");  //hidden으로 보낸 이메일을 받는다.
		
		System.out.println("email  : "+email);
		

			
			request.setAttribute("email", email); //email을 eamil로 설정?
			
			//다시 뷰로 보냄
			request.getRequestDispatcher("views/user/sub/member/pwdChange.jsp").forward(request, response);
			
		}
		
		
	                        

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
