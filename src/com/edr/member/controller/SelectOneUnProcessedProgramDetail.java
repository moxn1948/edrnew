package com.edr.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.generalProgram.model.vo.Gp;
import com.edr.generalProgram.model.vo.GpDetail;
import com.edr.member.model.service.MemberService;
import com.edr.order.model.vo.OrderDetail;
import com.edr.order.model.vo.OrderList;
import com.edr.product.model.vo.Product;

/**
 * Servlet implementation class SelectOneUnProcessedProgramDetail
 */
@WebServlet("/selectUnprocessedDetail.ugp")
public class SelectOneUnProcessedProgramDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneUnProcessedProgramDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		long orderCode = Long.parseLong(request.getParameter("orderCode"));
		
		OrderList orderList = new OrderList();
		orderList.setOrderCode(orderCode);
		
		HashMap<String, Object> hmap = new MemberService().selectOneUnProcessedProgramDetail(orderList);
		Gp gp = (Gp)hmap.get("gp");
		OrderDetail recPerson = (OrderDetail)hmap.get("recPerson");
		ArrayList<GpDetail> gpDetailList = (ArrayList)hmap.get("gpDetailList");
		
		if( hmap != null ) {

			request.setAttribute("gp", gp);
			request.setAttribute("recPerson", recPerson);
			request.setAttribute("gpDetailList", gpDetailList);
			request.getRequestDispatcher("views/user/sub/popup/gpDetailPop.jsp").forward(request, response);
			
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
