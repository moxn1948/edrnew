package com.edr.member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.model.vo.PageInfo;
import com.edr.guide.model.service.AdGuideService;
import com.edr.member.model.service.MemberAdSelectService;

/**
 * Servlet implementation class AdFilterMemberServlet
 */
@WebServlet("/memberfilter.ad")
public class AdFilterMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdFilterMemberServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String str = request.getParameter("str");


		// 페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage = 1;
		
		int listCount = 0;
		
		ArrayList<HashMap<String, Object>> list = null;

		PageInfo pi = null;

		if (str.equals("TOURIST") || str.equals("GUIDE")) {

			if (request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			// 한 페이지에 보여질 목록 갯수
			limit = 5;

			listCount = new MemberAdSelectService().adMemberFilterTGlistCount(str);

			// 총 페이지수 계산
			maxPage = (int) ((double) listCount / limit + 0.8);

			// 현재 페이지에 보여 줄 시작 페이지수
			startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			// 목록 아래 쪽에 보여질 마지막 페이지 수
			endPage = startPage + 5 - 1;

			if (maxPage < endPage) {
				endPage = maxPage;
			}

			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			System.out.println("pi : " + pi);
			
			list = new MemberAdSelectService().adMemberTGFilter(currentPage, limit, str);
			
		} else if (str.equals("Y") || str.equals("N")) {

			if (request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}

			limit = 5;

			listCount = 0;

			listCount = new MemberAdSelectService().adMemberFilterYNlistCount(str);

			maxPage = (int) ((double) listCount / limit + 0.8);

			startPage = (((int) ((double) currentPage / limit + 0.8)) - 1) * 5 + 1;

			endPage = startPage + 5 - 1;

			if (maxPage < endPage) {
				endPage = maxPage;
			}

			pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

			list = new MemberAdSelectService().adMemberYNFilter(str, currentPage, limit);

		}
		String page = "";

		if (list != null) {
			page = "views/admin/sub/member/touristFilter.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			request.setAttribute("str", str);
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "필터링 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
