package com.edr.member.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberAdSelectService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdSelectOneMemberServlet
 */
@WebServlet("/selectOneMember.ad")
public class AdSelectOneMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdSelectOneMemberServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("str"));		
		System.out.println(num);
		
		HashMap<String, Object> hmap = new MemberAdSelectService().selectOneMember(num); 
		
		String page = "";
		
		if(hmap != null) {
			page = "views/admin/sub/member/touristDetail.jsp";
			request.setAttribute("hmap", hmap);
		}else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "투어객 게시판 상세 조회 실패!");
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
