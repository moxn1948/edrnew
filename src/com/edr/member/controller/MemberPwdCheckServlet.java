package com.edr.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class MemberPwdCheckServlet
 */
@WebServlet("/checkPwd.me")
public class MemberPwdCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberPwdCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//값꺼내기
		
		String pwd = request.getParameter("userPwd");
		String email = request.getParameter("userEmail");
		
		System.out.println(pwd);
		System.out.println("email "+email);
	
		Member requestMember = new Member();
		
		requestMember.setEmail(email);
		requestMember.setMpwd(pwd);
		
		
		System.out.println("memeberPwd: "+pwd);
		System.out.println("member email: "+email);
		int result = new MemberService().checkPwd(requestMember);
		System.out.println(result);
		

		//수정하고 싶은 흐름  mtype==tourist면 mhPageEdit으로 보내고
		// if else(mtype == guide)면  guideProfileEdit으로 보내고 
		// else일때  비밀번호가 일치하지 않습니다 하고싶음
		
		if(result>0) {
			
			
			response.sendRedirect("views/user/sub/my_page/myPageEdit.jsp");
			
		}else {
			  
			request.setAttribute("msg", "비밀번호가 일치하지 않습니다.");
			request.getRequestDispatcher("views/user/sub/my_page/myPage.jsp").forward(request, response);
			
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
