package com.edr.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberAdSelectService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdUpdateMemberServlet
 */
@WebServlet("/updateMember.ad")
public class AdUpdateMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdUpdateMemberServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		String nickname = request.getParameter("nickname");

		Member m = new Member();
		m.setNickName(nickname);
		m.setMno(mno);

		int result = new MemberAdSelectService().adUpdateMember(m);

		String page = "";

		if (result > 0) {
			response.sendRedirect("/edr/selectMember.ad");
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "이용자 정보 수정 실패!!");

			request.getRequestDispatcher(page).forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
