package com.edr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class UpdateMemberBankServlet
 */
@WebServlet("/updateMemberBank.me")
public class UpdateMemberBankServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateMemberBankServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String accountName = request.getParameter("userName");
		String bankName = request.getParameter("bankName");
		String accountNumber = request.getParameter("accountNumber");
		int mno = Integer.parseInt(request.getParameter("mno"));
		
		Member requestMember = new Member();
		requestMember.setMno(mno);
		requestMember.setBankName(bankName);
		requestMember.setAccountName(accountName);
		requestMember.setAccountNo(accountNumber);
		
		int result = new MemberService().updateMemberBank(requestMember);
		
		Member reLoginMember = new MemberService().reLogin(mno);
		
		if(result > 0) {
			request.getSession().invalidate();
			response.sendRedirect(request.getContextPath() + "/reLogin?userEmail="+reLoginMember.getEmail()+"&pwd="+reLoginMember.getMpwd());
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
