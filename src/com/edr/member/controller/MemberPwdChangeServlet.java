package com.edr.member.controller;

import java.io.IOException;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.synth.SynthSpinnerUI;

import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class MemberPwdChangeServlet
 */
@WebServlet("/pwdChange.me")
public class MemberPwdChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberPwdChangeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//값꺼내기
		String pwd1 = request.getParameter("userPwd");
		String email =request.getParameter("userEmail");
		
		System.out.println("pwd1: "+pwd1);
		System.out.println("email: "+email);
		Member requestMember = new Member();
		requestMember.setMpwd(pwd1);
		requestMember.setEmail(email);
		int result = new MemberService().changePwd(requestMember);

		System.out.println("result: "+result);
		if(result>0) {
			
			request.getSession().invalidate();
			response.sendRedirect(request.getContextPath() + "/index");
			
		}else {
			request.setAttribute("msg", "비밀번호를 확인해주세요");
			request.getRequestDispatcher("views/user/sub/my_page/myPageEdit.jsp").forward(request, response);
			
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
