package com.edr.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.member.model.service.MemberAdSelectService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class AdDeleteMemberServlet
 */
@WebServlet("/deleteMember.ad")
public class AdDeleteMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdDeleteMemberServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int mno = Integer.parseInt(request.getParameter("mno"));

		Member m = new Member();
		m.setMno(mno);
	
		int result = new MemberAdSelectService().adDeleteMember(m);

		String page = "";
		
		if (result > 0) {
			response.sendRedirect("/edr/selectMember.ad");
			
		} else {
			page = "views/admin/sub/common/errorPage.jsp";
			request.setAttribute("msg", "이용자 정보 삭제 실패!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
