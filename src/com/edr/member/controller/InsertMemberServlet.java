package com.edr.member.controller;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edr.common.Gmail;
import com.edr.member.model.service.MemberService;
import com.edr.member.model.vo.Member;

/**
 * Servlet implementation class InsertMemberServlet
 */
@WebServlet("/join.me")
public class InsertMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertMemberServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    	String nickName = request.getParameter("userName");
    	String userEmail = request.getParameter("userEmail");
    	String userPwd = request.getParameter("userPwd");
    	String userGender = request.getParameter("userGender");
    	String userAge = request.getParameter("userAgeList");

    	Member checkMember = new Member();
    	checkMember.setNickName(nickName);
    	checkMember.setEmail(userEmail);


    	Member m = new Member();

    	m.setNickName(nickName);
    	m.setEmail(userEmail);
    	m.setMpwd(userPwd);
    	m.setMgender(userGender);
    	m.setMbirth(userAge);

    	int idCheckResult = new MemberService().idCheck(checkMember);
    	int nickNameCheckResult = new MemberService().checkEmail(checkMember);

    	if(idCheckResult == 0 || nickNameCheckResult == 0) {

    		int result = new MemberService().insertMember(m);

    		if(result > 0) {
    			Properties prop = new Properties();
    			prop.put("mail.smtp.user", "gmail");
    			prop.put("mail.smtp.host", "smtp.gmail.com");
    			prop.put("mail.smtp.port", "465");
    			prop.put("mail.smtp.starttls.enable", "true");
    			prop.put("mail.smtp.auth", "true");
    			prop.put("mail.smtp.socketFactory.port", "465");
    			prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    			prop.put("mail.smtp.socketFactory.fallback", "false");
    			
    			Authenticator auth = new Gmail();

    			Session sess = Session.getInstance(prop, auth);

    			sess.setDebug(true);
    			String host = "http://192.168.30.135:8001/edr/";
    			MimeMessage msg = new MimeMessage(sess); // 메일의 내용을 담을 객체
    			String sendTitle = "모두가_꿈꾸던_여행 [ 애드립  ] 에 가입해주셔서 감사합니다.";
    			String sendContent = "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><link rel='stylesheet' type='text/css' media='screen' href='main.css'></head><body><div style='width: 300px; height: 200px; text-align: center;box-shadow: 1px 1px 2px 2px rgba(100, 100, 100, 0.4);'><h2>EDR</h2><h4> 애드립에 가입해 주셔서 감사합니다 </h4><H5>아래 버튼들 눌러 인증 해주시기 바랍니다.</H5><a href='http://192.168.30.135:8001/edr/access.me?email="+userEmail+"'>여기</a>를 눌러주세요</div></body></html>";

    			try {
    				Address userEmailAddr = new InternetAddress(userEmail);

    				msg.setSubject(sendTitle);
    				msg.setContent(sendContent, "text/html;charset=UTF-8");
    				msg.addRecipient(Message.RecipientType.TO, userEmailAddr);

    				Transport.send(msg);

    			} catch (MessagingException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			
    			request.setAttribute("msg", "이메일로 인증메일이 발송되었습니다. ");
    			response.sendRedirect(request.getContextPath()+"/index");

    		}else {
    			request.setAttribute("msg", "회원가입 실패");
    			response.sendRedirect(request.getContextPath()+"/index");
    		}
    	}else {
    		request.setAttribute("msg", "중복확인을 해주세요");
			response.sendRedirect("/edr/views/user/sub/member/join.jsp");
    	}
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
